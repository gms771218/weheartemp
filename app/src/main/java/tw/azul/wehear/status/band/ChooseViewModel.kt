package tw.azul.wehear.status.band

import android.app.Application
import android.view.View
import androidx.navigation.findNavController
import tw.azul.wehear.R
import tw.azul.wehear.portal.device.ChooseBandViewModel

class ChooseViewModel(app: Application) : ChooseBandViewModel(app) {
    override fun gotoNext(view: View) {
        view.findNavController().navigate(R.id.action_bandChooseFragment_to_bandInfoFragment)
    }
}
