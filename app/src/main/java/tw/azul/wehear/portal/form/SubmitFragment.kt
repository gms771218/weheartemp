package tw.azul.wehear.portal.form

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import tw.azul.wehear.R
import tw.azul.wehear.databinding.FragmentPortalSubmitBinding

class SubmitFragment : Fragment() {

    companion object {
        fun newInstance() = SubmitFragment()
    }

    private lateinit var viewModel: SubmitViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = FragmentPortalSubmitBinding.inflate(inflater).root

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SubmitViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        viewModel.countDownLiveData().observe(viewLifecycleOwner, Observer {
            if (it == 0) {
                findNavController().navigate(R.id.action_submitFragment_to_initFragment)
            }
        })
    }
}
