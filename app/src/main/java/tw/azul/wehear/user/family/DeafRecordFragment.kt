package tw.azul.wehear.user.family

import android.app.AlertDialog
import android.app.DatePickerDialog
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.format.DateUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import com.gms.widget.layout.component.*
import tw.azul.wehear.MainActivity

import tw.azul.wehear.R
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.dailyInfo.HealthRecordEntity
import java.util.*

/**
 * 身障者紀錄Layout
 */
class DeafRecordFragment : Fragment() {

    companion object {
        fun newInstance() = DeafRecordFragment()
    }

    // 身障者ID
    var deafUserId = 0

    private lateinit var viewModel: DeafRecordViewModel

    lateinit var mCalendar: Calendar

    // region =====View=====

    var progressDig: AlertDialog? = null

    lateinit var ibtnPreDate: ImageButton
    lateinit var ibtnNextDate: ImageButton
    lateinit var tvDate: TextView

    lateinit var scrollView: NestedScrollView
    lateinit var sBarWalk: CustomSeekBar
    lateinit var tvWalkCount: TextView
    lateinit var tvWalkTarget: TextView
    lateinit var tvCalorie: TextView
    lateinit var tvWalkTime: TextView
    lateinit var tvWalkDistance: TextView
    lateinit var tvWeek1: WeekTextView
    lateinit var tvWeek2: WeekTextView
    lateinit var tvWeek3: WeekTextView
    lateinit var tvWeek4: WeekTextView
    lateinit var tvWeek5: WeekTextView
    lateinit var tvWeek6: WeekTextView
    lateinit var tvWeek7: WeekTextView

    lateinit var tvHour: TextView
    lateinit var tvMinutes: TextView
    lateinit var tvDeepSleep: TextView
    lateinit var tvLightSleep: TextView

    lateinit var tvHeart: TextView
    lateinit var tvOxygen: TextView
    lateinit var tvUpdateDate: TextView

    lateinit var currentDate: String

    lateinit var tvWeeks: List<WeekTextView>

    // endregion =====View=====

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_deaf_record, container, false)
        initView(view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        var _name = arguments?.getString("user_name")
        var _fts_name = arguments?.getString("user_fts_name")
        deafUserId = arguments?.getInt("user_id", 0) ?: kotlin.run { 0 }
        (activity as MainActivity).setToolbarOptions(true , "${_fts_name} ${_name}的健康紀錄")

        viewModel = ViewModelProviders.of(this).get(DeafRecordViewModel::class.java)
        initDate()
    }

    fun initView(view: View) {

        ibtnPreDate = view.findViewById(R.id.ibtn_pre_date)
        ibtnNextDate = view.findViewById(R.id.ibtn_next_date)
        tvDate = view.findViewById(R.id.tv_date)

        scrollView = view.findViewById<NestedScrollView>(R.id.scroll_view)
        sBarWalk = view.findViewById<CustomSeekBar>(R.id.seek_bar_walk)
        tvWalkCount = view.findViewById<TextView>(R.id.tv_walk_count)
        tvWalkTarget = view.findViewById<TextView>(R.id.tv_walk_target)
        tvCalorie = view.findViewById<TextView>(R.id.tv_calorie)
        tvWalkTime = view.findViewById<TextView>(R.id.tv_walk_time)
        tvWalkDistance = view.findViewById<TextView>(R.id.tv_walk_distance)
        tvWeek1 = view.findViewById<WeekTextView>(R.id.tv_week_1)
        tvWeek2 = view.findViewById<WeekTextView>(R.id.tv_week_2)
        tvWeek3 = view.findViewById<WeekTextView>(R.id.tv_week_3)
        tvWeek4 = view.findViewById<WeekTextView>(R.id.tv_week_4)
        tvWeek5 = view.findViewById<WeekTextView>(R.id.tv_week_5)
        tvWeek6 = view.findViewById<WeekTextView>(R.id.tv_week_6)
        tvWeek7 = view.findViewById<WeekTextView>(R.id.tv_week_7)
        tvWeeks = listOf(tvWeek1, tvWeek2, tvWeek3, tvWeek4, tvWeek5, tvWeek6, tvWeek7)

        tvHour = view.findViewById(R.id.tv_hour)
        tvMinutes = view.findViewById(R.id.tv_minutes)
        tvDeepSleep = view.findViewById(R.id.tv_deep_sleep)
        tvLightSleep = view.findViewById(R.id.tv_light_sleep)

        tvHeart = view.findViewById(R.id.tv_heart)
        tvOxygen = view.findViewById(R.id.tv_oxygen)
        tvUpdateDate = view.findViewById(R.id.tv_update_date)



        tvDate.setOnClickListener {
            currentDate.split("-").apply {
                var year = get(0).toInt()
                var month = get(1).toInt().let { it - 1 }
                var dayOfMonth = get(2).toInt()
                DatePickerDialog(context, { view, year, month, dayOfMonth ->
                    mCalendar.set(year, month, dayOfMonth)
                    tvDate.showDate(mCalendar)
                }, year, month, dayOfMonth)
                    .apply {
                        datePicker.maxDate = Calendar.getInstance().timeInMillis
                    }.show()
            }
        }

        ibtnPreDate.setOnClickListener {
            var value = mCalendar.get(Calendar.DAY_OF_YEAR)
            mCalendar.set(Calendar.DAY_OF_YEAR, --value)
            tvDate.showDate(mCalendar)
        }

        ibtnNextDate.setOnClickListener {
            var value = mCalendar.get(Calendar.DAY_OF_YEAR)
            mCalendar.set(Calendar.DAY_OF_YEAR, ++value)
            tvDate.showDate(mCalendar)
        }

    }

    fun initDate() {
        mCalendar = Calendar.getInstance(Locale.TAIWAN)
        tvDate.showDate(mCalendar)
    }

    /**
     * 顯示日期
     * Kotlin extension
     */
    fun TextView.showDate(calendar: Calendar) {

        var dateMark: String =
            if (DateUtils.isToday(calendar.timeInMillis)) {
                ibtnNextDate.visibility = View.GONE
                "今日"
            } else {
                ibtnNextDate.visibility = View.VISIBLE
                if (isYesterday(calendar.timeInMillis)) "昨日"
                else ""
            }

        currentDate =
                "${calendar.get(Calendar.YEAR)}-${calendar.get(Calendar.MONTH) + 1}-${calendar.get(Calendar.DATE)}"
        currentDate.split("-").apply {
            text = "${get(0)}年${get(1)}月${get(2)}日 $dateMark"
        }

        tvWeeks.map {
            it.isSelected = false
        }
        tvWeeks.get(calendar.get(Calendar.DAY_OF_WEEK) - 1).isSelected = true

        scrollView.scrollTo(0, 0)

        viewModel.getHealthRecord(deafUserId, currentDate).observe(viewLifecycleOwner::getLifecycle){
            if (it.status != Status.LOADING) {
                progressDig?.dismiss()
                if (it.data != null)
                    setHealthRecord(it.data)
            } else {
                if (progressDig == null) {
                    var builder = AlertDialog.Builder(context)
                    builder.setMessage("請稍候")
                    progressDig = builder.create()
                }
                progressDig?.show()
            }
        }
    }

    /**
     * 設定健康紀錄
     */
    fun setHealthRecord(entity: HealthRecordEntity) {
        tvWalkCount.text = entity.step_count.toString()
        tvWalkTarget.text = "目標${entity.user_step_goal}步"
        tvCalorie.text = "${entity.step_calorie}卡"
        tvWalkTime.text =
                "${secondToHM(entity.step_moving_time).split(":")[0]}小時${secondToHM(entity.step_moving_time).split(":")[1]}分鐘"
        tvWalkDistance.text = distanceKilometer(entity.step_distance)
        sBarWalk.progress = (entity.step_count.toFloat() / entity.user_step_goal * 100).toInt()

        // --- 睡眠 ---
        tvHour.text = secondToHM(entity.sleep_duration).split(":")[0].toString()
        tvMinutes.text = secondToHM(entity.sleep_duration).split(":")[1].toString()
        tvDeepSleep.text =
                "${secondToHM(entity.sleep_duration_deep).split(":")[0]}小時${secondToHM(entity.sleep_duration_deep).split(
                    ":"
                )[1]}分鐘"
        tvLightSleep.text =
                "${secondToHM(entity.sleep_duration_light).split(":")[0]}小時${secondToHM(entity.sleep_duration_light).split(
                    ":"
                )[1]}分鐘"

        tvHeart.text = entity.hr_bpm_avg.toInt().toString()
        tvOxygen.text = entity.highest_percent.toString()
        tvUpdateDate.text = entity.latest_datetime.let {
            if (it.isEmpty()) "無資料" else it
        }


        for (pos in 0..tvWeeks.size - 1) {
            if (pos < entity.step_week.size) {
                // 有資料
                tvWeeks.get(pos).setStatus(entity.step_week.get(pos))
            } else {
                // 無資料
                tvWeeks.get(pos).setStatus(-1)
            }
        }
    }


    fun isYesterday(time: Long): Boolean {
        // 今天時間 = 昨天時間 + 時間差
        return DateUtils.isToday(time + DateUtils.DAY_IN_MILLIS)
    }

    /**
     * value 秒數換時分秒
     */
    fun secondToHM(value: Int): String {
        var value = value
        val second = (value % 60)
        val minute = (value / 60) % 60
        val hour = (value / 3600)
        return String.format(
            "%02d:%02d:%02d",
            hour,
            minute,
            second
        )
    }

    /**
     * 公尺換公里
     * value : 公尺
     */
    fun distanceKilometer(value: Int): String {
        return String.format(
            "%d公里%d公尺",
            value / 1000,
            value % 1000
        )
    }

} // class close
