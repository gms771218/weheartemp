package tw.azul.wehear.status

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import tw.azul.wehear.api.DeviceApiRepository

class DetailViewModel(val app: Application) : AndroidViewModel(app) {

    /*
        裝置狀態 (query Web API)
     */
    private val device_status: LiveData<List<DeviceApiRepository.Status?>> = DeviceApiRepository.Instance.status()

    val enabledLiveData = MediatorLiveData<Boolean>().apply {
        addSource(device_status) { list ->
            val status = list?.firstOrNull {
                it?.nts_id == nts_id
            }
            postValue(status?.ns_enable ?: 0 > 0)
        }
    }

    var nts_id:Int = -1

}
