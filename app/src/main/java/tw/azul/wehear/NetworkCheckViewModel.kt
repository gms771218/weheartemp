package tw.azul.wehear

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.os.Handler
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class NetworkCheckViewModel(app: Application) : AndroidViewModel(app) {

    val isNetworkAvailableLiveData = MutableLiveData<Boolean>().apply { value = true }

    private lateinit var mHandler: Handler
    init {
        /**
         * TODO: 應該改用下列 API
         * {@link android.net.ConnectivityManager.NetworkCallback}
         * {@link ConnectivityManager#registerDefaultNetworkCallback}
         * {@link ConnectivityManager#registerNetworkCallback}
         */
        mHandler = Handler { msg ->
            if (msg.what == 1) {
                runCatching {
                    val connectivityManager = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val networkInfo = connectivityManager.activeNetworkInfo
                    isNetworkAvailableLiveData.value = when {
                        networkInfo == null -> false
                        !networkInfo.isConnected -> false
                        !networkInfo.isAvailable -> false
                        else -> true
                    }
                }.exceptionOrNull()?.printStackTrace()
                mHandler.sendEmptyMessageDelayed(1, 1000)
                true
            }
            false
        }
        mHandler.sendEmptyMessageDelayed(1, 1000)
    }

    override fun onCleared() {
        mHandler.removeMessages(1)
        super.onCleared()
    }
}