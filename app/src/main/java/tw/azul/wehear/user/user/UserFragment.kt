package tw.azul.wehear.user.user

import android.app.AlertDialog
import android.content.ComponentName
import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.IntRange
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import tw.azul.wehear.MainActivity
import tw.azul.wehear.PortalActivity

import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.user.UserEntity
import java.util.*

class UserFragment : Fragment() {

    companion object {
        fun newInstance() = UserFragment()
    }

    lateinit var imgHeader: ImageView
    lateinit var tvName: TextView
    lateinit var tvSex: TextView
    lateinit var tvAge: TextView

    lateinit var tvHeight: TextView
    lateinit var tvWeight: TextView
    lateinit var tvStepSize: TextView
    lateinit var tvAddress: TextView

    lateinit var btnLogout: Button

    var progressDig: AlertDialog? = null

    lateinit var userViewModel: UserViewModel

    val USER_ID = AccountRoom.TokenLiveData.value?.userId!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.layout_user_info, container, false)
        initView(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? MainActivity)?.showToolbarAndHideNavigation()
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        userViewModel.getUser(USER_ID).observe(this, Observer {
            if (it.status != Status.LOADING) {
                progressDig?.dismiss()
                setUserInfo(it.data)
            } else {
                if (progressDig == null) {
                    var builder = AlertDialog.Builder(context)
                    builder.setMessage("請稍候")
                    progressDig = builder.create()
                }
                progressDig?.show()
            }
        })

        userViewModel.finishSignalLiveData.observe(viewLifecycleOwner::getLifecycle) { signal ->
            if (signal) {
                activity?.run{
                    startActivity(Intent.makeRestartActivityTask(ComponentName(this, PortalActivity::class.java)))
                    finish()
                }
            }
        }

        btnLogout.setOnClickListener(userViewModel::logout)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater?.inflate(R.menu.menu_band_setting, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item?.itemId) {
            R.id.action_edit -> {
                NavHostFragment.findNavController(this).navigate(R.id.action_userFragment_to_userEditFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun initView(view: View) {

        imgHeader = view.findViewById(com.gms.widget.layout.R.id.img_header)
        btnLogout = view.findViewById(com.gms.widget.layout.R.id.btn_logout)

        tvName = view.findViewById(com.gms.widget.layout.R.id.tv_name)
        tvSex = view.findViewById(com.gms.widget.layout.R.id.tv_sex)
        tvAge = view.findViewById(com.gms.widget.layout.R.id.tv_age)

        com.gms.widget.layout.R.id.layout_user_height
        var layoutHeight = view.findViewById<View>(com.gms.widget.layout.R.id.layout_user_height)
        layoutHeight.findViewById<TextView>(com.gms.widget.layout.R.id.textView).setText("身高")
        tvHeight = layoutHeight.findViewById(com.gms.widget.layout.R.id.tv_value)
        tvHeight.setText("--cm")

        var layoutWeight = view.findViewById<View>(com.gms.widget.layout.R.id.layout_user_weight)
        layoutWeight.findViewById<TextView>(com.gms.widget.layout.R.id.textView).setText("體重")
        tvWeight = layoutWeight.findViewById(com.gms.widget.layout.R.id.tv_value)
        tvWeight.setText("--kg")

        var layoutStepSize = view.findViewById<View>(com.gms.widget.layout.R.id.layout_user_step_size)
        layoutStepSize.findViewById<TextView>(com.gms.widget.layout.R.id.textView).setText("步長")
        tvStepSize = layoutStepSize.findViewById(com.gms.widget.layout.R.id.tv_value)
        tvStepSize.setText("--cm")

        var layoutAddress = view.findViewById<View>(com.gms.widget.layout.R.id.layout_user_address)
        layoutAddress.findViewById<TextView>(com.gms.widget.layout.R.id.textView).setText("地址")
        tvAddress = layoutAddress.findViewById(com.gms.widget.layout.R.id.tv_value)
        tvAddress.setText("-----")
    }

    fun setUserInfo(userEntity: UserEntity?) {
        if (userEntity != null) {
            tvName.setText(userEntity.user_name)
            tvSex.setText(userEntity.isUser_is_male?.let { isMale-> if (isMale) "男" else "女" }?:"")
            tvAge.setText(getAge(userEntity.user_birthday_year,userEntity.user_birthday_month , userEntity.user_birthday_day))
            tvHeight.setText(userEntity.user_height , "cm")
            tvWeight.setText(userEntity.user_weight ,"kg")
            tvStepSize.setText(userEntity.user_stride_length ,"cm")
            tvAddress.setText(userEntity.user_address)
        }
    }

    /**
     * 計算年齡
     */
    fun getAge(birY: Int, @IntRange(from = 1 , to = 12) bitM : Int,@IntRange(from = 1 , to = 31) bitD : Int): String {
        var result = 0
        var calendar = Calendar.getInstance() ;
        if( calendar.get(Calendar.MONTH)+1 > bitM )
            result = calendar.get(Calendar.YEAR) - birY ;
        else if( (calendar.get(Calendar.MONTH)+1 == bitM)&&(calendar.get(Calendar.DATE)>=bitD) )
            result = calendar.get(Calendar.YEAR) - birY;
        else
            result = calendar.get(Calendar.YEAR) - birY - 1  ;
        return result.toString()
    }

    fun TextView.setText(value: Int , tag : String) {
        if (value < 0) {
            this.setText("")
        } else {
            this.setText("${value}$tag")
        }
    }

}
