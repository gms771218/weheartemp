package tw.azul.wehear

import android.content.*
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams
import androidx.annotation.NavigationRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.fcm.NotificationDialogFactory
import tw.azul.wehear.user.InheritFamilyViewModel

@Suppress("LocalVariableName", "PrivatePropertyName")
class MainActivity : AppCompatActivity() {

    init {
        android.util.Log.i("Faty", "AccountRoom.TokenLiveData.value?.user_id = ${AccountRoom.TokenLiveData.value?.user_id}")
        android.util.Log.i("Faty", "AccountRoom.TokenLiveData.value?.userId  = ${AccountRoom.TokenLiveData.value?.userId}")
        android.util.Log.i("Faty", "AccountRoom.TokenLiveData.value?.token   = ${AccountRoom.TokenLiveData.value?.token}")
    }

    private val ConnectErrorReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            AlertDialog.Builder(context, R.style.Theme_MaterialComponents_Light_Dialog_Alert)
                .setMessage("帳號重複登入")
                .setCancelable(false)
                .setPositiveButton("OK") { _, _ ->
                    viewModel.logout()
                }.show()
        }
    }

    private lateinit var mainConstraintSet: ConstraintSet
    private lateinit var inheritFamilyViewModel: InheritFamilyViewModel
    private lateinit var viewModel: MainViewModel
    private var checkDialog : AlertDialog? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // 設定 Toolbar
        setSupportActionBar(toolbar)
        // 設定 Bottom Navigation
        val color_status   = ResourcesCompat.getColorStateList(resources, R.color.color_status , null)
        val color_health   = ResourcesCompat.getColorStateList(resources, R.color.color_health , null)
        val color_sos      = ResourcesCompat.getColorStateList(resources, R.color.color_sos    , null)
        val color_setting  = ResourcesCompat.getColorStateList(resources, R.color.color_setting, null)
        // 預設分頁
        var selectedItemId: Int? = null
        navigation.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                selectedItemId -> {
                    return@OnNavigationItemSelectedListener false
                }
                R.id.navigation_status -> {
                    if (::inheritFamilyViewModel.isInitialized) inheritFamilyViewModel.peekFSData() // 查詢家庭資訊
                    if (::viewModel.isInitialized) with(viewModel) {
                        peekRSC() // 上傳 RSC
                        checkUserDevice() // 檢查使用者裝置
                    }
                    navigation.apply {
                        itemIconTintList = color_status
                        itemTextColor = color_status
                    }
                    selectedItemId = item.itemId.save("selectedItemId")
                    setupNavHostFragment(R.navigation.nav_status)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_health -> {
                    if (::inheritFamilyViewModel.isInitialized) inheritFamilyViewModel.peekFSData() // 查詢家庭資訊
                    if (::viewModel.isInitialized) with(viewModel) {
                        //peekRSC() // 不用上傳 RSC, 內頁自會上傳
                        checkUserDevice() // 檢查使用者裝置
                    }
                    navigation.apply {
                        itemIconTintList = color_health
                        itemTextColor = color_health
                    }
                    selectedItemId = item.itemId.save("selectedItemId")
                    setupNavHostFragment(R.navigation.nav_health)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_sos -> {
                    if (::inheritFamilyViewModel.isInitialized) inheritFamilyViewModel.peekFSData() // 查詢家庭資訊
                    if (::viewModel.isInitialized) with(viewModel) {
                        peekRSC() // 上傳 RSC
                        checkUserDevice() // 檢查使用者裝置
                    }
                    navigation.apply {
                        itemIconTintList = color_sos
                        itemTextColor = color_sos
                    }
                    selectedItemId = item.itemId.save("selectedItemId")
                    setupNavHostFragment(R.navigation.nav_sos)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_setting -> {
                    if (::inheritFamilyViewModel.isInitialized) inheritFamilyViewModel.peekFSData() // 查詢家庭資訊
                    if (::viewModel.isInitialized) with(viewModel) {
                        peekRSC() // 上傳 RSC
                        checkUserDevice() // 檢查使用者裝置
                    }
                    navigation.apply {
                        itemIconTintList = color_setting
                        itemTextColor = color_setting
                    }
                    selectedItemId = item.itemId.save("selectedItemId")
                    setupNavHostFragment(R.navigation.nav_user)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        })
        // 注入內容索引 (預設是 status fragment)
        navigation.selectedItemId = R.id.navigation_status.restore("selectedItemId")
        // 注入 mainConstraintSet
        mainConstraintSet = ConstraintSet().apply { clone(mainConstraintLayout) }
        // 注入 ViewModel
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.peekRSC() // 上傳 RSC
        viewModel.checkUserDevice() // 檢查使用者裝置
        viewModel.updateFcmToken(this)
        viewModel.finishSignalLiveData.observe(this::getLifecycle) { signal ->
            if (signal) {
                // 註銷FCM Token
                viewModel.unregisterFcmToken()
                startActivity(
                    Intent.makeRestartActivityTask(
                        ComponentName(
                            this,
                            PortalActivity::class.java
                        )
                    )
                )
                finish()
            }
        }
        // 設定 navigation red point
        inheritFamilyViewModel = ViewModelProviders.of(this).get(InheritFamilyViewModel::class.java).apply {
            navigation_red_point.apply {
                // 拔起來
                (parent as? ViewGroup)?.removeView(this)
                // 插進去
                navigation.findViewById<FrameLayout>(R.id.navigation_setting)?.addView(
                    this,
                    LayoutParams(layoutParams).apply { gravity = Gravity.CENTER_HORIZONTAL }
                )
                // 關注 visibilityLiveData 決定紅點顯示與否
                visibilityLiveData.observe(::getLifecycle, ::setVisibility)
            }
        }
        // 關注網路
        ViewModelProviders.of(this).get(NetworkCheckViewModel::class.java).apply {
            val alertDialog: AlertDialog = AlertDialog
                .Builder(this@MainActivity, R.style.Theme_MaterialComponents_Light_Dialog_Alert).run {
                    setCancelable(false)
                    setTitle("提示")
                    setMessage("請重新確認網路連線")
                    setNegativeButton("確定", null)
                    create()
                }
            isNetworkAvailableLiveData.observe(this@MainActivity, object : Observer<Boolean> {
                init {
                    alertDialog.setOnDismissListener {
                        isNetworkAvailableLiveData.observe(this@MainActivity, this)
                    }
                }
                override fun onChanged(available: Boolean?) {
                    when (available) {
                        true -> {/* 有網路 */}
                        false -> {
                            if (!alertDialog.isShowing) {
                                isNetworkAvailableLiveData.removeObserver(this)
                                alertDialog.show()
                            }
                        }
                        null -> {
                            alertDialog.setOnDismissListener(null)
                            alertDialog.dismiss()
                        }
                    }
                }
            })
        }
    }

    private fun setupNavHostFragment(@NavigationRes idRes: Int) {
        NavHostFragment.create(idRes).apply {
            supportFragmentManager.beginTransaction()
                .setPrimaryNavigationFragment(this)
                .setCustomAnimations(
                    android.R.anim.fade_in, android.R.anim.fade_out,
                    android.R.anim.fade_in, android.R.anim.fade_out
                )
                .replace(R.id.mainContainer, this)
                .runOnCommit {
                    setupActionBarWithNavController(navController, AppBarConfiguration(navController.graph))
                    navController.addOnDestinationChangedListener { _, destination, _ ->
                        // 去箭頭
                        supportActionBar?.setDisplayHomeAsUpEnabled(NoArrowDestination.indexOf(destination.id) < 0)
                        // 恢復 isPreventBackPressed
                        isPreventBackPressed = false
                    }
                }
                .commit()
        }
    }

    fun setToolbarOptions(showToolbarArrow: Boolean = true, title: CharSequence? = supportActionBar?.title) {
        supportActionBar?.apply {
            if (this.title != title) setTitle(title)
            setDisplayHomeAsUpEnabled(showToolbarArrow)
        }
        isPreventBackPressed = !showToolbarArrow
    }

    private var isPreventBackPressed:Boolean = false

    override fun onBackPressed() {
        if (isPreventBackPressed) {
            /* nothing to do */
            return
        }
        if (NoArrowDestination.indexOf(findNavController(R.id.mainContainer).currentDestination?.id ?: -1) >= 0) {
            //android.widget.Toast.makeText(this, "關閉 App", android.widget.Toast.LENGTH_SHORT).show()
            AlertDialog.Builder(this, R.style.Theme_MaterialComponents_Light_Dialog_Alert).apply {
                setCancelable(false)
                //setTitle("提示")
                setMessage("關閉 App")
                setOnDismissListener { finish() }
                Handler().postDelayed(show()::dismiss, 1000)
            }
            return
        }
        super.onBackPressed()
    }

    override fun onSupportNavigateUp()
    /*
        https://developer.android.com/topic/libraries/architecture/navigation/navigation-ui#action_bar
        **must be override onSupportNavigateUp**
     */
    = findNavController(R.id.mainContainer).navigateUp() || super.onSupportNavigateUp()

    override fun onStart() {
        super.onStart()
        onNewIntent(intent)
        val filter = IntentFilter(getString(R.string.action_repeat_login))
        registerReceiver(ConnectErrorReceiver, filter)
        checkVersion()
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(ConnectErrorReceiver)
//        checkDialog?.apply {
//            checkDialog?.dismiss()
//        }
    }

    /**
     * 隱藏 Toolbar, 隱藏 Bottom Navigation
     */
    fun hideToolbarAndHideNavigation() {
//        androidx.transition.TransitionManager.beginDelayedTransition(mainConstraintLayout)
//        mainConstraintSet.apply {
//            setVisibility(R.id.toolbar, ConstraintSet.GONE)
//            setVisibility(R.id.navigation, ConstraintSet.GONE)
//            applyTo(mainConstraintLayout)
//        }
        toolbar.visibility = View.GONE
        navigation.visibility = View.GONE
        if (::inheritFamilyViewModel.isInitialized) inheritFamilyViewModel.peekFSData() // 查詢家庭資訊
    }

    /**
     * 隱藏 Toolbar, 顯示 Bottom Navigation
     */
    fun hideToolbarAndShowNavigation() {
//        androidx.transition.TransitionManager.beginDelayedTransition(mainConstraintLayout)
//        mainConstraintSet.apply {
//            setVisibility(R.id.toolbar, ConstraintSet.GONE)
//            setVisibility(R.id.navigation, ConstraintSet.VISIBLE)
//            applyTo(mainConstraintLayout)
//        }
        toolbar.visibility = View.GONE
        navigation.visibility = View.VISIBLE
        if (::inheritFamilyViewModel.isInitialized) inheritFamilyViewModel.peekFSData() // 查詢家庭資訊
    }

    /**
     * 顯示 Toolbar, 隱藏 Bottom Navigation
     */
    fun showToolbarAndHideNavigation() {
//        androidx.transition.TransitionManager.beginDelayedTransition(mainConstraintLayout)
//        mainConstraintSet.apply {
//            setVisibility(R.id.toolbar, ConstraintSet.VISIBLE)
//            setVisibility(R.id.navigation, ConstraintSet.GONE)
//            applyTo(mainConstraintLayout)
//        }
        toolbar.visibility = View.VISIBLE
        navigation.visibility = View.GONE
        if (::inheritFamilyViewModel.isInitialized) inheritFamilyViewModel.peekFSData() // 查詢家庭資訊
    }

    /**
     * 顯示 Toolbar, 顯示 Bottom Navigation
     */
    fun showToolbarAndShowNavigation() {
//        androidx.transition.TransitionManager.beginDelayedTransition(mainConstraintLayout)
//        mainConstraintSet.apply {
//            setVisibility(R.id.toolbar, ConstraintSet.VISIBLE)
//            setVisibility(R.id.navigation, ConstraintSet.VISIBLE)
//            applyTo(mainConstraintLayout)
//        }
        toolbar.visibility = View.VISIBLE
        navigation.visibility = View.VISIBLE
        if (::inheritFamilyViewModel.isInitialized) inheritFamilyViewModel.peekFSData() // 查詢家庭資訊
    }

    fun checkVersion(){
        viewModel.checkVersionLiveData().observe(this, Observer { version ->
            version?.apply {
                if (version.vts_id != 2) {
                } else {
                    version?.apply {
                        checkDialog?:kotlin.run {
                            checkDialog = AlertDialog.Builder(this@MainActivity, R.style.Theme_MaterialComponents_Light_Dialog_Alert)
                                .apply {
                                    setCancelable(false)
                                    setTitle("軟體更新")
                                    setMessage("目前APP已有最新版本，建議您更新！以獲得最佳體驗喔！")
                                    setPositiveButton("前往更新") { dialog, which ->
                                        val intent = Intent(Intent.ACTION_VIEW)
                                        intent.setData(Uri.parse("market://details?id=${context.packageName}"))
                                        startActivity(intent)
                                        dialog.dismiss()
                                    }
                                    if (version.vs_force == 0)
                                        setNegativeButton("暫時不用") { dialog, which ->
                                            dialog.dismiss()
                                        }
                                    setOnDismissListener {
                                        checkDialog = null
                                    }
                                }.show()
                        }
                    }
                }
            }
        })
    }

    // Kotlin Extension used by Self
    private fun Int.save(key: String):Int {
        App.GetPreferences(application).edit().putInt(key, this).apply()
        return this
    }

    private fun Int.restore(key: String):Int {
        return App.GetPreferences(application).getInt(key, this)
    }

    companion object {
        private val NoArrowDestination = arrayOf(
            R.id.mainStatusFragment,
            R.id.bandInfoFragment,
            R.id.bandGoalFragment,
            R.id.hostAddressFragment,
            R.id.mainHealthFragment,
            R.id.mainSOSFragment,
            R.id.mainUserFragment
        )
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        // 處理推播內容
        intent?.getStringExtra("data")?.apply {
            NotificationDialogFactory(this@MainActivity ,  AccountRoom.TokenLiveData.value?.userId?:0 ).doParseIntent(intent)
        }
        setIntent(null)
    }
}
