package tw.azul.wehear.status.band.form

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.fragment_portal_info.*

import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.databinding.FragmentBandSettingInfoBinding

class InfoFragment : Fragment() {

    companion object {
        fun newInstance() = InfoFragment()
    }

    private lateinit var dataBinding: FragmentBandSettingInfoBinding
    private lateinit var viewModel: InfoViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_band_setting_info, container, false)
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(InfoViewModel::class.java)
        dataBinding.viewmodel = viewModel
        dataBinding.setLifecycleOwner(viewLifecycleOwner)
        viewModel.getUser(AccountRoom.TokenLiveData.value?.userId!!).observe(viewLifecycleOwner::getLifecycle){}
        btn_next.setOnClickListener {
            // 檢查欄位
            if (value_tall.valid() and value_weight.valid() and value_step_length.valid()){
                viewModel.saveUser(AccountRoom.TokenLiveData.value?.userId!!).observe(viewLifecycleOwner::getLifecycle){}
                NavHostFragment.findNavController(this).navigate(R.id.action_bandInfoFragment_to_bandGoalFragment)
            }
        }
    }

}
