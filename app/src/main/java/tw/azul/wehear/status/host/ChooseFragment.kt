package tw.azul.wehear.status.host

import android.bluetooth.BluetoothDevice
import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_host_setting_choose.*
import tw.azul.wehear.R
import tw.azul.wehear.databinding.FragmentHostSettingChooseBinding
import tw.azul.wehear.widget.MiddleDividerItemDecoration

class ChooseFragment : Fragment() {

    companion object {
        fun newInstance() = ChooseFragment()
    }

    private lateinit var databinding: FragmentHostSettingChooseBinding
    private lateinit var viewModel: ChooseViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        databinding = DataBindingUtil.inflate(inflater, R.layout.fragment_host_setting_choose, container, false)
        return databinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ResourcesCompat.getDrawable(resources, R.drawable.divider_20dp, null)?.let {
            val middleDivider =
                MiddleDividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            middleDivider.setDrawable(it)
            hosts_list.addItemDecoration(middleDivider)
        }
        hosts_list.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }
        databinding.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ChooseViewModel::class.java)
        viewModel.lifecycleOwner = viewLifecycleOwner
        databinding.viewmodel = viewModel
    }

    private lateinit var observer:Observer<Boolean>

    override fun onResume() {
        super.onResume()
        observer = object: Observer<Boolean> {
            override fun onChanged(bleStatus: Boolean) {
                viewModel.bleStatusDelObserver(this)
                if (bleStatus) {
                    viewModel.hostListLiveData.observe(viewLifecycleOwner, Observer {
                        hosts_list.adapter = HostsAdapter(it, viewModel)
                    })
                }
            }
        }
        viewModel.bleStatusAddObserver(viewLifecycleOwner, observer)
    }

    override fun onPause() {
        super.onPause()
        viewModel.bleStatusDelObserver(observer)
    }

    class HostsAdapter(val list: List<BluetoothDevice>, val viewmodel:ChooseViewModel) : RecyclerView.Adapter<HostViewHolder>() {

        var nowCompoundButton: CompoundButton? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HostViewHolder =
            HostViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_choose_host,
                    parent,
                    false
                )
            ).apply {
                device_address.setOnCheckedChangeListener {newCompoundButton, isChecked ->
                    if (isChecked) {
                        nowCompoundButton?.isChecked = false
                        nowCompoundButton = newCompoundButton
                        viewmodel.setBluetoothDevice(list[adapterPosition])
                    }
                }
            }

        override fun onBindViewHolder(holder: HostViewHolder, position: Int) {
            holder.apply {
                list[position].let {
                    device_name.text = it.name
                    device_address.text = it.address
                }
            }
        }

        override fun getItemCount(): Int = list.size
    }

    class HostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val res: Resources = itemView.resources
        val device_name: TextView = itemView.findViewById(R.id.device_name)
        val device_address: RadioButton = itemView.findViewById(R.id.device_address)
    }
}
