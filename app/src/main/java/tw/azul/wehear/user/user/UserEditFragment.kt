package tw.azul.wehear.user.user

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.snackbar.Snackbar
import tw.azul.wehear.MainActivity
import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.request.RequestUpdateUserEntity
import tw.azul.wehear.net.model.user.UserEntity
import tw.azul.wehear.user.walk.SettingWalkFragment


/**
 * 編輯使用者
 */
class UserEditFragment : Fragment() {

    val USER_ID = AccountRoom.TokenLiveData.value?.userId!!

    lateinit var etHeight: EditText
    lateinit var etWeight: EditText
    lateinit var etWalkSize: EditText
    lateinit var etAddress: EditText
    lateinit var btnCheck: Button

    private lateinit var progressDig: AlertDialog

    lateinit var userViewModel: UserViewModel

    var mUserEntity: UserEntity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.layout_edit_user_info, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? MainActivity)?.showToolbarAndHideNavigation()
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        userViewModel.getUser(USER_ID).observe(this, Observer {
            if (it.status != Status.LOADING) {
                it.data?.apply {
                    setUserEntity(this)
                }
            }
        })
    }

    fun initView(view: View) {
        etHeight = view.findViewById(R.id.et_height)
        etWeight = view.findViewById(R.id.et_weight)
        etWalkSize = view.findViewById(R.id.et_walk_size)
        etAddress = view.findViewById(R.id.et_address)
        btnCheck = view.findViewById(R.id.btn_check)
    }

    fun setUserEntity(user: UserEntity) {
        mUserEntity = user;
        etHeight.setText(if(user.user_height < 0) "" else user.user_height.toString())
        etWeight.setText(if(user.user_weight < 0) "" else user.user_weight.toString())
        etWalkSize.setText(if(user.user_stride_length < 0) "" else user.user_stride_length.toString())
        etAddress.setText(user.user_address)

        btnCheck.setOnClickListener {

            var requestUpdateUserEntity = RequestUpdateUserEntity.create(USER_ID, user, UserEntity().also {
                etHeight.text.isEmpty().also { isEmpty ->
                    it.user_height = if (!isEmpty) etHeight.text.toString().toInt() else user.user_height
                }
                etWeight.text.isEmpty().also { isEmpty ->

                    it.user_weight = if (!isEmpty) etWeight.text.toString().toInt() else user.user_weight
                }
                etWalkSize.text.isEmpty().also { isEmpty ->

                    it.user_stride_length =
                            if (!isEmpty) etWalkSize.text.toString().toInt() else user.user_stride_length
                }
                etAddress.text.isEmpty().also { isEmpty ->

                    it.user_address = if (!isEmpty) etAddress.text.toString() else ""
                }
            })

            userViewModel.updateUser(USER_ID, requestUpdateUserEntity).observe(this, Observer {

                if (it.status != Status.LOADING) {
                    progressDig.dismiss()
                    if (it.data?.status != 1000)
                        //android.widget.Toast.makeText(context, it.data?.msg, android.widget.Toast.LENGTH_SHORT).show()
                        view?.apply {
                            Snackbar.make(this, "錯誤 ${it.data?.msg?:""}", Snackbar.LENGTH_INDEFINITE).apply {
                                setActionTextColor(ResourcesCompat.getColor(resources, R.color.colorAccent, null))
                                setAction("知道了") { dismiss() }
                                show()
                            }
                        }
                    else
                        activity?.onBackPressed()
                } else {
                    if (!::progressDig.isInitialized) {
                        progressDig = AlertDialog.Builder(context).setMessage("請稍候").create()
                    }
                    progressDig.show()
                }
            })
        }
    }


} // class close