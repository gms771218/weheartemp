package tw.azul.wehear.fcm

import android.content.Intent
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.gms.widget.dialog.*
import com.google.gson.Gson
import tw.azul.wehear.fcm.model.NotificationEntity

/**
 * 顯示推播通知視窗
 */
class NotificationDialogFactory {

    var fragmentActivity: FragmentActivity;
    var userId = 0

    constructor(fragmentActivity: FragmentActivity, userId: Int) {
        this.fragmentActivity = fragmentActivity
        this.userId = userId
    }

    fun doParseIntent(intent: Intent) {
        var jsString: String = intent.getStringExtra("data")
        jsString!!.let {
            Gson().fromJson(it, NotificationEntity::class.java)
        }.apply {
            isMe = userId == user_id
            showNotificationDialog(fragmentActivity.supportFragmentManager, this)
        }
    }

    // 顯示推播
    fun showNotificationDialog(supportFragmentManager: FragmentManager, entity: NotificationEntity) {

        when (entity.nts_id) {
            // 門鈴
            1 -> {
                DoorDialog_3(Runnable { }).setContent(entity.body).show(supportFragmentManager, "door")
            }
            // 電話
            2 -> {
                PhoneDialog_3(Runnable { }).setContent(entity.body).show(supportFragmentManager, "phone")
            }
            // 3 遙控器(呼叫)
            3 -> {
                CallingDialog_2(Runnable { }).setContent(entity.body).show(supportFragmentManager, "call")
            }
            // 4 火災
            4 -> {
                if (entity.isMe)
                    FireDialog_1(Runnable { }).setContent(entity.body_self).show(
                        supportFragmentManager,
                        "fire"
                    )
                else
                    FireDialog_1_ForFamily(Runnable { })
                        .setName(entity.name)
                        .setMapImg(entity.user_map)
                        .setMapLink(entity.user_map_link)
                        .setAddress(entity.user_address)
                        .setPhone(entity.user_phone)
                        .setContent(entity.body)
                        .show(
                            supportFragmentManager,
                            "fire"
                        )
            }
            // SOS
            5 -> {
                SOSDialog_1(Runnable { })
                    .setName(entity.name)
                    .setAddress(entity.user_address)
                    .setMapImg(entity.user_map)
                    .setMapLink(entity.user_map_link)
                    .setPhone(entity.user_phone)
                    .show(supportFragmentManager, "sos")
            }
            // 心率
            11 -> {
                if (entity.isMe) {
                    // 自己
                    if (entity.pass == 0) {
                        //  緊急
                        HeartDialog_1(Runnable { })
                            .setStandard(entity.standard)
                            .setValue(entity.value)
                            .setMoreOrLess(entity.more_or_less)
                            .setContent(entity.body)
                            .show(supportFragmentManager, "heart")
                    } else {
                        // 警告
                        HeartDialog_2(Runnable { })
                            .setStandard(entity.standard)
                            .setValue(entity.value)
                            .setMoreOrLess(entity.more_or_less)
                            .setContent(entity.body)
                            .show(supportFragmentManager, "heart")
                    }
                } else {
                    // 家人
                    if (entity.pass == 0) {
                        //  緊急
                        HeartDialog_1_ForFamily(Runnable { })
                            .setStandard(entity.standard)
                            .setValue(entity.value)
                            .setMoreOrLess(entity.more_or_less)
                            .setName(entity.name)
                            .setPhone(entity.user_phone)
                            .setContent(entity.body)
                            .show(supportFragmentManager, "heart")
                    } else {
                        // 警告
                        HeartDialog_2_ForFamily(Runnable { })
                            .setStandard(entity.standard)
                            .setValue(entity.value)
                            .setMoreOrLess(entity.more_or_less)
                            .setName(entity.name)
                            .setPhone(entity.user_phone)
                            .setContent(entity.body)
                            .show(supportFragmentManager, "heart")
                    }
                }
            }
            // 血氧
            12 -> {
                if (entity.isMe) {
                    // 自己
                    if (entity.pass == 0) {
                        // 緊急
                        OxygenDialog_1(Runnable { })
                            .setStandard(entity.standard)
                            .setValue(entity.value)
                            .setMoreOrLess(entity.more_or_less)
                            .setContent(entity.body)
                            .show(supportFragmentManager, "oxygen")
                    } else {
                        // 警告
                        OxygenDialog_2(Runnable { })
                            .setStandard(entity.standard)
                            .setValue(entity.value)
                            .setMoreOrLess(entity.more_or_less)
                            .setContent(entity.body)
                            .show(supportFragmentManager, "oxygen")
                    }
                } else {
                    // 家人
                    if (entity.pass == 0) {
                        // 緊急
                        OxygenDialog_1_ForFamily(Runnable { })
                            .setStandard(entity.standard)
                            .setValue(entity.value)
                            .setMoreOrLess(entity.more_or_less)
                            .setName(entity.name)
                            .setDesc(entity.content)
                            .setPhone(entity.user_phone)
                            .setContent(entity.body)
                            .show(supportFragmentManager, "oxygen")
                    } else {
                        // 警告
                        OxygenDialog_2_ForFamily(Runnable { })
                            .setStandard(entity.standard)
                            .setValue(entity.value)
                            .setMoreOrLess(entity.more_or_less)
                            .setName(entity.name)
                            .setDesc(entity.content)
                            .setPhone(entity.user_phone)
                            .setContent(entity.body)
                            .show(supportFragmentManager, "oxygen")
                    }
                }
            }
            // 昨晚睡眠
            13 -> {
                if (entity.isMe) {
                    // 自己
                    SleepingDialog_2(Runnable { })
                        .setStandard(entity.standard)
                        .setValue(entity.value)
                        .setMoreOrLess(entity.more_or_less)
                        .setContent(entity.body)
                        .show(supportFragmentManager, "sleep")
                } else {
                    // 家人
                    SleepingDialog_2_Family(Runnable { })
                        .setStandard(entity.standard)
                        .setValue(entity.value)
                        .setMoreOrLess(entity.more_or_less)
                        .setName(entity.name)
                        .setPhone(entity.user_phone)
                        .setContent(entity.body)
                        .show(supportFragmentManager, "sleep")
                }
            }
            // 今日步行
            14 -> {
                // 達成目標
                if (entity.isMe) {
                    // 自己
                    if (entity.goal == 1)
                    // 達成目標
                        WalkingDialog_3(Runnable { })
                            .setStandard(entity.standard)
                            .setContent(entity.body)
                            .show(supportFragmentManager, "walk")
                    else
                    // 未達成目標
                        WalkingDialog_2(Runnable { })
                            .setMoreOrLess(entity.more_or_less)
                            .setStandard(entity.standard)
                            .setValue(entity.value)
                            .setContent(entity.body)
                            .show(supportFragmentManager, "walk")
                } else {
                    // 家人
                    if (entity.goal == 1)
                    // 達成目標
                        WalkingDialog_3_ForFamily_Success(Runnable { })
                            .setName(entity.name)
                            .setStandard(entity.standard)
                            .setValue(entity.value)
                            .setPhone(entity.user_phone)
                            .show(supportFragmentManager, "walk")
                    else
                    // 未達成目標
                        WalkingDialog_3_ForFamily(Runnable { })
                            .setHead(entity.head)
                            .setStandard(entity.standard)
                            .setValue(entity.value)
                            .setPhone(entity.user_phone)
                            .setName(entity.name)
                            .setDesc(entity.content)
                            .setMoreOrLess(entity.more_or_less)
                            .setContent(entity.body)
                            .show(supportFragmentManager, "walk")
                }
            }
            else -> {

            }
        }
    }
}
