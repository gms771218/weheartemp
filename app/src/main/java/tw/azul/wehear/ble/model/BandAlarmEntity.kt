@file:Suppress("unused")

package tw.azul.wehear.ble.model

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.IntRange
import java.util.*
import java.util.Calendar.*

/**
 * 手環鬧鐘 data class
 */

data class BandAlarmEntity(
    var key: Int = -1,
    // 0 is 12 AM (midnight) and 12 is 12 PM (noon).
    @IntRange(from = 0, to = 23)
    var hour: Int = 0,
    @IntRange(from = 0, to = 59)
    var minute: Int = 0,
    var themeName: String = "",
    var weeks: BooleanArray = BooleanArray(7),
    var enable: Boolean = true
) : Parcelable {
    fun toTime(): String {
        (hour == 0 || hour > 12)
        return String.format("%02d:%02d", HOURS_NUMBERS[hour % 12], minute)
    }

    fun toAmPm(): String {

        return if (hour < 12) "上午" else "下午"
    }

    fun toThemeName(): String = themeName.takeIf { it.isNotEmpty() } ?: "鬧鐘"

    fun toWeek(): String {
        val weekdayList = weeks.mapIndexed { index, w -> if (w) WEEKDAY_NAMES[index] else null }
                               .filterNotNull()
        return when(weekdayList.size) {
            7 -> WEEKDAY_NAMES[7] // 每天
            0 -> {
                /*val (nowHour, nowMinute) = Calendar.getInstance().run { get(HOUR_OF_DAY) to get(MINUTE) }
                when {
                    (nowHour   > hour)   -> "無(明天)"
                    (nowHour   < hour)   -> "無(今天)"
                    (nowMinute < minute) -> "無(今天)"
                    else -> "無(明天)"
                }*/
                "無"
            }
            else -> weekdayList.reduce { acc, s -> "$acc $s" }
        }
    }

    // 製作副本
    constructor(origin: BandAlarmEntity) : this(
        key = origin.key,
        hour = origin.hour,
        minute = origin.minute,
        themeName = origin.themeName,
        weeks = origin.weeks,
        enable = origin.enable
    )

    constructor(source: Parcel) : this(
        source.readInt(),
        source.readInt(),
        source.readInt(),
        source.readString() ?: "",
        source.createBooleanArray() ?: BooleanArray(7),
        source.readInt() == 1
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(key)
        writeInt(hour)
        writeInt(minute)
        writeString(themeName)
        writeBooleanArray(weeks)
        writeInt((if (enable) 1 else 0))
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BandAlarmEntity

        if (key != other.key) return false
        if (hour != other.hour) return false
        if (minute != other.minute) return false
        if (themeName != other.themeName) return false
        if (!weeks.contentEquals(other.weeks)) return false
        if (enable != other.enable) return false

        return true
    }

    override fun hashCode(): Int {
        var result = key
        result = 31 * result + hour
        result = 31 * result + minute
        result = 31 * result + themeName.hashCode()
        result = 31 * result + weeks.contentHashCode()
        result = 31 * result + enable.hashCode()
        return result
    }

    companion object {
        private val HOURS_NUMBERS = intArrayOf(12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
        private val WEEKDAY_NAMES = arrayOf("日", "一", "二", "三", "四", "五", "六", "每天")
        @JvmField
        val CREATOR: Parcelable.Creator<BandAlarmEntity> = object : Parcelable.Creator<BandAlarmEntity> {
            override fun createFromParcel(source: Parcel): BandAlarmEntity = BandAlarmEntity(source)
            override fun newArray(size: Int): Array<BandAlarmEntity?> = arrayOfNulls(size)
        }
    }
}
//{
//
//
//
//    //    var hour: Int = 0
////        get() = field
////        @SuppressLint("SupportAnnotationUsage") @IntRange(from = 0 , to = 23)
////        set(value) {
////            field = value
////        }
////    var minute: Int = 0
////        get() = field
////        @SuppressLint("SupportAnnotationUsage") @IntRange(from = 0 , to = 59)
////        set(value) {
////            field = value
////        }
////
////    var themeName: String = ""
////        get() = field
////        set(value) {
////            field = value
////        }
////
////    var weeks: BooleanArray = BooleanArray(7)
////        get() = field
////        internal set(value) {
////            field = value
////        }
//
//
//}