package tw.azul.wehear.status.band

import android.bluetooth.BluetoothDevice
import android.graphics.drawable.Animatable
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController

import kotlinx.android.synthetic.main.fragment_band_setting_search.*
import tw.azul.wehear.MainActivity

import tw.azul.wehear.R
import tw.azul.wehear.databinding.FragmentBandSettingSearchBinding

class SearchFragment : Fragment() {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private lateinit var databinding: FragmentBandSettingSearchBinding
    private lateinit var viewModel: SearchViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        databinding = DataBindingUtil.inflate(inflater, R.layout.fragment_band_setting_search, container, false)
        return databinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        databinding.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        databinding.viewmodel = viewModel
        // 決定 Toolbar 行為
        (activity as? MainActivity)?.showToolbarAndHideNavigation()
    }

    private lateinit var observer:Observer<Boolean>

    override fun onResume() {
        super.onResume()
        observer = object: Observer<Boolean> {
            override fun onChanged(bleStatus: Boolean) {
                viewModel.bleStatusDelObserver(this)
                if (bleStatus) {
                    viewModel.searchScanCallbackObserve(viewLifecycleOwner, Observer { set ->
                        when {
                            set?.size ?: 0 != 0 -> {
                                findNavController().navigate(R.id.action_bandSearchFragment_to_bandChooseFragment)
                            }
                            System.currentTimeMillis().minus(viewModel.start_time) < 30 * 1000L -> {
                                viewModel.searchAgain()
                            }
                        }
                    })
                }
            }
        }
        viewModel.bleStatusAddObserver(viewLifecycleOwner, observer)
    }

    override fun onPause() {
        super.onPause()
        if (::observer.isInitialized) {
            viewModel.bleStatusDelObserver(observer)
            viewModel.removeScanCallbackObserve(viewLifecycleOwner)
        }
    }

    override fun onStart() {
        super.onStart()
        // 開始動畫
        (logo.background as? Animatable) ?.start()
    }
}
