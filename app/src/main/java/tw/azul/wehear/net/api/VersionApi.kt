package tw.azul.wehear.net.api

import androidx.lifecycle.LiveData
import retrofit2.http.GET
import tw.azul.wehear.net.model.base.ListEntity
import tw.azul.wehear.net.model.version.VersionEntity
import tw.azul.wehear.net.retrofit.RetrofitClient

interface VersionApi {

    /**
     * 取得使用者
     */
    @GET(RetrofitClient.BASE_URL + "version")
    fun getVersions(): LiveData<ApiResponse<ListEntity<VersionEntity>>>

}