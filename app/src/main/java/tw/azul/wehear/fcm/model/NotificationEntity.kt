package tw.azul.wehear.fcm.model

/**
 * 推播內容物件
 */
class NotificationEntity {

    // 是否為自己發送的通知
    var isMe: Boolean = true

    // 通知唯一碼
    var str_hex: String? = null
    // 通知類型
    var nts_id: Int? = null
    // 發送通知的使用者id
    var user_id: Int? = null
    var name: String? = null
    // 文字訊息內容
    var body: String? = null

    // ------
    // 發送通知的使用者地址圖片連結
    var user_map: String? = null
    // 地圖連結
    var user_map_link : String? = null
    // 發給自己的文字訊息
    var body_self: String? = null
    // 發送通知的使用者手機號碼
    var user_phone: String? = null
    // 發送通知的使用者地址
    var user_address: String? = null
    // 顯示燈號(1為綠燈、橘燈，0為紅燈)
    var pass: Int = 0
    // 值
    var value: Int = 0
    // 標準值
    var standard: Int = 0
    // 步行目標是否達標(1或0)
    var goal: Int? = null
    // 高於或低於(1或0)
    var more_or_less: Int = 0
    // 血氧的內容
    var content: String? = null
    // 文字訊息的頭
    var head:String = ""
}