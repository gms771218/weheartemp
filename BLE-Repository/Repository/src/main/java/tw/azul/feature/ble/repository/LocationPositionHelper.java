package tw.azul.feature.ble.repository;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import static android.provider.Settings.Secure.LOCATION_MODE;
import static android.provider.Settings.Secure.LOCATION_MODE_OFF;
import static com.google.android.gms.location.LocationServices.SettingsApi;

public class LocationPositionHelper {

    public static boolean CheckLocationSettings(@NonNull Activity activity, boolean showDialog) {
        boolean isOff = Settings.Secure.getInt(activity.getContentResolver(), LOCATION_MODE, LOCATION_MODE_OFF) == LOCATION_MODE_OFF;
        if (isOff && showDialog) {
            LocationSettingsRequest.CheckLocationSettings(activity);
        }
        return !isOff;
    }

    private static class LocationSettingsRequest {

        private static final int REQUEST_CODE_DUMMY = 10001;
        private static LocationSettingsRequest mInstance;

        private static LocationSettingsRequest GetInstance(Context context) {
            if (mInstance == null) mInstance = new LocationSettingsRequest(context);
            return mInstance;
        }

        static void CheckLocationSettings(Activity activity) {
            int availableCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
            if (availableCode != ConnectionResult.SUCCESS) {
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(availableCode, activity, REQUEST_CODE_DUMMY);
                dialog.setCancelable(false);
                dialog.show();
                return;
            }
            GetInstance(activity).checkLocationSettings(activity);
        }

        private final com.google.android.gms.location.LocationSettingsRequest.Builder mBuilder;
        private final GoogleApiClient googleApiClient;
        private LocationSettingsRequest(Context context) {
            mBuilder = new com.google.android.gms.location
                    .LocationSettingsRequest.Builder()
                    .setNeedBle(false) // 注意: 當 true 時, 在 Android 5 會是真的開起藍牙, 但 Android 6+ 僅是開啟定位的藍牙掃描 → 手動開啟藍牙
                    .setAlwaysShow(true)
                    .addLocationRequest(
                            new LocationRequest().setPriority(LocationRequest.PRIORITY_LOW_POWER)
                    );
            googleApiClient = new GoogleApiClient
                    .Builder(context.getApplicationContext())
                    .addApi(LocationServices.API)
                    .build();
            googleApiClient.connect();
        }

        private void checkLocationSettings(Activity activity){
            ResultCallback<LocationSettingsResult> result = locationSettingsResult -> {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        /* nothing to do because LOCATION has been ON */
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied.
                        // But could be fixed by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(activity, REQUEST_CODE_DUMMY);
                            break;
                        } catch (Throwable e) {
//                            tw.idv.fy.library.utils.LineNumberLogger.LogW(e);
                        }
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied.
                        // However, we have no way to fix the settings
                    default:
                        new AlertDialog.Builder(activity, android.R.style.Theme_Material_Dialog_Alert)
                                .setPositiveButton("ok", (d, w) ->
                                        activity.startActivity(
                                                new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                                        )
                                )
                                .setMessage("Turn on Device Location")
                                .setCancelable(false)
                                .show();
                }
//                tw.idv.fy.library.utils.LineNumberLogger.LogI(status.getStatusMessage());
            };
            SettingsApi.checkLocationSettings(googleApiClient, mBuilder.build()).setResultCallback(result);
        }
    }
}
