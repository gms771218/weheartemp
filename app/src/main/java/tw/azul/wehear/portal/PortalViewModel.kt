package tw.azul.wehear.portal

import android.accounts.AccountManager
import android.app.Application
import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.*
import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRepository
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.account.AccountRoom.Status
import tw.azul.wehear.account.AccountRoom.Status.*
import tw.idv.fy.chrometab.ChromeTabHelper

class PortalViewModel(private val app: Application) : AndroidViewModel(app) {

    val signal: LiveData<Boolean> = MutableLiveData() // (測試用)

    val loading: MutableLiveData<Boolean> = MediatorLiveData<Boolean>().apply {
        addSource(AccountRoom.StatusLiveData) { status ->
            value = when (status) {
                is Connecting -> true
                is Init, is Error, is Connected -> false
            }
        }
    }

    val accountNameLiveData: MutableLiveData<String> = MutableLiveData()
    val passwordLiveData: MutableLiveData<String> = MutableLiveData()

    init {
        AccountManager.get(app).apply {
            getAccountsByType(app.packageName).firstOrNull()?.let { account ->
                accountNameLiveData.postValue(account.name)
                passwordLiveData.postValue(getPassword(account))
            }
        }
    }

    fun getStatusLiveData(): LiveData<Status> = Transformations.map(AccountRoom.StatusLiveData) { it }

    @Suppress("UNUSED_PARAMETER")
    fun login(view: View) {
        val acc:String? = accountNameLiveData.value
        val pwd:String? = passwordLiveData.value
        if (acc != null && pwd != null) {
            AccountRoom.StatusLiveData.value = Init
            MediatorLiveData<Status>().apply {
                addSource(AccountRoom.StatusLiveData) { status ->
                    value = status
                    if (status is Error) {
                        removeSource(AccountRoom.StatusLiveData)
                    }
                }
            }.also{
                (loading as MediatorLiveData).addSource(it) { status ->
                    when (status) {
                        is Error -> {
                            loading.removeSource(it)
                        }
                        is Connected -> {
                            //view.findNavController().navigate(R.id.action_portalFragment_to_searchBandFragment)
                            (signal as MutableLiveData).value = true
                        }
                    }
                }
            }
            AccountRepository.Instance(app).login(acc, pwd)
        }
    }

    fun startOfficialWebsite() {
        ChromeTabHelper.Open(
            app,
            Uri.parse("http://www.vapor.com.tw"),
            ChromeTabHelper.DefaultEnterAnimationBundle(app),
            ChromeTabHelper.DefaultExitAnimationBundle(app),
            Intent().putExtra(
                ChromeTabHelper.EXTRA_TOOLBAR_COLOR,
                ResourcesCompat.getColor(app.resources, R.color.colorAccent, null)
            )
        )
    }

}
