@file:Suppress("FunctionName", "LocalVariableName", "UsePropertyAccessSyntax", "EXPERIMENTAL_API_USAGE",
    "SpellCheckingInspection"
)

package tw.azul.wehear.ble

import android.app.Application
import android.bluetooth.*
import android.bluetooth.BluetoothGatt.GATT_SUCCESS
import android.bluetooth.BluetoothProfile.STATE_CONNECTED
import android.bluetooth.BluetoothProfile.STATE_DISCONNECTED
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import tw.azul.wehear.App
import tw.azul.wehear.api.DeviceApiRepository
import tw.azul.wehear.ble.HostGattRepository.Companion.SERVICES_UUID
import tw.azul.wehear.ble.HostGattRepository.Companion.U
import tw.azul.wehear.ble.HostGattRepository.Companion.WRITE_NO_UUID
import tw.idv.fy.kotlin.utils.toBleCommByteArr
import tw.idv.fy.kotlin.utils.toHexStrList

class HostConnector private constructor(private val application: Application) {

    companion object {
        @Volatile
        private var singleton: HostConnector? = null
        fun Instance(application: Application): HostConnector = singleton ?: synchronized(Companion) {
            singleton = singleton ?: HostConnector(application)
            return singleton!!
        }
    }

    private val mMainHandler = Handler(Looper.getMainLooper())
    private val connectionStateLiveData = MutableLiveData<ConnectionState>().apply { value = ConnectionState.Init }

    private val defaultInterceptorResultLiveData: LiveData<InterceptorResult> =
        MutableLiveData<InterceptorResult>().apply { value = InterceptorResult.Accept() }

    private val defaultInterceptor: ((hex: String) -> LiveData<InterceptorResult>) = { defaultInterceptorResultLiveData }

    sealed class InterceptorResult {
        data class Accept(val device_id: String? = null) : InterceptorResult()
        object Reject : InterceptorResult()
    }

    fun connectGatt(
        bluetoothDevice: BluetoothDevice? = null,
        doConnect: Boolean? = bluetoothDevice != null,
        interceptor: ((hex: String) -> LiveData<InterceptorResult>) = defaultInterceptor
    ): LiveData<ConnectionState> {
        if (bluetoothDevice == null || doConnect == false) {
            return connectionStateLiveData
        }
        when {
            !BluetoothAdapter.getDefaultAdapter().isEnabled -> {
                connectionStateLiveData.postValue(ConnectionState.Init)
                return connectionStateLiveData
            }
            connectionStateLiveData.value is ConnectionState.CONNECTED -> {
                return connectionStateLiveData
            }
            connectionStateLiveData.value is ConnectionState.DISCONNECTED -> {
                (connectionStateLiveData.value as ConnectionState.DISCONNECTED).apply{
                    // 保險起見, double check
                    gatt.disconnect()
                    gatt.close()
                }
            }
        }
        connectionStateLiveData.postValue(ConnectionState.Init)
        object : HostGattRepository.BluetoothGattCallback(application) {
            var gatt:BluetoothGatt?
            init {
                gatt = bluetoothDevice.connectGatt(application, false, this)
            }
            override fun onConnectionStateChange(bluetoothGatt: BluetoothGatt, status: Int, newState: Int) {
                android.util.Log.v("Faty", "[Host] $bluetoothDevice = $newState (STATE_CONNECTED = $STATE_CONNECTED)")
                when (newState) {
                    STATE_CONNECTED -> bluetoothGatt.discoverServices()
                    STATE_DISCONNECTED -> closeGATT(bluetoothGatt)
                }
            }
            override fun onServicesDiscovered(bluetoothGatt: BluetoothGatt, status: Int) {
                android.util.Log.v("Faty", "[Host] $bluetoothDevice = $status (GATT_SUCCESS = $GATT_SUCCESS)")
                if (status == GATT_SUCCESS) {
                    super.onServicesDiscovered(bluetoothGatt, status)
                } else {
                    closeGATT(bluetoothGatt)
                }
            }

            // 為了AOP, 連線成功前最後一哩不必強制 CallSuper
            override fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic) {
                val uByteArray = characteristic.value.toUByteArray()
                if (U[0x01] == uByteArray[1]) { // 送出裝置識別碼
                    val device_unique = uByteArray.toHexStrList().slice(3..8).joinToString(separator = "")
                    // 開始 AOP (最後一哩)
                    mMainHandler.post {
                        interceptor(device_unique).observeForever { result ->
                            when (result) {
                                is InterceptorResult.Accept -> {
                                    if (result.device_id != null) {
                                        gatt.device.setDeviceId(result.device_id)
                                    }
                                    connectionStateLiveData.postValue(ConnectionState.CONNECTED(gatt))
                                }
                                else -> {
                                    if (result != InterceptorResult.Reject) {
                                        android.util.Log.wtf("Faty", "InterceptorResult is o.w.")
                                    }
                                    closeGATT(gatt)
                                }
                            }
                        }
                    }
                } else {
                    super.onCharacteristicChanged(gatt, characteristic)
                }
            }

            override fun onDescriptorWrite(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {
                super.onDescriptorWrite(gatt, descriptor, status)
                if (status == GATT_SUCCESS) {
                    if (interceptor != defaultInterceptor) {
                        gatt.getService(SERVICES_UUID)
                            .getCharacteristic(WRITE_NO_UUID)
                            .apply {
                                setValue("aa0106ffffffffffff0d".toBleCommByteArr())//.LogD()
                                gatt.writeCharacteristic(this)//.LogD()
                            }
                    } else {
                        connectionStateLiveData.postValue(ConnectionState.CONNECTED(gatt))
                    }
                }/* else {
                    closeGATT(gatt)
                }*/

            }

            private fun closeGATT(bluetoothGatt: BluetoothGatt) {
                this.gatt?.disconnect()
                this.gatt?.close()
                this.gatt = null
                connectionStateLiveData.postValue(ConnectionState.DISCONNECTED(bluetoothGatt))
            }
        }
        return connectionStateLiveData
    }

    // 檢查清單中是否有本家 deviceId
    fun check(device: BluetoothDevice?, deviceIdList: List<String>) = deviceIdList.any { it == device?.getDeviceId() }

    fun reset() {
        connectionStateLiveData.value.run {
            when(this) {
                is ConnectionState.CONNECTED -> {
                    gatt.disconnect()
                    gatt.close()
                    gatt.device
                }
                is ConnectionState.DISCONNECTED -> {
                    gatt.disconnect()
                    gatt.close()
                    gatt.device
                }
                else -> null
            }?.apply {
                val device_id: String? = getDeviceId()
                if (device_id != null) {
                    DeviceApiRepository.Instance.remove(device_id).observeForever {
                        android.util.Log.w("Faty", "$device_id.remove = $it")
                    }
                }
                setDeviceId(null)
            }
        }
        connectionStateLiveData.postValue(ConnectionState.Init)
    }

    sealed class ConnectionState {
        object Init: ConnectionState()
        data class CONNECTED(val gatt: BluetoothGatt) : ConnectionState()
        data class DISCONNECTED(val gatt: BluetoothGatt) : ConnectionState()
    }

    private fun BluetoothDevice?.setDeviceId(device_id: String?) = apply {
        App.GetPreferences(application).edit().putString(this?.toString(), device_id).apply()
    }

    private fun BluetoothDevice?.getDeviceId(): String? = run {
        App.GetPreferences(application).getString(this?.toString(), null)
    }

}