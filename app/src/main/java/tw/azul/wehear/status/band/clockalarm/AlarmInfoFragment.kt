@file:Suppress("LocalVariableName")

package tw.azul.wehear.status.band.clockalarm

import android.os.Bundle
import android.view.*
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_band_alarm_info.*
import tw.azul.wehear.MainActivity
import tw.azul.wehear.R
import tw.azul.wehear.ble.model.BandAlarmEntity
import tw.azul.wehear.databinding.FragmentBandAlarmInfoBinding
import tw.azul.wehear.user.family.MyFamilyFragment
import java.util.*
import java.util.Calendar.HOUR_OF_DAY
import java.util.Calendar.MINUTE

class AlarmInfoFragment : Fragment() {

    companion object {
        fun newInstance() = AlarmInfoFragment()
        // 檢視模式
        private const val MODEL_VIEW_TYPE = View.GONE
        // 編輯模式
        private const val MODEL_EDIT_TYPE = View.VISIBLE
    }

    private lateinit var viewModel: AlarmInfoViewModel
    private lateinit var dataBinding: FragmentBandAlarmInfoBinding

    // 是否顯示OptionsMenu -> 沒有資料時 or 只有請求時
    private var isShowOptionsMenu = false
    // 編輯模式
    private var viewType: Int = MODEL_VIEW_TYPE

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentBandAlarmInfoBinding.inflate(inflater)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AlarmInfoViewModel::class.java)
        dataBinding.viewModel = viewModel
        dataBinding.setLifecycleOwner(viewLifecycleOwner)

        viewModel.alarmArrayListLiveData.observe(viewLifecycleOwner::getLifecycle) {
            alarms_list.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = AlarmInfoAdapter(it, viewType == MODEL_EDIT_TYPE)
                setHasFixedSize(true)
            }
            // change style
            (it?.size ?: 0 > 0).apply {
                if ( this != isShowOptionsMenu){
                    isShowOptionsMenu = this
                    activity?.invalidateOptionsMenu()
                    setModelType(MODEL_VIEW_TYPE)
                }
            }
            // 判斷是否禁增
            btn_next.isEnabled = it?.size ?: 0 < 4
            // entity 回歸
            arguments?.getParcelable<BandAlarmEntity>("alarm")?.apply {
                viewModel.addAlarm(activity, this)
                arguments?.clear()
            }
        }

        btn_next.setOnClickListener {
            val (nowH, nowM) = Calendar.getInstance().run { get(HOUR_OF_DAY) to get(MINUTE) }
            goEditPage(BandAlarmEntity(key = alarms_list?.adapter?.itemCount ?: 0, hour = nowH, minute = nowM))
        }
        setModelType(viewType)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBleStatusLiveData().observe(viewLifecycleOwner::getLifecycle) { bleStatus ->
            if (bleStatus.band.isConnected != true) {
                findNavController().popBackStack(R.id.bandSettingFragment, false)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        if (!isShowOptionsMenu)
            return
        if (viewType == MyFamilyFragment.MODEL_VIEW_TYPE)
            inflater?.inflate(R.menu.menu_band_setting, menu)
        else if (viewType == MyFamilyFragment.MODEL_EDIT_TYPE)
            inflater?.inflate(R.menu.menu_edit_finish, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item?.itemId) {
            R.id.action_edit -> {
                setModelType(MODEL_EDIT_TYPE)
                activity?.invalidateOptionsMenu()
                true
            }
            R.id.action_finish -> {
                setModelType(MODEL_VIEW_TYPE)
                activity?.invalidateOptionsMenu()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setModelType(type: Int) {
        viewType = type
        alarms_list.adapter?.apply {
            (this as AlarmInfoAdapter).setModelType(type)
        }

        when (type) {
            MODEL_VIEW_TYPE -> {
                btn_next.visibility = View.VISIBLE
                (activity as MainActivity).setToolbarOptions(true , "手錶鬧鐘")
            }
            MODEL_EDIT_TYPE -> {
                btn_next.visibility = View.GONE
                (activity as MainActivity).setToolbarOptions(false , "編輯鬧鐘")
            }
        }
    }

    fun goEditPage(entity: BandAlarmEntity) {

        val bundle = Bundle().apply {
            putParcelable("alarm", entity)
        }
        NavHostFragment.findNavController(this).navigate(R.id.action_alarmInfoFragment_to_alarmBuildFragment, bundle)
    }

    /**
     * Adapter Class
     */
    inner class AlarmInfoAdapter(val list: List<BandAlarmEntity>?, isDelete: Boolean) :
        RecyclerView.Adapter<AlarmInfoViewHolder>() {

        private var _isDelete = isDelete

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlarmInfoViewHolder =
            AlarmInfoViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_alarm_info,
                    parent,
                    false
                )
            )

        override fun onBindViewHolder(holder: AlarmInfoViewHolder, position: Int) {
            // TODO: To change body of created functions use File | Settings | File Templates.
            holder.bind(list?.get(position) ?: BandAlarmEntity(), _isDelete, position)
        }

        override fun getItemCount(): Int = list?.size ?: 0

        fun setModelType(type: Int) {
            _isDelete = type == MODEL_EDIT_TYPE
            notifyDataSetChanged()
        }
    }

    inner class AlarmInfoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @Suppress("UNUSED_PARAMETER")
        fun bind(entity: BandAlarmEntity, isDelete: Boolean = false, position: Int) {
            val am_pm: TextView = itemView.findViewById(R.id.am_pm)
            val time: TextView = itemView.findViewById(R.id.time)
            val theme: TextView = itemView.findViewById(R.id.theme)
            val weeks: TextView = itemView.findViewById(R.id.weeks)
            val enable: SwitchCompat = itemView.findViewById(R.id.enable)
            val delete: ImageButton = itemView.findViewById(R.id.delete)

            am_pm.text = entity.toAmPm()
            time.text = entity.toTime()

            theme.text = entity.toThemeName()
            weeks.text = entity.toWeek()


            delete.visibility = if (isDelete) View.VISIBLE else View.GONE
            enable.visibility = if (isDelete) View.GONE else View.VISIBLE

            enable.isChecked = entity.enable

            enable.setOnCheckedChangeListener { _, isChecked ->
                entity.enable = isChecked
                viewModel.updateAlarm(activity, entity)
            }
            delete.setOnClickListener {
                viewModel.deleteAlarm(entity)
            }

            itemView.setOnClickListener {
                if (isDelete) {
                    goEditPage(BandAlarmEntity(entity)) // 以副本傳出
                }
            }

        }

    }
}
