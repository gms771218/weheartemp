package tw.azul.wehear.user.family


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import tw.azul.wehear.MainActivity
import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.net.model.family.FamilyEntity


/**
 *
 * 編輯家人
 *
 */
class EditFamilyFragment : Fragment() {

    val USER_ID = AccountRoom.TokenLiveData.value?.userId!!

    lateinit var etNickname: EditText
    lateinit var spinnerRelation: Spinner
    lateinit var btnCheck: Button

    lateinit var familyViewModel: FamilyViewModel

    var currentEntity: FamilyEntity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?  = inflater.inflate(R.layout.fragment_edit_family, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? MainActivity)?.showToolbarAndHideNavigation()
        //  依狀態處理，會需要隱藏返回鍵
        setHasOptionsMenu(true)
        familyViewModel = ViewModelProviders.of(this).get(FamilyViewModel::class.java)

        // 取得傳遞的資料
        currentEntity = arguments?.getString("edit_family")
            .let {
                Gson().fromJson(it, FamilyEntity::class.java)
            }.apply {
                etNickname.setText(fs_nickname)
                spinnerRelation.setSelection(--fts_id)
            }
    }

    fun initView(view: View) {
        etNickname = view.findViewById(R.id.et_nickname)
        spinnerRelation = view.findViewById(R.id.spinner_relation)
        btnCheck = view.findViewById(R.id.btn_check)

        btnCheck.setOnClickListener {
            currentEntity?.run {
                updateFamily(this)
            }
        }
    }

    // 更新資料
    fun updateFamily(entity: FamilyEntity) {
        var fts_id = spinnerRelation.selectedItemPosition + 1
        var fs_nickname = etNickname.text.toString()
        familyViewModel.modifyFamily(USER_ID, entity.user_id, fts_id, fs_nickname, 1).observe(this, Observer {
            it?.isSuccess?.also { isSuccess ->
                if (isSuccess) {
                    //android.widget.Toast.makeText(context, "修改成功", android.widget.Toast.LENGTH_SHORT).show()
                    NavHostFragment.findNavController(this).navigate(R.id.action_editFamilyFragment_to_myFamilyFg)
                } else {
                    // 失敗
                    //android.widget.Toast.makeText(context, "修改失敗", android.widget.Toast.LENGTH_SHORT).show()
                    view?.apply {
                        Snackbar.make(this, "修改失敗", Snackbar.LENGTH_INDEFINITE).apply {
                            setActionTextColor(ResourcesCompat.getColor(resources, R.color.colorAccent, null))
                            setAction("知道了") { dismiss() }
                            show()
                        }
                    }
                }
            }
        })
    }

} // class close
