package tw.azul.wehear.user.family

import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import tw.azul.wehear.R
import com.gms.widget.layout.component.CustomSeekBar
import com.google.gson.Gson
import tw.azul.wehear.MainActivity
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.family.FamilyBaseVO
import tw.azul.wehear.net.model.family.FamilyEntity
import kotlin.collections.ArrayList

/**
 * 我的家人
 * 檢視模式 & 編輯模式
 */
class MyFamilyFragment : Fragment() {

    companion object ModelType {
        // 檢視模式
        val MODEL_VIEW_TYPE = View.GONE
        // 編輯模式
        val MODEL_EDIT_TYPE = View.VISIBLE

        /**
         * value 秒數
         */
        fun secondToHMS(value: Int): String {
            var value = value
            val second = (value % 60)
            val minute = (value / 60) % 60
            val hour = (value / 3600) % 24
            return String.format(
                "%02d:%02d:%02d",
                hour,
                minute,
                second
            )
        }
    }

    val USER_ID = AccountRoom.TokenLiveData.value?.userId!!

    lateinit var mRecyclerView: RecyclerView
    lateinit var btnAdd: Button

    lateinit var defaultView: View
    lateinit var familyView: View
    lateinit var btnAddFamily: Button

    var progressDig: AlertDialog? = null

    var userAdapter = UserAdapter()

    var familyList: List<FamilyBaseVO> = arrayListOf()

    lateinit var familyViewModel: FamilyViewModel

    // 是否顯示OptionsMenu -> 沒有資料時 or 只有請求時
    private var isShowOptionsMenu = false

    // 編輯模式
    var viewType: Int = MODEL_VIEW_TYPE

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_my_family, container, false)
        initView(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? MainActivity)?.showToolbarAndHideNavigation()
        familyViewModel = ViewModelProviders.of(this).get(FamilyViewModel::class.java)
        setModelType(viewType)
        familyViewModel.getFamilys(USER_ID).observe(this, Observer {
            if (it.status != Status.LOADING) {
                progressDig?.dismiss()
                it.data?.apply {
                    setFamilys(it.data)
                }?:kotlin.run {
                    setFamilys(emptyList())
                }
            } else {
                if (progressDig == null) {
                    var builder = AlertDialog.Builder(context)
                    builder.setMessage("請稍候")
                    progressDig = builder.create()
                }
                progressDig?.show()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        if (!isShowOptionsMenu)
            return
        if (viewType == MODEL_VIEW_TYPE)
            inflater?.inflate(R.menu.menu_band_setting, menu)
        else if (viewType == MODEL_EDIT_TYPE)
            inflater?.inflate(R.menu.menu_edit_finish, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item?.itemId) {
            R.id.action_edit -> {
                setModelType(MODEL_EDIT_TYPE)
                activity?.invalidateOptionsMenu()
                true
            }
            R.id.action_finish -> {
                setModelType(MODEL_VIEW_TYPE)
                activity?.invalidateOptionsMenu()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun initView(view: View) {

        defaultView = view.findViewById(R.id.layout_empty_family)
        familyView = view.findViewById(R.id.layout_family)
        btnAddFamily = defaultView.findViewById(R.id.btn_add_family)

        mRecyclerView = familyView.findViewById(R.id.recyclerView)
        mRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        mRecyclerView.adapter = userAdapter

        btnAdd = familyView.findViewById(R.id.btn_add_family)

        btnAddFamily.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_myFamilyFg_to_searchFamilyFragment)
        }
        btnAdd.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_myFamilyFg_to_searchFamilyFragment)
        }
    }

    // 設定目前的模式
    fun setModelType(type: Int) {
        viewType = type
        userAdapter.setModelType(type)
        when (type) {
            MODEL_VIEW_TYPE -> {
                btnAdd.visibility = View.VISIBLE
                (activity as MainActivity).setToolbarOptions(true , "我的家人")
            }
            MODEL_EDIT_TYPE -> {
                btnAdd.visibility = View.GONE
                (activity as MainActivity).setToolbarOptions(false , "編輯我的家人")
            }
        }
    }


    // 刪除家人
    fun deleteFamily(entity: FamilyEntity) {
        var builder: AlertDialog.Builder = AlertDialog.Builder(context , R.style.Theme_MaterialComponents_Light_Dialog_Alert)
        builder.setTitle(
            context?.resources?.getString(
                R.string.msg_delete_family_title,
                entity.fts_name,
                entity.user_name
            )
        )
        builder.setMessage(context?.resources?.getString(R.string.msg_delete_family_content))
        builder.setPositiveButton(
            context?.resources?.getString(R.string.btn_delete)
        ) { dialog, which ->
            //            updateFamilyPage(entity, true)
            familyViewModel.modifyFamily(USER_ID, entity.user_id, entity.fts_id, entity.fs_nickname, 0)
                .observe(this, Observer {
                    it?.isSuccess?.also { isSuccess ->
                        if (isSuccess) {
                            updateFamilyPage(entity, true)
                        } else {
                            // 失敗
                        }
                    }
                })
        }
        builder.setNegativeButton(
            context?.resources?.getString(R.string.btn_cancel), { dialog, which -> })

        var dialog = builder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            .setTextColor(context?.resources?.getColor(R.color.azul_blue_1)!!)
        dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            .setBackgroundColor(Color.TRANSPARENT)
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
            .setTextColor(context?.resources?.getColor(R.color.azul_blue_1)!!)
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
            .setBackgroundColor(Color.TRANSPARENT)
    }

    // 拒絕家人
    fun rejectFamily(entity: FamilyEntity) {
//        updateFamilyPage(entity, true)
        familyViewModel.modifyFamily(USER_ID, entity.user_id, entity.fts_id, entity.fs_nickname, 0)
            .observe(this, Observer {
                it?.isSuccess?.also { isSuccess ->
                    if (isSuccess) {
                        updateFamilyPage(entity, true)
                    } else {
                        // 失敗
                    }
                }
            })
    }

    // 接受家人
    fun acceptFamily(entity: FamilyEntity) {
//        updateFamilyPage(entity, false)
        familyViewModel.modifyFamily(USER_ID, entity.user_id, entity.fts_id, entity.fs_nickname, 1)
            .observe(this,
                Observer {
                    it?.isSuccess?.also { isSuccess ->
                        if (isSuccess) {
                            goEditFamilyPage(entity)
                        } else {
                            // 失敗
                        }
                    }
                })
    }

    // 編輯家人
    fun goEditFamilyPage(entity: FamilyEntity) {
        NavHostFragment.findNavController(this).navigate(R.id.action_myFamilyFg_to_editFamilyFragment
            , Bundle()
                .apply {
                    putString("edit_family", Gson().toJson(entity))
                })
    }

    // 刷新資料
    fun updateFamilyPage(entity: FamilyEntity, isDelete: Boolean) {
        var list = userAdapter.source.toList()
        if (isDelete) {
            list = list.filter { it != entity }
        } else {
            // 同意
            list.find { it == entity }?.also {
                (it as FamilyEntity).fs_accept = 1
            }
        }

        // 檢查並清除Header
        (list as ArrayList)?.takeIf {
            it.size == 1 && it.get(0).getType() == FamilyBaseVO.HEADER_TYPE
        }?.run {
            this.clear()
        }

        if (list == null || list.isEmpty()) {
            familyList = list
            familyView.visibility = View.INVISIBLE
            defaultView.visibility = View.VISIBLE
            setModelType(MODEL_VIEW_TYPE)
        }

        if (list.any { it.getType() == FamilyBaseVO.REQUEST_ADD_IN }) {
            if (list.filter { it.getType() == FamilyBaseVO.HEADER_TYPE }.isEmpty())
                (list as ArrayList).add(FamilyBaseVO.createEmptyUser())
        } else {
            (list as ArrayList).removeAll { it.getType() == FamilyBaseVO.HEADER_TYPE }
        }
        userAdapter.submitList(list.sortedByDescending { it.getType() })

        (list.filter {
            it.getType() == FamilyBaseVO.USER_FAMILY || it.getType() == FamilyBaseVO.USER_DEAF
        }.size > 0).apply {
            if ( this != isShowOptionsMenu){
                isShowOptionsMenu = this
                activity?.invalidateOptionsMenu()
            }
            if (!this){
                // 沒有家人資料
                (activity as MainActivity).setToolbarOptions(true , "我的家人")
            }
        }

    }

    fun setFamilys(data: List<FamilyBaseVO>) {

        var newData = ArrayList<FamilyBaseVO>()
        newData.addAll(data)

        // region--- 測試用資料 region ---
//        newData.clear()
//        for (i in 0..2) {
//            newData.add(FamilyEntity().apply {
//                user_name = "Name $i"
//                user_phone = "0910 000 0$i"
//                fs_accept = i % 2
//            })
//        }
//        newData.get(0)?.setType(FamilyBaseVO.USER_DEAF)
//        newData.get(3)?.setType(FamilyBaseVO.REQUEST_ADD_IN)
//        newData.get(4)?.setType(FamilyBaseVO.REQUEST_ADD_IN)

        // endregion--- 測試用資料 ---

        if (newData == null || newData.isEmpty()) {
            familyView.visibility = View.INVISIBLE
            defaultView.visibility = View.VISIBLE
            activity?.invalidateOptionsMenu()
            return;
        }
        familyView.visibility = View.VISIBLE
        defaultView.visibility = View.INVISIBLE

        var list = ArrayList<FamilyBaseVO>()
        list.addAll(newData)

        if (list.any { it.getType() == FamilyBaseVO.REQUEST_ADD_IN }) {
            if (list.filter { it.getType() == FamilyBaseVO.HEADER_TYPE }.isEmpty())
                list.add(FamilyBaseVO.createEmptyUser())
        } else {
            list.removeAll { it.getType() == FamilyBaseVO.HEADER_TYPE }
        }
        userAdapter.submitList(list.sortedByDescending { it.getType() })

        (list.filter { it.getType() == FamilyBaseVO.USER_FAMILY || it.getType() == FamilyBaseVO.USER_DEAF }.size > 0)
            .apply {
                if (this != isShowOptionsMenu) {
                    isShowOptionsMenu = this
                    activity?.invalidateOptionsMenu()
                }
            }
    }

    // 標記為內部類別，即可直接放問外部方法
    inner class UserAdapter : ListAdapter<FamilyBaseVO, RecyclerView.ViewHolder>(DiffCallback()) {

        var type = MODEL_VIEW_TYPE

        lateinit var source: List<FamilyBaseVO>

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            if (viewType == FamilyBaseVO.USER_FAMILY) {
                var view = LayoutInflater.from(parent.context).inflate(R.layout.item_family, parent, false)
                return ViewHolder(view)
            } else if (viewType == FamilyBaseVO.USER_DEAF) {
                var inflater = LayoutInflater.from(parent.context)
                return DeafViewHolder(inflater.inflate(R.layout.item_family_deaf, parent, false))
            } else if (viewType == FamilyBaseVO.REQUEST_ADD_IN) {
                var inflater = LayoutInflater.from(parent.context)
                return RequestViewHolder(inflater.inflate(R.layout.item_family_add_request, parent, false))
            } else if (viewType == FamilyBaseVO.HEADER_TYPE) {
                var inflater = LayoutInflater.from(parent.context)
                return RequestHeaderViewHolder(inflater.inflate(R.layout.item_family_add_request_header, parent, false))
            } else {
                var inflater = LayoutInflater.from(parent.context)
                return ViewHolder(inflater.inflate(R.layout.item_family, parent, false))
            }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if (holder is ViewHolder) {
                holder.bind(getItem(position) as FamilyEntity)
                holder.type(type)
            } else if (holder is DeafViewHolder) {
                holder.bind(getItem(position) as FamilyEntity)
                holder.type(type)
            } else if (holder is RequestViewHolder) {
                holder.bind(getItem(position) as FamilyEntity)
            }
        }

        // 類型
        override fun getItemViewType(position: Int): Int {
            return getItem(position).getType()
        }

        override fun submitList(list: List<FamilyBaseVO>?) {
            source = list!!
            familyList = list!!
            activity?.invalidateOptionsMenu()
            super.submitList(list)
        }

        fun setModelType(type: Int) {
            this.type = type
            notifyDataSetChanged()
        }


        // 家人(一般狀態)
        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(user: FamilyEntity) {
                var _name =
                    "${user.fts_name} ${user.user_name} ${if(user.fs_nickname.isEmpty()) "" else "(${user.fs_nickname})"}"
                itemView.findViewById<TextView>(R.id.tv_name).text = _name
                itemView.findViewById<TextView>(R.id.tv_phone).text = user.user_phone
                itemView.findViewById<ImageButton>(R.id.ibtn_delete).setOnClickListener { deleteFamily(user) }
                itemView.setOnClickListener {
                    if (itemView.findViewById<ImageButton>(R.id.ibtn_delete).visibility == View.VISIBLE)
                        goEditFamilyPage(user)
                }
            }

            fun type(type: Int) {
                itemView.findViewById<ImageButton>(R.id.ibtn_delete).visibility = type
            }
        }

        // 身障者
        inner class DeafViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(user: FamilyEntity) {
                var _name =
                    "${user.fts_name} ${user.user_name} ${if(user.fs_nickname.isEmpty()) "" else "(${user.fs_nickname})"}"
                itemView.findViewById<TextView>(R.id.tv_name).text = _name
                itemView.findViewById<TextView>(R.id.tv_phone).text = user.user_phone
                itemView.findViewById<TextView>(R.id.tv_walk_count).text = user.step_count.toString()
                itemView.findViewById<TextView>(R.id.tv_walk_target).text = "目標${user.user_step_goal}步"
                itemView.findViewById<CustomSeekBar>(R.id.seek_bar_walk).progress =
                        (1.0f * user.step_count / user.user_step_goal * 100).toInt()
                var totalSleep = secondToHMS(user.total_sleep).split(":")
                itemView.findViewById<TextView>(R.id.tv_hour).text = totalSleep[0]
                itemView.findViewById<TextView>(R.id.tv_minutes).text = totalSleep[1];

                var deepSleep = secondToHMS(user.deep_sleep).split(":")
                itemView.findViewById<TextView>(R.id.tv_deep_sleep).text = "${deepSleep[0]}小時${deepSleep[1]}分"

                var lightSleep = secondToHMS(user.light_sleep).split(":")
                itemView.findViewById<TextView>(R.id.tv_light_sleep).text = "${lightSleep[0]}小時${lightSleep[1]}分"
                itemView.findViewById<ImageButton>(R.id.ibtn_delete).setOnClickListener { deleteFamily(user) }
                itemView.setOnClickListener {
                    // 顯示身障者的健康狀況
                    when(viewType){
                        MODEL_VIEW_TYPE -> {
                            NavHostFragment.findNavController(this@MyFamilyFragment)
                                .navigate(R.id.action_myFamilyFg_to_deafRecordFragment
                                    , Bundle().apply {
                                        putInt("user_id", user.user_id)
                                        putString("user_name", user.user_name)
                                        putString("user_fts_name", user.fts_name)
                                    }
                                )
                        }
                        MODEL_EDIT_TYPE ->{
                            goEditFamilyPage(user)
                        }
                    }
                }
            }

            fun type(type: Int) {
                itemView.findViewById<ImageButton>(R.id.ibtn_delete).visibility = type
            }
        }

        // 新增、拒絕
        inner class RequestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(user: FamilyEntity) {
                itemView.findViewById<TextView>(R.id.tv_name).text = user.user_name
                itemView.findViewById<TextView>(R.id.tv_phone).text = user.user_phone

                itemView.findViewById<Button>(R.id.btn_accept).setOnClickListener {
                    acceptFamily(user)
                }
                itemView.findViewById<Button>(R.id.btn_reject).setOnClickListener {
                    rejectFamily(user)
                }
            }
        }

        inner class RequestHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    }

    class DiffCallback : DiffUtil.ItemCallback<FamilyBaseVO>() {
        override fun areItemsTheSame(oldItem: FamilyBaseVO, newItem: FamilyBaseVO): Boolean {
            if (oldItem is FamilyEntity && newItem is FamilyEntity)
                return (oldItem as FamilyEntity).user_id == (newItem as FamilyEntity).user_id
            else
                return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: FamilyBaseVO, newItem: FamilyBaseVO): Boolean {
            return oldItem == newItem
        }
    }


} // class close