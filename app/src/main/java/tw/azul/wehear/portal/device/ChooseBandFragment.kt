package tw.azul.wehear.portal.device

import android.bluetooth.BluetoothDevice
import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_portal_choose_band.*
import tw.azul.wehear.GotoMainActivityDelegate
import tw.azul.wehear.R
import tw.azul.wehear.databinding.FragmentPortalChooseBandBinding
import tw.azul.wehear.widget.MiddleDividerItemDecoration

class ChooseBandFragment : Fragment() {

    companion object {
        fun newInstance() = ChooseBandFragment()
    }

    private lateinit var dataBinding: FragmentPortalChooseBandBinding
    private lateinit var viewModel: ChooseBandViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentPortalChooseBandBinding.inflate(inflater)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ResourcesCompat.getDrawable(resources, R.drawable.divider_20dp, null)?.let {
            val middleDivider =
                MiddleDividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            middleDivider.setDrawable(it)
            bands_list.addItemDecoration(middleDivider)
        }
        bands_list.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }
        dataBinding.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ChooseBandViewModel::class.java)
        viewModel.delegate = GotoMainActivityDelegate(activity)
        viewModel.lifecycleOwner = viewLifecycleOwner
        dataBinding.viewModel = viewModel
    }

    private lateinit var observer:Observer<Boolean>

    override fun onResume() {
        super.onResume()
        observer = object: Observer<Boolean> {
            override fun onChanged(bleStatus: Boolean) {
                viewModel.bleStatusDelObserver(this)
                if (bleStatus) {
                    viewModel.bandListLiveData.observe(viewLifecycleOwner, Observer {
                        bands_list.adapter = BandsAdapter(it, viewModel)
                    })
                }
            }
        }
        viewModel.bleStatusAddObserver(viewLifecycleOwner, observer)
    }

    override fun onPause() {
        super.onPause()
        if (::observer.isInitialized) {
            viewModel.bleStatusDelObserver(observer)
            viewModel.bandListLiveData.removeObservers(viewLifecycleOwner)
        }
    }

    class BandsAdapter(val list: List<BluetoothDevice>, val viewmodel:ChooseBandViewModel) : RecyclerView.Adapter<BandViewHolder>() {

        private var nowCompoundButton: CompoundButton? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BandViewHolder =
            BandViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_choose_band,
                    parent,
                    false
                )
            ).apply {
                device_address.setOnCheckedChangeListener {newCompoundButton, isChecked ->
                    if (isChecked) {
                        nowCompoundButton?.isChecked = false
                        nowCompoundButton = newCompoundButton
                        viewmodel.setBluetoothDevice(list[adapterPosition])
                    }
                }
            }

        override fun onBindViewHolder(holder: BandViewHolder, position: Int) {
            holder.apply {
                list[position].let {
                    device_name.text = it.name
                    device_address.text = it.address
                }
            }
        }

        override fun getItemCount(): Int = list.size
    }

    class BandViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val res: Resources = itemView.resources
        val device_name: TextView = itemView.findViewById(R.id.device_name)
        val device_address: RadioButton = itemView.findViewById(R.id.device_address)
    }
}