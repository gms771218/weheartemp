@file:Suppress("FunctionName", "LocalVariableName")

package tw.azul.wehear.api

import android.net.Uri
import androidx.annotation.WorkerThread
import okhttp3.MediaType
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import tw.azul.wehear.BuildConfig
import tw.azul.wehear.account.AccountRoom
import kotlin.concurrent.thread

class WebAPI private constructor() {

    companion object {
        val Instance = WebAPI()
        private val BaseUri = Uri.parse(BuildConfig.BASE_URL)
        private val JSON    = MediaType.parse("application/json; charset=utf-8")
        private val Gson    = com.google.gson.Gson()
        private val JsonBodyFromMap: (Map<*, *>) -> RequestBody = Gson.run {
            {
                JsonBodyFromString(toJson(it))
            }
        }
        private val JsonBodyFromString: (String) -> RequestBody = {
            RequestBody.create(JSON, it)
        }
        private val OkHttpClient = okhttp3.OkHttpClient()
        private val CallAPI: (Request) -> Response = OkHttpClient.run {
            {
                newCall(it).execute()
            }
        }
        private val Parse: (String) -> ResponseBase = Gson.run {
            {
                fromJson<ResponseBase>(it, ResponseBase::class.java)
            }
        }
        var Executor:java.util.concurrent.Executor? = null
    }

    private data class ResponseBase(val returnCode: Int, val returnMessage: String, val data: Any? = null)

    sealed class ResponseType {
        data class Success(val data: Any? = null) : ResponseType()
        data class Failure(val code: Int  = 5000, val errMsg: String = "unknown") : ResponseType()
    }

    /**
     * 當 API 為 POST 時, 回傳預載 user_id 的 MutableMap
     */
    fun preparePOST(vararg p: Pair<String, Any?>): Map<String, Any?> =
        mutableMapOf("_" to any(), "user_id" to AccountRoom.TokenLiveData.value?.user_id).run { p.toMap(this) }
    private fun any(): Any? = run { "%.6f".format(Math.random()).toFloat() }

    /**
     * 登入
     */
    @WorkerThread
    fun SignUp(json: String): ResponseType = try {
        BaseUri
            .uponBuilder {
                appendEncodedPath("user/phone/signUp")
            }
            .newRequestBuilder {
                post(JsonBodyFromString(json))
            }
            .toCall {
                body()?.string()?.takeIf {
                    it.isNotEmpty()
                }?.run {
                    android.util.Log.v("Faty", "登入: $this")
                    Parse(this).run {
                        if (returnCode == 1000) {
                            ResponseType.Success()
                        } else {
                            ResponseType.Failure(returnCode, returnMessage)
                        }
                    }
                }?:run {
                    ResponseType.Failure()
                }
            }
    } catch (e: Throwable) {
        e.printStackTrace()
        ResponseType.Failure(errMsg = e.localizedMessage)
    }

    /**
     * 登出 (刪除使用者所有裝置)
     */
    @WorkerThread
    fun SignOut(): ResponseType = try {
        val userData = AccountRoom.TokenLiveData.value
        val user_id  = (userData?.user_id?: "").toString()
        val token    = (userData?.token  ?: "").toString()
        BaseUri
            .uponBuilder {
                appendEncodedPath("device/user")
                appendEncodedPath(user_id)
            }
            .newRequestBuilder {
                addHeader("token", token)
                delete()
            }
            .toCall {
                body()?.string()?.takeIf {
                    it.isNotEmpty()
                }?.run {
                    android.util.Log.v("Faty", "登出: $this")
                    Parse(this).run {
                        if (returnCode == 1000) {
                            ResponseType.Success()
                        } else {
                            ResponseType.Failure(returnCode, returnMessage)
                        }
                    }
                }?:run {
                    ResponseType.Failure()
                }
            }
    } catch (e: Throwable) {
        e.printStackTrace()
        ResponseType.Failure(errMsg = e.localizedMessage)
    }

    /**
     * 新增裝置
     */
    @WorkerThread
    fun DeviceAdd(jsonMap: Map<*, *>): ResponseType = try {
        val userData = AccountRoom.TokenLiveData.value
        val token    = (userData?.token  ?: "").toString()
        BaseUri
            .uponBuilder{
                appendEncodedPath("device")
            }
            .newRequestBuilder {
                addHeader("token", token)
                post(JsonBodyFromMap(jsonMap))
            }
            .toCall {
                body()?.string()?.takeIf {
                    it.isNotEmpty()
                }?.run {
                    android.util.Log.v("Faty", "新增裝置: $this")
                    Parse(this).run {
                        if (returnCode == 1000) {
                            ResponseType.Success(data)
                        } else {
                            ResponseType.Failure(returnCode, returnMessage)
                        }
                    }
                }?:run {
                    ResponseType.Failure()
                }
            }
    } catch (e: Throwable) {
        e.printStackTrace()
        ResponseType.Failure(errMsg = e.localizedMessage)
    }

    /**
     * 修改裝置
     */
    @WorkerThread
    fun DeviceModify(device_id: String, jsonMap: Map<*, *>): ResponseType = try {
        val userData = AccountRoom.TokenLiveData.value
        val token    = (userData?.token  ?: "").toString()
        BaseUri
            .uponBuilder{
                appendEncodedPath("device")
                appendEncodedPath(device_id)
            }
            .newRequestBuilder {
                addHeader("token", token)
                put(JsonBodyFromMap(jsonMap))
            }
            .toCall {
                body()?.string()?.takeIf {
                    it.isNotEmpty()
                }?.run {
                    android.util.Log.v("Faty", "修改裝置: $this")
                    Parse(this).run {
                        if (returnCode == 1000) {
                            ResponseType.Success(data)
                        } else {
                            ResponseType.Failure(returnCode, returnMessage)
                        }
                    }
                }?:run {
                    ResponseType.Failure()
                }
            }
    } catch (e: Throwable) {
        e.printStackTrace()
        ResponseType.Failure(errMsg = e.localizedMessage)
    }

    /**
     * 移除裝置 (單一)
     */
    @WorkerThread
    fun DeviceRemove(device_id: String): ResponseType = try {
        val userData = AccountRoom.TokenLiveData.value
        val token    = (userData?.token  ?: "").toString()
        BaseUri
            .uponBuilder{
                appendEncodedPath("device")
                appendEncodedPath(device_id)
            }
            .newRequestBuilder {
                addHeader("token", token)
                delete()
            }
            .toCall {
                body()?.string()?.takeIf {
                    it.isNotEmpty()
                }?.run {
                    android.util.Log.v("Faty", "移除裝置: $this")
                    Parse(this).run {
                        if (returnCode == 1000) {
                            ResponseType.Success(data)
                        } else {
                            ResponseType.Failure(returnCode, returnMessage)
                        }
                    }
                }?:run {
                    ResponseType.Failure()
                }
            }
    } catch (e: Throwable) {
        e.printStackTrace()
        ResponseType.Failure(errMsg = e.localizedMessage)
    }

    /**
     * 裝置狀態 (主機四個附件)
     */
    @WorkerThread
    fun DeviceStatus(): ResponseType = try {
        val userData = AccountRoom.TokenLiveData.value
        val user_id  = (userData?.user_id?: "").toString()
        val token    = (userData?.token  ?: "").toString()
        BaseUri
            .uponBuilder{
                appendEncodedPath("device/status")
                appendQueryParameter("user_id", user_id)
            }
            .newRequestBuilder {
                addHeader("token", token)
            }
            .toCall {
                body()?.string()?.takeIf {
                    it.isNotEmpty()
                }?.run {
                    android.util.Log.v("Faty", "裝置狀態(四個附件): $this")
                    Parse(this).run {
                        if (returnCode == 1000) {
                            ResponseType.Success(data)
                        } else {
                            ResponseType.Failure(returnCode, returnMessage)
                        }
                    }
                }?:run {
                    ResponseType.Failure()
                }
            }
    } catch (e: Throwable) {
        e.printStackTrace()
        ResponseType.Failure(errMsg = e.localizedMessage)
    }

    /**
     * 使用者配件狀態 (Band 和 Host)
     */
    @WorkerThread
    fun UserDevices(): ResponseType = try {
        val userData = AccountRoom.TokenLiveData.value
        val user_id  = (userData?.user_id?: "-1").toString() // 不能空字串, 否則會回傳所有人裝置狀態
        val token    = (userData?.token  ?: "-1").toString()
        BaseUri
            .uponBuilder{
                appendEncodedPath("devices")
                appendQueryParameter("user_id", user_id)
            }
            .newRequestBuilder {
                addHeader("token", token)
            }
            .toCall {
                body()?.string()?.takeIf {
                    it.isNotEmpty()
                }?.run {
                    android.util.Log.v("Faty", "使用者配件狀態(Band和Host): $this")
                    Parse(this).run {
                        if (returnCode == 1000) {
                            ResponseType.Success(data)
                        } else {
                            ResponseType.Failure(returnCode, returnMessage)
                        }
                    }
                }?:run {
                    ResponseType.Failure()
                }
            }
    } catch (e: Throwable) {
        e.printStackTrace()
        ResponseType.Failure(errMsg = e.localizedMessage)
    }

    /**
     * 新增修改通知
     */
    @WorkerThread
    fun Notification(jsonMap: Map<*, *>): ResponseType = try {
        val userData = AccountRoom.TokenLiveData.value
        val token    = (userData?.token  ?: "").toString()
        BaseUri
            .uponBuilder{
                appendEncodedPath("notification")
            }
            .newRequestBuilder {
                addHeader("token", token)
                post(JsonBodyFromMap(jsonMap))
            }
            .toCall {
                body()?.string()?.takeIf {
                    it.isNotEmpty()
                }?.run {
                    android.util.Log.v("Faty", "新增修改通知: $this")
                    Parse(this).run {
                        if (returnCode == 1000) {
                            ResponseType.Success(data)
                        } else {
                            ResponseType.Failure(returnCode, returnMessage)
                        }
                    }
                }?:run {
                    ResponseType.Failure()
                }
            }
    } catch (e: Throwable) {
        e.printStackTrace()
        ResponseType.Failure(errMsg = e.localizedMessage)
    }

    /**
     * App 發送通知
     */
    @WorkerThread
    fun NotificationEmit(nts_id: Int, str_hex: String) = try {
        val userData = AccountRoom.TokenLiveData.value
        val user_id  = (userData?.user_id?: "").toString()
        val token    = (userData?.token  ?: "").toString()
        BaseUri
            .uponBuilder{
                appendEncodedPath("notification/app")
                appendQueryParameter("user_id", user_id)
                appendQueryParameter("nts_id", nts_id.toString())
                appendQueryParameter("str_hex", str_hex)
            }
            .newRequestBuilder {
                addHeader("token", token)
            }
            .toCall {
                body()?.string()?.takeIf {
                    it.isNotEmpty()
                }?.run {
                    android.util.Log.v("Faty", "App 發送通知: $this")
                    Parse(this).run {
                        if (returnCode == 1000) {
                            ResponseType.Success(data)
                        } else {
                            ResponseType.Failure(returnCode, returnMessage)
                        }
                    }
                }?:run {
                    ResponseType.Failure()
                }
            }
    } catch (e: Throwable) {
        e.printStackTrace()
        ResponseType.Failure(errMsg = e.localizedMessage)
    }

    /**
     * 新增步數紀錄
     */
    @Deprecated(message = "使用 DailyRepository.postStepRecord (謝謝GMS)")
    @WorkerThread
    fun Steps(): ResponseType = try {
        val userData = AccountRoom.TokenLiveData.value
        val token    = (userData?.token  ?: "").toString()
        BaseUri
            .uponBuilder{
                appendEncodedPath("steps")
            }
            .newRequestBuilder {
                addHeader("token", token)
            }
            .toCall {
                body()?.string()?.takeIf {
                    it.isNotEmpty()
                }?.run {
                    android.util.Log.v("Faty", "新增步數紀錄: $this")
                    Parse(this).run {
                        if (returnCode == 1000) {
                            ResponseType.Success(data)
                        } else {
                            ResponseType.Failure(returnCode, returnMessage)
                        }
                    }
                }?:run {
                    ResponseType.Failure()
                }
            }
    } catch (e: Throwable) {
        e.printStackTrace()
        ResponseType.Failure(errMsg = e.localizedMessage)
    }


    /********************************************************************************
     * Kotlin Extension used by Self
     ********************************************************************************/
    private fun Uri.uponBuilder(block: (Uri.Builder.() -> Uri.Builder)? = null): Uri.Builder =
        buildUpon().run { if (block == null) this else block(this) }
    private fun Uri.Builder.newRequestBuilder(block: (Request.Builder.() -> Request.Builder)? = null): Request.Builder =
        Request.Builder().url(toString()).run { if (block == null) this else block(this) }
    private fun Request.Builder.toCall(block: Response.() -> ResponseType): ResponseType = CallAPI(build()).use(block)
}

fun WebThread(runnable: Runnable) = WebAPI.Executor?.execute(runnable) ?: thread(block = runnable::run)
fun WebThread(command: () -> Any) = WebThread(Runnable { command() })