package tw.azul.wehear.user.nofitication

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Switch
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.work.*
import com.gms.widget.layout.R
import com.google.gson.Gson
import tw.azul.wehear.MainActivity
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.notification.NotificationEntity
import tw.azul.wehear.net.model.request.RequestUserNotificationEntity
import tw.azul.wehear.net.worker.UpdateNotificationWorker
import java.util.ArrayList

/**
 * 通知提醒 - 家人
 * 045_發布警示_家人
 */
class SettingNotificationFamilyFragment : Fragment() {

    val USER_ID = AccountRoom.TokenLiveData.value?.userId!!

    val NOTIFY_TYPE_HEART = 11;
    val NOTIFY_TYPE_OXYGEN = 12;
    val NOTIFY_TYPE_SLEEP = 13;
    val NOTIFY_TYPE_WALK = 14;

    // 睡眠通知時間
    val sleepNotificationHour = arrayOf(9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21)
    // 走路通知時間
    val walkNotificationHour = arrayOf(20, 21, 22, 23, 0)


    lateinit var switchHeart: Switch
    lateinit var switchOxygen: Switch
    lateinit var switchSleep: Switch
    lateinit var switchWalk: Switch

    lateinit var spinnerSleep: Spinner
    lateinit var spinnerWalk: Spinner

    var progressDig: AlertDialog? = null

    lateinit var notificationViewModel: NotificationViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.layout_notify_family, container, false)
        initView(view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? MainActivity)?.showToolbarAndHideNavigation()

        notificationViewModel = ViewModelProviders.of(this).get(NotificationViewModel::class.java)

        notificationViewModel.getUserNotification(USER_ID).observe(this, Observer {
            if (it.status != Status.LOADING) {
                progressDig?.dismiss()
                setUserNotification(it?.data)
            } else {
                if (progressDig == null) {
                    var builder = AlertDialog.Builder(context)
                    builder.setMessage("請稍候")
                    progressDig = builder.create()
                }
                progressDig?.show()
            }
        })
    }

    fun initView(view: View) {
        switchHeart = view.findViewById(R.id.switch_heart)
        switchOxygen = view.findViewById(R.id.switch_oxygen)
        switchSleep = view.findViewById(R.id.switch_sleep)
        switchWalk = view.findViewById(R.id.switch_walk)

        spinnerSleep = view.findViewById(R.id.spinner_sleep_hour)
        spinnerWalk = view.findViewById(R.id.spinner_walk_hour)
    }

    fun setListener() {

        // --- switch ---
        switchHeart.setOnCheckedChangeListener { buttonView, isChecked ->
            uploadUserNotification()
        }
        switchOxygen.setOnCheckedChangeListener { buttonView, isChecked ->
            uploadUserNotification()
        }
        switchSleep.setOnCheckedChangeListener { buttonView, isChecked ->
            uploadUserNotification()
        }
        switchWalk.setOnCheckedChangeListener { buttonView, isChecked ->
            uploadUserNotification()
        }

        // --- spinner ---
        spinnerSleep.onItemSelectedListener = SelectedListener

        spinnerWalk.onItemSelectedListener = SelectedListener

    }


    fun setUserNotification(list: List<NotificationEntity>?) {
        // 心率
        list?.find { it.nts_id == NOTIFY_TYPE_HEART }?.run {
            setHeartNotification(this)
        }

        // 血氧
        list?.find { it.nts_id == NOTIFY_TYPE_OXYGEN }?.run {
            setOxygenNotification(this)
        }

        // 睡眠
        list?.find { it.nts_id == NOTIFY_TYPE_SLEEP }?.run {
            setSleepNotification(this)
        }

        // 步行
        list?.find { it.nts_id == NOTIFY_TYPE_WALK }?.run {
            setWalkNotification(this)
        }
        setListener()
    }

    /**
     * 設定心率通知
     */
    fun setHeartNotification(entity: NotificationEntity) {
        switchHeart.isChecked = entity.ns_enable == 1
    }

    /**
     * 設定有氧通知
     */
    fun setOxygenNotification(entity: NotificationEntity) {
        switchOxygen.isChecked = entity.ns_enable == 1
    }

    /**
     * 設定睡眠通知
     */
    fun setSleepNotification(entity: NotificationEntity) {
        switchSleep.isChecked = entity.ns_enable == 1
        // 在每日N時
        sleepNotificationHour.indexOf(entity.ns_hour).also {
            spinnerSleep.setSelection(if (it == -1) 0 else it)
        }
    }

    /**
     * 設定步行通知
     */
    fun setWalkNotification(entity: NotificationEntity) {
        switchWalk.isChecked = entity.ns_enable == 1
        walkNotificationHour.indexOf(entity.ns_hour).also {
            spinnerWalk.setSelection(if (it == -1) 0 else it)
        }
    }


    /**
     * 上傳通知設定
     */
    fun uploadUserNotification() {
        var list: ArrayList<RequestUserNotificationEntity> = ArrayList<RequestUserNotificationEntity>()
        with(list) {
            add(RequestUserNotificationEntity(USER_ID, NOTIFY_TYPE_HEART, switchHeart.isChecked))
            add(RequestUserNotificationEntity(USER_ID, NOTIFY_TYPE_OXYGEN, switchOxygen.isChecked))
            add(RequestUserNotificationEntity(USER_ID, NOTIFY_TYPE_SLEEP, switchSleep.isChecked).apply {
                // 需要轉換成秒數
                setNs_hour(sleepNotificationHour.get(spinnerSleep.selectedItemPosition))
            })
            add(RequestUserNotificationEntity(USER_ID, NOTIFY_TYPE_WALK, switchWalk.isChecked).apply {
                setNs_hour(walkNotificationHour.get(spinnerWalk.selectedItemPosition))
            })
        }

        val data = Data.Builder()
            .putString(
                UpdateNotificationWorker.DATA_KEY,
                Gson().toJson(list, ArrayList<RequestUserNotificationEntity>()::class.java)
            )
            .build()
        val constraints = Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
        val oneTimeWorkRequest = OneTimeWorkRequest.Builder(UpdateNotificationWorker::class.java!!)
            .setInputData(data)
            .setConstraints(constraints)
            .build()
        WorkManager.getInstance().enqueue(oneTimeWorkRequest)
    }

    // ==================== Inner Object ====================
    var SelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            if (parent == null) return;
            uploadUserNotification()
        }
    }

} // class close