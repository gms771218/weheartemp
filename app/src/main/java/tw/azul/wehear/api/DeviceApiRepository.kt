package tw.azul.wehear.api

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class DeviceApiRepository private constructor() {

    companion object {
        val Instance = DeviceApiRepository()
    }

    data class Status(val nts_id: Int, val is_connect: Int, val ns_enable: Int)
    sealed class Device(open val dt: DT? = null) {
        data class Success(override val dt: DT, val device_id: String): Device()
        data class Failure(override val dt: DT, val errCode: Int, val errMsg: String) : Device()
    }

    /**
     * 新增裝置
     */
    fun add(dt: DT, is_cover: String): LiveData<Device> {
        val liveData = MutableLiveData<Device>()
        WebThread {
            val jsonMap = when (dt) {
                is DT.WeHearHost -> WebAPI.Instance.preparePOST(
                    "dt_id" to dt.dt_id,
                    //"device_mac" to dt.device?.address,
                    "device_name" to dt.name,
                    "device_unique" to dt.hex,
                    "is_cover" to is_cover
                )
                is DT.WeHearWatch -> WebAPI.Instance.preparePOST(
                    "dt_id" to dt.dt_id,
                    "device_mac" to dt.device.address,
                    "device_name" to dt.name,
                    "is_cover" to is_cover
                )
                is DT.HX_SHOCK -> WebAPI.Instance.preparePOST(
                    "dt_id" to dt.dt_id,
                    //"device_mac" to dt.device?.address,
                    "device_name" to dt.name,
                    "device_unique" to dt.hex,
                    "is_cover" to is_cover
                )
            }
            val response: WebAPI.ResponseType = WebAPI.Instance.DeviceAdd(jsonMap)
            val result:Device = when(response) {
                is WebAPI.ResponseType.Success -> {
                    val data = response.data
                    try {
                        if (data is Map<*, *>) {
                            Device.Success(dt, (data["device_id"] as Double).toInt().toString())
                        } else {
                            throw Exception("不知名錯誤")
                        }
                    } catch (e:Throwable) {
                        e.printStackTrace()
                        Device.Failure(dt, errCode = 5000, errMsg = e.localizedMessage)
                    }
                }
                is WebAPI.ResponseType.Failure -> {
                    Device.Failure(dt, errCode = response.code, errMsg = response.errMsg)
                }
            }
            android.util.Log.i("Faty", "新增裝置 = $result")
            liveData.postValue(result)
        }
        return liveData
    }

    /**
     * 修改裝置
     */
    fun modify(device_id: String, vararg params: Pair<String, Any?>) : LiveData<WebAPI.ResponseType> {
        val liveData = MutableLiveData<WebAPI.ResponseType>()
        WebThread {
            val jsonMap = WebAPI.Instance.preparePOST(*params)
            val response: WebAPI.ResponseType = WebAPI.Instance.DeviceModify(device_id, jsonMap)
            android.util.Log.i("Faty", "修改裝置 = $response")
            liveData.postValue(response)
        }
        return liveData
    }

    /**
     * 移除裝置 (單一)
     */
    fun remove(device_id: String): LiveData<WebAPI.ResponseType> {
        val liveData = MutableLiveData<WebAPI.ResponseType>()
        WebThread {
            val response: WebAPI.ResponseType = WebAPI.Instance.DeviceRemove(device_id)
            android.util.Log.i("Faty", "移除裝置 = $response")
            liveData.postValue(response)
        }
        return liveData
    }

    /**
     * 裝置狀態 (主機四個附件)
     */
    fun status():LiveData<List<Status?>> {
        val liveData = MutableLiveData<List<Status?>>()
        WebThread {
            val response: WebAPI.ResponseType = WebAPI.Instance.DeviceStatus()
            val result = when(response) {
                is WebAPI.ResponseType.Success -> {
                    val data = response.data
                    try {
                        (data as List<*>).map{
                            if (it is Map<*, *>) {
                                Status(
                                    (it["nts_id"] as Double).toInt(),
                                    (it["is_connect"] as Double).toInt(),
                                    (it["ns_enable"] as Double).toInt()
                                )
                            } else {
                                null
                            }
                        }.takeIf {
                            it.all { it != null }
                        }
                    } catch (e:Throwable) {
                        e.printStackTrace()
                        null
                    }
                }
                is WebAPI.ResponseType.Failure -> {
                    null
                }
            }
            android.util.Log.i("Faty", "裝置狀態 = $result")
            liveData.postValue(result)
        }
        return liveData
    }

    /**
     * 使用者裝置 (Band 和 Host)
     */
    fun devices(): LiveData<WebAPI.ResponseType> {
        val liveData = MutableLiveData<WebAPI.ResponseType>()
        WebThread {
            val response: WebAPI.ResponseType = WebAPI.Instance.UserDevices()
            android.util.Log.i("Faty", "使用者裝置(Band和Host) = $response")
            liveData.postValue(response)
        }
        return liveData
    }

}

/**
 * 裝置類型列舉
 */
sealed class DT(val dt_id: Int, open val device: BluetoothDevice, open val name: String? = device.name) {
    /*
        WeHear/Host
     */
    data class WeHearHost(
        var hex: String? = null,
        override val device: BluetoothDevice,
        override val name: String = device.name ?: "WeHear"
    ) : DT(dt_id = 3, device = device)
    /*
        WeHearWatch/Watch
     */
    data class WeHearWatch(
        override val device: BluetoothDevice,
        override val name: String = device.name ?: "WeHearWatch"
    ) : DT(dt_id = 4, device = device)
    /*
        HX_SHOCK/Host
     */
    data class HX_SHOCK(
        var hex: String? = null,
        override val device: BluetoothDevice,
        override val name: String = device.name ?: "HX_SHOCK"
    ) : DT(dt_id = 5, device = device)
}