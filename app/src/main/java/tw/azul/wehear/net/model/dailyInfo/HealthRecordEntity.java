package tw.azul.wehear.net.model.dailyInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

/**
 * 健康紀錄資料
 */
public class HealthRecordEntity {


    @Expose
    @SerializedName("oximetry")
    public List<OximetryEntity> oximetry = Collections.emptyList();
    @Expose
    @SerializedName("heart_rates")
    public List<Heart_ratesEntity> heart_rates = Collections.emptyList();

    /**
     * 一週步數記錄陣列
     */
    @Expose
    @SerializedName("step_week")
    public List<Integer> step_week = Collections.emptyList();

    /**
     * 查詢星期
     */
    @Expose
    @SerializedName("weekday")
    public int weekday;

    /**
     * 最新更新時間
     */
    @Expose
    @SerializedName("latest_datetime")
    public String latest_datetime = "unknown";

    /**
     * 最高血氧
     */
    @Expose
    @SerializedName("highest_percent")
    public int highest_percent;

    /**
     * 平均心律
     */
    @Expose
    @SerializedName("hr_bpm_avg")
    public float hr_bpm_avg;

    /**
     * 淺睡紀錄持續持間(秒)
     */
    @Expose
    @SerializedName("sleep_duration_light")
    public int sleep_duration_light;

    /**
     * 深睡紀錄持續持間(秒)
     */
    @Expose
    @SerializedName("sleep_duration_deep")
    public int sleep_duration_deep;

    /**
     * 睡眠紀錄持續持間(秒)
     */
    @Expose
    @SerializedName("sleep_duration")
    public int sleep_duration;

    /**
     * 距離(公尺)
     */
    @Expose
    @SerializedName("step_distance")
    public int step_distance;

    /**
     * 步數紀錄持續時間(秒)
     */
    @Expose
    @SerializedName("step_moving_time")
    public int step_moving_time;

    /**
     * 步數紀錄卡路里
     */
    @Expose
    @SerializedName("step_calorie")
    public int step_calorie;

    /**
     * 步數
     */
    @Expose
    @SerializedName("step_count")
    public int step_count;

    /**
     * 使用者每天走路步數目標
     */
    @Expose
    @SerializedName("user_step_goal")
    public int user_step_goal;


    /**
     * 使用者id
     */
    @Expose
    @SerializedName("user_id")
    public int user_id;

    /**
     * 心率資料
     */
    public static class Heart_ratesEntity {

        /**
         * 心率ids
         */
        @Expose
        @SerializedName("hr_id")
        public int hr_id;

        /**
         * 心率
         */
        @Expose
        @SerializedName("hr_bpm")
        public int hr_bpm;

        /**
         * 心率日期時間
         */
        @Expose
        @SerializedName("hr_datetime")
        public String hr_datetime = "unknown";


    }

    /**
     * 血氧資料
     */
    public static class OximetryEntity {

        /**
         * 血氧id
         */
        @Expose
        @SerializedName("oy_id")
        public int oy_id;

        /**
         * 血氧
         */
        @Expose
        @SerializedName("oy_percent")
        public int oy_percent;

        /**
         * 血氧日期時間
         */
        @Expose
        @SerializedName("oy_datetime")
        public String oy_datetime = "unknown";
    }
}
