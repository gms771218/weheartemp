package tw.azul.wehear.net.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tw.azul.wehear.App;
import tw.azul.wehear.net.AppExecutors;
import tw.azul.wehear.net.NetworkBoundResource;
import tw.azul.wehear.net.api.ApiResponse;
import tw.azul.wehear.net.api.UserApi;
import tw.azul.wehear.net.model.base.Resource;
import tw.azul.wehear.net.model.base.ResponseIdResultEntity;
import tw.azul.wehear.net.model.base.SingleEntity;
import tw.azul.wehear.net.model.request.RequestUpdateUserEntity;
import tw.azul.wehear.net.model.user.UserEntity;
import tw.azul.wehear.net.retrofit.RetrofitClient;

import java.lang.reflect.Type;
import java.util.Map;

public class UserRepository {

    public static final String KEY_USER_STRIDE_LENGTH = "key.user.stride_length";

    private static UserRepository instance;

    private Application app ;

    private AppExecutors appExecutors;

    private UserApi userApi;

    public static UserRepository getInstance() {
        if (instance == null) {
            throw new ExceptionInInitializerError("Not Initializer , Please call initialization method");
        }
        return instance;
    }

    public static void initialization(AppExecutors appExecutors , Application app) {
        if (instance == null) {
            synchronized (UserRepository.class) {
                if (instance == null) {
                    instance = new UserRepository();
                    instance.app = app ;
                    instance.appExecutors = appExecutors;
                    instance.userApi = RetrofitClient.getInstance().createEndpoint(UserApi.class);
                }
            }
        }
    }

    /**
     * 取得使用者
     *
     * @param user_id
     * @return
     */
    public LiveData<Resource<UserEntity>> getUser(final int user_id) {
        return new NetworkBoundResource<UserEntity, SingleEntity<UserEntity>>(appExecutors) {

            MutableLiveData<UserEntity> liveData;

            @Override
            protected void saveCallResult(SingleEntity<UserEntity> item) {
                if (item.data == null) {
                    // 沒有資料
                    // {
                    //  "returnCode": 1000,
                    //  "returnMessage": "操作成功",
                    //  "data": null
                    //}
                } else {
                    if (item.status == 1000)
                        saveLocal(item.data) ;
                    liveData.postValue(item.data);
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable UserEntity data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<UserEntity> loadFromDb() {
                if (liveData == null) {
                    liveData = new MediatorLiveData<UserEntity>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<SingleEntity<UserEntity>>> createCall() {
                return userApi.getUser(user_id);
            }
        }.asLiveData();
    }


    /**
     * 修改使用者資料
     * @param user_id
     * @param requestUpdateUserEntity
     * @return
     */
    public LiveData<Resource<ResponseIdResultEntity>> postUser(
            final int user_id ,
            final RequestUpdateUserEntity requestUpdateUserEntity) {
        return new NetworkBoundResource<ResponseIdResultEntity, SingleEntity<ResponseIdResultEntity>>(appExecutors) {

            MediatorLiveData<ResponseIdResultEntity> liveData ;

            @Override
            protected void saveCallResult(SingleEntity<ResponseIdResultEntity> item) {
                if (item.status == 1000)
                    saveLocal(requestUpdateUserEntity) ;
                item.data.status = item.status ;
                item.data.msg = item.msg ;
                liveData.postValue(item.data);
            }

            @Override
            protected boolean shouldFetch(@Nullable ResponseIdResultEntity data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<ResponseIdResultEntity> loadFromDb() {
                if (liveData == null){
                    liveData = new MediatorLiveData<ResponseIdResultEntity>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>> createCall() {
                Type type = new TypeToken<Map<String, String>>() {
                }.getType();
                Map<String, String> formData = new Gson().fromJson(new Gson().toJson(requestUpdateUserEntity), type);
                if ( formData.get("user_is_male") != null)
                    formData.put("user_is_male" ,Boolean.valueOf(formData.get("user_is_male")) ? "1" : "0" ) ;
                if ( formData.get("user_app_token") != null && formData.get("user_app_token").equals("unknown") )
                    formData.remove("user_app_token") ;
                if ( formData.get("user_name") != null && formData.get("user_name").equals("unknown") )
                    formData.remove("user_name") ;
                return userApi.putUserNotification(user_id ,formData );
            }
        }.asLiveData();

    }


    private RequestBody createRequestBody(MediaType contentType, String value) {
        return RequestBody.create(contentType, value);
    }

    /**
     * 本地端儲存
     * @param userEntity
     */
    private void saveLocal(UserEntity userEntity){
        if (userEntity.getUser_stride_length() != null && userEntity.getUser_stride_length() != -1) {
            App.GetPreferences(app)
               .edit()
               .putInt(KEY_USER_STRIDE_LENGTH, userEntity.getUser_stride_length())
               .apply();
        }
    }

} // class close
