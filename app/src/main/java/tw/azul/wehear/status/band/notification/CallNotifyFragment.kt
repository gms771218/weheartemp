package tw.azul.wehear.status.band.notification

import android.Manifest.permission.READ_PHONE_STATE
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import tw.azul.wehear.R
import tw.azul.wehear.databinding.FragmentBandCallBinding

class CallNotifyFragment : Fragment() {

    companion object {
        fun newInstance() = CallNotifyFragment()
    }

    private lateinit var dataBinding: FragmentBandCallBinding
    private lateinit var viewModel: CallNotifyViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_band_call, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CallNotifyViewModel::class.java)
        dataBinding.viewModel = viewModel
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBleStatusLiveData().observe(viewLifecycleOwner::getLifecycle) { bleStatus ->
            if (bleStatus.band.isConnected != true) {
                findNavController().popBackStack(R.id.bandSettingFragment, false)
            }
        }
        runCatching {
            val activity = requireNotNull(activity)
            when {
                ActivityCompat.checkSelfPermission(activity, READ_PHONE_STATE) == PERMISSION_GRANTED -> return
                shouldShowRequestPermissionRationale(READ_PHONE_STATE) -> requestPermissions(
                    arrayOf(READ_PHONE_STATE), READ_PHONE_STATE.hashCode().and(0xFF)
                )
                else -> AlertDialog.Builder(activity, R.style.Theme_MaterialComponents_Light_Dialog_Alert).apply {
                    setPositiveButton("前往設定") { _, _ ->
                        Intent(ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                            data = Uri.parse("package:" + activity.packageName)
                            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(this)
                        }
                    }
                    setNegativeButton("知道了") { _, _ -> findNavController().popBackStack() }
                    setTitle("授權提示")
                    setMessage("「來電提醒」需要「電話」權限")
                    setCancelable(false)
                    show()
                }
            }
        }.onFailure {
            findNavController().popBackStack()
        }.exceptionOrNull()?.printStackTrace()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (READ_PHONE_STATE.hashCode().and(0xFF) == requestCode && PERMISSION_GRANTED != grantResults[0]) {
            findNavController().popBackStack()
        }
    }
}
