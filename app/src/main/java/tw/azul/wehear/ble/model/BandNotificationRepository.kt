@file:Suppress("EnumEntryName", "NonAsciiCharacters", "SpellCheckingInspection", "LocalVariableName", "MayBeConstant",
    "FunctionName", "ClassName", "Registered"
)

package tw.azul.wehear.ble.model

import android.Manifest.permission.READ_PHONE_STATE
import android.Manifest.permission.RECEIVE_SMS
import android.app.Application
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.Telephony.Sms.Intents.SMS_RECEIVED_ACTION
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.telephony.PhoneStateListener
import android.telephony.PhoneStateListener.LISTEN_CALL_STATE
import android.telephony.TelephonyManager
import android.telephony.TelephonyManager.*
import androidx.core.content.PermissionChecker
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import tw.azul.wehear.App
import tw.azul.wehear.ble.AccessoryRepository
import tw.azul.wehear.ble.BandGattRepository
import tw.azul.wehear.ble.model.AppNotified.*
import tw.azul.wehear.ble.model.CallInStatus.*
import tw.idv.fy.kotlin.utils.LogE
import tw.idv.fy.kotlin.utils.LogI
import kotlin.math.min

class BandNotificationRepository private constructor(private val application: Application) {

    companion object {
        private const val DEFAULT_SETTINGS_VALUE = 0//65528=[0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0]
        private val MAX_CELLS = 6 // 最多 60 個cells, 暫定6個: 約 30 個國字
        @Volatile
        private var singleton: BandNotificationRepository? = null
        fun Instance(application: Application): BandNotificationRepository = singleton ?: synchronized(Companion) {
            singleton = singleton ?: BandNotificationRepository(application)
            return singleton!!
        }
    }

    private val settingLiveData = MediatorLiveData<ByteArray>()

    fun getSettingLiveData(): LiveData<ByteArray> = Transformations.map(settingLiveData) { it }

    fun settingNotify(params: ByteArray) = BandGattRepository.Instance(application).settingNotify(params = params)

    fun callInNotify(status: CallInStatus, message: String) {
        // 先過濾是否要通知
        if (settingLiveData.value?.run { this[CallIn.index] } == BYTE_ZERO) {
            return
        }
        // 開始通知
        val messageByteArray = message.toByteArray()
        val trimMsgByteArray = ByteArray(18) { index ->
            runCatching {
                messageByteArray[index]
            }.getOrDefault(0)
        }
        BandGattRepository.Instance(application).apply {
            when (status) {
                電話進線 -> {
                    callInNotify(byteArray = byteArrayOf(*電話進線.comm, *trimMsgByteArray))
                }
                來電未接, 沒有進線 -> {
                    callInNotify(byteArray = byteArrayOf(*沒有進線.comm, *trimMsgByteArray))
                    callInNotify(byteArray = byteArrayOf(*來電未接.comm, *trimMsgByteArray))
                }
            }
        }
    }

    fun emitMessage(appNotified: AppNotified, message: String) {
        // 先過濾是否要通知
        if (settingLiveData.value?.run { this[appNotified.index] } == BYTE_ZERO) {
            return
        }
        // 開始通知
        val messageByteArray = message.toByteArray()
        val cells_total: Int = min(messageByteArray.size / 17 + 1, MAX_CELLS)
        messageByteArray.foldIndexed(Array(cells_total) { ByteArray(17) })
        { index, result, byte ->
            val groupIndex = index / 17
            when (groupIndex) {
                in 0 until cells_total -> result.apply {
                    this[groupIndex][index % 17] = byte
                }
                else -> result
            }
        }.mapIndexed { index, byteArray ->
             byteArrayOf(
                 appNotified.id,
                 cells_total.toByte(),
                 (index + 1).toByte(),
                 *byteArray
             )
         }.let {
            BandGattRepository.Instance(application).emitNotify(bleCommList = it)
        }

    }

    class LifecycleService: androidx.lifecycle.LifecycleService() {
        override fun onCreate() {
            super.onCreate()
            var sourceLiveData: LiveData<ByteArray>? = null
            AccessoryRepository.Instance(application).getBleStatusLiveData().observe(::getLifecycle) { bleStatus ->
                val mediatorLiveData = Instance(application).settingLiveData
                val isConnected = bleStatus?.band?.isConnected == true
                when {
                    isConnected && sourceLiveData == null -> {
                        mediatorLiveData.apply {
                            sourceLiveData = Instance(application).settingNotify( params =
                            App.GetPreferences(application)
                               .getInt(application.packageName + ".SettingsValue", DEFAULT_SETTINGS_VALUE)
                               .maskByteArray()
                               .apply {
                                   // 保險起見, 確保首位置零
                                   this[0] = 0
                                   // 無電話授權, 預設關閉
                                   PermissionChecker.checkSelfPermission(application, READ_PHONE_STATE).also {
                                       if (it != PERMISSION_GRANTED) this[CallIn.index] = 0
                                   }
                                   // 無簡訊授權, 預設關閉
                                   PermissionChecker.checkSelfPermission(application, RECEIVE_SMS).also {
                                       if (it != PERMISSION_GRANTED) this[SMS.index] = 0
                                   }
                               }
                            )
                            addSource(sourceLiveData as LiveData, ::postValue)
                        }
                    }
                    !isConnected && sourceLiveData != null -> {
                        mediatorLiveData.apply {
                            removeSource(sourceLiveData as LiveData)
                            sourceLiveData = null
                        }
                    }
                }
            }
            // 讀取設定
            Instance(application).getSettingLiveData().observe(::getLifecycle) { settings ->
                (settings?.contentToString() ?: "null").LogI("settingNotify:")
                // byteArray 轉換成 mask 整數
                val SettingsValue = settings?.maskInt() ?: DEFAULT_SETTINGS_VALUE
                // 儲存 SettingsValue
                App.GetPreferences(application).edit()
                   .putInt(application.packageName + ".SettingsValue", SettingsValue)
                   .apply()
            }
        }
    }

    // App 通知監聽
    class NotificationListener: NotificationListenerService() {
        override fun onNotificationPosted(sbn: StatusBarNotification?) {
            super.onNotificationPosted(sbn)
            (sbn?:"null").LogE("onNotificationPosted")
            val dialerRegex = "dialer".toRegex(RegexOption.IGNORE_CASE)
            val smsRegex = "sms".toRegex(RegexOption.IGNORE_CASE)
            when { // 過濾訊息, 交由 來電/簡訊 通知提醒
                dialerRegex.containsMatchIn(sbn?.packageName ?: "") -> return
                smsRegex.containsMatchIn(sbn?.packageName ?: "") -> return
                smsRegex.containsMatchIn(sbn?.tag?:"") -> return
            }
            (AppNotified.values().firstOrNull { it.pkg == sbn?.packageName } ?: Others)
            .apply {
                sbn?.notification?.tickerText?.let { msg ->
                    Instance(application).emitMessage(this, msg.toString())
                }
            }
        }
    }

    // SMS 監聽
    class SmsReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            runCatching {
                requireNotNull(context)
                require(context.applicationContext is Application)
                require(intent?.action == SMS_RECEIVED_ACTION)
                Instance(context.applicationContext as Application).emitMessage(SMS, "簡訊通知提醒")
            }.exceptionOrNull()?.printStackTrace()
        }
    }

    // 來電監聽 (Service/BroadcastReceiver: 二擇一)
    class CallInListener: Service() {
        class Receiver: BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                runCatching {
                    requireNotNull(context)
                    require(context.applicationContext is Application)
                    require(intent?.action == ACTION_PHONE_STATE_CHANGED)
                    CallInListener.CallInHandle(context.applicationContext as Application, intent)
                }.exceptionOrNull()?.printStackTrace()
            }
        }
        override fun onCreate() {
            super.onCreate()
            (getSystemService(NotificationListenerService.TELEPHONY_SERVICE) as TelephonyManager)
                    .listen(object : PhoneStateListener() {
                        override fun onCallStateChanged(state: Int, phoneNumber: String?) {
                            super.onCallStateChanged(state, phoneNumber)
                            CallInHandle(application, Intent(ACTION_PHONE_STATE_CHANGED).apply {
                                putExtra(EXTRA_INCOMING_NUMBER, phoneNumber)
                                putExtra(
                                    EXTRA_STATE,
                                    when (state) {
                                        CALL_STATE_RINGING -> "RINGING"
                                        CALL_STATE_OFFHOOK -> "OFFHOOK"
                                        CALL_STATE_IDLE -> "IDLE"
                                        else -> null
                                    }
                                )
                            })
                        }
                    }, LISTEN_CALL_STATE)
        }
        override fun onStartCommand(intent: Intent?, flags: Int, startId: Int) = START_NOT_STICKY
        override fun onBind(intent: Intent?) = null
        companion object {
            private fun CallInHandle(application: Application, intent: Intent?) {
                val phoneNumber = intent?.getStringExtra(EXTRA_INCOMING_NUMBER)
                val state = intent?.getStringExtra(EXTRA_STATE)
                when (state) {
                    "RINGING" -> 電話進線
                    "OFFHOOK" -> 來電未接
                    "IDLE" -> 沒有進線
                    else -> null
                }?.apply {
                    Instance(application).callInNotify(this, phoneNumber ?: name)
                }
                "$state = $phoneNumber".LogI()
            }
        }
    }
}

sealed class CallInStatus(val name: String, vararg bytes: Byte) {
    object 電話進線 : CallInStatus("電話進線", BYTE_ONE , BYTE_ZERO)
    object 來電未接 : CallInStatus("來電未接", BYTE_ZERO, BYTE_ONE )
    object 沒有進線 : CallInStatus("沒有進線", BYTE_ZERO, BYTE_ZERO)

    val comm: ByteArray = bytes
}

enum class AppNotified(val id: Byte, val index: Int, val title: String, val pkg: String) {
    CallIn(0x00, 1, "來電", "CallIn"),
    SMS(0x03, 2, "簡訊", "SMS"), // 短信/簡訊
    WeChat(0x02, 3, "微信", "com.tencent.mm"), // 微信
    QQ(0x01, 4, "QQ", "com.tencent.mobileqq"),
    Skype(0x04, 5, "Skype", "com.skype.raider"),
    WhatsApp(0x05, 6, "WhatsApp", "com.whatsapp"),
    Facebook(0x06, 7, "臉書", "com.facebook.katana"),
    Twitter(0x07, 8, "推特", "com.twitter.android"),
    Line(0x08, 9, "LINE", "jp.naver.line.android"),
    Youtube(0x09, 10, "Youtube", "com.google.android.youtube"),
    Gmail(0x0A, 11, "Gmail", "com.google.android.gm"),
    Instagram(0x0B, 12, "Instagram", "com.instagram.android"),
    Calendar(0x0C, 13, "日曆", "com.google.android.calendar"),
    FacebookMessenger(0x0D, 14, "Messenger", "com.facebook.orca"),
    LineAt(0x0E, 15, "LINE＠App", "com.linecorp.lineat.android"),
    Others(0x0F, 16, "應用程式", "Others")
}

private fun ByteArray.maskInt(): Int = foldIndexed(0) { index: Int, result: Int, byte: Byte ->
    if (byte == BYTE_ZERO) result else result.or(1 shl index)
}

private fun Int.maskByteArray(): ByteArray = ByteArray(20) { index ->
    if (and(1 shl index) == UINT_ZERO) BYTE_ZERO else BYTE_ONE
}

private const val UINT_ZERO: Int  = 0
private const val BYTE_ZERO: Byte = 0
private const val BYTE_ONE : Byte = 1
