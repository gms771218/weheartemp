package tw.azul.wehear.status.host.form

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import androidx.navigation.findNavController
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.request.RequestUpdateUserEntity
import tw.azul.wehear.net.model.user.UserEntity
import tw.azul.wehear.net.repository.UserRepository

class AddressViewModel : ViewModel() {

    private var userRepository: UserRepository = UserRepository.getInstance()

    val address: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }

    fun gotoMainPage(view: View) {
        view.findNavController().popBackStack()
    }

    fun getUser(user_id: Int) : LiveData<Result> {
        var liveData = MediatorLiveData<Result>()
        liveData.addSource(userRepository.getUser(user_id)) {
            if (it.status != Status.LOADING) {
                if (it.status == Status.SUCCESS) {
                    address.postValue(it.data?.user_address)
                    liveData.postValue(Result(true , ""))
                } else if (it.status == Status.ERROR) {
                    liveData.postValue(Result(false ,""))
                }
            }
        }
        return liveData
    }

    fun saveUser(user_id: Int): LiveData<Result> {
        var liveData = MediatorLiveData<Result>()
        liveData.addSource(userRepository.postUser(
            user_id
            , RequestUpdateUserEntity.create(user_id
                , UserEntity()
                , UserEntity().apply {
                    this.user_address = address.value
                })
        )){}
        return liveData
    }


    class Result {
        var isSuccess: Boolean? = null
        var msg: String = ""

        constructor(isSuccess: Boolean?, msg: String) {
            this.isSuccess = isSuccess
            this.msg = msg
        }
    }
}
