package tw.azul.wehear.net.api

import androidx.lifecycle.LiveData
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import tw.azul.wehear.net.model.base.ListEntity
import tw.azul.wehear.net.model.base.ResponseIdResultEntity
import tw.azul.wehear.net.model.base.SingleEntity
import tw.azul.wehear.net.model.dailyInfo.HealthRecordEntity
import tw.azul.wehear.net.model.dailyInfo.SleepStatusEntity
import tw.azul.wehear.net.retrofit.RetrofitClient

/**
 * 日常紀錄api
 */
interface DailyInfoApi {

    /**
     * 取得健康紀錄
     *
     * @return
     */
    @GET(RetrofitClient.BASE_URL + "healthrecord")
    fun getHealthRecord(@Query("user_id") user_id: Int, @Query("datetime") datetime: String): LiveData<ApiResponse<SingleEntity<HealthRecordEntity>>>

    /**
     * 取得睡眠狀態列舉資料
     */
    @GET(RetrofitClient.BASE_URL + "sleep/statuses")
    fun getSleepStatuses() : LiveData<ListEntity<SleepStatusEntity>>

    /**
     * 新增心率
     */
    @POST(RetrofitClient.BASE_URL + "heartRates")
    fun postHeartRates(@Body requestBody : RequestBody) : LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>>

    /**
     * 新增血氧
     */
    @POST(RetrofitClient.BASE_URL + "oximetry")
    fun postOximetry(@Body requestBody : RequestBody) : LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>>


    /**
     * 新增睡眠
     */
    @POST(RetrofitClient.BASE_URL + "sleeps")
    fun postSleep(@Body requestBody : RequestBody) : LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>>

    /**
     * 新增步數、跑步紀錄
     */
    @POST(RetrofitClient.BASE_URL + "steps")
    fun postSteps(@Body requestBody : RequestBody) : LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>>



}