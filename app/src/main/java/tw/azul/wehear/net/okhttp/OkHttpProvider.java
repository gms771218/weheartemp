package tw.azul.wehear.net.okhttp;

import android.content.Context;
import android.content.Intent;
import com.google.gson.Gson;
import okhttp3.*;
import okio.Buffer;
import okio.BufferedSource;
import tw.azul.wehear.R;
import tw.azul.wehear.account.AccountRoom;
import tw.azul.wehear.net.model.base.StatusCodeBase;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

/**
 * Created by gms on 2018/4/15.
 */

public class OkHttpProvider {

    static final String TAG = "OkHttpProvider";

    static Context mContext;

    public static void Init(Context context) {
        mContext = context;
    }

    public static OkHttpClient createOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(new TokenInterceptor());
        builder.readTimeout(60, TimeUnit.SECONDS);
        return builder.build();
    }


    /**
     * refer : https://www.jianshu.com/p/62ab11ddacc8s
     */
    public static class TokenInterceptor implements Interceptor {

        private final Charset UTF8 = Charset.forName("UTF-8");

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();

            // try the request
            Request compressedRequest = request.newBuilder()
                    .header("token", AccountRoom.Companion.getTokenLiveData().getValue().getToken())
                    .build();

            Response originalResponse = chain.proceed(compressedRequest);

            /**通过如下的办法曲线取到请求完成的数据
             *
             * 原本想通过  originalResponse.body().string()
             * 去取到请求完成的数据,但是一直报错,不知道是okhttp的bug还是操作不当
             *
             * 然后去看了okhttp的源码,找到了这个曲线方法,取到请求完成的数据后,根据特定的判断条件去判断token过期
             */
            ResponseBody responseBody = originalResponse.body();
            BufferedSource source = responseBody.source();
            source.request(Long.MAX_VALUE); // Buffer the entire body.
            Buffer buffer = source.buffer();
            Charset charset = UTF8;
            MediaType contentType = responseBody.contentType();
            if (contentType != null) {
                charset = contentType.charset(UTF8);
            }

            String bodyString = buffer.clone().readString(charset);

            if (originalResponse.code() == 500) {
                // TODO 未處理500錯誤
//                mContext.sendBroadcast(new Intent("action_500"));
            } else {
                StatusCodeBase responseStatusCode = new Gson().fromJson(bodyString, StatusCodeBase.class);
                if (responseStatusCode.status == 1100) {//根据和服务端的约定判断token过期
                    mContext.sendBroadcast(new Intent(mContext.getString(R.string.action_repeat_login)));
                }
            }
            return originalResponse;
        }
    }

}
