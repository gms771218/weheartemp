package tw.azul.wehear.status.band.form

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import androidx.navigation.findNavController
import tw.azul.wehear.R
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.request.RequestUpdateUserEntity
import tw.azul.wehear.net.model.user.UserEntity
import tw.azul.wehear.net.repository.UserRepository
import tw.azul.wehear.portal.form.GoalViewModel

class GoalViewModel : ViewModel() {

    val SUGGEST_STEP_GOAL = 10000

    private var userRepository: UserRepository = UserRepository.getInstance()

    val stepGoal: MutableLiveData<Int> = MutableLiveData<Int>().apply { value = SUGGEST_STEP_GOAL }

    fun gotoMainPage(view: View) {
        view.findNavController().navigate(R.id.action_bandGoalFragment_to_mainStatusFragment)
    }

    fun getUser(user_id: Int): LiveData<UserEntity> {
        var liveData = MediatorLiveData<UserEntity>()
        liveData.addSource(userRepository.getUser(user_id), {
            if (it.status != Status.LOADING) {
                if (it.status == Status.SUCCESS) {
                    var _value = it.data?.user_step_goal
                    stepGoal.postValue(if (_value == 0) SUGGEST_STEP_GOAL else _value)
                    liveData.postValue(it.data)
                } else if (it.status == Status.ERROR) {
                    liveData.postValue(null)
                }
            }
        })
        return liveData
    }

    fun saveUser(user_id: Int): LiveData<GoalViewModel.Result> {
        var liveData = MediatorLiveData<GoalViewModel.Result>()
        liveData.addSource(userRepository.postUser(
            user_id
            , RequestUpdateUserEntity.create(user_id
                , UserEntity()
                , UserEntity().apply {
                    this.user_step_goal = stepGoal.value
                })
        )
            , {
                if (it.status != Status.LOADING) {
                    if (it.status == Status.SUCCESS) {
                        liveData.postValue(GoalViewModel.Result(true, ""))
                    } else if (it.status == Status.ERROR) {
                        liveData.postValue(GoalViewModel.Result(false, "使用者資料儲存錯誤"))
                    }
                }
            })
        return liveData
    }
}
