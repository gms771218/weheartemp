package tw.azul.wehear.portal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigator
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_portal.*
import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.databinding.FragmentPortalBinding

class PortalFragment : Fragment() {

    companion object {
        fun newInstance() = PortalFragment()
    }

    private lateinit var dataBinding: FragmentPortalBinding

    init {
//        androidx.transition.ChangeBounds().also {
//            sharedElementEnterTransition  = it
//            sharedElementReturnTransition = it
//        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dataBinding = FragmentPortalBinding.inflate(inflater)
        dataBinding.btnRegister.setOnClickListener { view ->
            view.findNavController().navigate(R.id.action_portalFragment_to_registerFragment, FragmentNavigatorExtras(logo to "LOGO"))
        }
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ViewModelProviders.of(this).get(PortalViewModel::class.java).apply {
            dataBinding.viewmodel = this
            // 測試用
            signal.observe(viewLifecycleOwner, Observer { signal ->
                if (signal) {
                    findNavController().navigate(
                        R.id.action_portalFragment_to_searchBandFragment,
                        FragmentNavigatorExtras(logo to "LOGO")
                    )
                }
            })
            // 錯誤提示
            getStatusLiveData().observe(viewLifecycleOwner::getLifecycle) { status ->
                if (status is AccountRoom.Status.Error) {
                    activity?.apply {
                        AlertDialog.Builder(this, R.style.Theme_MaterialComponents_Light_Dialog_Alert).apply {
                            setCancelable(false)
                            setTitle("錯誤")
                            setMessage(status.errMsg)
                            setPositiveButton("知道了") { dialog, _ -> dialog.dismiss() }
                            show()
                        }
                    }
                }
            }
        }
    }

    /*
        Kotlin Extension by Self
     */
    private fun NavController.navigate(resId: Int, extras: Navigator.Extras) = navigate(resId, null, null, extras)
}
