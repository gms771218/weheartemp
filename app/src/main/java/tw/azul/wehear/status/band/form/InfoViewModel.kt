package tw.azul.wehear.status.band.form

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.request.RequestUpdateUserEntity
import tw.azul.wehear.net.model.user.UserEntity
import tw.azul.wehear.net.repository.UserRepository

class InfoViewModel : ViewModel() {

    val DEFAULT_VALUE = -1

    private var userRepository: UserRepository = UserRepository.getInstance()

    val height: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }

    val weight: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }

    val strideLength: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }


    fun getUser(user_id: Int): LiveData<UserEntity> {
        var liveData = MediatorLiveData<UserEntity>()
        liveData.addSource(userRepository.getUser(user_id)) {
            if (it.status != Status.LOADING) {
                if (it.status == Status.SUCCESS) {
                    height.postValue(if (it.data?.user_height == DEFAULT_VALUE) "" else it.data?.user_height.toString())
                    weight.postValue(if (it.data?.user_weight == DEFAULT_VALUE) "" else it.data?.user_weight.toString())
                    strideLength.postValue(if (it.data?.user_stride_length == DEFAULT_VALUE) "" else it.data?.user_stride_length.toString())
                    liveData.postValue(it.data)
                } else if (it.status == Status.ERROR) {
                    liveData.postValue(null)
                }
            }
        }
        return liveData
    }

    fun saveUser(user_id: Int): LiveData<Result> {
        var liveData = MediatorLiveData<Result>()
        liveData.addSource(
            userRepository.postUser(
                user_id
                , RequestUpdateUserEntity.create(user_id
                    , UserEntity()
                    , UserEntity().apply {
                        this.user_height = if (height.value?.isEmpty()!!) 0 else height.value?.toInt()
                        this.user_weight = if (weight.value?.isEmpty()!!) 0 else weight.value?.toInt()
                        this.user_stride_length =
                                if (strideLength.value?.isEmpty()!!) 0 else strideLength.value?.toInt()
                    })
            )
        ) {}
        return liveData
    }

    class Result {
        var isSuccess: Boolean? = null
        var msg: String = ""

        constructor(isSuccess: Boolean?, msg: String) {
            this.isSuccess = isSuccess
            this.msg = msg
        }
    }
}
