package tw.azul.wehear.ble

import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor

interface IBluetoothGattCallback {

    var gatt: BluetoothGatt?

    //fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int)

    fun onServicesDiscovered(gatt: BluetoothGatt, status: Int)

    fun onCharacteristicWrite(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic, status: Int)

    fun onCharacteristicRead(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic, status: Int)

    //fun onPhyUpdate(gatt: BluetoothGatt, txPhy: Int, rxPhy: Int, status: Int)

    //fun onMtuChanged(gatt: BluetoothGatt, mtu: Int, status: Int)

    //fun onReliableWriteCompleted(gatt: BluetoothGatt, status: Int)

    fun onDescriptorWrite(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int)

    fun onDescriptorRead(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int)

    fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic)

    //fun onPhyRead(gatt: BluetoothGatt, txPhy: Int, rxPhy: Int, status: Int)

    //fun onReadRemoteRssi(gatt: BluetoothGatt, rssi: Int, status: Int)
}

@Suppress("DELEGATED_MEMBER_HIDES_SUPERTYPE_OVERRIDE")
open class BluetoothGattCallback(val delegate: IBluetoothGattCallback) :
    android.bluetooth.BluetoothGattCallback(), IBluetoothGattCallback by delegate