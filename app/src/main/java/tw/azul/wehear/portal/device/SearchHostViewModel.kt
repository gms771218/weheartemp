@file:Suppress("PropertyName", "MemberVisibilityCanBePrivate")

package tw.azul.wehear.portal.device

import android.app.Application
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.os.Handler
import android.os.ParcelUuid
import android.view.View
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import tw.azul.feature.ble.repository.BleRepository
import tw.azul.wehear.GOTOMAINACTIVITYDEFAULT
import tw.azul.wehear.IGotoMainActivity
import tw.azul.wehear.ble.DeviceRepository
import tw.azul.wehear.utils.FilterBuilder
import tw.azul.wehear.utils.SettingsBuilder
import tw.azul.wehear.utils.build
import java.util.*

open class SearchHostViewModel(val app: Application) : AndroidViewModel(app) {

    var delegate: IGotoMainActivity = GOTOMAINACTIVITYDEFAULT

    companion object {
        private const val ThresholdCount = 3
        private const val TimeOut: Long = 6 * 1000L
        private val ScanSetting: ScanSettings = SettingsBuilder().build {
            setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
        }
        private val ScanFilters:List<ScanFilter> = listOf(
            FilterBuilder().build {
                setDeviceName("HX_SHOCK")
                setServiceUuid(ParcelUuid.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e"))
            },
            FilterBuilder().build {
                setDeviceName("WeHear")
                setServiceUuid(ParcelUuid.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e"))
            }
        )
    }

    private val bleRepository = BleRepository.Instance(app)
    fun bleStatusAddObserver(lifecycleOwner: LifecycleOwner, observer: Observer<Boolean>) {
        bleRepository.bleStatusAddObserver(lifecycleOwner, observer)
    }
    val bleStatusDelObserver  = bleRepository::bleStatusDelObserver

    val noFoundLiveData = MediatorLiveData<Boolean>().apply { value = false }

    private lateinit var searchScanCallback:SearchScanCallback
    private val hostListLiveData = DeviceRepository.Instance(app).hostListLiveData
    private val searchResultLiveData = MutableLiveData<Set<BluetoothDevice>>().apply {
        searchScanCallback = SearchScanCallback {
            val result = searchScanCallback.deviceSearchCountMap.filterValues { count -> count > ThresholdCount }.keys
            postValue(result)
            hostListLiveData.postValue(result.toList())
        }
        noFoundLiveData.addSource(this) { searchResultSet ->
            noFoundLiveData.value = searchResultSet.isEmpty()
        }
    }

    private class SearchScanCallback(val callback: () -> Unit) : ScanCallback(), Runnable {
        val deviceSearchCountMap = mutableMapOf<BluetoothDevice, Int>()
        val mHandler = Handler()
        fun search() {
            deviceSearchCountMap.clear()
            mHandler.postDelayed(this, TimeOut)
            BluetoothAdapter.getDefaultAdapter().bluetoothLeScanner?.startScan(ScanFilters, ScanSetting, this)
        }
        fun terminate() {
            BluetoothAdapter.getDefaultAdapter().bluetoothLeScanner?.stopScan(this)
            mHandler.removeCallbacks(this)
        }
        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            android.util.Log.w("Faty","$errorCode")
        }
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)
            with(result.device) {
                if (ScanFilters.any { Objects.equals(it.deviceName, name ?: address) && it.matches(result) }) {
                    deviceSearchCountMap.apply {
                        put(this@with, getOrElse(this@with) { 0 } + 1)
                    }
                }
            }
        }
        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
            super.onBatchScanResults(results)
            android.util.Log.w("Faty","$results")
        }
        override fun run() {
            callback()
        }
    }

    fun searchScanCallbackObserve(owner: LifecycleOwner, observer: Observer<Set<BluetoothDevice>>) {
        searchResultLiveData.observe(owner, observer)
        search()
    }

    fun removeScanCallbackObserve(owner: LifecycleOwner) {
        searchScanCallback.terminate()
        searchResultLiveData.removeObservers(owner)
    }

    fun searchAgain() {
        hostListLiveData.postValue(null)
        noFoundLiveData.value = false
        searchScanCallback.search()
    }

    @Suppress("UNUSED_PARAMETER")
    fun search(view: View? = null) {
        start_time = System.currentTimeMillis()
        searchAgain()
    }

    var start_time: Long = System.currentTimeMillis()
}