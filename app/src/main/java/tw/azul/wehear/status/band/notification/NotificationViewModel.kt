@file:Suppress("SpellCheckingInspection")

package tw.azul.wehear.status.band.notification

import android.app.Application
import android.view.View
import androidx.lifecycle.*
import tw.azul.wehear.ble.AccessoryRepository
import tw.azul.wehear.ble.model.AppNotified
import tw.azul.wehear.ble.model.AppNotified.*
import tw.azul.wehear.ble.model.BandNotificationRepository
import tw.idv.fy.kotlin.utils.LogW

class NotificationViewModel(private val app: Application) : AndroidViewModel(app) {

    companion object {
        private const val Loadin = View.VISIBLE
        private const val Loaded = View.GONE
    }

    fun getBleStatusLiveData(): LiveData<AccessoryRepository.BleStatus> =
        AccessoryRepository.Instance(app).getBleStatusLiveData()

    // 來源
    private val sourceLiveData = BandNotificationRepository.Instance(app).getSettingLiveData()
    // 輸出
    private val observer = Observer<ByteArray> { settings: ByteArray ->
        if (sourceLiveData.value?.contentEquals(settings) == false) {
            settings.contentToString().LogW("輸出:")
            BandNotificationRepository.Instance(app).settingNotify(settings)
            loadType.postValue(Loadin)
        } else {
            loadType.postValue(Loaded)
        }
    }
    // 彙整
    private val settingLiveData = MediatorLiveData<ByteArray>().apply {
        addSource(sourceLiveData) { settings ->
            if (settings != null) {
                removeSource(sourceLiveData)
                postValue(ByteArray(20) { i -> settings[i] }) //
            }
        }
        observeForever(observer)
    }
    // loading
    val loadType = MutableLiveData<Int>()
    // 開關
    val lineNotified = MediatorLiveData<Boolean>()
    val lineAtNotified = MediatorLiveData<Boolean>()
    val fbMessengerNotified = MediatorLiveData<Boolean>()
    val fbNotified = MediatorLiveData<Boolean>()
    val wechatNotified = MediatorLiveData<Boolean>()
    val gmailNotified = MediatorLiveData<Boolean>()
    val igNotified = MediatorLiveData<Boolean>()
    val whatsappNotified = MediatorLiveData<Boolean>()
    val skypeNotified = MediatorLiveData<Boolean>()
    val youtubeNotified = MediatorLiveData<Boolean>()
    val calendarNotified = MediatorLiveData<Boolean>()
    val othersNotified = MediatorLiveData<Boolean>()
    init {
        arrayOf(
            lineNotified to Line,
            lineAtNotified to LineAt,
            fbMessengerNotified to FacebookMessenger,
            fbNotified to Facebook,
            wechatNotified to WeChat,
            gmailNotified to Gmail,
            igNotified to Instagram,
            whatsappNotified to WhatsApp,
            skypeNotified to Skype,
            youtubeNotified to Youtube,
            calendarNotified to Calendar,
            othersNotified to Others
        ).forEach {pair: Pair<MediatorLiveData<Boolean>, AppNotified> ->
            val (mediatorLiveData, appNotified) = pair
            // 注入來源
            mediatorLiveData.apply {
                addSource(sourceLiveData) { settings: ByteArray? ->
                    postValue(settings != null && settings[appNotified.index] > 0)
                }
            }
            // 輸出結果
            settingLiveData.apply {
                addSource(mediatorLiveData) { isNotified: Boolean ->
                    value?.let {
                        it[appNotified.index] = if (isNotified) 0x01 else 0x00
                        postValue(it)
                    }
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        with(settingLiveData) {
            removeSource(sourceLiveData)
            removeObserver(observer)
        }
    }
}
