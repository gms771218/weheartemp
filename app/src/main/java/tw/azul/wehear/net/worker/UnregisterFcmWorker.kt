package tw.azul.wehear.net.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.firebase.iid.FirebaseInstanceId


/**
 *  註銷FCM
 */
class UnregisterFcmWorker(context: Context, parameters: WorkerParameters) : Worker(context, parameters) {

    override fun doWork(): Result {
        FirebaseInstanceId.getInstance().deleteInstanceId()
        return Result.success()
    }
}