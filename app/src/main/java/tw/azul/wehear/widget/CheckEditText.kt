package tw.azul.wehear.widget

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.text.Editable
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.EditText
import androidx.core.content.res.ResourcesCompat
import tw.azul.wehear.R

/***
 * 自訂檢查欄位功能的EditText
 */
class CheckEditText : EditText {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    @Suppress("PrivatePropertyName")
    private var hint_color_normal: ColorStateList = ColorStateList.valueOf(
        ResourcesCompat.getColor(resources, R.color.tx_color3, null)
    )
    @Suppress("PrivatePropertyName")
    private var hint_color_error: ColorStateList = ColorStateList.valueOf(
        ResourcesCompat.getColor(resources, R.color.color4, null)
    )

    private var isValid = true
    private var tempHint: CharSequence = ""
    private var tempText: Editable? = null
    private val emptyText = Editable.Factory.getInstance().newEditable("")

    fun valid(errorMsg: String = "此處為必填欄位", validRule: (EditText.() -> Boolean) = { text.isNotEmpty() }): Boolean {
        // 檢查
        val isValid = run(validRule)
        // 套樣式
        when {
            this.isValid && !isValid -> styleWrong(errorMsg)
            !this.isValid && isValid -> styleRight()
        }
        // 焦點或否
        if (isValid) clearFocus()
        else requestFocus()
        // 紀錄
        this.isValid = isValid
        // 回覆
        return isValid
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (!isValid) {
            isValid = true
            styleRight()
        }
        return super.onTouchEvent(event)
    }

    private fun styleRight() {
        isCursorVisible = true
        hint = tempHint
        text = tempText
        background.level = 0
        setHintTextColor(hint_color_normal)
    }

    private fun styleWrong(errorMsg: String) {
        isCursorVisible = false
        tempHint = hint
        tempText = text
        hint = errorMsg
        text = emptyText
        background.level = 1
        setHintTextColor(hint_color_error)
    }
}