package tw.azul.feature.ble.repository

sealed class BleState {
    object Init        : BleState()
    object Disconnected: BleState()
    object Connected   : BleState()
    data class Connecting(val name: String): BleState()
}