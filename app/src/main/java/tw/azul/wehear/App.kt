package tw.azul.wehear

import android.app.Application
import android.content.Intent
import android.content.SharedPreferences
import tw.azul.feature.ble.repository.BleRepository
import tw.azul.wehear.account.AccountRepository
import tw.azul.wehear.api.WebAPI
import tw.azul.wehear.ble.AccessoryRepository
import tw.azul.wehear.ble.BleConnectionListener
import tw.azul.wehear.ble.model.BandBatteryRepository
import tw.azul.wehear.ble.model.BandNotificationRepository
import tw.azul.wehear.ble.model.RunSpeedCadenceRepository
import tw.azul.wehear.net.AppExecutors
import tw.azul.wehear.net.okhttp.OkHttpProvider
import tw.azul.wehear.net.repository.*

class App: Application() {

    private lateinit var mAppExecutors: AppExecutors

    override fun onCreate() {
        super.onCreate()
        // 初始化
        AccountRepository.Instance(this)
        BleRepository.Instance(this)
        OkHttpProvider.Init(this)
        // 初始化
        mAppExecutors = AppExecutors().apply {
            FamilyRepository.initialization(this)
            UserRepository.initialization(this , this@App)
            NotificationRepository.initialization(this)
            DailyRepository.initialization(this)
            VersionRepository.initialization(this);
            // 偷用
            WebAPI.Executor = networkIO()
        }
        // ><"
        startService(Intent(applicationContext, AccessoryRepository.LifecycleService::class.java))
        // ><"
        startService(Intent(applicationContext, BandBatteryRepository.LifecycleService::class.java))
        // ><"
        startService(Intent(applicationContext, RunSpeedCadenceRepository.LifecycleService::class.java))
        // ><"
        startService(Intent(applicationContext, BandNotificationRepository.LifecycleService::class.java))
        // ><"
        startService(Intent(applicationContext, BleConnectionListener::class.java))
        registerActivityLifecycleCallbacks(MyActivityLifecycleCallback())
    }

    companion object {
        @JvmStatic
        fun GetPreferences(app: Application): SharedPreferences {
            if (app !is App) throw IllegalAccessException()
            return app.getSharedPreferences(app.packageName, MODE_PRIVATE)
        }
    }
}