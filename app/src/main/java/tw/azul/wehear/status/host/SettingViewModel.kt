package tw.azul.wehear.status.host

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import tw.azul.wehear.ble.DeviceRepository
import tw.azul.wehear.ble.HostConnector

class SettingViewModel(val app:Application) : AndroidViewModel(app) {

    val deviceNameLiveData: LiveData<String>
    val deviceAddrLiveData: LiveData<String>
    init {
        val hostLiveData = DeviceRepository.Instance(app).hostLiveData
        deviceNameLiveData = MutableLiveData<String>().apply { value = hostLiveData.value?.name    }
        deviceAddrLiveData = MutableLiveData<String>().apply { value = hostLiveData.value?.address }
    }

    fun hostUnbind(view: View) {
        // 清除 HostConnector
        HostConnector.Instance(app).reset()
        // 清除 Host in DeviceRepository
        DeviceRepository.Instance(app).resetHost()
        // 彈回去
        view.findNavController().popBackStack()
    }
}
