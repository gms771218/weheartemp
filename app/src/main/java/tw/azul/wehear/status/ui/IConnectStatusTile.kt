package tw.azul.wehear.status.ui

import android.view.View
import androidx.annotation.CallSuper

interface IConnectStatusTile {
    enum class Status {
        UnConnect,
        Connected,
    }
    @CallSuper
    fun setStatus(status: Status) {
        (this as? View)?.isEnabled = status == Status.Connected
    }
}