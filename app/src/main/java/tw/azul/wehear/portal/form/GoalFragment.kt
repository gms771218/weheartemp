package tw.azul.wehear.portal.form

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_portal_goal.*
import tw.azul.wehear.GotoMainActivityDelegate
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.databinding.FragmentPortalGoalBinding

class GoalFragment : Fragment() {

    private companion object {
        private val DISPLAY_VALUE: Array<String> = Array(30) { i -> (i * 1000 + 1000).toString() }
    }

    private lateinit var dataBinding: FragmentPortalGoalBinding
    private lateinit var viewModel: GoalViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentPortalGoalBinding.inflate(inflater)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        picker.apply {
            wrapSelectorWheel = false
            displayedValues = DISPLAY_VALUE
            maxValue = 29
            minValue = 0
            value = 7
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(GoalViewModel::class.java).apply {
            delegate = GotoMainActivityDelegate(activity)
            dataBinding.viewmodel = this
            dataBinding.setLifecycleOwner(viewLifecycleOwner)
        }

        viewModel.getUser(AccountRoom.TokenLiveData.value?.userId!!).observe(viewLifecycleOwner, Observer {})
        btn_next.setOnClickListener {
            viewModel.saveUser(AccountRoom.TokenLiveData.value?.userId!!).observe(viewLifecycleOwner, Observer {})
            viewModel.delegate.gotoMainActivity()
        }
    }

}
