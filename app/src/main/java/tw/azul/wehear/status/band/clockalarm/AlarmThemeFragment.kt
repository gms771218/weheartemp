package tw.azul.wehear.status.band.clockalarm

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_band_alarm_theme.*
import tw.azul.wehear.R
import tw.azul.wehear.ble.model.BandAlarmEntity

import tw.azul.wehear.databinding.FragmentBandAlarmThemeBinding

/**
 * 修改手環鬧鐘主題
 */
class AlarmThemeFragment : Fragment() {

    companion object {
        fun newInstance() = AlarmThemeFragment()
    }

    private lateinit var viewModel: AlarmThemeViewModel
    private lateinit var dataBinding: FragmentBandAlarmThemeBinding
    private lateinit var bandAlarmEntity: BandAlarmEntity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentBandAlarmThemeBinding.bind(
            inflater.inflate(
                R.layout.fragment_band_alarm_theme,
                container,
                false
            )
        )
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AlarmThemeViewModel::class.java)
        dataBinding.viewModel = viewModel
        dataBinding.setLifecycleOwner(viewLifecycleOwner)

        arguments?.getParcelable<BandAlarmEntity>("alarm")?.apply {
            bandAlarmEntity = this
            viewModel.setName(this.themeName)
        }

        btn_check.setOnClickListener {
            viewModel.name.observe(viewLifecycleOwner::getLifecycle) {
                bandAlarmEntity.themeName = it
                NavHostFragment.findNavController(this).navigate(R.id.action_alarmThemeFragment_to_alarmBuildFragment
                    , Bundle().apply {
                        putParcelable("alarm", bandAlarmEntity)
                    })
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBleStatusLiveData().observe(viewLifecycleOwner::getLifecycle) { bleStatus ->
            if (bleStatus.band.isConnected != true) {
                findNavController().popBackStack()
            }
        }
    }
}
