package tw.azul.wehear.sos

import android.graphics.PointF
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gms.widget.layout.component.SosBar
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import tw.azul.feature.ble.repository.LocationPositionHelper
import tw.azul.wehear.MainActivity
import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom

/**
 * SOS 頁面
 * TODO 目前只取最後定位位置，非GPS位置監聽
 */
class SOSFragment : Fragment() {

    companion object {
        fun newInstance() = SOSFragment()
    }

    val USER_ID = AccountRoom.TokenLiveData.value?.userId!!

    private lateinit var viewModel: SOSViewModel

    var isSosSending = false

    var location: PointF = PointF(0f, 0f)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.layout_sos, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val tv_sos_success = view.findViewById<View>(R.id.tv_sos_success)
        val layout_mask = view.findViewById<View>(R.id.layout_mask)
        var posTag = 0f
        view.findViewById<SosBar>(R.id.sosbar)?.setPositionListener { position ->
            layout_mask.alpha = position
            tv_sos_success.let { view ->

                if (position == 1f && !isSosSending) {
                    isSosSending = true
                    viewModel.sendSOS(USER_ID, System.currentTimeMillis().toString(), location.x, location.y)
                        .observe(this, Observer { result ->
                            if (result != null) {
                                if (result.isSuccess == true)
                                    view.visibility = View.VISIBLE
                                else {
                                    if (!::snackBarHint.isInitialized) {
                                        android.widget.Toast
                                            .makeText(context, "發送失敗", android.widget.Toast.LENGTH_SHORT)
                                            .show()
                                        return@Observer
                                    }
                                    snackBarHint.setText("發送失敗").show()
                                }
                                isSosSending = false
                            }
                        })
                }
                if (position > posTag && view.visibility == View.VISIBLE) view.visibility = View.INVISIBLE
            }
            posTag = position
        }
        snackBarHint = Snackbar.make(view, "發送失敗", Snackbar.LENGTH_INDEFINITE).apply {
            setActionTextColor(ResourcesCompat.getColor(resources, R.color.colorAccent, null))
            setAction("知道了") { dismiss() }
        }
    }

    private lateinit var snackBarHint: Snackbar

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SOSViewModel::class.java)
        (activity as? MainActivity)?.hideToolbarAndShowNavigation()
    }

    override fun onResume() {
        super.onResume()
        runCatching {
            LocationPositionHelper.CheckLocationSettings(requireActivity(), true)
        }.onSuccess { isEnabled -> if (isEnabled) getLocation()
        }.onFailure { e ->
            snackBarHint.setText(e.localizedMessage).show()
            e.printStackTrace()
        }
    }

    // 取得位置
    @Throws(SecurityException::class)
    fun getLocation() {
        LocationServices.getFusedLocationProviderClient(context!!).lastLocation
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    location = PointF(
                        it.result?.latitude?.toFloat() ?: kotlin.run { 0f },
                        it.result?.longitude?.toFloat() ?: kotlin.run { 0f }
                    )
                }
            }
    }


}
