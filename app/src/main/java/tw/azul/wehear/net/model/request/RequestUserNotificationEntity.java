package tw.azul.wehear.net.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestUserNotificationEntity {

    /**
     * 使用者id
     */
    @SerializedName("user_id")
    public int user_id;

    /**
     * 通知類型id
     */
    @SerializedName("nts_id")
    public int nts_id;

    /**
     * 通知是否啟用(1或0)
     */
    @SerializedName("ns_enable")
    public int ns_enable;

    @SerializedName("ns_min")
    public Integer ns_min;

    @SerializedName("ns_max")
    public Integer ns_max;

    @SerializedName("ns_hour")
    public Integer ns_hour;


    public RequestUserNotificationEntity(int user_id, int nts_id, boolean ns_enable) {
        this.user_id = user_id;
        this.nts_id = nts_id;
        this.ns_enable = ns_enable ? 1 : 0 ;
    }


    public void setNs_min(Integer ns_min) {
        this.ns_min = ns_min;
    }

    public void setNs_max(Integer ns_max) {
        this.ns_max = ns_max;
    }

    public void setNs_hour(Integer ns_hour) {
        this.ns_hour = ns_hour;
    }

    @Override
    public String toString() {
        return "RequestUserNotificationEntity{" +
                "user_id=" + user_id +
                ", nts_id=" + nts_id +
                ", ns_enable=" + ns_enable +
                ", ns_min=" + ns_min +
                ", ns_max=" + ns_max +
                ", ns_hour=" + ns_hour +
                '}';
    }
}
