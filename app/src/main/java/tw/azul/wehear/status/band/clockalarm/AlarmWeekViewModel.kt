package tw.azul.wehear.status.band.clockalarm

import android.app.Application
import androidx.lifecycle.*
import tw.azul.wehear.ble.AccessoryRepository

class AlarmWeekViewModel(private val app: Application) : AndroidViewModel(app) {

    // 週日
    var sun: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = true }
    // 週ㄧ
    var mon: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = true }
    // 週二
    var tue: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = true }
    // 週三
    var wed: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = true }
    // 週四
    var thu: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = true }
    // 週五
    var fri: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = true }
    // 週六
    var sat: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = true }


    var weekLiveData: MediatorLiveData<BooleanArray> = MediatorLiveData<BooleanArray>().apply {
        value = booleanArrayOf(sun.value!!, mon.value!!, tue.value!!, wed.value!!, thu.value!!, fri.value!!, sat.value!!)
    }

    fun setWeek(array: BooleanArray = BooleanArray(7)){
        sun.postValue(array[0])
        mon.postValue(array[1])
        tue.postValue(array[2])
        wed.postValue(array[3])
        thu.postValue(array[4])
        fri.postValue(array[5])
        sat.postValue(array[6])

        weekLiveData.apply {
            addSource(sun) {
                value?.set(0, it)
                postValue(value)
            }
            addSource(mon) {
                value?.set(1, it)
                postValue(value)
            }
            addSource(tue) {
                value?.set(2, it)
                postValue(value)
            }
            addSource(wed) {
                value?.set(3, it)
                postValue(value)
            }
            addSource(thu) {
                value?.set(4, it)
                postValue(value)
            }
            addSource(fri) {
                value?.set(5, it)
                postValue(value)
            }
            addSource(sat) {
                value?.set(6, it)
                postValue(value)
            }
        }
    }

    fun observerResult(array: BooleanArray = BooleanArray(7)): LiveData<BooleanArray> {
        sun.postValue(array[0])
        mon.postValue(array[1])
        tue.postValue(array[2])
        wed.postValue(array[3])
        thu.postValue(array[4])
        fri.postValue(array[5])
        sat.postValue(array[6])

        weekLiveData.apply {
            addSource(sun) {
                value?.set(0, it)
                postValue(value)
            }
            addSource(mon) {
                value?.set(1, it)
                postValue(value)
            }
            addSource(tue) {
                value?.set(2, it)
                postValue(value)
            }
            addSource(wed) {
                value?.set(3, it)
                postValue(value)
            }
            addSource(thu) {
                value?.set(4, it)
                postValue(value)
            }
            addSource(fri) {
                value?.set(5, it)
                postValue(value)
            }
            addSource(sat) {
                value?.set(6, it)
                postValue(value)
            }
        }
        return weekLiveData
    }

    fun getBleStatusLiveData(): LiveData<AccessoryRepository.BleStatus> =
        AccessoryRepository.Instance(app).getBleStatusLiveData()
}
