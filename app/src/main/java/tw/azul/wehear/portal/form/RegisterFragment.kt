package tw.azul.wehear.portal.form

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_portal_register.*
import tw.azul.wehear.R
import tw.azul.wehear.databinding.FragmentPortalRegisterBinding

class RegisterFragment : Fragment() {

    companion object {
        fun newInstance() = RegisterFragment()
    }

    private lateinit var dataBinding: FragmentPortalRegisterBinding
    private lateinit var viewModel: RegisterViewModel

    init {
//        ChangeBounds().also {
//            sharedElementEnterTransition = it
//            sharedElementReturnTransition = it
//        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentPortalRegisterBinding.inflate(inflater)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.also {
            ArrayAdapter.createFromResource(it, R.array.gender, android.R.layout.simple_spinner_item).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner_gender.adapter = adapter
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)
        dataBinding.viewmodel = viewModel
        dataBinding.setLifecycleOwner(viewLifecycleOwner)

        viewModel.errStatusLiveData.observe(viewLifecycleOwner::getLifecycle) {
            if (it == null) { // 防呆
                return@observe
            }

            if (value_check_pwd.valid()) {
                value_check_pwd.valid(it) {
                    it != "密碼確認有誤"
                }
            }
            if (value_password.valid()) {
                value_password.valid(it) {
                    it != "使用者密碼未填"
                }
            }
            if (value_phone.valid("手機格式錯誤") { "09\\d{8}".toRegex().matches(text) }) {
                value_phone.valid(it) {
                    !"(使用者手機未填|使用者手機格式錯誤|使用者手機過長|使用者手機重複)".toRegex().matches(it)
                }
            }
            if (value_name.valid()) {
                value_name.valid(it) {
                    it != "使用者姓名長度過長"
                }
            }
        }
    }

}
