package tw.azul.wehear.status.band.clockalarm

import android.app.Application
import androidx.lifecycle.*
import tw.azul.wehear.ble.AccessoryRepository

class AlarmThemeViewModel(private val app: Application) : AndroidViewModel(app) {

    var name: MutableLiveData<String> = MediatorLiveData<String>().apply { value = "" }


    fun setName(name: String) {
        this.name.postValue(name)
    }

    fun getBleStatusLiveData(): LiveData<AccessoryRepository.BleStatus> =
        AccessoryRepository.Instance(app).getBleStatusLiveData()
}
