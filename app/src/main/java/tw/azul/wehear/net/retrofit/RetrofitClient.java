package tw.azul.wehear.net.retrofit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tw.azul.wehear.BuildConfig;
import tw.azul.wehear.net.LiveDataCallAdapterFactory;
import tw.azul.wehear.net.okhttp.OkHttpProvider;

/**
 * Created by gms on 2018/4/15.
 */

public class RetrofitClient {

   public final static String BASE_URL = BuildConfig.BASE_URL ;

    private final Retrofit mRetrofit;

    private static RetrofitClient instance;

    private RetrofitClient() {
        OkHttpClient okHttpClient = OkHttpProvider.createOkHttpClient();
        Retrofit.Builder builder = new Retrofit.Builder();
        builder
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                // 設置數據解析器
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory());

        this.mRetrofit = builder.build();
    }

    public synchronized static RetrofitClient getInstance() {
        if (instance == null) {
            instance = new RetrofitClient();
        }
        return instance;
    }

    public <T> T createEndpoint(Class<T> clazz) {
        return mRetrofit.create(clazz);
    }

}

