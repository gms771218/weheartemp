@file:Suppress("FunctionName", "ConstantConditionIf")

package tw.azul.wehear.account

import android.accounts.Account
import android.accounts.AccountManager
import android.accounts.AccountManager.KEY_ACCOUNT_NAME
import android.app.Application
import android.app.NotificationManager
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import tw.azul.wehear.App
import tw.azul.wehear.account.AccountRoom.Status
import tw.azul.wehear.api.WebAPI
import tw.azul.wehear.api.WebThread
import tw.azul.wehear.ble.BandConnector
import tw.azul.wehear.ble.DeviceRepository
import tw.azul.wehear.ble.HostConnector

class AccountRepository private constructor(private val app: Application) {

    companion object {
        private const val isCacheToken = false
        @Volatile
        private var singleton: AccountRepository? = null
        @JvmStatic
        fun Instance(context: Application): AccountRepository = singleton ?: synchronized(Companion) {
            singleton = singleton ?: AccountRepository(context)
            return singleton!!
        }
    }

    private val statusLiveData = MutableLiveData<Status>()

    init {
        AccountRoom.StatusLiveData.apply { addSource(statusLiveData) { value = it } }
        //login(specificAccount = false) // 代表系統自動找第一個帳號登入 (specificAccount: 是否指定帳號, false = 系統選)
        val isAutoLoginIn = App.GetPreferences(app).getBoolean("AccountRoom.Status.Connected", false)
        login(specificAccount = !isAutoLoginIn)
    }

    private fun getToken(account: Account, pwd: String?) {
        statusLiveData.value = Status.Connecting
        AccountManager.get(app).apply {
            peekAuthToken(account, app.packageName)?.let { token ->
                statusLiveData.value = Status.Connected(account, token)
                // 清除 token 從 AccountManager (測試用)
                if (!isCacheToken) invalidateAuthToken(app.packageName, token)
                return@apply
            }
            val options = Bundle().apply { putString(AccountManager.KEY_PASSWORD, pwd ?: getPassword(account)) }
            getAuthToken(account, app.packageName, options, true, { future ->
                if (future.isCancelled) { // 防呆
                    statusLiveData.value = Status.Error(account, errMsg = "unknown")
                    return@getAuthToken
                }
                if (!future.isDone) { // 防呆
                    statusLiveData.value = Status.Connecting
                    return@getAuthToken
                }
                // 真正會執行的
                future.result.getString(AccountManager.KEY_AUTHTOKEN)?.also { token ->
                    // 清除 token 從 AccountManager (測試用)
                    if (!isCacheToken) invalidateAuthToken(app.packageName, token)
                }
            }, Handler())
        }
    }

    private fun login(acc: String? = null, pwd: String? = null, specificAccount: Boolean = true) {
        AccountManager.get(app).apply {
            getAccountsByType(app.packageName).asList().firstOrNull { account ->
                !specificAccount || account.name == acc
            }?.let { account ->
                getToken(account, pwd)
            }?: if (acc == null || !specificAccount) {
                statusLiveData.postValue(Status.Init)
            } else {
                getToken(Account(acc, app.packageName), pwd)
            }
        }
    }

    fun login(acc: String, pwd: String) {
        login(acc, pwd, true)
    }

    fun register(
        user_account: String,
        user_password: String,
        user_name: String,
        user_is_male: Int,
        user_birthday_year: Int,
        user_birthday_month: Int,
        user_birthday_day: Int,
        user_aid: Int
    ): LiveData<Status> {
        val status = MutableLiveData<Status>().apply { value = Status.Connecting }
        AccountManager.get(app).apply {
            val options = Bundle().apply {
                putString("user_account"     , user_account)
                putString("user_password"    , user_password)
                putString("user_name"        , user_name)
                putInt("user_is_male"        , user_is_male)
                putInt("user_birthday_year"  , user_birthday_year)
                putInt("user_birthday_month" , user_birthday_month)
                putInt("user_birthday_day"   , user_birthday_day)
                putInt("user_aid"            , user_aid)
                putSerializable("SERVICE_ID" , AccountService.INTERN_ID)
            }
            addAccount(app.packageName, app.packageName, null, options, null, {
                try {
                    /*
                    android.widget.Toast
                        .makeText(app, "${it.result[KEY_ACCOUNT_NAME]}\n註冊成功", android.widget.Toast.LENGTH_SHORT)
                        .apply {
                            view.findViewById<TextView>(android.R.id.message)?.gravity = Gravity.CENTER
                            show()
                        }*/
                    require(it.isDone && it.result[KEY_ACCOUNT_NAME] == user_account)
                    status.value = Status.Connected()
                } catch (e:Throwable) {
                    //android.widget.Toast.makeText(app, e.localizedMessage, android.widget.Toast.LENGTH_SHORT).show()
                    status.value = Status.Error(errMsg = e.localizedMessage)
                    e.printStackTrace()
                }
            }, Handler())
        }
        return status
    }

    fun logout() { // 現在登出不清除 account manager 記載(！！！)
        /*
        // 清除 account manager 記載
        val status = AccountRoom.StatusLiveData.value
        when (status) {
            is Status.Connected -> {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                    @Suppress("DEPRECATION")
                    AccountManager.get(app).removeAccount(status.account, null, null)
                } else {
                    AccountManager.get(app).removeAccountExplicitly(status.account)
                }
            }
        }*/
        // 登出
        WebThread(WebAPI.Instance::SignOut)
        // 清除 DeviceRepository (藍牙裝置紀錄)
        DeviceRepository.Instance(app).reset()
        // 清除 BandConnector (Band 連線紀錄)
        BandConnector.Instance(app).reset()
        // 清除 HostConnector (Host 連線紀錄)
        HostConnector.Instance(app).reset()
        // 關閉藍牙
        BluetoothAdapter.getDefaultAdapter().disable()
        // 清除 SharedPreferences
        App.GetPreferences(app).edit().clear().apply()
        // 清除 AccountRoom (最後才清 token 和 userId)
        AccountRoom.reset()
        // 清除推播訊息
        val mNotificationManager = app.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.cancelAll()
    }
}