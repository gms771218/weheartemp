package tw.azul.wehear.net.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * 新增血氧物件
 */
public class RequestOximetryEntity {

    /**
     * 使用者id
     */
    @Expose
    @SerializedName("user_id")
    private int user_id;

    @Expose
    @SerializedName("oximetry")
    private List<OximetryEntity> oximetry;


    public RequestOximetryEntity(int user_id) {
        this.user_id = user_id;
        oximetry = new ArrayList<OximetryEntity>();
    }

    public void addOximetry(OximetryEntity entity) {
        oximetry.add(entity);
    }

    public static class OximetryEntity {

        /**
         * 血氧
         */
        @Expose
        @SerializedName("oy_percent")
        private int oy_percent;

        /**
         * 血氧紀錄日期時間
         */
        @Expose
        @SerializedName("oy_datetime")
        private long oy_datetime;

        public OximetryEntity(int oy_percent, long oy_datetime) {
            this.oy_percent = oy_percent;
            this.oy_datetime = oy_datetime;
        }
    }
}
