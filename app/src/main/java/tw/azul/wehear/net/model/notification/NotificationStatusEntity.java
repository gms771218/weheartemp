package tw.azul.wehear.net.model.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 通知狀態
 */
public class NotificationStatusEntity {

    /**
     * 通知狀態id
     */
    @Expose
    @SerializedName("nss_id")
    public int nss_id;

    /**
     * 通知狀態名稱
     */
    @Expose
    @SerializedName("nss_name")
    public String nss_name;

}
