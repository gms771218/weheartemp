package tw.azul.wehear.status.band.clockalarm

import android.os.Build
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_band_alarm_build.*
import tw.azul.wehear.R
import tw.azul.wehear.ble.model.BandAlarmEntity

import tw.azul.wehear.databinding.FragmentBandAlarmBuildBinding

/**
 * 建立、修改手環鬧鐘
 */
class AlarmBuildFragment : Fragment() {

    companion object {
        fun newInstance() = AlarmBuildFragment()
    }

    private lateinit var viewModel: AlarmBuildViewModel
    private lateinit var buildBinding: FragmentBandAlarmBuildBinding
    private var mBandAlarmEntity = BandAlarmEntity()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        buildBinding = FragmentBandAlarmBuildBinding.bind(
            inflater.inflate(
                R.layout.fragment_band_alarm_build,
                container,
                false
            )
        )
        return buildBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AlarmBuildViewModel::class.java)
        buildBinding.viewModel = viewModel
        buildBinding.setLifecycleOwner(viewLifecycleOwner)

        viewModel.alarmLiveData.observe(viewLifecycleOwner::getLifecycle) {
            mBandAlarmEntity = it
        }

        time_picker.setOnTimeChangedListener { view, hourOfDay, minute ->
            viewModel.setHour(hourOfDay)
            viewModel.setMinute(minute)
        }

        ly_alarm_clock_week.setOnClickListener {
            var bundle: Bundle? = null

            Bundle().apply {
                putParcelable("alarm", mBandAlarmEntity)
                bundle = this
            }

            NavHostFragment.findNavController(this)
                .navigate(R.id.action_alarmBuildFragment_to_alarmWeekFragment, bundle)
        }

        ly_alarm_clock_theme.setOnClickListener {
            var bundle: Bundle? = null
            Bundle().apply {
                putParcelable("alarm", mBandAlarmEntity)
                bundle = this
            }

            NavHostFragment.findNavController(this)
                .navigate(R.id.action_alarmBuildFragment_to_alarmThemeFragment, bundle)
        }


        btn_save.setOnClickListener {
            var bundle: Bundle? = null
            Bundle().apply {
                putParcelable("alarm", mBandAlarmEntity)
                bundle = this
            }
            NavHostFragment.findNavController(this).navigate(R.id.action_alarmBuildFragment_to_alarmInfoFragment,bundle)
        }

        arguments?.getParcelable<BandAlarmEntity>("alarm")?.apply {
            viewModel.setAlarm(this)
            setTimePicker(this.hour , this.minute)
        } ?: kotlin.run {
            viewModel.setAlarm(BandAlarmEntity())
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBleStatusLiveData().observe(viewLifecycleOwner::getLifecycle) { bleStatus ->
            if (bleStatus.band.isConnected != true) {
                findNavController().popBackStack()
            }
        }
    }

    private fun setTimePicker(hour: Int, minute: Int) {
        time_picker.setIs24HourView(false)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            time_picker.hour = hour
            time_picker.minute = minute
        } else {
            time_picker.currentMinute = hour
            time_picker.currentMinute = minute
        }
//        time_picker.setOnTimeChangedListener { view, hourOfDay, minute ->
//            viewModel.setHour(hourOfDay)
//            viewModel.setMinute(minute)
//        }
    }


} // class close
