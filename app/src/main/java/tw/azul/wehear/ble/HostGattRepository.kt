@file:Suppress("EXPERIMENTAL_API_USAGE", "SpellCheckingInspection")

package tw.azul.wehear.ble

import android.app.Application
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGatt.GATT_SUCCESS
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import androidx.annotation.CallSuper
import tw.azul.wehear.api.NTS
import tw.azul.wehear.api.NotificationApiRepository
import tw.idv.fy.kotlin.utils.*
import java.util.*

class HostGattRepository {

    companion object {
        /*private*/ val SERVICES_UUID: UUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e")
          private   val NOTIFY_C_UUID: UUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e")
          private   val NOTIFY_D_UUID: UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")
        /*private*/ val WRITE_NO_UUID: UUID = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e")
//        private val Ux55 = 0x55.toUByte()
//        private val Ux00 = 0x00.toUByte()
//        private val Ux01 = 0x01.toUByte()
//        private val Ux02 = 0x02.toUByte()
//        private val Ux03 = 0x03.toUByte()
        val U = tw.idv.fy.kotlin.utils.byteArrayOf(*IntArray(0x100) { i -> i }).toUByteArray()
    }

    @Suppress("UsePropertyAccessSyntax")
    abstract class BluetoothGattCallback(private val app: Application) : android.bluetooth.BluetoothGattCallback() {

        //@CallSuper // 為了AOP, 連線成功前不必強制 CallSuper
        override fun onServicesDiscovered(bluetoothGatt: BluetoothGatt, status: Int) {
            if (status == GATT_SUCCESS) {
                bluetoothGatt
                    .getService(SERVICES_UUID)
                    .getCharacteristic(NOTIFY_C_UUID)
                    .getDescriptor(NOTIFY_D_UUID).apply {
                        setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)//.LogD()
                        bluetoothGatt.writeDescriptor(this)//.LogD()
                    }
            }
        }

        @CallSuper
        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int
        ) {
            "onCharacteristicRead = ${characteristic.value.toUByteArray().toHexStrList()}".LogW()
        }

        @CallSuper
        override fun onCharacteristicWrite(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int
        ) {
            "onCharacteristicWrite = ${characteristic.value.toUByteArray().toHexStrList()} ($status = $GATT_SUCCESS)".LogW()
        }

        //@CallSuper // 為了AOP, 連線成功前不必強制 CallSuper
        override fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic) {
            val uByteArray = characteristic.value.toUByteArray()
            when {
                U[0x55] != uByteArray[0] -> { /* nothing to do */ }
                U[0x01] == uByteArray[1] -> { // 送出裝置識別碼
                    //tw.azul.wehear.App.GetPreferences(app).getString(gatt.device.address, null)?.let {
                    //    tw.azul.wehear.api.DeviceApiRepository.Instance.modify(
                    //        it,
                    //        "dt_id" to 5,
                    //        "device_unique" to uByteArray.toHexStrList().slice(3..8).joinToString(separator = "")
                    //    )
                    //}
                    "應該不會進到這, 因為之前的AOP就消化掉".LogA()
                }
                U[0x03] == uByteArray[1] -> { // App 發送通知
                    when (uByteArray[10]) {
                        // 門鈴
                        U[0x00] -> NTS.門鈴
                        // 電話
                        U[0x01] -> NTS.電話
                        // 警示器
                        U[0x02] -> NTS.遙控器
                        // 火災
                        U[0x03] -> NTS.火災
                        else -> null
                    }?.let { nts ->
                        NotificationApiRepository.Instance(app).emit(
                            nts,
                            uByteArray.toHexStrList().joinToString(separator = "")
                        )
                    }
                }
            }
            "onCharacteristicChanged = ${uByteArray.toHexStrList()}".LogW()
        }

        @CallSuper
        override fun onDescriptorWrite(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {
            "onDescriptorWrite = $status (GATT_SUCCESS = $GATT_SUCCESS)".LogW()
            when (status) {
                GATT_SUCCESS -> {
                    //descriptor.value.toUByteArray().contentToString().LogI()
                    gatt.getService(SERVICES_UUID).apply {
                        getCharacteristic(NOTIFY_C_UUID)
                            .apply {
                                gatt.setCharacteristicNotification(this, true)//.LogD()
                            }
                        //// 交由 AOP 先消化
                        //getCharacteristic(WRITE_NO_UUID)
                        //    .apply {
                        //        setValue("aa0106ffffffffffff0d".toBleCommByteArr())//.LogD()
                        //        gatt.writeCharacteristic(this)//.LogD()
                        //    }
                    }
                }
            }
        }

        @CallSuper
        override fun onDescriptorRead(gatt: BluetoothGatt?, descriptor: BluetoothGattDescriptor?, status: Int) {
            "onDescriptorRead = $status".LogW()
        }
    }

}