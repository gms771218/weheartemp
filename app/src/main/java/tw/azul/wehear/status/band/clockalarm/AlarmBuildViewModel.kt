package tw.azul.wehear.status.band.clockalarm

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import tw.azul.wehear.ble.AccessoryRepository
import tw.azul.wehear.ble.model.BandAlarmEntity

class AlarmBuildViewModel(private val app: Application) : AndroidViewModel(app) {

    private var bandAlarmEntity = BandAlarmEntity()

    // 鬧鐘重複
    var weeks: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }
    // 鬧鐘主題
    var theme: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }

    var alarmLiveData : MutableLiveData<BandAlarmEntity> = MutableLiveData<BandAlarmEntity>().apply {
        value = bandAlarmEntity
    }

    fun setAlarm(alarmEntity: BandAlarmEntity){
        bandAlarmEntity = alarmEntity
        bandAlarmEntity.enable = true // 直接開啟
        setWeeks(bandAlarmEntity.weeks)
        setTheme(bandAlarmEntity.themeName)
        setHour(bandAlarmEntity.hour)
        setMinute(bandAlarmEntity.minute)
    }

    private fun setWeeks(array: BooleanArray = BooleanArray(7)) {
        bandAlarmEntity.weeks = array
        weeks.postValue(bandAlarmEntity.toWeek())
        alarmLiveData.postValue(bandAlarmEntity)
    }

    private fun setTheme(value: String) {
        bandAlarmEntity.themeName = value
        theme.postValue(bandAlarmEntity.toThemeName())
        alarmLiveData.postValue(bandAlarmEntity)
    }

    fun setHour(value:Int){
        bandAlarmEntity.hour = value
        weeks.postValue(bandAlarmEntity.toWeek()) // 更新是否為今天或明天
        alarmLiveData.postValue(bandAlarmEntity)
    }

    fun setMinute(value:Int){
        bandAlarmEntity.minute = value
        weeks.postValue(bandAlarmEntity.toWeek()) // 更新是否為今天或明天
        alarmLiveData.postValue(bandAlarmEntity)
    }

    fun getBleStatusLiveData(): LiveData<AccessoryRepository.BleStatus> =
        AccessoryRepository.Instance(app).getBleStatusLiveData()
}
