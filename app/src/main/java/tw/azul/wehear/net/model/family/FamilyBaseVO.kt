package tw.azul.wehear.net.model.family


open class FamilyBaseVO {


    // 類型 : 0:家人 ; 1:身障 ; 2 : 新請求資訊
    private var type: Int = 0


    companion object {
        fun createEmptyUser(): FamilyBaseVO {
            var _user = FamilyBaseVO()
            _user.type = HEADER_TYPE
            return _user
        }

        const val HEADER_TYPE = 0x1000;
        const val USER_FAMILY = 0;
        const val USER_DEAF = 1;
        const val REQUEST_ADD_IN = 2;
    }

    open fun setType(type : Int){
        this.type = type
    }

    open fun getType(): Int {
        return type;
    }

}