package tw.azul.wehear.net.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import com.google.gson.Gson;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tw.azul.wehear.net.AppExecutors;
import tw.azul.wehear.net.NetworkBoundResource;
import tw.azul.wehear.net.api.ApiResponse;
import tw.azul.wehear.net.api.DailyInfoApi;

import tw.azul.wehear.net.model.base.Resource;
import tw.azul.wehear.net.model.base.ResponseIdResultEntity;
import tw.azul.wehear.net.model.base.SingleEntity;
import tw.azul.wehear.net.model.dailyInfo.HealthRecordEntity;
import tw.azul.wehear.net.model.request.RequestHeartRateEntity;
import tw.azul.wehear.net.model.request.RequestOximetryEntity;
import tw.azul.wehear.net.model.request.RequestSleepEntity;
import tw.azul.wehear.net.model.request.RequestStepEntity;
import tw.azul.wehear.net.retrofit.RetrofitClient;

/**
 * 日常紀錄
 */
public class DailyRepository {

    private static DailyRepository instance;

    private AppExecutors appExecutors;

    private DailyInfoApi dailyInfoApi;

    public static DailyRepository getInstance() {
        if (instance == null) {
            throw new ExceptionInInitializerError("Not Initializer , Please call initialization method");
        }
        return instance;
    }

    public static void initialization(AppExecutors appExecutors) {
        if (instance == null) {
            synchronized (DailyRepository.class) {
                if (instance == null) {
                    instance = new DailyRepository();
                    instance.appExecutors = appExecutors;
                    instance.dailyInfoApi = RetrofitClient.getInstance().createEndpoint(DailyInfoApi.class);
                }
            }
        }
    }


    /**
     * 取得健康紀錄
     *
     * @param user_id  使用者id
     * @param datetime 時間
     * @return
     */
    public LiveData<Resource<HealthRecordEntity>> getHealthRecord(final int user_id, final String datetime) {

        return new NetworkBoundResource<HealthRecordEntity, SingleEntity<HealthRecordEntity>>(appExecutors) {

            MediatorLiveData<HealthRecordEntity> liveData;

            @Override
            protected void saveCallResult(SingleEntity<HealthRecordEntity> item) {
                if (item.data != null)
                    liveData.postValue(item.data);
            }

            @Override
            protected boolean shouldFetch(@Nullable HealthRecordEntity data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<HealthRecordEntity> loadFromDb() {
                if (liveData == null) {
                    liveData = new MediatorLiveData<HealthRecordEntity>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<SingleEntity<HealthRecordEntity>>> createCall() {
                return dailyInfoApi.getHealthRecord(user_id, datetime);
            }
        }.asLiveData();

    }

    /**
     * 上傳心率測量結果
     *
     * @param requestHeartRateEntity
     * @return
     */
    public LiveData<Resource<ResponseIdResultEntity>> postHealthRecord(final RequestHeartRateEntity requestHeartRateEntity) {

        return new NetworkBoundResource<ResponseIdResultEntity, SingleEntity<ResponseIdResultEntity>>(appExecutors) {

            MediatorLiveData<ResponseIdResultEntity> liveData;

            @Override
            protected void saveCallResult(SingleEntity<ResponseIdResultEntity> item) {
                item.data.msg = item.msg;
                item.data.status = item.status ;
                liveData.postValue(item.data);
            }

            @Override
            protected boolean shouldFetch(@Nullable ResponseIdResultEntity data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<ResponseIdResultEntity> loadFromDb() {
                if (liveData == null) {
                    liveData = new MediatorLiveData<ResponseIdResultEntity>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>> createCall() {
                String jsonString = new Gson().toJson(requestHeartRateEntity);
                RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonString);
                return dailyInfoApi.postHeartRates(body);
            }
        }.asLiveData();
    }


    /**
     * 上傳血氧測量結果
     * @param requestOximetryEntity
     * @return
     */
    public LiveData<Resource<ResponseIdResultEntity>> postOximetryRecord(final RequestOximetryEntity requestOximetryEntity) {
        return new NetworkBoundResource<ResponseIdResultEntity, SingleEntity<ResponseIdResultEntity>>(appExecutors) {

            MediatorLiveData<ResponseIdResultEntity> liveData;

            @Override
            protected void saveCallResult(SingleEntity<ResponseIdResultEntity> item) {
                item.data.msg = item.msg;
                item.data.status = item.status ;
                liveData.postValue(item.data);
            }

            @Override
            protected boolean shouldFetch(@Nullable ResponseIdResultEntity data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<ResponseIdResultEntity> loadFromDb() {
                if (liveData == null) {
                    liveData = new MediatorLiveData<ResponseIdResultEntity>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>> createCall() {
                String jsonString = new Gson().toJson(requestOximetryEntity);
                RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonString);
                return dailyInfoApi.postOximetry(body);
            }
        }.asLiveData();
    }

    /**
     * 上傳睡眠紀錄
     * @param requestSleepEntity
     * @return
     */
    public LiveData<Resource<ResponseIdResultEntity>> postSleepRecord(final RequestSleepEntity requestSleepEntity) {
        return new NetworkBoundResource<ResponseIdResultEntity, SingleEntity<ResponseIdResultEntity>>(appExecutors) {

            MediatorLiveData<ResponseIdResultEntity> liveData;

            @Override
            protected void saveCallResult(SingleEntity<ResponseIdResultEntity> item) {
                item.data.msg = item.msg;
                item.data.status = item.status ;
                liveData.postValue(item.data);
            }

            @Override
            protected boolean shouldFetch(@Nullable ResponseIdResultEntity data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<ResponseIdResultEntity> loadFromDb() {
                if (liveData == null) {
                    liveData = new MediatorLiveData<ResponseIdResultEntity>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>> createCall() {
                String jsonString = new Gson().toJson(requestSleepEntity);
                RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonString);
                return dailyInfoApi.postSleep(body);
            }
        }.asLiveData() ;
    }

    /**
     * 上傳步數紀錄
     * @param requestStepEntity
     * @return
     */
    public LiveData<Resource<ResponseIdResultEntity>> postStepRecord(final RequestStepEntity requestStepEntity) {
        return new NetworkBoundResource<ResponseIdResultEntity, SingleEntity<ResponseIdResultEntity>>(appExecutors) {

            MediatorLiveData<ResponseIdResultEntity> liveData;

            @Override
            protected void saveCallResult(SingleEntity<ResponseIdResultEntity> item) {
                item.data.msg = item.msg;
                item.data.status = item.status ;
                liveData.postValue(item.data);
            }

            @Override
            protected boolean shouldFetch(@Nullable ResponseIdResultEntity data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<ResponseIdResultEntity> loadFromDb() {
                if (liveData == null) {
                    liveData = new MediatorLiveData<ResponseIdResultEntity>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>> createCall() {
                String jsonString = new Gson().toJson(requestStepEntity);
                RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonString);
                return dailyInfoApi.postSteps(body);
            }
        }.asLiveData() ;
    }

} // class close
