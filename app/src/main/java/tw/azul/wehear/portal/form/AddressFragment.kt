package tw.azul.wehear.portal.form

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.fragment_portal_address.*
import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.databinding.FragmentPortalAddressBinding

class AddressFragment : Fragment() {

    companion object {
        fun newInstance() = AddressFragment()
    }

    private lateinit var dataBinding: FragmentPortalAddressBinding
    private lateinit var viewModel: AddressViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding =  FragmentPortalAddressBinding.inflate(inflater)
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AddressViewModel::class.java)
        dataBinding.viewmodel = viewModel
        dataBinding.setLifecycleOwner(viewLifecycleOwner)
        viewModel.getUser(AccountRoom.TokenLiveData.value?.userId!!).observe(viewLifecycleOwner , Observer {})
        btn_next.setOnClickListener {
            if (value_tall.valid()){
                viewModel.saveUser(AccountRoom.TokenLiveData.value?.userId!!).observe(viewLifecycleOwner , Observer {})
                NavHostFragment.findNavController(this).navigate(R.id.action_addressFragment_to_goalFragment)
            }
        }
    }

}
