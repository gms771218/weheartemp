package tw.azul.wehear.net.model.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gms on 2018/4/16.
 *
 * Entity 主要的資料類型
 *
 * format : {"status":,"msg":[],"data":[ EntityType ]}
 *
 */

public class ListEntity<EntityType> extends StatusCodeBase {

    @Expose
    @SerializedName("data")
    public List<EntityType> data;


}
