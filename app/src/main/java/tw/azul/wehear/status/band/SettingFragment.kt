package tw.azul.wehear.status.band

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import tw.azul.wehear.MainActivity
import tw.azul.wehear.databinding.FragmentBandSettingBinding

class SettingFragment : Fragment() {

    companion object {
        fun newInstance() = SettingFragment()
    }

    private lateinit var databinding: FragmentBandSettingBinding
    private lateinit var viewModel: SettingViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        databinding = FragmentBandSettingBinding.inflate(inflater)
        return databinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        databinding.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SettingViewModel::class.java)
        // 觀察連線狀況
        viewModel.getBleStatusLiveData().observe(viewLifecycleOwner::getLifecycle) { bleStatus ->
            val isEnabled = bleStatus.band.run { isPaired == true && isConnected == true }
            databinding.isConnected = isEnabled
        }
        // 注入 viewmodel
        databinding.viewmodel = viewModel
        // 決定 Toolbar 行為
        (activity as? MainActivity)?.showToolbarAndHideNavigation()
    }
}
