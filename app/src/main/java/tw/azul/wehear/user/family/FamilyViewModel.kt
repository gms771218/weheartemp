@file:Suppress("SpellCheckingInspection", "PropertyName")

package tw.azul.wehear.user.family

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import tw.azul.wehear.net.model.base.Resource
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.family.FamilyEntity
import tw.azul.wehear.net.repository.FamilyRepository

open class FamilyViewModel : ViewModel() {

    private var familyRepository: FamilyRepository = FamilyRepository.getInstance()

    fun getFamilys(user_id: Int): LiveData<Resource<List<FamilyEntity>>> {
        return familyRepository.getFamilys(user_id)
    }

    fun getFamily(user_id: Int, user_phone: String): LiveData<Resource<FamilyEntity>> {
        return familyRepository.getFamily(user_id, user_phone)
    }

    fun modifyFamily(
        user_id: Int,
        user_id_family: Int,
        fts_id: Int,
        fs_nickname: String,
        fs_accept: Int
    ): LiveData<ModifyResult> = familyRepository.postFamily(user_id, user_id_family, fts_id, fs_nickname, fs_accept).run {
        Transformations.map(this) {
            ModifyResult().apply{
                this.fs_accept = fs_accept
                this.isSuccess = when (it.status) {
                    Status.SUCCESS ->  true
                    Status.ERROR -> false
                    else -> null
                }
            }
        }
    }

    data class ModifyResult(var isSuccess: Boolean? = null, var fs_accept: Int = -1) {
        override fun toString() = "ModifyResult(isSuccess=$isSuccess, fs_accept=$fs_accept)"
    }

} // class close
