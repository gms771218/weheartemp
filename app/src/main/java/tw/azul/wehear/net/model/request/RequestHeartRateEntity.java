package tw.azul.wehear.net.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * 新增心率物件
 */
public class RequestHeartRateEntity {

    @Expose
    @SerializedName("heart_rates")
    private List<HeartRatesEntity> heart_rates;
    @Expose
    @SerializedName("user_id")
    private int user_id;

    public RequestHeartRateEntity(int user_id) {
        this.user_id = user_id;
        heart_rates = new ArrayList<HeartRatesEntity>();
    }

    public void addHeartRate(HeartRatesEntity entity){
        heart_rates.add(entity);
    }


    public static class HeartRatesEntity {
        /**
         * 心率
         */
        @Expose
        @SerializedName("hr_bpm")
        private int hr_bpm;

        /**
         * 心率紀錄日期時間(秒)。 ex:1514764800
         */
        @Expose
        @SerializedName("hr_datetime")
        private long hr_datetime;

        public HeartRatesEntity(int hr_bpm, long hr_datetime) {
            this.hr_datetime = hr_datetime;
            this.hr_bpm = hr_bpm;
        }
    }
}
