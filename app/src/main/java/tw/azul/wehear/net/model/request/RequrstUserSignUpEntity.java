package tw.azul.wehear.net.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 使用者註冊物件
 */
public class RequrstUserSignUpEntity {


    /**
     * 使用者帳號(手機)
     */
    @Expose
    @SerializedName("user_account")
    public String user_account;

    /**
     * 使用者密碼
     */
    @Expose
    @SerializedName("user_password")
    public String user_password;

    /**
     * 使用者姓名
     */
    @Expose
    @SerializedName("user_name")
    public String user_name;

    /**
     * 使用者性別是否為男性(1或0)
     */
    @Expose
    @SerializedName("user_is_male")
    public int user_is_male;

    /**
     * 使用者生日年
     */
    @Expose
    @SerializedName("user_birthday_year")
    public int user_birthday_year;

    /**
     * 使用者生日月
     */
    @Expose
    @SerializedName("user_birthday_month")
    public int user_birthday_month;

    /**
     * 使用者生日日
     */
    @Expose
    @SerializedName("user_birthday_day")
    public int user_birthday_day;

    /**
     * 使用者是否有帶助聽器(1或0)
     */
    @Expose
    @SerializedName("user_aid")
    public int user_aid;


}
