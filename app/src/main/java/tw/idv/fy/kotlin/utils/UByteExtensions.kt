package tw.idv.fy.kotlin.utils

/**
    Kotlin Extension used by Self
 **/

// 16 進位表示文字 轉 UByte, 例如: "FF" → UByte(0xFF)
fun String.toUByteByRadix16() = toUByte(16)

// UByte 轉 16 進位表示文字, 例如: UByte(0x0F) → "0F"
fun UByte.toStringByRadix16() = "%02X".format(toInt())

// UByteArray 轉 16 進位表示文字 串列(List)
fun UByteArray.toHexStrList() = map(UByte::toStringByRadix16)

// 16 進位表示文字 轉 Android Ble Comm 使用的型式: ByteArray
fun String.toBleCommByteArr() = chunked(2).map(String::toUByteByRadix16).map(UByte::toByte).toByteArray()

// 整數陣列 轉 ByteArray
fun byteArrayOf(vararg elements: Int) = elements.map(Int::toByte).toByteArray()