@file:Suppress("FunctionName")

package tw.azul.wehear.ble

import android.app.Application
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import tw.azul.wehear.App
import tw.azul.wehear.api.DeviceApiRepository
import tw.azul.wehear.api.WebAPI
import tw.idv.fy.kotlin.utils.LogE
import tw.idv.fy.kotlin.utils.LogW

class DeviceRepository private constructor(val application: Application) {

    companion object {
        private const val BAND = "BAND"
        private const val HOST = "HOST"
        @Volatile
        private var singleton: DeviceRepository? = null
        fun Instance(application: Application): DeviceRepository = singleton ?: synchronized(Companion) {
            singleton = singleton ?: DeviceRepository(application)
            return singleton!!
        }
    }

    val bandLiveData = object: MutableLiveData<BluetoothDevice>() {
        init {
            value?.also(::postValue)
        }
        override fun setValue(value: BluetoothDevice?) {
            super.setValue(value.save(BAND))
        }

        override fun postValue(value: BluetoothDevice?) {
            super.postValue(value.save(BAND))
        }

        override fun getValue(): BluetoothDevice? {
            return super.getValue().restore(BAND)
        }
    }
    val bandListLiveData = MutableLiveData<List<BluetoothDevice>>()

    val hostLiveData = object: MutableLiveData<BluetoothDevice>() {
        init {
            value?.also(::postValue)
        }
        override fun setValue(value: BluetoothDevice?) {
            super.setValue(value.save(HOST))
        }

        override fun postValue(value: BluetoothDevice?) {
            super.postValue(value.save(HOST))
        }

        override fun getValue(): BluetoothDevice? {
            return super.getValue().restore(HOST)
        }
    }
    val hostListLiveData = MediatorLiveData<List<BluetoothDevice>>()

    /**
     * 確認裝置
     */
    fun check() {
        DeviceApiRepository.Instance.devices().apply {
            observeForever(object : Observer<WebAPI.ResponseType> {
                override fun onChanged(response: WebAPI.ResponseType) {
                    removeObserver(this)
                    (response as? WebAPI.ResponseType.Success)
                        ?.run { data as? Map<*, *> }
                        ?.run { get("devices") as? List<*> }
                        ?.mapNotNull { item -> item.let { it as? Map<*, *> }?.get("device_id") as? Number }
                        ?.map(Number::toInt)
                        ?.map(Int::toString)
                        ?.also { deviceIdList ->
                            // 檢查 Band 的 deviceId 是否有在清單裡
                            BandConnector.Instance(application).takeUnless { bandConnector ->
                                bandConnector.check(bandLiveData.value, deviceIdList)
                            }?.apply {
                                reset() // BandConnector 的
                                resetBand()
                                "使用者沒該Band".LogE()
                            }?:run {
                                "使用者有該Band".LogW()
                            }
                            // 檢查 Host 的 deviceId 是否有在清單裡
                            HostConnector.Instance(application).takeUnless { hostConnector ->
                                hostConnector.check(hostLiveData.value, deviceIdList)
                            }?.apply {
                                reset() // hostConnector 的
                                resetHost()
                                "使用者沒該Host".LogE()
                            }?:run {
                                "使用者有該Host".LogW()
                            }
                        }
                }
            })
        }
    }

    fun resetBand() {
        bandLiveData.postValue(null)
        bandListLiveData.postValue(emptyList())
    }

    fun resetHost() {
        hostLiveData.postValue(null)
        hostListLiveData.postValue(emptyList())
    }

    fun reset() {
        resetBand()
        resetHost()
    }

    private fun BluetoothDevice?.save(key: String): BluetoothDevice? = apply {
        App.GetPreferences(application).edit().putString(key, this?.toString()).apply()
    }

    private fun BluetoothDevice?.restore(key: String): BluetoothDevice? =
        App.GetPreferences(application).getString(key, null)
            ?.run{
                BluetoothAdapter.getDefaultAdapter().getRemoteDevice(this)
            }
            ?:this
}