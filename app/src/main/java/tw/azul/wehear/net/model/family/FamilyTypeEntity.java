package tw.azul.wehear.net.model.family;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 家人類型
 */
public class FamilyTypeEntity {

    /**
     * 家人類型id
     */
    @Expose
    @SerializedName("fts_id")
    private int fts_id;

    /**
     * 家人類型名稱
     */
    @Expose
    @SerializedName("fts_name")
    private String fts_name;

}
