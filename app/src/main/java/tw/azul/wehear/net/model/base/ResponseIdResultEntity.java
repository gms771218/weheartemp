package tw.azul.wehear.net.model.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 之後將所有Response id 整合到這裡
 * Created by gms on 2018/6/9.
 * <p>
 * 處理只有id的回傳
 */

public class ResponseIdResultEntity extends StatusCodeBase {

    /**
     * fs_id 家人id
     * user_id 使用者id
     */
    @Expose
    @SerializedName(value = "id", alternate = {"fs_id" , "user_id"})
    public int id;

}

