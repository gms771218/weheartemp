package tw.azul.wehear

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.google.gson.Gson

class PortalActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_portal)
        val host = NavHostFragment.create(R.navigation.nav_portal)
        supportFragmentManager.beginTransaction()
                .setPrimaryNavigationFragment(host)
                .replace(R.id.portalContainer, host)
                .commitNow()

//        checkHasNotification()

        // 關注網路
        ViewModelProviders.of(this).get(NetworkCheckViewModel::class.java).apply {
            val alertDialog: AlertDialog = AlertDialog
                .Builder(this@PortalActivity, R.style.Theme_MaterialComponents_Light_Dialog_Alert).run {
                    setCancelable(false)
                    setTitle("提示")
                    setMessage("請重新確認網路連線")
                    setNegativeButton("確定", null)
                    create()
                }
            isNetworkAvailableLiveData.observe(this@PortalActivity, object : Observer<Boolean> {
                init {
                    alertDialog.setOnDismissListener {
                        isNetworkAvailableLiveData.observe(this@PortalActivity, this)
                    }
                }
                override fun onChanged(available: Boolean?) {
                    when (available) {
                        true -> {/* 有網路 */}
                        false -> {
                            if (!alertDialog.isShowing) {
                                isNetworkAvailableLiveData.removeObserver(this)
                                alertDialog.show()
                            }
                        }
                        null -> {
                            alertDialog.setOnDismissListener(null)
                            alertDialog.dismiss()
                        }
                    }
                }
            })
        }
    }

    /**
     * 檢查推播(背景推送打開app時檢查)
     */
    @Suppress("unused", "ReplacePutWithAssignment", "CanBeVal", "UNNECESSARY_SAFE_CALL",
        "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "RemoveCurlyBracesFromTemplate"
    )
    fun checkHasNotification() {
        var bundle: Bundle? = intent?.extras
        if (bundle != null) {
            var map = HashMap<String, String>()
            for (key in bundle?.keySet()!!) {
                map.put(key, bundle.getString(key))
            }
            if (map.size == 0) return
            Gson().toJson(map)?.apply {
                val intent = Intent()
                intent.action = "${packageName}.NOTIFICATION"
                intent.putExtra("data", this)
                sendBroadcast(intent)
            }
        }
    }

    override fun onBackPressed() {
        if (findNavController(R.id.portalContainer).navigateUp()) return
        AlertDialog.Builder(this, R.style.Theme_MaterialComponents_Light_Dialog_Alert).apply {
            setCancelable(false)
            //setTitle("提示")
            setMessage("關閉 App")
            setOnDismissListener { finish() }
            Handler().postDelayed(show()::dismiss, 1000)
        }
    }
}
