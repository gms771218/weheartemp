package tw.azul.wehear.net.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tw.azul.wehear.App;
import tw.azul.wehear.net.AppExecutors;
import tw.azul.wehear.net.NetworkBoundResource;
import tw.azul.wehear.net.api.ApiResponse;
import tw.azul.wehear.net.api.UserApi;
import tw.azul.wehear.net.api.VersionApi;
import tw.azul.wehear.net.model.base.ListEntity;
import tw.azul.wehear.net.model.base.Resource;
import tw.azul.wehear.net.model.base.ResponseIdResultEntity;
import tw.azul.wehear.net.model.base.SingleEntity;
import tw.azul.wehear.net.model.request.RequestUpdateUserEntity;
import tw.azul.wehear.net.model.user.UserEntity;
import tw.azul.wehear.net.model.version.VersionEntity;
import tw.azul.wehear.net.retrofit.RetrofitClient;

import java.lang.reflect.Type;
import java.util.*;

public class VersionRepository {

    public static final String KEY_USER_STRIDE_LENGTH = "key.user.stride_length";

    private static VersionRepository instance;

    private AppExecutors appExecutors;

    private VersionApi versionApi;

    public static VersionRepository getInstance() {
        if (instance == null) {
            throw new ExceptionInInitializerError("Not Initializer , Please call initialization method");
        }
        return instance;
    }

    public static void initialization(AppExecutors appExecutors) {
        if (instance == null) {
            synchronized (VersionRepository.class) {
                if (instance == null) {
                    instance = new VersionRepository();
                    instance.appExecutors = appExecutors;
                    instance.versionApi = RetrofitClient.getInstance().createEndpoint(VersionApi.class);
                }
            }
        }
    }

    /**
     * 取得版本資訊
     * @return
     */
    public LiveData<Resource<VersionEntity>> getVersion() {

        return new NetworkBoundResource<VersionEntity, ListEntity<VersionEntity>>(appExecutors){

            MutableLiveData<VersionEntity> liveData ;

            @Override
            protected void saveCallResult(ListEntity<VersionEntity> item) {
                if (item.data != null) {
                    List<VersionEntity> list = item.data ;
                    Collections.sort(list, new Comparator<VersionEntity>() {
                        @Override
                        public int compare(VersionEntity o1, VersionEntity o2) {
                            return o2.vs_id - o1.vs_id;
                        }
                    });
                    VersionEntity versionEntity = null;
                    while (list.size()>0 ) {
                        versionEntity = list.remove(0) ;
                        if (versionEntity.vts_id == 2) {
                            break;
                        }
                    }
                    liveData.postValue(versionEntity);
                } else {
                    //                    liveData.postValue(new ArrayList<VersionEntity>());
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable VersionEntity data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<VersionEntity> loadFromDb() {
                if (liveData == null) {
                    liveData = new MutableLiveData<VersionEntity>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<ListEntity<VersionEntity>>> createCall() {
                return versionApi.getVersions();
            }
        }.asLiveData();
    }




} // class close
