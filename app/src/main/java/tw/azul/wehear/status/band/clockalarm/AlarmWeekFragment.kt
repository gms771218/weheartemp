package tw.azul.wehear.status.band.clockalarm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_band_alarm_week.*
import tw.azul.wehear.R
import tw.azul.wehear.ble.model.BandAlarmEntity
import tw.azul.wehear.databinding.FragmentBandAlarmWeekBinding


/**
 * 修改手環鬧鐘時間
 */
class AlarmWeekFragment : Fragment() {

    companion object {
        fun newInstance() = AlarmWeekFragment()
    }

    private lateinit var viewModel: AlarmWeekViewModel
    private lateinit var databinding: FragmentBandAlarmWeekBinding

    private lateinit var bandAlarmEntity: BandAlarmEntity
    private lateinit var weeks: BooleanArray

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        databinding = FragmentBandAlarmWeekBinding.bind(inflater.inflate(R.layout.fragment_band_alarm_week, container, false))
        return databinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AlarmWeekViewModel::class.java)
        databinding.viewModel = viewModel
        databinding.setLifecycleOwner(viewLifecycleOwner)


        bandAlarmEntity = arguments?.getParcelable<BandAlarmEntity>("alarm")?:kotlin.run { BandAlarmEntity() }

        viewModel.observerResult(bandAlarmEntity.weeks).observe(viewLifecycleOwner::getLifecycle){}

        btn_check.setOnClickListener {
//            viewModel.observerResult(bandAlarmEntity.weeks).observe(viewLifecycleOwner::getLifecycle) { arrays ->
//
//            }

            bandAlarmEntity.weeks = viewModel.weekLiveData.value!!
            NavHostFragment.findNavController(this).navigate(R.id.action_alarmWeekFragment_to_alarmBuildFragment,
                Bundle().apply {
                    putParcelable("alarm", bandAlarmEntity)
                });


//            viewModel.weekLiveData.observe(viewLifecycleOwner::getLifecycle){ arrays ->
//                bandAlarmEntity.weeks = arrays
//                viewModel.weekLiveData.removeObservers(viewLifecycleOwner)
//                NavHostFragment.findNavController(this).navigate(R.id.action_alarmWeekFragment_to_alarmBuildFragment,
//                    Bundle().apply {
//                        putParcelable("alarm", bandAlarmEntity)
//                    });
//            }

        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBleStatusLiveData().observe(viewLifecycleOwner::getLifecycle) { bleStatus ->
            if (bleStatus.band.isConnected != true) {
                findNavController().popBackStack()
            }
        }
    }

}
