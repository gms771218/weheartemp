package tw.azul.wehear.status.host

import android.bluetooth.BluetoothDevice
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_host_setting_search.*
import tw.azul.wehear.MainActivity
import tw.azul.wehear.R
import tw.azul.wehear.databinding.FragmentHostSettingSearchBinding

class SearchFragment : Fragment() {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private lateinit var dataBinding: FragmentHostSettingSearchBinding
    private lateinit var viewModel: SearchViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_host_setting_search, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        dataBinding.viewmodel = viewModel
        // 決定 Toolbar 行為
        (activity as? MainActivity)?.showToolbarAndHideNavigation()
    }

    private lateinit var observer:Observer<Boolean>

    override fun onResume() {
        super.onResume()
        observer = object: Observer<Boolean> {
            override fun onChanged(bleStatus: Boolean) {
                viewModel.bleStatusDelObserver(this)
                if (bleStatus) {
                    viewModel.searchScanCallbackObserve(viewLifecycleOwner, Observer { set ->
                        when {
                            set?.size ?: 0 != 0 -> {
                                findNavController().navigate(R.id.action_hostSearchFragment_to_hostChooseFragment)
                            }
                            System.currentTimeMillis().minus(viewModel.start_time) < 30 * 1000L -> {
                                viewModel.searchAgain()
                            }
                        }
                    })
                }
            }
        }
        viewModel.bleStatusAddObserver(viewLifecycleOwner, observer)
    }

    override fun onPause() {
        super.onPause()
        if (::observer.isInitialized) {
            viewModel.bleStatusDelObserver(observer)
            viewModel.removeScanCallbackObserve(viewLifecycleOwner)
        }
    }

    override fun onStart() {
        super.onStart()
        // 開始動畫
        (logo.background as? Animatable) ?.start()
    }
}
