package tw.azul.feature.ble.repository.example

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import tw.azul.feature.ble.repository.BleRepository

class MainActivityViewModel(application: Application) : AndroidViewModel(application) {
    init {
        BleRepository.Instance(getApplication())
    }
}
