package tw.azul.wehear.net.model.version;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * 版本
 */
public class VersionEntity {

    /**
     * 版本類型id
     */
    @Expose
    @SerializedName("vts_id")
    public int vts_id;

    /**
     * 版本類型名稱
     */
    @Expose
    @SerializedName("vts_name")
    public String vts_name;

    /**
     * 版本id
     */
    @Expose
    @SerializedName("vs_id")
    public int vs_id;

    /**
     * 版本名稱
     */
    @Expose
    @SerializedName("vs_name")
    public String vs_name;

    /**
     * 版本號
     */
    @Expose
    @SerializedName("vs_number")
    public int vs_number;

    /**
     * 版本時間
     */
    @Expose
    @SerializedName("vs_datetime")
    public int vs_datetime;

    /**
     * 是否強制更新
     */
    @Expose
    @SerializedName("vs_force")
    public int vs_force;
    @Expose
    @SerializedName("created_at")
    public String created_at;
    @Expose
    @SerializedName("updated_at")
    public String updated_at;


}
