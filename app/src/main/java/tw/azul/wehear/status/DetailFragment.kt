package tw.azul.wehear.status

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import tw.azul.wehear.MainActivity
import tw.azul.wehear.R
import tw.azul.wehear.api.NTS
import tw.azul.wehear.api.NotificationApiRepository
import tw.azul.wehear.databinding.FragmentMainStatusDetailBinding

class DetailFragment : Fragment() {

    companion object {
        fun newInstance() = DetailFragment()
    }

    private lateinit var dataBinding: FragmentMainStatusDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_status_detail, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ViewModelProviders.of(this).get(DetailViewModel::class.java).apply {
            enabledLiveData.observe(viewLifecycleOwner::getLifecycle) { ns_enable ->
                NTS.get(nts_id)?.let { nts ->
                    NotificationApiRepository.Instance(app).notification(nts = nts, ns_enable = ns_enable)
                }
            }
            nts_id = DetailFragmentArgs.fromBundle(arguments).ntsId
            dataBinding.viewModel = this
        }
        // 決定 Toolbar 行為
        (activity as? MainActivity)?.showToolbarAndHideNavigation()
    }

}
