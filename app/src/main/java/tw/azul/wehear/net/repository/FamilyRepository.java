package tw.azul.wehear.net.repository;

import androidx.annotation.IntRange;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tw.azul.wehear.net.AppExecutors;
import tw.azul.wehear.net.NetworkBoundResource;
import tw.azul.wehear.net.api.ApiResponse;
import tw.azul.wehear.net.api.FamilyApi;
import tw.azul.wehear.net.model.base.ListEntity;
import tw.azul.wehear.net.model.base.ResponseIdResultEntity;
import tw.azul.wehear.net.model.family.FamilyEntity;
import tw.azul.wehear.net.model.base.Resource;
import tw.azul.wehear.net.model.base.SingleEntity;
import tw.azul.wehear.net.retrofit.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

/**
 * 家人
 */
public class FamilyRepository {

    private static FamilyRepository instance;

    private AppExecutors appExecutors;

    private FamilyApi familyApi;

    public static FamilyRepository getInstance() {
        if (instance == null) {
            throw new ExceptionInInitializerError("Not Initializer , Please call initialization method");
        }
        return instance;
    }

    public static void initialization(AppExecutors appExecutors) {
        if (instance == null) {
            synchronized (FamilyRepository.class) {
                if (instance == null) {
                    instance = new FamilyRepository();
                    instance.appExecutors = appExecutors;
                    instance.familyApi = RetrofitClient.getInstance().createEndpoint(FamilyApi.class);
                }
            }
        }
    }

    /**
     * 取得家人
     *
     * @return
     */
    public LiveData<Resource<FamilyEntity>> getFamily(final int user_id, final String user_phone) {

        return new NetworkBoundResource<FamilyEntity, SingleEntity<FamilyEntity>>(appExecutors) {

            MutableLiveData<FamilyEntity> liveData;

            @Override
            protected void saveCallResult(SingleEntity<FamilyEntity> item) {
                if (item.data != null) {
                    liveData.postValue(item.data);
                } else
                    liveData.postValue(null);
            }

            @Override
            protected boolean shouldFetch(@Nullable FamilyEntity data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<FamilyEntity> loadFromDb() {
                if (liveData == null) {
                    liveData = new MutableLiveData<FamilyEntity>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<SingleEntity<FamilyEntity>>> createCall() {
                return familyApi.getFamily(user_id, user_phone);
            }

            @Override
            protected void onFetchFailed() {
                super.onFetchFailed();
            }
        }.asLiveData();
    }


    /**
     * 取得家人列表
     *
     * @return
     */
    public LiveData<Resource<List<FamilyEntity>>> getFamilys(final int user_id) {
        return new NetworkBoundResource<List<FamilyEntity>, ListEntity<FamilyEntity>>(appExecutors) {

            MutableLiveData<List<FamilyEntity>> liveData;

            @Override
            protected void saveCallResult(ListEntity<FamilyEntity> item) {
                if (item.data != null) {
                    liveData.postValue(item.data);
                } else
                    liveData.postValue(new ArrayList<FamilyEntity>());
            }

            @Override
            protected boolean shouldFetch(@Nullable List<FamilyEntity> data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<List<FamilyEntity>> loadFromDb() {
                if (liveData == null) {
                    liveData = new MutableLiveData<List<FamilyEntity>>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<ListEntity<FamilyEntity>>> createCall() {
                return familyApi.getFamilys(user_id);
            }

            @Override
            protected void onFetchFailed() {
                super.onFetchFailed();
            }
        }.asLiveData();
    }


    /**
     * 新增修改家人
     * @param user_id           使用者id
     * @param user_id_family    家人的使用者id
     * @param fts_id            家人類型id
     * @param fs_nickname       家人暱稱
     * @param fs_accept         是否同意邀請(1或0)。邀請、同意 1 ; 取消、拒絕 0
     * @return
     */
    public LiveData<Resource<ResponseIdResultEntity>> postFamily(final int user_id, final int user_id_family
            , final int fts_id, final String fs_nickname
            ,@IntRange(from = 0 , to = 1) final int fs_accept) {

        return new NetworkBoundResource<ResponseIdResultEntity, SingleEntity<ResponseIdResultEntity>>(appExecutors) {

            MediatorLiveData<ResponseIdResultEntity> liveData;

            @Override
            protected void saveCallResult(SingleEntity<ResponseIdResultEntity> item) {
                item.data.status = item.status;
                item.data.msg = item.msg;
                liveData.postValue(item.data);
            }

            @Override
            protected boolean shouldFetch(@Nullable ResponseIdResultEntity data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<ResponseIdResultEntity> loadFromDb() {
                if (liveData == null) {
                    liveData = new MediatorLiveData<ResponseIdResultEntity>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>> createCall() {
                RequestBody bodyUserId = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(user_id));
                RequestBody bodyUserIdFamily = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(user_id_family));
                RequestBody bodyFtsId = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(fts_id));
                RequestBody bodyFsName = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(fs_nickname));
                RequestBody bodyFsAccept = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(fs_accept));
                return familyApi.postFamily(bodyUserId, bodyUserIdFamily, bodyFtsId, bodyFsName, bodyFsAccept);
            }
        }.asLiveData();
    }


} // class close
