package tw.azul.wehear.account

import android.accounts.AbstractAccountAuthenticator
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.content.Intent
import android.os.*
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.google.gson.Gson
import org.json.JSONObject
import tw.azul.wehear.App
import tw.azul.wehear.BuildConfig
import tw.azul.wehear.account.AccountRoom.Status
import tw.azul.wehear.account.AccountRoom.Token
import tw.azul.wehear.api.WebAPI
import java.io.InputStreamReader
import java.lang.Math.random
import java.net.URL
import java.util.*

class AccountService : LifecycleService() {

    companion object {
        val INTERN_ID: UUID = UUID.randomUUID()
        const val HINT_WHAT = 10002
    }

    data class ResponseBody(val returnCode: Int, val returnMessage: String, val data: Token)

    private lateinit var mHandler: Handler
    private lateinit var mAuthenticator: Authenticator
    private lateinit var responseLiveData: MutableLiveData<Status>

    override fun onCreate() {
        android.util.Log.v("Faty", "AccountService.onCreate")
        super.onCreate()
        mHandler = Handler {
            when (it.what) {
                HINT_WHAT -> {
                    android.widget.Toast.makeText(this, "not supported", android.widget.Toast.LENGTH_LONG).show()
                    return@Handler true
                }
            }
            false
        }
        mAuthenticator = Authenticator(this)
        // 繫結 StatusLiveData
        responseLiveData = MutableLiveData<Status>().apply {
            observe(this@AccountService) { AccountRoom.StatusLiveData.postValue(it) }
        }
        // 激活 TokenLiveData
        AccountRoom.TokenLiveData.observe(this, Observer {
            android.util.Log.e("Faty", "")
        })
    }

    override fun onDestroy() {
        android.util.Log.v("Faty", "AccountService.onDestroy")
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder {
        super.onBind(intent)
        return mAuthenticator.iBinder
    }

    private class Authenticator(val accountService: AccountService): AbstractAccountAuthenticator(accountService) {

        override fun getAuthTokenLabel(authTokenType: String?): String {
            Message.obtain(accountService.mHandler, HINT_WHAT).sendToTarget()
            throw UnsupportedOperationException()
        }

        override fun confirmCredentials(
            response: AccountAuthenticatorResponse?,
            account: Account?,
            options: Bundle?
        ): Bundle? {
            Message.obtain(accountService.mHandler, HINT_WHAT).sendToTarget()
            return null
        }

        override fun updateCredentials(
            response: AccountAuthenticatorResponse?,
            account: Account?,
            authTokenType: String?,
            options: Bundle?
        ): Bundle {
            Message.obtain(accountService.mHandler, HINT_WHAT).sendToTarget()
            throw UnsupportedOperationException()
        }

        override fun getAuthToken(
            response: AccountAuthenticatorResponse,
            account: Account,
            authTokenType: String,
            options: Bundle?
        ): Bundle? = try {
            val password = options?.getString(AccountManager.KEY_PASSWORD)
            URL("${BuildConfig.BASE_URL}user/phone/login?user_account=${account.name}&user_password=$password&r=${random()}")
            .openStream()
            .use {
                val gson = Gson()
                val responseBody = gson.fromJson<ResponseBody>(InputStreamReader(it), ResponseBody::class.java)
                with(responseBody) {
                    returnCode.takeUnless { code ->
                        code == 1000
                    }?.run {
                        accountService.responseLiveData.postValue(Status.Error(account, errMsg = returnMessage))
                        // 註記之後不自動登入
                        App.GetPreferences(accountService.application)
                           .edit().putBoolean("AccountRoom.Status.Connected", false)
                           .apply()
                        return@with null
                    }?:run {
                        val tokenStr = gson.toJson(data)
                        accountService.responseLiveData.postValue(Status.Connected(account, tokenStr))
                        // 登入成功才在 AccountManager 建立帳號
                        AccountManager.get(accountService).apply {
                            // 全部移除帳號, 除了接下來要新增的帳號 (只能一個帳號)
                            getAccountsByType(accountService.packageName)
                                .filterNot { acc ->
                                    if (acc == account) {
                                        if (getPassword(account) != password) {
                                            setPassword(account, password) // 如果密碼不一樣 → 變更密碼
                                        }
                                        true
                                    } else {
                                        false
                                    }
                                }
                                .forEach { account ->
                                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                                        @Suppress("DEPRECATION")
                                        removeAccount(account, null, null)
                                    } else {
                                        removeAccountExplicitly(account)
                                    }
                                }
                            // 新增帳號 (只能一個帳號)
                            addAccountExplicitly(account, password, null)
                        }
                        // 註記之後自動登入
                        App.GetPreferences(accountService.application)
                           .edit().putBoolean("AccountRoom.Status.Connected", true)
                           .apply()
                        android.util.Log.wtf("Faty", "isAutoLoginIn(true) = $tokenStr")
                        // 回傳時 token 自動被儲存至 AccountManager
                        return@with Bundle().apply {
                            putString(AccountManager.KEY_AUTHTOKEN, tokenStr)
                            putString(AccountManager.KEY_ACCOUNT_NAME, account.name)
                            putString(AccountManager.KEY_ACCOUNT_TYPE, account.type)
                        }
                    }
                }
            }
        } catch (e: Throwable) {
            accountService.responseLiveData.postValue(Status.Error(account, errMsg = e.localizedMessage))
            e.printStackTrace()
            null
        }

        override fun hasFeatures(
            response: AccountAuthenticatorResponse?,
            account: Account?,
            features: Array<out String>?
        ): Bundle {
            Message.obtain(accountService.mHandler, HINT_WHAT).sendToTarget()
            throw UnsupportedOperationException()
        }

        override fun editProperties(response: AccountAuthenticatorResponse?, accountType: String?): Bundle {
            Message.obtain(accountService.mHandler, HINT_WHAT).sendToTarget()
            throw UnsupportedOperationException()
        }

        override fun addAccount(
            response: AccountAuthenticatorResponse,
            accountType: String,
            authTokenType: String?,
            requiredFeatures: Array<out String>?,
            options: Bundle?
        ): Bundle? = options?.takeIf { it.getSerializable("SERVICE_ID")?.equals(INTERN_ID) == true }
            ?.let {
                val map = mapOf(
                    "user_account" to it["user_account"],
                    "user_password" to it["user_password"],
                    "user_name" to it["user_name"],
                    "user_is_male" to it["user_is_male"],
                    "user_birthday_year" to it["user_birthday_year"],
                    "user_birthday_month" to it["user_birthday_month"],
                    "user_birthday_day" to it["user_birthday_day"],
                    "user_aid" to it["user_aid"]
                )
                val responseType = WebAPI.Instance.SignUp(JSONObject(map).toString())
                when (responseType) {
                    is WebAPI.ResponseType.Success -> {
                        android.util.Log.i("Faty", "$responseType")
                        AccountManager.get(accountService).apply {
                            // 全部移除帳號 (保險起見)
                            getAccountsByType(accountService.packageName)
                                .forEach { account ->
                                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                                        @Suppress("DEPRECATION")
                                        removeAccount(account, null, null)
                                    } else {
                                        removeAccountExplicitly(account)
                                    }
                                }
                            // 註冊成功但還不會在 AccountManager 建立帳號
                        }
                        Bundle().apply {
                            putString(AccountManager.KEY_ACCOUNT_NAME, it["user_account"] as String)
                            putString(AccountManager.KEY_ACCOUNT_TYPE, accountType)
                        }
                    }
                    is WebAPI.ResponseType.Failure -> {
                        android.util.Log.w("Faty", "$responseType")
                        Bundle().apply {
                            putInt(AccountManager.KEY_ERROR_CODE, responseType.code)
                            putString(AccountManager.KEY_ERROR_MESSAGE, responseType.errMsg)
                        }
                    }
                }
            }
            ?:run {
                Message.obtain(accountService.mHandler, HINT_WHAT).sendToTarget()
                throw UnsupportedOperationException()
            }
    }

    // Kotlin Extension used by Self
    private fun <T> LiveData<T>.observe(owner: LifecycleOwner, observer: (T) -> Unit) = observe(owner, Observer(observer))
}
