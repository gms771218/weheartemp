package tw.azul.wehear

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent

interface IGotoMainActivity {
    fun gotoMainActivity()
}

class GotoMainActivityDelegate(val activity: Activity? = null) : IGotoMainActivity {
    override fun gotoMainActivity() {
        activity?.apply {
            startActivity(Intent(this, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            finish()
        }
    }
}

@SuppressLint("StaticFieldLeak")
val GOTOMAINACTIVITYDEFAULT = GotoMainActivityDelegate()
