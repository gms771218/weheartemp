package tw.azul.wehear.net.model.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 通知物件
 */
public class NotificationEntity {

    /**
     * 通知id
     */
    @Expose
    @SerializedName("ns_id")
    public int ns_id;

    /**
     * 使用者id
     */
    @Expose
    @SerializedName("user_id")
    public int user_id;

    /**
     * 通知類型id
     */
    @Expose
    @SerializedName("nts_id")
    public int nts_id;

    /**
     * 通知狀態id
     */
    @Expose
    @SerializedName("nss_id")
    public int nss_id;

    /**
     * 通知是否啟用
     */
    @Expose
    @SerializedName("ns_enable")
    public int ns_enable;

    /**
     * 是否觸發通知
     */
    @Expose
    @SerializedName("ns_notice")
    public int ns_notice;

    /**
     * 通知最小值
     */
    @Expose
    @SerializedName("ns_min")
    public int ns_min;

    /**
     * 通知最大值
     */
    @Expose
    @SerializedName("ns_max")
    public int ns_max;

    /**
     * 緯度
     */
    @Expose
    @SerializedName("ns_latitude")
    public int ns_latitude;

    /**
     * 經度
     */
    @Expose
    @SerializedName("ns_longitude")
    public int ns_longitude;

    /**
     * 通知小時
     */
    @Expose
    @SerializedName("ns_hour")
    public int ns_hour;

    /**
     * TODO api參數未未說明
     */
    @Expose
    @SerializedName("ns_hex")
    public String ns_hex;

    /**
     * TODO api參數未未說明
     */
    @Expose
    @SerializedName("ns_datetime")
    public String ns_datetime;

    /**
     * 建立時間
     */
    @Expose
    @SerializedName("created_at")
    public String created_at;

    /**
     * 更新時間
     */
    @Expose
    @SerializedName("updated_at")
    public String updated_at;
}
