package tw.azul.wehear.net.model.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import tw.azul.wehear.net.model.user.UserEntity

/**
 * 修改使用者基本資料
 */
class RequestUpdateUserEntity : UserEntity() {

    @Expose
    @SerializedName("user_id")
    var user_id: Int = 0

    companion object {
        fun create(user_id: Int, oldUser: UserEntity, newUser: UserEntity): RequestUpdateUserEntity {
            val user = RequestUpdateUserEntity()
            user.user_id = user_id

            // check 資料是否又需要異動

            user.setUser_name(if (oldUser.getUser_name() == newUser.getUser_name()) null else newUser.getUser_name())
            if (newUser.isUser_is_male != null)
                user.isUser_is_male =
                        if (oldUser.isUser_is_male == newUser.isUser_is_male) null else newUser.isUser_is_male
            else
                user.isUser_is_male = null

            user.setUser_height(if (oldUser.getUser_height() == newUser.getUser_height()) null else newUser.getUser_height())
            user.setUser_weight(if (oldUser.getUser_weight() == newUser.getUser_weight()) null else newUser.getUser_weight())
            user.setUser_stride_length(if (oldUser.getUser_stride_length() == newUser.getUser_stride_length()) null else newUser.getUser_stride_length())

            if (newUser.getUser_birthday_year() != -1)
                user.setUser_birthday_year(if (oldUser.getUser_birthday_year() == newUser.getUser_birthday_year()) null else newUser.getUser_birthday_year())
            else
                user.setUser_birthday_year(null)
            if (newUser.getUser_birthday_month() != -1)
                user.setUser_birthday_month(if (oldUser.getUser_birthday_month() == newUser.getUser_birthday_month()) null else newUser.getUser_birthday_month())
            else
                user.setUser_birthday_month(null)
            if (newUser.getUser_birthday_day() != -1)
                user.setUser_birthday_day(if (oldUser.getUser_birthday_day() == newUser.getUser_birthday_day()) null else newUser.getUser_birthday_day())
            else
                user.setUser_birthday_day(null)

            if (newUser.getUser_phone() != "unknown")
                user.setUser_phone(if (oldUser.getUser_phone() == newUser.getUser_phone()) null else newUser.getUser_phone())
            else
                user.setUser_phone(null)

            if (newUser.getUser_step_goal() != -1)
                user.setUser_step_goal(if (oldUser.getUser_step_goal() == newUser.getUser_step_goal()) null else newUser.getUser_step_goal())
            else
                user.setUser_step_goal(null)
            if (newUser.getUser_weekly_intensity() != -1)
                user.setUser_weekly_intensity(if (oldUser.getUser_weekly_intensity() == newUser.getUser_weekly_intensity()) null else newUser.getUser_weekly_intensity())
            else
                user.setUser_weekly_intensity(null)

            if (newUser.getUser_sleep_hour() != -1)
                user.setUser_sleep_hour(if (oldUser.getUser_sleep_hour() == newUser.getUser_sleep_hour()) null else newUser.getUser_sleep_hour())
            else
                user.setUser_sleep_hour(null)

            if (newUser.getUser_sleep_minute() != -1)
                user.setUser_sleep_minute(if (oldUser.getUser_sleep_minute() == newUser.getUser_sleep_minute()) null else newUser.getUser_sleep_minute())
            else
                user.setUser_sleep_minute(null)

            if (newUser.getUser_wake_hour() != -1)
                user.setUser_wake_hour(if (oldUser.getUser_wake_hour() == newUser.getUser_wake_hour()) null else newUser.getUser_wake_hour())
            else
                user.setUser_wake_hour(null)
            if (newUser.getUser_wake_minute() != -1)
                user.setUser_wake_minute(if (oldUser.getUser_wake_minute() == newUser.getUser_wake_minute()) null else newUser.getUser_wake_minute())
            else
                user.setUser_wake_minute(null)

            if (newUser.getUser_app_token() != "unknown")
                user.setUser_app_token(if (oldUser.getUser_app_token() == newUser.getUser_app_token()) null else newUser.getUser_app_token())
            else
                user.setUser_app_token(null)
            if (newUser.getUser_address() != "unknown")
                user.setUser_address(if (oldUser.getUser_address() == newUser.getUser_address()) null else newUser.getUser_address())
            else
                user.setUser_address(null)

            if (newUser.getUser_aid() != -1)
                user.setUser_aid(if (oldUser.getUser_aid() == newUser.getUser_aid()) null else newUser.getUser_aid())
            else
                user.setUser_aid(null)

            user.setUser_vo2_max(if (oldUser.getUser_vo2_max() == newUser.getUser_vo2_max()) null else newUser.getUser_vo2_max())
            user.setUser_lactate_threshold(if (oldUser.getUser_lactate_threshold() == newUser.getUser_lactate_threshold()) null else newUser.getUser_lactate_threshold())
            user.setUser_ftp(if (oldUser.getUser_ftp() == newUser.getUser_ftp()) null else newUser.getUser_ftp())
            user.setUser_cycling_vo2_max(if (oldUser.getUser_cycling_vo2_max() == newUser.getUser_cycling_vo2_max()) null else newUser.getUser_cycling_vo2_max())

            return user
        }
    }


    fun setUser_name(user_name: String?) {
        this.user_name = user_name
    }

    fun setUser_is_male(user_is_male: Boolean?) {
        this.user_is_male = user_is_male
    }

    fun setUser_height(user_height: Int?) {
        this.user_height = user_height
    }

    fun setUser_weight(user_weight: Int?) {
        this.user_weight = user_weight
    }

    fun setUser_birthday_year(user_birthday_year: Int?) {
        this.user_birthday_year = user_birthday_year
    }

    fun setUser_birthday_month(user_birthday_month: Int?) {
        this.user_birthday_month = user_birthday_month
    }

    fun setUser_birthday_day(user_birthday_day: Int?) {
        this.user_birthday_day = user_birthday_day
    }

    fun setUser_phone(user_phone: String?) {
        this.user_phone = user_phone
    }

    fun setUser_stride_length(user_stride_length: Int?) {
        this.user_stride_length = user_stride_length
    }

    fun setUser_step_goal(user_step_goal: Int?) {
        this.user_step_goal = user_step_goal
    }

    fun setUser_weekly_intensity(user_weekly_intensity: Int?) {
        this.user_weekly_intensity = user_weekly_intensity
    }

    fun setUser_sleep_hour(user_sleep_hour: Int?) {
        this.user_sleep_hour = user_sleep_hour
    }

    fun setUser_sleep_minute(user_sleep_minute: Int?) {
        this.user_sleep_minute = user_sleep_minute
    }

    fun setUser_wake_hour(user_wake_hour: Int?) {
        this.user_wake_hour = user_wake_hour
    }

    fun setUser_wake_minute(user_wake_minute: Int?) {
        this.user_wake_minute = user_wake_minute
    }

    fun setUser_vo2_max(user_vo2_max: Int?) {
        this.user_vo2_max = user_vo2_max
    }

    fun setUser_cycling_vo2_max(user_cycling_vo2_max: Int?) {
        this.user_cycling_vo2_max = user_cycling_vo2_max
    }

    fun setUser_lactate_threshold(user_lactate_threshold: Int?) {
        this.user_lactate_threshold = user_lactate_threshold
    }

    fun setUser_ftp(user_ftp: Int?) {
        this.user_ftp = user_ftp
    }

    fun setUser_app_token(user_app_token: String?) {
        this.user_app_token = user_app_token
    }

    fun setUser_address(user_address: String?) {
        this.user_address = user_address
    }

    fun setUser_aid(user_aid: Int?) {
        this.user_aid = user_aid
    }


}