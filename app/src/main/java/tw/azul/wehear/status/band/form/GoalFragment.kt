package tw.azul.wehear.status.band.form

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_band_setting_goal.*

import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.databinding.FragmentBandSettingGoalBinding

class GoalFragment : Fragment() {

    companion object {
        fun newInstance() = GoalFragment()
        private val DISPLAY_VALUE: Array<String> = Array(30) { i -> (i * 1000 + 1000).toString() }
    }

    private lateinit var databinding: FragmentBandSettingGoalBinding
    private lateinit var viewModel: GoalViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        databinding = DataBindingUtil.inflate(inflater, R.layout.fragment_band_setting_goal, container, false)
        return databinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        picker.apply {
            wrapSelectorWheel = false
            displayedValues = DISPLAY_VALUE
            maxValue = 29
            minValue = 0
            value = 7
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(GoalViewModel::class.java)
        databinding.viewmodel = viewModel
        databinding.setLifecycleOwner(viewLifecycleOwner)

        viewModel.getUser(AccountRoom.TokenLiveData.value?.userId!!).observe(viewLifecycleOwner, Observer {})
        btn_next.setOnClickListener {
            viewModel.saveUser(AccountRoom.TokenLiveData.value?.userId!!).observe(viewLifecycleOwner, Observer {})
            viewModel.gotoMainPage(it)
        }
    }

}
