package tw.azul.wehear.net.model.dailyInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 睡眠狀態資料
 */
public class SleepStatusEntity {

    /**
     * 睡眠狀態id
     */
    @Expose
    @SerializedName("ss_id")
    public int ss_id;

    /**
     * 睡眠狀態
     */
    @Expose
    @SerializedName("ss_description")
    public String ss_description;

}
