package tw.azul.wehear.user.user

import android.app.Application
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRepository
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.net.model.base.Resource
import tw.azul.wehear.net.model.base.ResponseIdResultEntity
import tw.azul.wehear.net.model.request.RequestUpdateUserEntity
import tw.azul.wehear.net.model.user.UserEntity
import tw.azul.wehear.net.repository.UserRepository

class UserViewModel(val app:Application) : AndroidViewModel(app) {

    private var userRepository: UserRepository = UserRepository.getInstance()

    fun getUser(user_id: Int): LiveData<Resource<UserEntity>> {
        return userRepository.getUser(user_id)
    }
//    fun getUser(user_id: Int): LiveData<UserEntity> {
//        var result : MediatorLiveData<UserEntity> = MediatorLiveData<UserEntity>()
//        var liveData = userRepository.getUser(user_id)
//        result.addSource(liveData , Observer {
//            if (it.status != Status.LOADING){
//                result.postValue(it.data)
//            }
//        })
//        return result ;
//    }

    fun updateUser(
        user_id: Int,
        requestUpdateUserEntity: RequestUpdateUserEntity
    ): LiveData<Resource<ResponseIdResultEntity>> {
        return userRepository.postUser(user_id, requestUpdateUserEntity)
    }

    fun logout(view: View) {
        //AccountRepository.Instance(app).logout()
        //android.widget.Toast.makeText(view.context, "登出", android.widget.Toast.LENGTH_LONG).show()
        AlertDialog.Builder(view.context, R.style.Theme_MaterialComponents_Light_Dialog_Alert).apply {
            setCancelable(false)
            //setTitle("提示")
            setMessage("登出")
            val alertDialog = show()
            Handler().postDelayed({
                alertDialog.dismiss()
                AccountRepository.Instance(app).logout()
            }, 1000)
        }
    }

    val finishSignalLiveData: LiveData<Boolean> = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(AccountRoom.StatusLiveData) { status ->
            when (status) {
                is AccountRoom.Status.Init -> value = true
            }
        }
    }

}
