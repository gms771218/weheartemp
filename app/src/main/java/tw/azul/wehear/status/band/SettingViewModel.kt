package tw.azul.wehear.status.band

import android.app.Application
import android.view.View
import androidx.lifecycle.*
import androidx.navigation.findNavController
import tw.azul.wehear.ble.AccessoryRepository
import tw.azul.wehear.ble.BandConnector
import tw.azul.wehear.ble.BandGattRepository
import tw.azul.wehear.ble.DeviceRepository
import tw.azul.wehear.ble.model.AppNotified
import tw.azul.wehear.ble.model.BandBatteryRepository
import tw.azul.wehear.ble.model.BandNotificationRepository

class SettingViewModel(val app:Application) : AndroidViewModel(app) {

    companion object {
        private const val BYTE_ONE: Byte = 1
    }

    private val notifySettingLiveData = BandNotificationRepository.Instance(app).getSettingLiveData()
    val enabledCallInNotify: LiveData<Boolean> = Transformations.map(notifySettingLiveData) { settings: ByteArray? ->
        settings != null && settings[AppNotified.CallIn.index] == BYTE_ONE
    }
    val enabledSmsNotify: LiveData<Boolean> = Transformations.map(notifySettingLiveData) {settings: ByteArray? ->
        settings != null && settings[AppNotified.SMS.index] == BYTE_ONE
    }

    val enabledSedentaryNotify : LiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }
    val enabledDrinkingNotify  : LiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }

    val bandPowerLiveData: LiveData<String> = Transformations.map(BandBatteryRepository.Instance.batteryLevel()) {
        if (it < 0) "" else "$it%"
    }

    fun getBleStatusLiveData(): LiveData<AccessoryRepository.BleStatus> =
        AccessoryRepository.Instance(app).getBleStatusLiveData()

    fun immediateAlert() {
        BandGattRepository.Instance(app).immediateAlert()
    }

    fun bandUnbind(view: View) {
        // 清除 Band in DeviceRepository
        DeviceRepository.Instance(app).resetBand()
        // 清除 BandConnector
        BandConnector.Instance(app).reset()
        // 彈回去
        view.findNavController().popBackStack()
    }

}
