package tw.azul.wehear

import android.app.Activity
import android.app.Application
import android.os.Bundle

/**
 * Created by gms on 2019.01.21
 * 因為某些裝置在前景會判斷失效，所以再新增此方法做輔助
 */
class MyActivityLifecycleCallback : Application.ActivityLifecycleCallbacks {

    companion object {
        var foreGroundCount = 0

        fun isRunOnForeGround() : Boolean {
            return foreGroundCount > 0
        }
    }

    override fun onActivityStarted(activity: Activity?) {
        foreGroundCount++
    }

    override fun onActivityStopped(activity: Activity?) {
        foreGroundCount--
    }

    override fun onActivityPaused(activity: Activity?) {

    }

    override fun onActivityResumed(activity: Activity?) {

    }

    override fun onActivityDestroyed(activity: Activity?) {

    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {

    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {

    }
}