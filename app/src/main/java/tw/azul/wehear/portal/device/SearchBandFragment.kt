package tw.azul.wehear.portal.device

import android.bluetooth.BluetoothDevice
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_portal_search_band.*
import tw.azul.wehear.GotoMainActivityDelegate
import tw.azul.wehear.R
import tw.azul.wehear.databinding.FragmentPortalSearchBandBinding


class SearchBandFragment : Fragment() {

    companion object {
        fun newInstance() = SearchBandFragment()
    }

    private lateinit var databinding: FragmentPortalSearchBandBinding
    private lateinit var viewmodel: SearchBandViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        databinding = FragmentPortalSearchBandBinding.inflate(inflater)
        return databinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        databinding.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewmodel = ViewModelProviders.of(this).get(SearchBandViewModel::class.java)
        viewmodel.delegate = GotoMainActivityDelegate(activity)
        databinding.viewmodel = viewmodel
    }

    private lateinit var observer:Observer<Boolean>

    override fun onResume() {
        super.onResume()
        observer = object: Observer<Boolean> {
            override fun onChanged(bleStatus: Boolean) {
                viewmodel.bleStatusDelObserver(this)
                if (bleStatus) {
                    viewmodel.searchScanCallbackObserve(viewLifecycleOwner, Observer { set ->
                        when {
                            set?.size ?: 0 != 0 -> {
                                findNavController().navigate(R.id.action_searchBandFragment_to_chooseBandFragment)
                            }
                            System.currentTimeMillis().minus(viewmodel.start_time) < 30 * 1000L -> {
                                viewmodel.searchAgain()
                            }
                        }
                    })
                }
            }
        }
        viewmodel.bleStatusAddObserver(viewLifecycleOwner, observer)
    }

    override fun onPause() {
        super.onPause()
        if (::observer.isInitialized) {
            viewmodel.bleStatusDelObserver(observer)
            viewmodel.removeScanCallbackObserve(viewLifecycleOwner)
        }
    }

    override fun onStart() {
        super.onStart()
        (logo.background as? Animatable) ?.start()
    }
}