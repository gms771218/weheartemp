package tw.azul.wehear.net.api

import androidx.lifecycle.LiveData
import okhttp3.RequestBody
import retrofit2.http.*
import tw.azul.wehear.net.model.base.ResponseIdResultEntity
import tw.azul.wehear.net.model.base.SingleEntity
import tw.azul.wehear.net.model.user.UserEntity
import tw.azul.wehear.net.retrofit.RetrofitClient

interface UserApi {

    /**
     * 取得使用者
     */
    @GET(RetrofitClient.BASE_URL + "user/{user_id}")
    fun getUser(@Path("user_id") user_id: Int): LiveData<ApiResponse<SingleEntity<UserEntity>>>


    /**
     * 修改使用者資料
     */
    @PUT(RetrofitClient.BASE_URL + "user/{user_id}")
    fun putUserNotification(
        @Path("user_id") user_id: Int,
        @QueryMap param : Map<String, String>
//        @PartMap param: Map<String, @JvmSuppressWildcards RequestBody>
    ): LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>>

}