package tw.azul.wehear.net.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import retrofit2.Call;
import tw.azul.wehear.net.AppExecutors;
import tw.azul.wehear.net.NetworkBoundResource;
import tw.azul.wehear.net.api.ApiResponse;
import tw.azul.wehear.net.api.NotificationApi;
import tw.azul.wehear.net.model.base.ListEntity;
import tw.azul.wehear.net.model.base.Resource;
import tw.azul.wehear.net.model.base.ResponseIdResultEntity;
import tw.azul.wehear.net.model.base.SingleEntity;
import tw.azul.wehear.net.model.notification.NotificationEntity;
import tw.azul.wehear.net.model.notification.NotificationStatusEntity;
import tw.azul.wehear.net.model.notification.NotificationTypeEntity;
import tw.azul.wehear.net.model.request.RequestUserNotificationEntity;
import tw.azul.wehear.net.retrofit.RetrofitClient;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 通知
 */
public class NotificationRepository {

    private static NotificationRepository instance;

    private AppExecutors appExecutors;

    private NotificationApi notificationApi;

    String _notificationStatus = null;
    String _notificationType = null;

    public static NotificationRepository getInstance() {
        if (instance == null) {
            throw new ExceptionInInitializerError("Not Initializer , Please call initialization method");
        }
        return instance;
    }

    public static void initialization(AppExecutors appExecutors) {
        if (instance == null) {
            synchronized (NotificationRepository.class) {
                if (instance == null) {
                    instance = new NotificationRepository();
                    instance.appExecutors = appExecutors;
                    instance.notificationApi = RetrofitClient.getInstance().createEndpoint(NotificationApi.class);
                }
            }
        }
    }

    /**
     * 新增修改使用者通知
     *
     * @param requestUserNotificationEntity
     * @return
     */
    public LiveData<Resource<ResponseIdResultEntity>> postUserNotification(
            final RequestUserNotificationEntity requestUserNotificationEntity) {
        return new NetworkBoundResource<ResponseIdResultEntity, SingleEntity<ResponseIdResultEntity>>(appExecutors) {

            MediatorLiveData<ResponseIdResultEntity> liveData;

            @Override
            protected void saveCallResult(SingleEntity<ResponseIdResultEntity> item) {

            }

            @Override
            protected boolean shouldFetch(@Nullable ResponseIdResultEntity data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<ResponseIdResultEntity> loadFromDb() {
                if (liveData == null) {
                    liveData = new MediatorLiveData<ResponseIdResultEntity>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>> createCall() {
                Type type = new TypeToken<Map<String, String>>() {
                }.getType();
                Map<String, String> stringMap = new Gson().fromJson(new Gson().toJson(requestUserNotificationEntity), type);
                Map<String, RequestBody> formData = new HashMap<String, RequestBody>();
                for (String key : stringMap.keySet()) {
                    formData.put(key, createRequestBody(MediaType.parse("multipart/form-data"), stringMap.get(key)));
                }
                return notificationApi.postUserNotification(formData);
            }
        }.asLiveData();
    }


    public Call<SingleEntity<ResponseIdResultEntity>> postUserNotificationFormCall(final RequestUserNotificationEntity requestUserNotificationEntity) {
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> stringMap = new Gson().fromJson(new Gson().toJson(requestUserNotificationEntity), type);
        Map<String, RequestBody> formData = new HashMap<String, RequestBody>();
        for (String key : stringMap.keySet()) {
            formData.put(key, createRequestBody(MediaType.parse("multipart/form-data"), stringMap.get(key)));
        }
        return notificationApi.postUserNotificationFormCall(formData);

    }


    /**
     * 取得使用者通知設定
     *
     * @param user_id
     * @return
     */
    public LiveData<Resource<List<NotificationEntity>>> getUserNotification(final int user_id) {
        return new NetworkBoundResource<List<NotificationEntity>, ListEntity<NotificationEntity>>(appExecutors) {

            MutableLiveData<List<NotificationEntity>> liveData;

            @Override
            protected void saveCallResult(ListEntity<NotificationEntity> item) {
                liveData.postValue(item.data);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<NotificationEntity> data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<List<NotificationEntity>> loadFromDb() {
                if (liveData == null) {
                    liveData = new MutableLiveData<List<NotificationEntity>>();
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<ListEntity<NotificationEntity>>> createCall() {
                return notificationApi.getUserNotification(user_id);
            }
        }.asLiveData();
    }

    /**
     * 取得通知狀態列表
     *
     * @return
     */
    public LiveData<Resource<List<NotificationStatusEntity>>> getNotificationStatus() {
        return new NetworkBoundResource<List<NotificationStatusEntity>, ListEntity<NotificationStatusEntity>>(appExecutors) {

            MutableLiveData<List<NotificationStatusEntity>> liveData;

            @Override
            protected void saveCallResult(ListEntity<NotificationStatusEntity> item) {
                _notificationStatus = new Gson().toJson(item.data);
                liveData.postValue(item.data);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<NotificationStatusEntity> data) {
                return _notificationStatus == null;
            }

            @NotNull
            @Override
            protected LiveData<List<NotificationStatusEntity>> loadFromDb() {
                if (liveData == null) {
                    liveData = new MutableLiveData<List<NotificationStatusEntity>>();
                    if (_notificationStatus != null)
                        liveData.postValue(new Gson().<List<NotificationStatusEntity>>fromJson(_notificationStatus, new TypeToken<List<NotificationStatusEntity>>() {
                        }.getType()));
                    else
                        liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<ListEntity<NotificationStatusEntity>>> createCall() {
                return notificationApi.getNotificationStatus();
            }
        }.asLiveData();
    }

    /**
     * 取得通知類型列表
     * 門鈴、電話、遙控器...etc
     */

    public LiveData<Resource<List<NotificationTypeEntity>>> getNotificationType() {
        return new NetworkBoundResource<List<NotificationTypeEntity>, ListEntity<NotificationTypeEntity>>(appExecutors) {

            MutableLiveData<List<NotificationTypeEntity>> liveData;

            @Override
            protected void saveCallResult(ListEntity<NotificationTypeEntity> item) {
                _notificationType = new Gson().toJson(item.data);
                liveData.postValue(item.data);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<NotificationTypeEntity> data) {
                return _notificationType == null;
            }

            @NotNull
            @Override
            protected LiveData<List<NotificationTypeEntity>> loadFromDb() {
                if (liveData == null) {
                    liveData = new MutableLiveData<List<NotificationTypeEntity>>();
                    if (_notificationStatus != null)
                        liveData.postValue(new Gson().<List<NotificationTypeEntity>>fromJson(_notificationStatus, new TypeToken<List<NotificationTypeEntity>>() {
                        }.getType()));
                    else
                        liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<ListEntity<NotificationTypeEntity>>> createCall() {
                return notificationApi.getNotificationType();
            }
        }.asLiveData();
    }


    /**
     * App發送通知
     *
     * @param user_id   使用者id
     * @param nts_id    通知類型id
     * @param str_hex   hex
     * @param latitude  緯度
     * @param longitude 經度
     * @return
     */
    public LiveData<Resource<List<ResponseIdResultEntity>>> getAppNotification(final int user_id , final int nts_id ,final String str_hex
            ,final float latitude ,final float longitude ) {
        return new NetworkBoundResource<List<ResponseIdResultEntity>, ListEntity<ResponseIdResultEntity>>(appExecutors) {

            MutableLiveData<List<ResponseIdResultEntity>> liveData ;
            @Override
            protected void saveCallResult(ListEntity<ResponseIdResultEntity> item) {
                if (item.status == 1000){

                }else {
                    ResponseIdResultEntity resultEntity = new ResponseIdResultEntity();
                    resultEntity.status = item.status ;
                    resultEntity.msg = item.msg ;
                    item.data.add(resultEntity);
                }
                liveData.postValue(item.data);
            }

            @Override
            protected boolean shouldFetch(@Nullable List<ResponseIdResultEntity> data) {
                return true;
            }

            @NotNull
            @Override
            protected LiveData<List<ResponseIdResultEntity>> loadFromDb() {
                if (liveData == null){
                    liveData = new MediatorLiveData<List<ResponseIdResultEntity>>() ;
                    liveData.postValue(null);
                }
                return liveData;
            }

            @NotNull
            @Override
            protected LiveData<ApiResponse<ListEntity<ResponseIdResultEntity>>> createCall() {
                String latitudeStr = String.valueOf(latitude) ;
                String longitudeStr = String.valueOf(longitude) ;
                return notificationApi.getAppNotification(user_id , nts_id , str_hex , latitudeStr ,longitudeStr );
            }
        }.asLiveData();
    }


    private RequestBody createRequestBody(MediaType contentType, String value) {
        return RequestBody.create(contentType, value);
    }


} // class close
