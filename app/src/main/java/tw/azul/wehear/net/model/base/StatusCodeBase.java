package tw.azul.wehear.net.model.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by gms on 2018/4/15.
 * <p>
 * Api 狀態資料
 * <p>
 * status code from
 * https://docs.google.com/spreadsheets/d/1kAjclcCa839M465zMlIy4oZJSb8h8EHkBw829obAU6g/edit#gid=0
 */

public class StatusCodeBase {

    @Expose
    @SerializedName("returnMessage")
    public String msg;

    @Expose
    @SerializedName("returnCode")
    public int status;

//    public StatusCodeBase(){}
//
//    public StatusCodeBase(String msg, int status) {
//        this.msg = msg;
//        this.status = status;
//    }
}
