package tw.azul.wehear.net.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * 新增睡眠物件
 */
public class RequestSleepEntity {

    /**
     * 使用者id
     */
    @Expose
    @SerializedName("user_id")
    private int user_id;

    @Expose
    @SerializedName("sleeps")
    private List<SleepsEntity> sleeps;

    public RequestSleepEntity(int user_id) {
        this.user_id = user_id;
        sleeps = new ArrayList<SleepsEntity>();
    }

    public void addSleep(SleepsEntity entity) {
        sleeps.add(entity);
    }

    public static class SleepsEntity {
        /**
         * 睡眠狀態列舉id
         */
        @Expose
        @SerializedName("ss_id")
        private int ss_id;

        /**
         * 睡眠卡路里
         */
        @Expose
        @SerializedName("sleep_calorie")
        private int sleep_calorie;

        /**
         * 睡眠持續時間(秒)
         */
        @Expose
        @SerializedName("sleep_duration")
        private int sleep_duration;

        /**
         * 睡眠紀錄時間戳記。ex:1514764800
         */
        @Expose
        @SerializedName("sleep_starttime")
        private long sleep_starttime;

        public SleepsEntity(int ss_id, int sleep_calorie, int sleep_duration, long sleep_starttime) {
            this.ss_id = ss_id;
            this.sleep_calorie = sleep_calorie;
            this.sleep_duration = sleep_duration;
            this.sleep_starttime = sleep_starttime;
        }
    }


}
