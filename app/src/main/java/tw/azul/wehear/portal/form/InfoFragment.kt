package tw.azul.wehear.portal.form

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_portal_info.*
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.databinding.FragmentPortalInfoBinding

class InfoFragment : Fragment() {

    companion object {
        fun newInstance() = InfoFragment()
    }

    private lateinit var dataBinding: FragmentPortalInfoBinding
    private lateinit var viewModel: InfoViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentPortalInfoBinding.inflate(inflater)
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(InfoViewModel::class.java)
        dataBinding.viewmodel = viewModel
        dataBinding.setLifecycleOwner(viewLifecycleOwner)
        viewModel.getUser(AccountRoom.TokenLiveData.value?.userId!!).observe(viewLifecycleOwner, Observer {})
        btn_next.setOnClickListener { view ->
            // 檢查欄位
            if (value_tall.valid() and value_weight.valid() and value_step_length.valid() ){
                viewModel.saveUser(AccountRoom.TokenLiveData.value?.userId!!).observe(viewLifecycleOwner , Observer {})
                viewModel.gotoNext(view)
            }
        }
    }

}
