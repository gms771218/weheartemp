package tw.azul.wehear.user.nofitication

import androidx.lifecycle.*
import tw.azul.wehear.net.model.base.Resource
import tw.azul.wehear.net.model.base.ResponseIdResultEntity
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.notification.NotificationEntity
import tw.azul.wehear.net.model.request.RequestUserNotificationEntity
import tw.azul.wehear.net.repository.NotificationRepository

class NotificationViewModel : ViewModel() {

    var notificationRepository: NotificationRepository = NotificationRepository.getInstance();

    /**
     * 取得使用者通知設定
     */
    fun getUserNotification(user_id: Int): LiveData<Resource<List<NotificationEntity>>> {
        var liveData = MediatorLiveData<Resource<List<NotificationEntity>>>()

        var status = notificationRepository.notificationStatus
        var type = notificationRepository.notificationType
        var userNotification = notificationRepository.getUserNotification(user_id)

        liveData.addSource(status, Observer {
            if (it.status == Status.SUCCESS) {
                liveData.addSource(type, Observer {
                    if (it.status == Status.SUCCESS) {
                        liveData.addSource(userNotification, Observer {
                            liveData.postValue(it)
                        })
                    }
                })
            }
        })
        return liveData;
    }

    // app發送通知
    private fun sendAppNotification(
        user_id: Int,
        nts_id: Int,
        str_hex: String,
        latitude: Float,
        longitude: Float
    ): LiveData<Result> {
        return Transformations.map(notificationRepository.getAppNotification(
            user_id,
            nts_id,
            str_hex,
            latitude,
            longitude
        ),
            {
                var result: Result? = null
                if (it.status != Status.LOADING) {
                    if (it.status == Status.SUCCESS) {
                        if (it.data?.size!! > 0) {
                            result = Result(false, it.data?.get(0)?.msg!!)
                        } else {
                            result = Result(true, "")
                        }
                    } else {
                        result = Result(false, it.data?.get(0)?.msg!!)
                    }
                }
                result
            })
    }

    /**
     * 發送門鈴通知
     */
    fun sendDoorNotification(user_id: Int, str_hex: String): LiveData<Result> {
        return sendAppNotification(user_id, 1, str_hex, 0f, 0f)
    }

    /**
     * 發送電話通知
     */
    fun sendPhoneNotification(user_id: Int, str_hex: String): LiveData<Result> {
        return sendAppNotification(user_id, 2, str_hex, 0f, 0f)
    }

    /**
     * 發送警示器通知
     */
    fun sendCallNotification(user_id: Int, str_hex: String): LiveData<Result> {
        return sendAppNotification(user_id, 3, str_hex, 0f, 0f)
    }

    /**
     * 發送火災通知
     */
    fun sendFireNotification(user_id: Int, str_hex: String, latitude: Float, longitude: Float): LiveData<Result> {
        return sendAppNotification(user_id, 4, str_hex, latitude, longitude)
    }


    /**
     * 修改使用者推播設定
     * 棄用
     */
    fun updateUserNotification(requestUserNotificationEntity: RequestUserNotificationEntity): LiveData<Resource<ResponseIdResultEntity>> {
        return notificationRepository.postUserNotification(requestUserNotificationEntity)
    }


    class Result {
        var isSuccess: Boolean? = null
        var msg: String = ""

        constructor(isSuccess: Boolean?, msg: String) {
            this.isSuccess = isSuccess
            this.msg = msg
        }
    }

}