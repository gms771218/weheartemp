package tw.azul.wehear.health

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Bundle
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.gms.widget.layout.component.*
import com.google.android.material.snackbar.Snackbar
import tw.azul.wehear.MainActivity
import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.ble.AccessoryRepository
import tw.azul.wehear.ble.model.RunSpeedCadenceEntity
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.dailyInfo.HealthRecordEntity
import tw.azul.wehear.net.model.request.RequestHeartRateEntity
import tw.azul.wehear.net.model.request.RequestOximetryEntity
import java.util.*


class HealthFragment : Fragment() {

    companion object {

        val TAG = "HealthFragment"

        fun newInstance() = HealthFragment()
    }

    val USER_ID = AccountRoom.TokenLiveData.value?.userId!!

    private lateinit var viewModel: HealthViewModel

    lateinit var mCalendar: Calendar
    // 是否配對手環 :
    var isPairWatch: Boolean = false
    var isConnectWatch: Boolean = false

    var heartRecordEntity: RequestHeartRateEntity? = null
    var oxygenRecordEntity: RequestOximetryEntity? = null

    // region =====View=====

    var progressDig: AlertDialog? = null

    lateinit var swipeRefresh : SwipeRefreshLayout
    lateinit var layoutTitleBar: ViewGroup
    lateinit var layoutWalk: ViewGroup
    lateinit var layoutSleep: ViewGroup
    lateinit var layoutUnpair: ViewGroup
    lateinit var ibtnPreDate: ImageButton
    lateinit var ibtnNextDate: ImageButton
    lateinit var tvDate: TextView

    lateinit var scrollView: NestedScrollView
    lateinit var sBarWalk: CustomSeekBar
    lateinit var tvWalkCount: TextView
    lateinit var tvWalkTarget: TextView
    lateinit var tvCalorie: TextView
    lateinit var tvWalkTime: TextView
    lateinit var tvWalkDistance: TextView
    lateinit var tvWeek1: WeekTextView
    lateinit var tvWeek2: WeekTextView
    lateinit var tvWeek3: WeekTextView
    lateinit var tvWeek4: WeekTextView
    lateinit var tvWeek5: WeekTextView
    lateinit var tvWeek6: WeekTextView
    lateinit var tvWeek7: WeekTextView

    lateinit var tvHour: TextView
    lateinit var tvMinutes: TextView
    lateinit var tvDeepSleep: TextView
    lateinit var tvLightSleep: TextView

    lateinit var tvHeart: TextView
    lateinit var tvOxygen: TextView
    lateinit var tvUpdateDate: TextView

    lateinit var btnDetect: Button

    // 心率
    lateinit var heartChart: AzulLineChart
    // 攝氧量
    lateinit var oxygenChart: AzulLineChart

    lateinit var currentDate: String

    lateinit var tvWeeks: List<WeekTextView>

    // endregion =====View=====


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_main_health, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(HealthViewModel::class.java)
        (activity as? MainActivity)?.hideToolbarAndShowNavigation()
        AccessoryRepository.Instance(viewModel.app).getBleStatusLiveData().observe(viewLifecycleOwner::getLifecycle) {
            isPairWatch = it.band.isPaired == true
            isConnectWatch = it.band.isConnected == true
            setIsPairWatch(isPairWatch)
        }
        // 設定 swipeRefresh
        swipeRefresh.apply {
            isRefreshing = true
            val owner = viewLifecycleOwner::getLifecycle
            val rscLiveData = MediatorLiveData<RunSpeedCadenceEntity>().apply{
                addSource(viewModel.getRSCLiveData(), ::setValue)
            }
            OnRefreshListener {
                viewModel.uploadRSC()
                rscLiveData.apply { value = null }.observe(owner) { data ->
                    if (data == null) return@observe // 這麼做目的是為了取代 getRSCLiveData 的殘留
                    rscLiveData.removeObservers(owner)
                    Runnable {
                        if (isResumed) {
                            isRefreshing = false
                            tvDate.showDate(if (::mCalendar.isInitialized) mCalendar else Calendar.getInstance(Locale.TAIWAN))
                        }
                    }.run {
                        postDelayed(this, 1000)
                    }
                }
            }.also(::setOnRefreshListener).onRefresh()
        }
    }


    fun initView(view: View) {

        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        layoutTitleBar = view.findViewById(R.id.layout_title_bar)
        layoutWalk = view.findViewById(R.id.layout_walk_record)
        layoutSleep = view.findViewById(R.id.layout_sleep_record)
        layoutUnpair = view.findViewById(R.id.layout_unpair)
        ibtnPreDate = view.findViewById(R.id.ibtn_pre_date)
        ibtnNextDate = view.findViewById(R.id.ibtn_next_date)
        tvDate = view.findViewById(R.id.tv_date)

        scrollView = view.findViewById<NestedScrollView>(R.id.scroll_view)
        sBarWalk = view.findViewById<CustomSeekBar>(R.id.seek_bar_walk)
        tvWalkCount = view.findViewById<TextView>(R.id.tv_walk_count)
        tvWalkTarget = view.findViewById<TextView>(R.id.tv_walk_target)
        tvCalorie = view.findViewById<TextView>(R.id.tv_calorie)
        tvWalkTime = view.findViewById<TextView>(R.id.tv_walk_time)
        tvWalkDistance = view.findViewById<TextView>(R.id.tv_walk_distance)
        tvWeek1 = view.findViewById<WeekTextView>(R.id.tv_week_1)
        tvWeek2 = view.findViewById<WeekTextView>(R.id.tv_week_2)
        tvWeek3 = view.findViewById<WeekTextView>(R.id.tv_week_3)
        tvWeek4 = view.findViewById<WeekTextView>(R.id.tv_week_4)
        tvWeek5 = view.findViewById<WeekTextView>(R.id.tv_week_5)
        tvWeek6 = view.findViewById<WeekTextView>(R.id.tv_week_6)
        tvWeek7 = view.findViewById<WeekTextView>(R.id.tv_week_7)
        tvWeeks = listOf(tvWeek1, tvWeek2, tvWeek3, tvWeek4, tvWeek5, tvWeek6, tvWeek7)

        tvHour = view.findViewById(R.id.tv_hour)
        tvMinutes = view.findViewById(R.id.tv_minutes)
        tvDeepSleep = view.findViewById(R.id.tv_deep_sleep)
        tvLightSleep = view.findViewById(R.id.tv_light_sleep)

        tvHeart = view.findViewById(R.id.tv_heart)
        tvOxygen = view.findViewById(R.id.tv_oxygen)
        tvUpdateDate = view.findViewById(R.id.tv_update_date)
        btnDetect = view.findViewById(R.id.btn_detect)

        heartChart = view.findViewById<HeartLineChart>(R.id.line_chart_heart)
        oxygenChart = view.findViewById<OxygenLineChart>(R.id.line_chart_oxygen)


        tvDate.setOnClickListener {
            currentDate.split("-").apply {
                var year = get(0).toInt()
                var month = get(1).toInt().let { it - 1 }
                var dayOfMonth = get(2).toInt()
                DatePickerDialog(context, { view, year, month, dayOfMonth ->
                    mCalendar.set(year, month, dayOfMonth)
                    tvDate.showDate(mCalendar)
                }, year, month, dayOfMonth)
                    .apply {
                        datePicker.maxDate = Calendar.getInstance().timeInMillis
                    }.show()
            }
        }

        ibtnPreDate.setOnClickListener {
            var value = mCalendar.get(Calendar.DAY_OF_YEAR)
            mCalendar.set(Calendar.DAY_OF_YEAR, --value)
            tvDate.showDate(mCalendar)
        }

        ibtnNextDate.setOnClickListener {
            var value = mCalendar.get(Calendar.DAY_OF_YEAR)
            mCalendar.set(Calendar.DAY_OF_YEAR, ++value)
            tvDate.showDate(mCalendar)
        }

        btnDetect.setOnClickListener {
            btnDetect.startDetect()
        }
    }

    fun initDate() {
        mCalendar = Calendar.getInstance(Locale.TAIWAN)
        tvDate.showDate(mCalendar)
    }

    /**
     * 顯示日期
     * Kotlin extension
     */
    fun TextView.showDate(calendar: Calendar) {

        var dateMark: String =
            if (DateUtils.isToday(calendar.timeInMillis)) {
                ibtnNextDate.visibility = View.GONE
                "今日"
            } else {
                ibtnNextDate.visibility = View.VISIBLE
                if (isYesterday(calendar.timeInMillis)) "昨日"
                else ""
            }

        currentDate =
                "${calendar.get(Calendar.YEAR)}-${calendar.get(Calendar.MONTH) + 1}-${calendar.get(Calendar.DATE)}"
        currentDate.split("-").apply {
            text = "${get(0)}年${get(1)}月${get(2)}日 $dateMark"
        }

        tvWeeks.map {
            it.isSelected = false
        }
        tvWeeks.get(calendar.get(Calendar.DAY_OF_WEEK) - 1).isSelected = true


        scrollView.scrollTo(0, 0)

        viewModel.getHealthRecord(USER_ID, currentDate).observe(viewLifecycleOwner, Observer {
            if (it.status != Status.LOADING) {
                progressDig?.dismiss()
                if (it.data != null)
                    setHealthRecord(it.data)
            } else {
                if (progressDig == null) {
                    var builder = AlertDialog.Builder(context)
                    builder.setMessage("請稍候")
                    progressDig = builder.create()
                }
                progressDig?.show()
            }
        })
    }


    /**
     * 設定健康紀錄
     */
    fun setHealthRecord(entity: HealthRecordEntity) {
        tvWalkCount.text = entity.step_count.toString()
        tvWalkTarget.text = "目標${entity.user_step_goal}步"
        tvCalorie.text = "${entity.step_calorie}卡"
        tvWalkTime.text =
                "${secondToHM(entity.step_moving_time).split(":")[0]}小時${secondToHM(entity.step_moving_time).split(":")[1]}分鐘"
        tvWalkDistance.text = distanceKilometer(entity.step_distance)
        sBarWalk.progress = (entity.step_count.toFloat() / entity.user_step_goal * 100).toInt()

        // --- 睡眠 ---
        tvHour.text = secondToHM(entity.sleep_duration).split(":")[0].toString()
        tvMinutes.text = secondToHM(entity.sleep_duration).split(":")[1].toString()
        tvDeepSleep.text =
                "${secondToHM(entity.sleep_duration_deep).split(":")[0]}小時${secondToHM(entity.sleep_duration_deep).split(
                    ":"
                )[1]}分鐘"
        tvLightSleep.text =
                "${secondToHM(entity.sleep_duration_light).split(":")[0]}小時${secondToHM(entity.sleep_duration_light).split(
                    ":"
                )[1]}分鐘"

        tvHeart.text = entity.hr_bpm_avg.toInt().toString()
        tvOxygen.text = entity.highest_percent.toString()
        tvUpdateDate.text = entity.latest_datetime.let {
            if (it.isEmpty()) "無資料" else it
        }

        heartChart.clearData()
        oxygenChart.clearData()
        var heartData: List<Float> = entity.heart_rates.map {
            it.hr_bpm.toFloat()
        }
        heartChart.showData(heartData)

        var oxygenData: List<Float> = entity.oximetry.map {
            it.oy_percent.toFloat()
        }
        oxygenChart.showData(oxygenData)


        for (pos in 0..tvWeeks.size - 1) {
            if (pos < entity.step_week.size) {
                // 有資料
                tvWeeks.get(pos).setStatus(entity.step_week.get(pos))
            } else {
                // 無資料
                tvWeeks.get(pos).setStatus(-1)
            }
        }
    }

    // 設定手環配對
    fun setIsPairWatch(boolean: Boolean) {
        isPairWatch = boolean
        layoutUnpair.visibility =
                if (isPairWatch) {
                    initDate()
                    setIsConnectWatch(isConnectWatch)
                    View.GONE
                } else View.VISIBLE
    }

    // 設定手環連線狀態
    fun setIsConnectWatch(boolean: Boolean) {
        isConnectWatch = boolean
        btnDetect.isEnabled = boolean
    }

    /**
     * 開始偵測
     */
    private lateinit var rscLiveData: LiveData<RunSpeedCadenceEntity>
    fun Button.startDetect() {
        if (text == "開始偵測") {
            text = "停止偵測"
            (activity as MainActivity).hideToolbarAndHideNavigation()
            layoutTitleBar.visibility = View.GONE
            layoutWalk.visibility = View.GONE
            layoutSleep.visibility = View.GONE
            swipeRefresh.isEnabled = false
            heartRecordEntity = RequestHeartRateEntity(USER_ID)
            oxygenRecordEntity = RequestOximetryEntity(USER_ID)
            heartChart.clearData()
            oxygenChart.clearData()
            tvHeart.text = "0"
            tvOxygen.text = "0"
            rscLiveData = viewModel.enabledNotifyRSC().apply {
                observe(viewLifecycleOwner, Observer(update))
            }
        } else {
            if (::rscLiveData.isInitialized) rscLiveData.removeObservers(viewLifecycleOwner)
            viewModel.disableNotifyRSC()
            stopDetect()
        }
    }

    /**
     * 結束偵測
     */
    fun stopDetect() {

        progressDig?.show()
        btnDetect.text = "開始偵測"
        (activity as MainActivity).hideToolbarAndShowNavigation()
        layoutTitleBar.visibility = View.VISIBLE
        layoutWalk.visibility = View.VISIBLE
        layoutSleep.visibility = View.VISIBLE
        swipeRefresh.isEnabled = true
        // 上傳資料
        viewModel.updateRecord(heartRecordEntity, oxygenRecordEntity).observe(this, Observer {
            progressDig?.dismiss()
            if (it.isSuccess == true) {
                // 上傳成功後，更新資料
                tvDate.showDate(mCalendar)
                //android.widget.Toast.makeText(context, "上傳成功", android.widget.Toast.LENGTH_SHORT).show()
            } else {
                //android.widget.Toast.makeText(context, "上傳失敗 : ${it.msg}", android.widget.Toast.LENGTH_SHORT).show()
                view?.apply {
                    Snackbar.make(this, "上傳失敗", Snackbar.LENGTH_INDEFINITE).apply {
                        setActionTextColor(ResourcesCompat.getColor(resources, R.color.colorAccent, null))
                        setAction("知道了") { dismiss() }
                        show()
                    }
                }
            }
        })
    }

    /**
     * 新增上傳的心率資料
     */
    fun addHeartData(value: Int) {
        heartRecordEntity?.addHeartRate(
            RequestHeartRateEntity.HeartRatesEntity(
                value,
                (System.currentTimeMillis() * 0.001).toLong()
            )
        )
    }

    /**
     * 新增血氧資料
     */
    fun addOxygenData(value: Int) {
        oxygenRecordEntity?.addOximetry(
            RequestOximetryEntity.OximetryEntity(
                value,
                (System.currentTimeMillis() * 0.001).toLong()
            )
        )
    }

    private val update: (rscEntity: RunSpeedCadenceEntity) -> Unit = { rscEntity ->

        val oValue = rscEntity.oxRate//(Math.random() * 30 + 70).toFloat()//
        if (oValue > 0) {
            addOxygenData(oValue)
            oxygenChart.showData(oValue.toFloat())
            tvOxygen.text = oValue.toInt().toString()
        }

        val hValue = rscEntity.heartRate//(Math.random() * 150 + 50).toFloat()//
        if (hValue > 0) {
            addHeartData(hValue)
            heartChart.showData(hValue.toFloat())
            tvHeart.text = hValue.toInt().toString()
        }

        // 顯示多筆資料
        //heartChart.showData(listOf<Float>(50f, 50f, 70f, 50f))

    }

    fun isYesterday(time: Long): Boolean {
        // 今天時間 = 昨天時間 + 時間差
        return DateUtils.isToday(time + DateUtils.DAY_IN_MILLIS)
    }

    /**
     * value 秒數換時分秒
     */
    fun secondToHM(value: Int): String {
        var value = value
        val second = (value % 60)
        val minute = (value / 60) % 60
        val hour = (value / 3600)
        return String.format(
            "%02d:%02d:%02d",
            hour,
            minute,
            second
        )
    }

    /**
     * 公尺換公里
     * value : 公尺
     */
    fun distanceKilometer(value: Int): String {
        return String.format(
            "%d公里%d公尺",
            value / 1000,
            value % 1000
        )
    }



} // class close
