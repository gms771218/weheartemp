package tw.azul.wehear.ble

import android.app.NotificationChannel
import android.app.NotificationManager
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.*
import androidx.core.app.NotificationCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.*
import tw.azul.feature.ble.repository.BleRepository
import tw.azul.wehear.R
import tw.azul.wehear.utils.FilterBuilder
import tw.azul.wehear.utils.SettingsBuilder
import tw.azul.wehear.utils.build
import tw.idv.fy.kotlin.utils.LogV

class BleConnectionListener : LifecycleService() {

    companion object {
        const val TAG = "BleConnectionListener"
        private val scanSetting = SettingsBuilder().build{
            setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
        }
    }

    private lateinit var deviceBandLiveData: LiveData<BluetoothDevice>
    private lateinit var deviceHostLiveData: LiveData<BluetoothDevice>
    private val bleEnabledLiveData = MutableLiveData<Boolean>()
    private val mMainHandler = Handler(Looper.getMainLooper())
    private lateinit var mHandler: Handler

    private val bandScanCallback = BandScanCallback()
    private val hostScanCallback = HostScanCallback()

    override fun onCreate() {
        super.onCreate()
        object : HandlerThread(TAG) {
            override fun onLooperPrepared() {
                super.onLooperPrepared()
                mHandler = Handler(looper) { msg ->
                    when (msg.what) {
                    }
                    false
                }
            }
        }.start()
        // 注入裝置資料
        deviceBandLiveData = DeviceRepository.Instance(application).bandLiveData
        deviceHostLiveData = DeviceRepository.Instance(application).hostLiveData
        // 觀察藍牙並自動打開
        val bleRepository = BleRepository.Instance(application)
        bleRepository.bleStatusAddObserver(this, object : Observer<Boolean> {
            private val observer = this
            private var oldValue = false
            override fun onChanged(isBleEnabled: Boolean?) {
                if (isBleEnabled == null || oldValue == isBleEnabled) {
                    return
                } else {
                    bleEnabledLiveData.postValue(isBleEnabled)
                }
                if (!(deviceBandLiveData.value == null && deviceHostLiveData.value == null || isBleEnabled)) {
                    bleRepository.bleStatusDelObserver(this)
                    mMainHandler.postDelayed({ // 延遲一秒, 讓藍牙背後關閉運作一下 (Android 5 藍牙關閉會不完整)
                        // 每次 bleStatusAddObserver 都會強制打開 BLE
                        bleRepository.bleStatusAddObserver(this@BleConnectionListener, object : Observer<Boolean> {
                            override fun onChanged(isBleEnabled: Boolean?) {
                                if (isBleEnabled == true) {
                                    bleRepository.bleStatusDelObserver(this)
                                    bleRepository.bleStatusAddObserver(this@BleConnectionListener, observer)
                                }
                            }
                        })
                    }, 1000)
                }
                oldValue = isBleEnabled
            }
        })
        val bleAccessoryStatusLiveData = AccessoryRepository.Instance(application).getBleStatusLiveData().run {
            MediatorLiveData<AccessoryRepository.BleStatus>().apply {
                addSource(this@run) {
                    if (value != it) postValue(it) // 過濾重複
                }
            }
        }
        MediatorLiveData<Boolean>().apply {
            addSource(bleEnabledLiveData) {
                if (value != it) postValue(it)   // 過濾重複
            }
        }.observe(this, Observer { bleEnabled ->
            if (bleEnabled) {
                bleAccessoryStatusLiveData.observe(this@BleConnectionListener, Observer { bleStatus->
                    bleStatus.LogV(TAG)
                    when {
                        bleStatus.band.isConnected == true -> {
                            bandScanCallback.stop()
                            when {
                                bleStatus.host.isConnected == true -> hostScanCallback.stop()
                                bleStatus.host.isPaired == true -> hostScanCallback.start()
                                else -> hostScanCallback.stop()
                            }
                        }
                        bleStatus.band.isPaired == true -> bandScanCallback.start()
                        else -> {
                            bandScanCallback.stop()
                            when {
                                bleStatus.host.isConnected == true -> hostScanCallback.stop()
                                bleStatus.host.isPaired == true -> hostScanCallback.start()
                                else -> hostScanCallback.stop()
                            }
                        }
                    }
                })
            } else {
                bleAccessoryStatusLiveData.removeObservers(this@BleConnectionListener)
                bandScanCallback.stop()
                hostScanCallback.stop()
            }
        })
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            (getSystemService(NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(
                NotificationChannel(TAG, "愛耳發", NotificationManager.IMPORTANCE_DEFAULT)
            )
        }
        with(application) {
            startForeground(1,
                NotificationCompat.Builder(this, AccessoryRepository.TAG)
                    .setContentTitle(getText(R.string.app_name))
                    .setContentText(getText(R.string.notification_bt_hint))
                    .setSmallIcon(android.R.drawable.stat_sys_data_bluetooth)
                    .setColor(ResourcesCompat.getColor(resources, R.color.bluetooth_sd_color, null))
                    .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.app_launcher_round))
                    .build()
            )
        }
        return super.onStartCommand(intent, flags, startId) // this is START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        super.onBind(intent)
        return null
    }

    override fun onUnbind(intent: Intent?): Boolean = true

    private inner class BandScanCallback : android.bluetooth.le.ScanCallback() {

        private lateinit var scanFilters: List<ScanFilter>

        internal fun start() {
            runCatching {
                val device = requireNotNull(deviceBandLiveData.value)
                scanFilters = mutableListOf(FilterBuilder().build {
                    setDeviceAddress(device.address) // 不一定會有 device name (因為 Parcel 只存 address)
                    setServiceUuid(ParcelUuid.fromString("0000fee7-0000-1000-8000-00805f9b34fb"))
                })
                BluetoothAdapter.getDefaultAdapter()?.bluetoothLeScanner?.startScan(scanFilters, scanSetting, this)
            }.exceptionOrNull()?.printStackTrace()
        }

        internal fun stop() {
            runCatching {
                BluetoothAdapter.getDefaultAdapter()?.bluetoothLeScanner?.stopScan(this)
            }.exceptionOrNull()?.printStackTrace()
        }

        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            val device = deviceBandLiveData.value
            if (result != null && result.device.address == device?.address && scanFilters.any { it.matches(result) }) {
                BluetoothAdapter.getDefaultAdapter()?.bluetoothLeScanner?.also { bluetoothLeScanner -> // 非空即藍牙有開
                    bluetoothLeScanner.stopScan(this)
                    mMainHandler.post {
                        MediatorLiveData<BandConnector.ConnectionState>().apply {
                            var isFirst = true
                            val sourceLiveData = BandConnector.Instance(application).connectGatt(result.device)
                            addSource(sourceLiveData) {
                                if (isFirst) { // 濾過第一個值
                                    isFirst = false
                                    return@addSource
                                }
                                postValue(it)
                            }
                            observe(this@BleConnectionListener, Observer { connectionState ->
                                when (connectionState) {
                                    is BandConnector.ConnectionState.CONNECTED -> {
                                        removeObservers(this@BleConnectionListener)
                                    }
                                    is BandConnector.ConnectionState.DISCONNECTED -> { // 會進來這, 代表不曾有過 CONNECTED
                                        removeObservers(this@BleConnectionListener)
                                        start() // 重新輪迴
                                    }
                                }
                            })
                        }
                    }
                }
            }
        }
    }

    private inner class HostScanCallback : android.bluetooth.le.ScanCallback() {

        private lateinit var scanFilters: List<ScanFilter>

        internal fun start() {
            runCatching {
                val device = requireNotNull(deviceHostLiveData.value)
                scanFilters = mutableListOf(FilterBuilder().build {
                    setDeviceAddress(device.address) // 不一定會有 device name (因為 Parcel 只存 address)
                    setServiceUuid(ParcelUuid.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e"))
                })
                BluetoothAdapter.getDefaultAdapter().bluetoothLeScanner?.startScan(scanFilters, scanSetting, this)
            }.exceptionOrNull()?.printStackTrace()
        }

        internal fun stop() {
            runCatching {
                BluetoothAdapter.getDefaultAdapter()?.bluetoothLeScanner?.stopScan(this)
            }.exceptionOrNull()?.printStackTrace()
        }

        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            val device = deviceHostLiveData.value
            if (result != null && result.device.address == device?.address && scanFilters.any { it.matches(result) }) {
                BluetoothAdapter.getDefaultAdapter()?.bluetoothLeScanner?.also { bluetoothLeScanner -> // 非空即藍牙有開
                    bluetoothLeScanner.stopScan(this)
                    mMainHandler.post{
                        MediatorLiveData<HostConnector.ConnectionState>().apply {
                            var isFirst = true
                            val sourceLiveData = HostConnector.Instance(application).connectGatt(result.device)
                            addSource(sourceLiveData) {
                                if (isFirst) { // 濾過第一個值
                                    isFirst = false
                                    return@addSource
                                }
                                postValue(it)
                            }
                            observe(this@BleConnectionListener, Observer { connectionState ->
                                when (connectionState) {
                                    is HostConnector.ConnectionState.CONNECTED -> {
                                        removeObservers(this@BleConnectionListener)
                                    }
                                    is HostConnector.ConnectionState.DISCONNECTED -> { // 會進來這, 代表不曾有過 CONNECTED
                                        removeObservers(this@BleConnectionListener)
                                        start() // 重新輪迴
                                    }
                                }
                            })
                        }
                    }
                }
            }
        }
    }
}