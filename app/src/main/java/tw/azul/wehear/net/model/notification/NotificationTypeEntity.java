package tw.azul.wehear.net.model.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 通知類型
 */
public class NotificationTypeEntity {

    /**
     * 通知類型id
     */
    @Expose
    @SerializedName("nts_id")
    public int nts_id;

    /**
     * 通知類型名稱
     */
    @Expose
    @SerializedName("nts_name")
    public String nts_name;

}
