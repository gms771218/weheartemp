package tw.azul.wehear.user.family

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel;
import tw.azul.wehear.net.model.base.Resource
import tw.azul.wehear.net.model.dailyInfo.HealthRecordEntity
import tw.azul.wehear.net.repository.DailyRepository

class DeafRecordViewModel : ViewModel() {

    var dailyRepository: DailyRepository = DailyRepository.getInstance()


    fun getHealthRecord(user_id: Int, datetime: String): LiveData<Resource<HealthRecordEntity>> {
        return dailyRepository.getHealthRecord(user_id, datetime)
    }

}
