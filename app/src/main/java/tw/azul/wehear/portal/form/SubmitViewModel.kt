package tw.azul.wehear.portal.form

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SubmitViewModel : ViewModel() {

    class CountDownLiveData : MutableLiveData<Int>() {
        override fun onActive() {
            super.onActive()
            Handler {
                value = it.what
                true
            }.apply {
                sendEmptyMessageDelayed(0, 2 * 1000L)
                sendEmptyMessageDelayed(1, 1 * 1000L)
                sendEmptyMessageDelayed(2, 0 * 1000L)
            }
        }
    }

    fun countDownLiveData(): CountDownLiveData = CountDownLiveData()
}
