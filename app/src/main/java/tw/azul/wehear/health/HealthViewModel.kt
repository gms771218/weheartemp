package tw.azul.wehear.health

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import tw.azul.wehear.ble.BandGattRepository
import tw.azul.wehear.ble.model.RunSpeedCadenceEntity
import tw.azul.wehear.ble.model.RunSpeedCadenceRepository
import tw.azul.wehear.net.model.base.Resource
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.dailyInfo.HealthRecordEntity
import tw.azul.wehear.net.model.request.RequestHeartRateEntity
import tw.azul.wehear.net.model.request.RequestOximetryEntity
import tw.azul.wehear.net.repository.DailyRepository

/**
 * 健康紀錄
 */
class HealthViewModel(val app : Application) : AndroidViewModel(app) {

    var dailyRepository: DailyRepository = DailyRepository.getInstance()

    fun getRSCLiveData() = RunSpeedCadenceRepository.Instance(app).getRSC()
    fun uploadRSC() = run {
        BandGattRepository.Instance(app).getHistoryRSC()
        BandGattRepository.Instance(app).getRSC()
    }
    fun peekRSC() = BandGattRepository.Instance(app).peekRSC()
    fun enabledNotifyRSC(): LiveData<RunSpeedCadenceEntity> = RunSpeedCadenceRepository.Instance(app).enabledNotifyRSC()
    fun disableNotifyRSC(): Unit = RunSpeedCadenceRepository.Instance(app).disableNotifyRSC()

    fun getHealthRecord(user_id: Int, datetime: String): LiveData<Resource<HealthRecordEntity>> {
        return dailyRepository.getHealthRecord(user_id, datetime)
    }

    /**
     * 上傳紀錄
     * requestHeartRateEntity   : 心率
     * requestOximetryEntity    : 血氧
     */
    fun updateRecord(
        requestHeartRateEntity: RequestHeartRateEntity?,
        requestOximetryEntity: RequestOximetryEntity?
    ): LiveData<Result> {
        var resultLiveData = MediatorLiveData<Result>()
        var result = Result(true, "")
        resultLiveData.addSource(dailyRepository.postHealthRecord(requestHeartRateEntity), {
            if (it.status != Status.LOADING) {
                if ((it.status == Status.SUCCESS && it.data?.status != 1000)
                    || it.data == Status.ERROR
                ) {
                    result.msg = it.data?.msg!!
                    result.isSuccess = false
                }
                resultLiveData.addSource(dailyRepository.postOximetryRecord(requestOximetryEntity), {
                    if ((it.status == Status.SUCCESS && it.data?.status != 1000)
                        || it.data == Status.ERROR
                    ) {
                        result.msg = it.data?.msg!!
                        result.isSuccess = false
                    }
                    resultLiveData.postValue(result)
                })
            }
        })
        return resultLiveData
    }


    class Result {
        var isSuccess: Boolean? = null
        var msg: String = ""

        constructor(isSuccess: Boolean?, msg: String) {
            this.isSuccess = isSuccess
            this.msg = msg
        }
    }

}
