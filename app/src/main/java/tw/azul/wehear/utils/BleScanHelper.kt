package tw.azul.wehear.utils

import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanSettings

/*
    Kotlin Extension by Self
*/
typealias SettingsBuilder = ScanSettings.Builder
typealias FilterBuilder   = ScanFilter.Builder
internal fun SettingsBuilder.build(block: (SettingsBuilder.() -> Unit)?): ScanSettings
        = apply { block?.let { run(it) } }.build()
internal fun FilterBuilder.build(block: (FilterBuilder.() -> Unit)?): ScanFilter
        = apply { block?.let { run(it) } }.build()