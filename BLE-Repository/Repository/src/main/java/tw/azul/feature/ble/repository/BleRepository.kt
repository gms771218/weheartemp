@file:Suppress("FunctionName")

package tw.azul.feature.ble.repository

import android.Manifest
import android.app.Application
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.provider.Settings.Secure
import android.provider.Settings.Secure.LOCATION_MODE
import android.provider.Settings.Secure.LOCATION_MODE_OFF
import androidx.core.content.PermissionChecker
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import tw.idv.fy.utils.checkpermissions.CheckPermissionsActivity

class BleRepository private constructor(val application: Application) {

    companion object {
        @Volatile
        private var singleton: BleRepository? = null
        fun Instance(application: Application): BleRepository = singleton ?: synchronized(Companion) {
            singleton = singleton ?: BleRepository(application)
            return singleton!!
        }
        private val BleRequestedPermissions = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION
            , Manifest.permission.ACCESS_FINE_LOCATION
            , Manifest.permission.BLUETOOTH_ADMIN
            , Manifest.permission.BLUETOOTH
        )
    }

    /*
        藍牙狀態: true = 啟用
     */
    private val bleStatusLiveData = MutableLiveData<Boolean>()

    private val bleStatusReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val state = intent?.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF)
            android.util.Log.w("Faty","state = $state")
            when (state) {
                BluetoothAdapter.STATE_OFF -> false
                //STATE_TURNING_ON -> true
                BluetoothAdapter.STATE_ON -> true
                //BluetoothAdapter.STATE_TURNING_OFF -> false
                //14/*STATE_BLE_TURNING_ON*/ -> true
                15/*STATE_BLE_ON*/ -> true
                //16/*STATE_BLE_TURNING_OFF*/ -> false
                else -> null
            }?.let{
                bleStatusLiveData.value = it
            }
        }
    }

    init {
        application.apply {
            registerReceiver(bleStatusReceiver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
        }
    }

    fun bleStatusAddObserver(lifecycleOwner: LifecycleOwner, observer: (t: Boolean) -> Unit) {
        bleStatusAddObserver(lifecycleOwner, Observer(observer))
    }

    fun bleStatusAddObserver(lifecycleOwner: LifecycleOwner, observer: Observer<Boolean>) {
        with(bleStatusLiveData) {
            if (lifecycleOwner !is IBleApp) checkPermissionsAndEnabled().apply {
                if (!state) {
                    Intent(application, Activity::class.java).apply {
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        application.startActivity(this)
                    }
                }
                value = state
            }
            observe(lifecycleOwner, observer)
        }
    }

    fun bleStatusDelObserver(observer: Observer<Boolean>) {
        bleStatusLiveData.removeObserver(observer)
    }

    private fun checkPermissionsAndEnabled(): CheckResult = when {
        !BleRequestedPermissions.all { PermissionChecker.checkSelfPermission(application, it) == PERMISSION_GRANTED } ->
        {
            CheckResult.NeedPermission
        }
        VERSION.SDK_INT >= VERSION_CODES.P &&
                !(application.getSystemService(Context.LOCATION_SERVICE) as LocationManager).isLocationEnabled ||
        VERSION.SDK_INT < VERSION_CODES.P &&
                Secure.getInt(application.contentResolver, LOCATION_MODE, LOCATION_MODE_OFF) == LOCATION_MODE_OFF ->
        {
            CheckResult.NeedLocationTurnOn
        }
        !runCatching {
            BluetoothAdapter.getDefaultAdapter().isEnabled
        }.onFailure { it.printStackTrace() }.getOrDefault(false) ->
        {
            CheckResult.NeedBluetoothTurnOn
        }
        else ->
        {
            CheckResult.Success
        }
    }

    // 中繼者
    class Activity : android.app.Activity() {
        override fun onResume() {
            super.onResume()
            when (Instance(application).checkPermissionsAndEnabled()) {
                CheckResult.Success -> {
                    finish()
                }
                CheckResult.NeedPermission -> {
                    Intent(application, CheckPermissionsActivity::class.java).apply {
                        putExtra(CheckPermissionsActivity.REQUESTED_PERMISSIONS_KEY, BleRequestedPermissions)
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        application.startActivity(this)
                    }
                }
                CheckResult.NeedLocationTurnOn -> {
                    Intent(application, LocationTurnOnActivity::class.java).apply {
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        application.startActivity(this)
                    }
                }
                CheckResult.NeedBluetoothTurnOn -> {
                    Intent(application, BleTurnOnActivity::class.java).apply {
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        application.startActivity(this)
                    }
                }
            }
        }
    }

    // 開啟定位
    class LocationTurnOnActivity : android.app.Activity() {
        override fun onResume() {
            super.onResume()
            if (LocationPositionHelper.CheckLocationSettings(this, true)) {
                finish()
            }
        }
    }

    private enum class CheckResult(val state: Boolean) {
        Success(true),
        NeedPermission(false),
        NeedLocationTurnOn(false),
        NeedBluetoothTurnOn(false)
    }
}