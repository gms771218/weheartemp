package tw.azul.wehear.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_init.*
import tw.azul.wehear.GotoMainActivityDelegate
import tw.azul.wehear.IGotoMainActivity
import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.account.AccountRoom.Status
import android.content.Intent.ACTION_VIEW
import android.net.Uri

class InitFragment : Fragment() {

    companion object {
        fun newInstance() = InitFragment()
    }

    private lateinit var viewModel: InitViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_init, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(InitViewModel::class.java)
        viewModel.accountStatusLiveData.observe(viewLifecycleOwner, Observer { status ->
            when (status) {
                is Status.Init, is Status.Error -> {
                    findNavController().navigate(
                        R.id.action_initFragment_to_portalFragment,
                        FragmentNavigatorExtras(logo to "LOGO")
                    )
                }
            }
        })
        viewModel.accountTokenLiveData.observe(viewLifecycleOwner, object: Observer<AccountRoom.Token> {
            val delegate: IGotoMainActivity = GotoMainActivityDelegate(activity)
            override fun onChanged(token: AccountRoom.Token?) {
                if (token == null) return

                viewModel.checkVersionLiveData().observe(viewLifecycleOwner, Observer { version ->
                    version?.apply {
                        if (version.vts_id != 2) {
                            delegate.gotoMainActivity()
                        } else {
                            version?.apply {
                                AlertDialog.Builder(context!!, R.style.Theme_MaterialComponents_Light_Dialog_Alert)
                                    .apply {
                                        setCancelable(false)
                                        setTitle("軟體更新")
                                        setMessage("目前APP已有最新版本，建議您更新！以獲得最佳體驗喔！")
                                        setPositiveButton("前往更新") { dialog, which ->
                                            val intent = Intent(ACTION_VIEW)
                                            intent.setData(Uri.parse("market://details?id=${context.packageName}"))
                                            startActivity(intent)
                                            dialog.dismiss()
                                            activity?.finish()
                                        }
                                        if (version.vs_force == 0)
                                            setNegativeButton("暫時不用") { dialog, which ->
                                                dialog.dismiss()
                                                delegate.gotoMainActivity()
                                            }
                                    }.show()
                            }
                        }
                    }
                })
            }
        })
    }



    /*
        Kotlin Extension by Self
     */
    //private fun Handler.postDelayed(delayMillis: Long, r: () -> Unit) = postDelayed(r, delayMillis)
    private fun NavController.navigate(resId: Int, extras: Navigator.Extras) = navigate(resId, null, null, extras)
}
