package tw.azul.wehear.fcm

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.PowerManager
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.gson.Gson
import tw.azul.wehear.MainActivity
import tw.azul.wehear.MyActivityLifecycleCallback
import tw.azul.wehear.R
import tw.azul.wehear.fcm.model.NotificationEntity

/**
 *
 * 收取自訂推播的廣播通知
 *
 */
class NotificationBroadcastReceiver : BroadcastReceiver() {

    companion object {
        val TAG = "NotificationReceiver"

        // Power lock 釋放時間
        const val WALK_LOCK_RELEASE_TIME : Long = 5000L

        const val GROUP_KEY_NOTIFICATION : String = "group_key_notification"

        /**
         * 顯示Dialog
         */
        fun OpenDialog(context: Context, intent: Intent) {
            context.startActivity(Intent(context, MainActivity::class.java).apply {
                putExtra("data", intent.getStringExtra("data"))
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            })
        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        Log.d(TAG, "NotificationBroadcastReceiver " + intent.getStringExtra("data"))
        if (checkAppRunForeground(context))
            OpenDialog(context, intent)
        else
            sendNotification(context, Gson().fromJson(intent.getStringExtra("data"), NotificationEntity::class.java))
    }

    /**
     * 顯示推播通知
     */
    private fun sendNotification(context: Context, entity: NotificationEntity) {
        val intent = Intent(context, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        intent.putExtra("data", Gson().toJson(entity))
        // requestCode需不一樣，否則當收到多筆推播時，打開會是同樣的資訊
        val pendingIntent = PendingIntent.getActivity(
            context, entity.str_hex?.hashCode()?:0 , intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val channelId = context.getApplicationContext().getPackageName()

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            mNotificationManager.createNotificationChannel(channel)
        }

        val notificationBuilder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.mipmap.logo_s)
            .setContentTitle("We hear")
            .setContentText(entity.body)
            .setContentIntent(pendingIntent)
            // 設定推播類別
            .setCategory(Notification.CATEGORY_MESSAGE)
            // 設定優先權
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setAutoCancel(true)
            .setDefaults(Notification.DEFAULT_ALL)
            .setGroup(GROUP_KEY_NOTIFICATION)

        val summaryNotification = NotificationCompat.Builder(context , channelId)
            .setSmallIcon(R.mipmap.logo_s)
            .setGroup(GROUP_KEY_NOTIFICATION)
            .setGroupSummary(true)

        mNotificationManager.notify(entity.str_hex?.hashCode()?:0 , notificationBuilder.build())
        // TODO 2019.01.02 7.0以下Group待處理
        // 2019.01.10 先不使用，會有空訊息未移除的問題，需自行計算推播數量，並清除
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
//            mNotificationManager.notify(0 ,summaryNotification.build() )
        screenWalkLock(context)
    }

    /**
     * 螢幕解鎖
     * notes : add <uses-permission android:name="android.permission.WAKE_LOCK"/>
     */
   private fun screenWalkLock(context: Context){
        var powerManager: PowerManager = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        var wakeLock = powerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP or PowerManager.SCREEN_DIM_WAKE_LOCK, "tag::power")
        wakeLock.acquire(WALK_LOCK_RELEASE_TIME)
    }

    /**
     * 檢查App是否在前景執行
     */
    internal fun checkAppRunForeground(context: Context): Boolean {

        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
        val list = activityManager!!.runningAppProcesses
        for (info in list) {
            if (info.processName == context.packageName) {
                // 目前正在前景執行
                var isForeGround = info.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND
                return isForeGround && MyActivityLifecycleCallback.isRunOnForeGround()
            }
        }
        return false
    }

}
