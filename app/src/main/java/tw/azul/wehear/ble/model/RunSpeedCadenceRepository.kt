@file:Suppress("NonAsciiCharacters", "EnumEntryName", "UNUSED_PARAMETER", "FunctionName")

package tw.azul.wehear.ble.model

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.ble.AccessoryRepository
import tw.azul.wehear.ble.BandGattRepository
import tw.azul.wehear.net.model.request.RequestSleepEntity
import tw.azul.wehear.net.model.request.RequestSleepEntity.SleepsEntity
import tw.azul.wehear.net.model.request.RequestStepEntity
import tw.azul.wehear.net.model.request.RequestStepEntity.StepEntity
import tw.azul.wehear.net.repository.DailyRepository
import tw.idv.fy.kotlin.utils.LogD
import tw.idv.fy.kotlin.utils.LogI
import tw.idv.fy.kotlin.utils.LogV
import java.util.*

class RunSpeedCadenceRepository private constructor(private val application: Application) {

    companion object {
        @Volatile
        private var singleton: RunSpeedCadenceRepository? = null
        fun Instance(application: Application): RunSpeedCadenceRepository = singleton ?: synchronized(Companion) {
            singleton = singleton ?: RunSpeedCadenceRepository(application)
            return singleton!!
        }
    }

    private val rscLiveData = MediatorLiveData<ByteArray>()
    private val rscHistoryLiveData = MediatorLiveData<ByteArray>()

    fun getRSC(): LiveData<RunSpeedCadenceEntity> = Transformations.map(rscLiveData) { rscByteArray ->
        RunSpeedCadenceEntity(rscByteArray)
    }

    fun getHistoryRSC(): LiveData<RunSpeedCadenceEntity> = Transformations.map(rscHistoryLiveData) { rscByteArray ->
        RunSpeedCadenceEntity(rscByteArray)
    }

    fun enabledNotifyRSC(): LiveData<RunSpeedCadenceEntity> = Transformations.map(
        BandGattRepository.Instance(application).enabledNotifyRSC()
    ) { rscByteArray -> RunSpeedCadenceEntity(rscByteArray) }

    fun disableNotifyRSC() = BandGattRepository.Instance(application).disableNotifyRSC()

    class LifecycleService: androidx.lifecycle.LifecycleService() {

        companion object {
            const val TAG = "RunSpeedCadence"
        }

        override fun onCreate() {
            super.onCreate()
            var sourceRSCLiveData: LiveData<ByteArray>? = null
            var sourceHistoryRSCLiveData: LiveData<ByteArray>? = null
            AccessoryRepository.Instance(application).getBleStatusLiveData().observe(::getLifecycle) { bleStatus ->
                val rscLiveData = Instance(application).rscLiveData
                val rscHistoryLiveData = Instance(application).rscHistoryLiveData
                val isBandConnected  = bleStatus?.band?.isConnected ?: false
                when {
                    isBandConnected && sourceRSCLiveData == null -> {
                        rscLiveData.apply {
                            sourceRSCLiveData = BandGattRepository.Instance(application).getRSC()
                            addSource(sourceRSCLiveData as LiveData, ::postValue)
                        }
                        rscHistoryLiveData.apply {
                            sourceHistoryRSCLiveData = BandGattRepository.Instance(application).getHistoryRSC()
                            addSource(sourceHistoryRSCLiveData as LiveData, ::postValue)
                        }
                    }
                    !isBandConnected && sourceRSCLiveData != null -> {
                        rscLiveData.apply {
                            removeSource(sourceRSCLiveData as LiveData)
                            sourceRSCLiveData = null
                        }
                        rscHistoryLiveData.apply {
                            removeSource(sourceHistoryRSCLiveData as LiveData)
                            sourceHistoryRSCLiveData = null
                        }
                    }
                }
            }
            // RSC 上傳 (今日)
            Instance(application).getRSC().observe(this, Observer { rscEntity ->
                rscEntity.LogV("RunSpeedCadenceEntity(今天):")
                uploadRSC(rscEntity)
            })
            // RSC 上傳 (昨日)
            Instance(application).getHistoryRSC().observe(this, Observer { rscHistoryEntity ->
                rscHistoryEntity.LogD("RunSpeedCadenceEntity(昨天):")
                val calendar = Calendar.getInstance().apply {
                    timeInMillis = (System.currentTimeMillis() / 1000) * 1000
                    set(Calendar.HOUR_OF_DAY, 23)
                    set(Calendar.MINUTE, 59)
                    set(Calendar.SECOND, 59)
                    add(Calendar.DAY_OF_YEAR, -1)
                }
                uploadRSC(
                    rscHistoryEntity,
                    calendar.timeInMillis / 1000
                )
            })
        }

        override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
            super.onStartCommand(intent, flags, startId)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                (getSystemService(NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(
                    NotificationChannel(TAG, "愛耳發", NotificationManager.IMPORTANCE_DEFAULT)
                )
            }
            with(application) {
                startForeground(1,
                    NotificationCompat.Builder(this, AccessoryRepository.TAG)
                        .setContentTitle(getText(R.string.app_name))
                        .setContentText(getText(R.string.notification_bt_hint))
                        .setSmallIcon(android.R.drawable.stat_sys_data_bluetooth)
                        .setColor(ResourcesCompat.getColor(resources, R.color.bluetooth_sd_color, null))
                        .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.app_launcher_round))
                        .build()
                )
            }
            return START_NOT_STICKY
        }

        private fun uploadRSC(rscEntity: RunSpeedCadenceEntity, time: Long = System.currentTimeMillis() / 1000) {
            AccountRoom.TokenLiveData.apply {
                observe(this@LifecycleService, Observer { token ->
                    removeObservers(this@LifecycleService)
                    //token.LogW("Token:")
                    runCatching {
                        DailyRepository.getInstance() to requireNotNull(token.userId)
                    }.onSuccess {
                        val (dailyRepository, userId) = it
                        val strideLength = 30.0 // TODO: 步伐長度之後再改變動
                        with(rscEntity) {
                            // 上傳步數紀錄
                            if (activeTime > 0 && steps > 0 && distance > 0 && calorie > 0 && strideLength > 0) {
                                dailyRepository.postStepRecord(RequestStepEntity(userId).apply {
                                    addStep(StepEntity().apply {
                                        st_id = 1
                                        step_split = 1
                                        step_datetime = time.toInt()
                                        step_moving_time = activeTime * 60
                                        step_count = steps
                                        step_distance = distance.toDouble()
                                        step_calorie = calorie.toDouble()
                                        step_stride_length = strideLength
                                    })
                                }).observe(this@LifecycleService, Observer {
                                    it.LogI("上傳步數紀錄:")
                                })
                            }
                            //// 上傳心率紀錄
                            //if (heartRate > 0) {
                            //    dailyRepository.postHealthRecord(RequestHeartRateEntity(userId).apply {
                            //        addHeartRate(HeartRatesEntity(heartRate, time))
                            //    })//.observe(this@LifecycleService, Observer {it.LogI("上傳心率紀錄:")})
                            //}
                            //// 上傳血氧紀錄
                            //if (oxRate > 0) {
                            //    dailyRepository.postOximetryRecord(RequestOximetryEntity(userId).apply {
                            //        addOximetry(OximetryEntity(oxRate, time))
                            //    }).observe(this@LifecycleService, Observer {
                            //        //it.LogI("上傳血氧紀錄:")
                            //    })
                            //}
                            // 上傳睡眠紀錄
                            if (calorie > 0 && (deepSleep > 0 || lightSleep > 0)) {
                                dailyRepository.postSleepRecord(RequestSleepEntity(userId).apply {
                                    if (deepSleep > 0) {
                                        addSleep(SleepsEntity(1, calorie, deepSleep * 60, time))
                                    }
                                    if (lightSleep > 0) {
                                        addSleep(SleepsEntity(2, calorie, lightSleep * 60, time))
                                    }
                                }).observe(this@LifecycleService, Observer {
                                    it.LogI("上傳睡眠紀錄:")
                                })
                            }
                        }
                    }.exceptionOrNull()?.printStackTrace()
                })
            }
        }
    }
}


data class RunSpeedCadenceEntity(
    val status: Status,
    val speed: Int,
    val steps: Int,
    val distance: Int,
    val calorie: Int,
    val activeTime: Int,
    val deepSleep: Int,
    val lightSleep: Int,
    val breakTime: Int,
    val heartRate: Int,
    val oxRate: Int
) {
    constructor(bytes: ByteArray) : this(
        Status.get(bytes[0]),
        bytes[1].toUnsignedInt(),
        (bytes[ 2] to bytes[ 3]).toMsbLsbInt(),
        (bytes[ 4] to bytes[ 5]).toMsbLsbInt(),
        (bytes[ 6] to bytes[ 7]).toMsbLsbInt(),
        (bytes[ 8] to bytes[ 9]).toMsbLsbInt(),
        (bytes[10] to bytes[11]).toMsbLsbInt(),
        (bytes[12] to bytes[13]).toMsbLsbInt(),
        (bytes[14] to bytes[15]).toMsbLsbInt(),
        bytes[16].toUnsignedInt(),
        bytes[17].toUnsignedInt()
    )
}

enum class Status(value: Byte) {
    步行(0x00),
    快走(0x01),
    跑步(0x02),
    白天休息(0x03),
    未睡眠(0x04),
    淺眠(0x05),
    深眠(0x06);

    companion object {
        fun get(value: Byte) = values()[value.toUnsignedInt()]
    }
}

private fun Pair<Byte,Byte>.toMsbLsbInt() = (second.toUnsignedInt() shl 8) or first.toUnsignedInt()
private fun Byte.toUnsignedInt():Int = toInt().and(0xFF)