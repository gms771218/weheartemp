package tw.azul.feature.ble.repository

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer

class BleTurnOnActivity : AppCompatActivity(), IBleApp {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        BleRepository.Instance(application).apply{
            var alertDialog: AlertDialog? = null
            bleStatusAddObserver(this@BleTurnOnActivity) { status ->
                if (status) {
                    if (alertDialog != null) {
                        alertDialog?.dismiss()
                        alertDialog = null
                    }
                    finish()
                }
            }
            bleStatusAddObserver(this@BleTurnOnActivity, object : Observer<Boolean> {
                override fun onChanged(status: Boolean) {
                    if (!status) {
                        bleStatusDelObserver(this)
                        alertDialog = AlertDialogBuilder(this@BleTurnOnActivity) {
                            setMessage("開啟藍牙中")
                            setCancelable(false)
                            show()
                        }
                        BluetoothAdapter.getDefaultAdapter().enable()
                    }
                }
            })
        }
    }

    override fun onBackPressed() {
        /* 禁止事項 */
    }

    @Suppress("FunctionName")
    private fun AlertDialogBuilder(context: Context, block: AlertDialog.Builder.() -> AlertDialog) =
        AlertDialog.Builder(context, R.style.Theme_MaterialComponents_Light_Dialog_Alert).run(block)
}