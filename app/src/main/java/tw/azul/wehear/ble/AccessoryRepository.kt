package tw.azul.wehear.ble

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import tw.azul.wehear.R

class AccessoryRepository private constructor(application: Application) {

    companion object {
        const val TAG = "Accessory"
        @Volatile
        private var singleton: AccessoryRepository? = null
        fun Instance(application: Application): AccessoryRepository = singleton ?: synchronized(Companion) {
            singleton = singleton ?: AccessoryRepository(application)
            return singleton!!
        }
    }

    private class BleStatusLiveData(private val application: Application) : MediatorLiveData<BleStatus>() {
        private var nowBandDevice: BluetoothDevice? = null
        private var nowHostDevice: BluetoothDevice? = null
        init {
            addSource(DeviceRepository.Instance(application).bandLiveData) { bandDevice ->
                android.util.Log.d("Faty", "bandDevice = $bandDevice")
                value = BleStatus(Status.Band(bandDevice != null), value?.host ?: Status.Host())
                when {
                    bandDevice != null -> {
                        val bandConnectLiveData = BandConnector.Instance(application).connectGatt(doConnect = false)
                        removeSource(bandConnectLiveData) // 規避重複增加相同來源但用不同的觀察者
                        addSource(bandConnectLiveData) { connectionState ->
                            value = BleStatus(
                                band = Status.Band(true, connectionState is BandConnector.ConnectionState.CONNECTED),
                                host = value?.host ?: Status.Host()
                            )
                        }
                        nowBandDevice = bandDevice
                    }
                    nowBandDevice != null -> {
                        val bandConnectLiveData = BandConnector.Instance(application).connectGatt(doConnect = false)
                        // 裝置已移除
                        removeSource(bandConnectLiveData)
                        value = BleStatus(band = Status.Band(), host = value?.host ?: Status.Host())
                        nowHostDevice = null
                    }
                }
            }
            addSource(DeviceRepository.Instance(application).hostLiveData) { hostDevice ->
                android.util.Log.d("Faty", "hostDevice = $hostDevice")
                value = BleStatus(value?.band ?: Status.Band(), Status.Host(hostDevice != null))
                when {
                    hostDevice != null -> {
                        val hostConnectLiveData = HostConnector.Instance(application).connectGatt(doConnect = false)
                        removeSource(hostConnectLiveData) // 規避重複增加相同來源但用不同的觀察者
                        addSource(hostConnectLiveData) { connectionState ->
                            value = BleStatus(
                                band = value?.band ?: Status.Band(),
                                host = Status.Host(true, connectionState is HostConnector.ConnectionState.CONNECTED)
                            )
                        }
                        nowHostDevice = hostDevice
                    }
                    nowHostDevice != null -> {
                        val hostConnectLiveData = HostConnector.Instance(application).connectGatt(doConnect = false)
                        // 裝置已移除
                        removeSource(hostConnectLiveData)
                        setValue(BleStatus(band = value?.band ?: Status.Band(), host = Status.Host()))
                        nowHostDevice = null
                    }
                }
            }
        }
    }

    private val bleStatusLiveData = BleStatusLiveData(application).apply {
        // 初始值
        value = BleStatus(Status.Band(), Status.Host())
    }

    sealed class Status(open val isPaired: Boolean? = false, open val isConnected: Boolean? = false) {
        data class Band(override val isPaired: Boolean? = false, override val isConnected: Boolean? = false) : Status(isPaired, isConnected)
        data class Host(override val isPaired: Boolean? = false, override val isConnected: Boolean? = false) : Status(isPaired, isConnected)
    }

    data class BleStatus(val band:Status.Band, val host:Status.Host)

    fun getBleStatusLiveData(): LiveData<BleStatus> = Transformations.map(bleStatusLiveData) { it }

    class LifecycleService: androidx.lifecycle.LifecycleService() {
        override fun onCreate() {
            super.onCreate()
            AccessoryRepository.Instance(application).bleStatusLiveData.apply {
                BandConnector.Instance(application).connectGatt().observe(this@LifecycleService::getLifecycle) {
                    when (it) {
                        /*is BandConnector.ConnectionState.Init -> {
                            setValue(BleStatus(Status.Band(), value?.host ?: Status.Host()))
                        }*/
                        is BandConnector.ConnectionState.CONNECTED -> {
                            value = BleStatus(
                                Status.Band(value?.band?.isPaired == true, value?.band?.isPaired == true),
                                value?.host ?: Status.Host()
                            )
                        }
                        is BandConnector.ConnectionState.Init, is BandConnector.ConnectionState.DISCONNECTED -> {
                            value = BleStatus(
                                Status.Band(value?.band?.isPaired == true, false),
                                value?.host ?: Status.Host()
                            )
                        }
                    }
                }
                HostConnector.Instance(application).connectGatt().observe(this@LifecycleService::getLifecycle) {
                    when (it) {
                        /*is HostConnector.ConnectionState.Init -> {
                            setValue(BleStatus(value?.band ?: Status.Band(), Status.Host()))
                        }*/
                        is HostConnector.ConnectionState.CONNECTED -> {
                            value = BleStatus(
                                value?.band ?: Status.Band(),
                                Status.Host(value?.host?.isPaired == true, value?.host?.isPaired == true)
                            )
                        }
                        is HostConnector.ConnectionState.Init, is HostConnector.ConnectionState.DISCONNECTED -> {
                            value = BleStatus(
                                value?.band ?: Status.Band(),
                                Status.Host(value?.host?.isPaired == true, false)
                            )
                        }
                    }
                }
                observe(this@LifecycleService::getLifecycle) {
                    android.util.Log.d("Faty","$it")
                }
            }
        }

        override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
            super.onStartCommand(intent, flags, startId)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                (getSystemService(NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(
                    NotificationChannel(TAG, "愛耳發", NotificationManager.IMPORTANCE_DEFAULT)
                )
            }
            with(application) {
                startForeground(1,
                    NotificationCompat.Builder(this, TAG)
                        .setContentTitle(getText(R.string.app_name))
                        .setContentText(getText(R.string.notification_bt_hint))
                        .setSmallIcon(android.R.drawable.stat_sys_data_bluetooth)
                        .setColor(ResourcesCompat.getColor(resources, R.color.bluetooth_sd_color, null))
                        .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.app_launcher_round))
                        .build()
                )
            }
            return START_NOT_STICKY
        }
    }
}