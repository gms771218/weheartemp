package tw.azul.wehear.status.band.clockalarm

import android.app.Activity
import android.app.Application
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import tw.azul.wehear.R
import tw.azul.wehear.ble.AccessoryRepository
import tw.azul.wehear.ble.model.BandAlarmEntity
import tw.azul.wehear.ble.model.BandAlarmRepository


class AlarmInfoViewModel(private val app: Application) : AndroidViewModel(app) {

    val loadingLiveData: LiveData<Boolean> = MutableLiveData<Boolean>().apply { value = true }

    val alarmArrayListLiveData = MediatorLiveData<ArrayList<BandAlarmEntity>>().apply {
        addSource(readAlarm()) {
            postValue(ArrayList(it))
            (loadingLiveData as MutableLiveData).postValue(false)
        }
    }

    fun deleteAlarm(deletedEntity: BandAlarmEntity) {
        alarmArrayListLiveData.value
             ?.filterNotTo(ArrayList()) { bandAlarmEntity ->
                bandAlarmEntity.key == deletedEntity.key
            }?.apply {
                // 重整索引
                forEachIndexed { index, bandAlarmEntity -> bandAlarmEntity.key = index }
                saveAlarm(this)
                //alarmArrayListLiveData.postValue(this)
            }
    }

    fun addAlarm(activity: Activity?, entity: BandAlarmEntity) {
        alarmArrayListLiveData.value?.apply {
            when (entity.key) {
                in 0 until size -> {
                    updateAlarm(activity, entity)
                }
                else -> {
                    add(entity)
                    saveAlarm(this)
                    //alarmArrayListLiveData.postValue(this)
                }
            }
        }
    }

    fun updateAlarm(activity: Activity?, entity: BandAlarmEntity) {
        alarmArrayListLiveData.value?.apply {
            runCatching{
                require(entity.key in 0 until size) {"無法修改"}
                this[entity.key] = entity // 直接替換
                saveAlarm(this)
                //alarmArrayListLiveData.postValue(this)
            }.exceptionOrNull()?.apply {
                (loadingLiveData as MutableLiveData).postValue(false)
                //android.widget.Toast.makeText(app, localizedMessage, android.widget.Toast.LENGTH_SHORT).show()
                activity?.apply {
                    AlertDialog.Builder(activity, R.style.Theme_MaterialComponents_Light_Dialog_Alert).apply {
                        setCancelable(false)
                        setTitle("錯誤")
                        setMessage(localizedMessage)
                        setPositiveButton("知道了") { dialog, _ -> dialog.dismiss() }
                        show()
                    }
                }
            }?.printStackTrace()
        }
    }

    /**
     * 讀取手環鬧鐘資料
     */
    private fun readAlarm(): LiveData<List<BandAlarmEntity>> {
        // 傳遞進來
        return BandAlarmRepository.Instance(app).getClockAlarmsList()
    }

    /**
     * 儲存手環鬧鐘資料
     */
    private fun saveAlarm(bandAlarmList: List<BandAlarmEntity>) {
        // 傳遞出去
        BandAlarmRepository.Instance(app).setClockAlarmsList(bandAlarmList)
        (loadingLiveData as MutableLiveData).postValue(true)
    }

    fun getBleStatusLiveData(): LiveData<AccessoryRepository.BleStatus> =
        AccessoryRepository.Instance(app).getBleStatusLiveData()
}
