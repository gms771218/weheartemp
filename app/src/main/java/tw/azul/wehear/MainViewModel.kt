package tw.azul.wehear

import android.app.Application
import android.util.Log
import androidx.annotation.NonNull
import androidx.lifecycle.*
import androidx.work.*
import com.google.firebase.iid.FirebaseInstanceId
import tw.azul.wehear.account.AccountRepository
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.ble.BandGattRepository
import tw.azul.wehear.ble.DeviceRepository
import tw.azul.wehear.net.model.base.Resource
import tw.azul.wehear.net.model.base.ResponseIdResultEntity
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.request.RequestUpdateUserEntity
import tw.azul.wehear.net.model.user.UserEntity
import tw.azul.wehear.net.model.version.VersionEntity
import tw.azul.wehear.net.repository.UserRepository
import tw.azul.wehear.net.repository.VersionRepository
import tw.azul.wehear.net.worker.UnregisterFcmWorker

class MainViewModel(val app: Application) : AndroidViewModel(app) {

    private var userRepository: UserRepository = UserRepository.getInstance()

    private val versionRepository = VersionRepository.getInstance()

    /**
     * 上傳本日健康紀錄
     */
    fun peekRSC() = BandGattRepository.Instance(app).peekRSC()

    /**
     * 檢查使用者裝置狀態
     */
    fun checkUserDevice() {
        DeviceRepository.Instance(app).check()
    }

    fun getUser(user_id: Int): LiveData<Resource<UserEntity>> {
        return userRepository.getUser(user_id);
    }

    fun updateUser(
        user_id: Int,
        requestUpdateUserEntity: RequestUpdateUserEntity
    ): LiveData<Resource<ResponseIdResultEntity>> {
        return userRepository.postUser(user_id, requestUpdateUserEntity)
    }

    fun logout() {
        AccountRepository.Instance(app).logout()
    }

    val finishSignalLiveData: LiveData<Boolean> = MediatorLiveData<Boolean>().apply {
        value = false
        addSource(AccountRoom.StatusLiveData) { status ->
            when (status) {
                is AccountRoom.Status.Init -> value = true
            }
        }
    }

    /**
     * 註銷FCM Token
     */
    fun unregisterFcmToken(){
        WorkManager.getInstance().onceRequest(UnregisterFcmWorker::class.java)
    }

    /**
     * TODO 取得 Token Sample
     */
    fun updateFcmToken(@NonNull owner: LifecycleOwner) {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener { task ->
            if (!task.isSuccessful()) {
                // error msg task.getException()
            } else {
                // Get new Instance ID token
                val token = task.getResult()?.getToken()
                var _user_id = AccountRoom.TokenLiveData.value?.user_id
                if (_user_id != null && _user_id != -1)
                    RequestUpdateUserEntity.create(_user_id, UserEntity(), UserEntity().newToken(token))
                        .also { requestEntity ->
                            updateUser(
                                _user_id,
                                requestEntity
                            ).observe(owner::getLifecycle) {
                                if (it.status == Status.SUCCESS) {
                                    Log.d("MainActivity", "更新Token = $token")
                                }
                            }
                        }
            }
        }
    }

    // 更新Token
    fun UserEntity.newToken(token: String?): UserEntity {
        this.user_app_token = token
        return this
    }

    // 委派一次性WorkManager
    fun WorkManager.onceRequest(@NonNull workerClass: Class<out ListenableWorker>) {
        enqueue(
            OneTimeWorkRequest.Builder(workerClass)
                .setConstraints(Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build())
                .build()
        )
    }

    fun checkVersionLiveData(): LiveData<VersionEntity> {
        return Transformations.map(versionRepository.version) {
            if (it.status!=Status.LOADING)
                if (it.data?.vs_name != BuildConfig.VERSION_NAME) it.data!! else VersionEntity()
            else
                null
        }
    }

} // class close