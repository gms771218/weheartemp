package tw.azul.wehear.net.model.family;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 家人資料
 */
public class FamilyEntity extends FamilyBaseVO {

    // 預設預選index -> 朋友
    final int DEFAULT_RELATION_INDEX = 14 ;

    /**
     * 家人id
     */
    @Expose
    @SerializedName("fs_id")
    public int fs_id;

    /**
     * 使用者id
     */
    @Expose
    @SerializedName("user_id")
    public int user_id;

    /**
     * 使用者名稱
     */
    @Expose
    @SerializedName("user_name")
    public String user_name;

    /**
     * 使用者電話
     */
    @Expose
    @SerializedName("user_phone")
    public String user_phone;

    /**
     * 家人類型id
     */
    @Expose
    @SerializedName("fts_id")
    public int fts_id = DEFAULT_RELATION_INDEX;

    /**
     * 家人類型名稱
     */
    @Expose
    @SerializedName("fts_name")
    public String fts_name;

    /**
     * 家人暱稱
     */
    @Expose
    @SerializedName("fs_nickname")
    public String fs_nickname;

    /**
     * 使用者每天走路步數目標
     */
    @Expose
    @SerializedName("user_step_goal")
    public int user_step_goal;

    /**
     * 使用者走路步數
     */
    @Expose
    @SerializedName("step_count")
    public int step_count;

    /**
     * 使用者總睡眠時間(分)
     */
    @Expose
    @SerializedName("total_sleep")
    public int total_sleep;

    /**
     * 使用者深睡時間(分)
     */
    @Expose
    @SerializedName("deep_sleep")
    public int deep_sleep;

    /**
     * 使用者淺睡時間(分)
     */
    @Expose
    @SerializedName("light_sleep")
    public int light_sleep;

    /**
     * 是否同意邀請
     * 0 : 邀請中
     * 1 : 已邀請
     */
    @Expose
    @SerializedName("fs_accept")
    public int fs_accept;

    /**
     * 是否連結裝置
     * 0 : 正常
     * 1 : 聽障者
     */
    @Expose
    @SerializedName("is_device")
    public int is_device;

    /**
     * 邀約狀態
     * 0 : 未邀請
     * 1 : 已邀請
     * 2 : 已為家人
     */
    @Expose
    @SerializedName("relation")
    public int relation;

    @Override
    public int getType() {
        if (fs_accept == 0) {
            return FamilyBaseVO.REQUEST_ADD_IN;
        } else {
            if (is_device == 0) {
                return FamilyBaseVO.USER_FAMILY;
            } else {
                return FamilyBaseVO.USER_DEAF;
            }
        }
    }

    @Override
    public String toString() {
        return "FamilyEntity{" +
                "fs_id=" + fs_id +
                ", user_id=" + user_id +
                ", user_name='" + user_name + '\'' +
                ", user_phone='" + user_phone + '\'' +
                ", fts_id=" + fts_id +
                ", fts_name='" + fts_name + '\'' +
                ", fs_nickname='" + fs_nickname + '\'' +
                ", user_step_goal=" + user_step_goal +
                ", step_count=" + step_count +
                ", total_sleep=" + total_sleep +
                ", deep_sleep=" + deep_sleep +
                ", light_sleep=" + light_sleep +
                ", fs_accept=" + fs_accept +
                ", is_device=" + is_device +
                ", relation=" + relation +
                '}';
    }
}
