package tw.azul.wehear.status.band.notification

import android.Manifest.permission.RECEIVE_SMS
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import tw.azul.wehear.R
import tw.azul.wehear.databinding.FragmentBandSmsBinding

class SmsNotifyFragment : Fragment() {

    companion object {
        fun newInstance() = SmsNotifyFragment()
    }

    private lateinit var dataBinding: FragmentBandSmsBinding
    private lateinit var viewModel: SmsNotifyViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_band_sms, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SmsNotifyViewModel::class.java)
        dataBinding.viewModel = viewModel
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBleStatusLiveData().observe(viewLifecycleOwner::getLifecycle) { bleStatus ->
            if (bleStatus.band.isConnected != true) {
                findNavController().popBackStack(R.id.bandSettingFragment, false)
            }
        }
        runCatching {
            val activity = requireNotNull(activity)
            when {
                ActivityCompat.checkSelfPermission(activity, RECEIVE_SMS) == PERMISSION_GRANTED -> return
                shouldShowRequestPermissionRationale(RECEIVE_SMS) -> requestPermissions(
                    arrayOf(RECEIVE_SMS), RECEIVE_SMS.hashCode().and(0xFF)
                )
                else -> AlertDialog.Builder(activity, R.style.Theme_MaterialComponents_Light_Dialog_Alert).apply {
                    setPositiveButton("前往設定") { _, _ ->
                        Intent(ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                            data = Uri.parse("package:" + activity.packageName)
                            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(this)
                        }
                    }
                    setNegativeButton("知道了") { _, _ -> findNavController().popBackStack() }
                    setTitle("授權提示")
                    setMessage("「簡訊提醒」需要「SMS」權限")
                    setCancelable(false)
                    show()
                }

            }
        }.onFailure {
            findNavController().popBackStack()
        }.exceptionOrNull()?.printStackTrace()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (RECEIVE_SMS.hashCode().and(0xFF) == requestCode && grantResults[0] != PERMISSION_GRANTED) {
            findNavController().popBackStack()
        }
    }
}
