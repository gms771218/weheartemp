package tw.azul.wehear.user.nofitication

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.work.*
import com.gms.widget.layout.R
import com.google.gson.Gson
import tw.azul.wehear.MainActivity
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.notification.NotificationEntity
import tw.azul.wehear.net.model.request.RequestUserNotificationEntity
import tw.azul.wehear.net.worker.UpdateNotificationWorker
import java.util.ArrayList

/**
 * 通知提醒 - 聽障者
 * 045_發布警示_聽障者
 */
class SettingNotificationDeafFragment : Fragment() {

    val USER_ID = AccountRoom.TokenLiveData.value?.userId!!

    val NOTIFY_TYPE_HEART = 11;
    val NOTIFY_TYPE_OXYGEN = 12;
    val NOTIFY_TYPE_SLEEP = 13;
    val NOTIFY_TYPE_WALK = 14;

    // 睡眠通知時間
    val sleepNotificationHour = arrayOf(9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21)
    // 走路通知時間
    val walkNotificationHour = arrayOf(20, 21, 22, 23, 0)

    lateinit var switchHeart: Switch
    lateinit var switchOxygen: Switch
    lateinit var switchSleep: Switch
    lateinit var switchWalk: Switch

    lateinit var spinnerHeartLow: Spinner
    lateinit var spinnerHeartHeight: Spinner

    lateinit var spinnerOxygenLow: Spinner

    lateinit var spinnerSleepLow: Spinner
    lateinit var spinnerSleepHeight: Spinner
    lateinit var spinnerSleep: Spinner

    lateinit var spinnerWalkLow: Spinner
    lateinit var spinnerWalkHeight: Spinner
    lateinit var spinnerWalk: Spinner

    var progressDig: AlertDialog? = null

    lateinit var notificationViewModel: NotificationViewModel

    lateinit var HEART_LOW_SCOPE: Array<String>
    lateinit var HEART_HEIGHT_SCOPE: Array<String>
    lateinit var OXYGEN_LOW_SCOPE: Array<String>
    lateinit var SLEEP_LOW_SCOPE: Array<String>
    lateinit var SLEEP_HEIGHT_SCOPE: Array<String>
    lateinit var SLEEP_HOUR_SCOPE: Array<String>
    lateinit var WALK_SCOPE_LOW: Array<String>
    lateinit var WALK_SCOPE_HIGH: Array<String>
    lateinit var WALK_HOUR_SCOPE: Array<String>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.layout_notify_deaf, container, false)
        initView(view)
        return view;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? MainActivity)?.showToolbarAndHideNavigation()

        HEART_LOW_SCOPE = resources.getStringArray(R.array.heart_low_scope)
        HEART_HEIGHT_SCOPE = resources.getStringArray(R.array.heart_height_scope)
        OXYGEN_LOW_SCOPE = resources.getStringArray(R.array.oxygen_low_scope)
        SLEEP_LOW_SCOPE = resources.getStringArray(R.array.sleep_low_scope)
        SLEEP_HEIGHT_SCOPE = resources.getStringArray(R.array.sleep_height_scope)
        SLEEP_HOUR_SCOPE = resources.getStringArray(R.array.sleep_notification_hour_scope)
        WALK_SCOPE_LOW = resources.getStringArray(R.array.walk_scope_low)
        WALK_SCOPE_HIGH = resources.getStringArray(R.array.walk_scope_high)
        WALK_HOUR_SCOPE = resources.getStringArray(R.array.notification_hour_scope)

        notificationViewModel = ViewModelProviders.of(this).get(NotificationViewModel::class.java)

        notificationViewModel.getUserNotification(USER_ID).observe(this, Observer {
            if (it.status != Status.LOADING) {
                progressDig?.dismiss()
                setUserNotification(it?.data)
            } else {
                if (progressDig == null) {
                    var builder = AlertDialog.Builder(context)
                    builder.setMessage("請稍候")
                    progressDig = builder.create()
                }
                progressDig?.show()
            }
        })
    }

    fun initView(view: View) {
        switchHeart = view.findViewById(R.id.switch_heart)
        switchOxygen = view.findViewById(R.id.switch_oxygen)
        switchSleep = view.findViewById(R.id.switch_sleep)
        switchWalk = view.findViewById(R.id.switch_walk)

        spinnerHeartLow = view.findViewById(R.id.spinner_heart_low)
        spinnerHeartHeight = view.findViewById(R.id.spinner_heart_height)

        spinnerOxygenLow = view.findViewById(R.id.spinner_oxygen_low)

        spinnerSleepLow = view.findViewById(R.id.spinner_sleep_low)
        spinnerSleepHeight = view.findViewById(R.id.spinner_sleep_height)
        spinnerSleep = view.findViewById(R.id.spinner_sleep_hour)


        spinnerWalkLow = view.findViewById(R.id.spinner_walk_low)
        spinnerWalkHeight = view.findViewById(R.id.spinner_walk_height)
        spinnerWalk = view.findViewById(R.id.spinner_walk_hour)
    }

    fun setListener() {

        // --- switch ---
        switchHeart.setOnCheckedChangeListener { buttonView, isChecked ->
            uploadUserNotification()
        }
        switchOxygen.setOnCheckedChangeListener { buttonView, isChecked ->
            uploadUserNotification()
        }
        switchSleep.setOnCheckedChangeListener { buttonView, isChecked ->
            uploadUserNotification()
        }
        switchWalk.setOnCheckedChangeListener { buttonView, isChecked ->
            uploadUserNotification()
        }

        // --- spinner ---

        spinnerHeartLow.onItemSelectedListener = SelectedListener
        spinnerHeartHeight.onItemSelectedListener = SelectedListener

        spinnerOxygenLow.onItemSelectedListener = SelectedListener

        spinnerSleepLow.onItemSelectedListener = SelectedListener
        spinnerSleepHeight.onItemSelectedListener = SelectedListener
        spinnerSleep.onItemSelectedListener = SelectedListener

        spinnerWalkLow.onItemSelectedListener = SelectedListener
        spinnerWalkHeight.onItemSelectedListener = SelectedListener
        spinnerWalk.onItemSelectedListener = SelectedListener

    }

    fun setUserNotification(list: List<NotificationEntity>?) {
        // 心率
        list?.find { it.nts_id == NOTIFY_TYPE_HEART }?.run {
            setHeartNotification(this)
        }

        // 血氧
        list?.find { it.nts_id == NOTIFY_TYPE_OXYGEN }?.run {
            setOxygenNotification(this)
        }

        // 睡眠
        list?.find { it.nts_id == NOTIFY_TYPE_SLEEP }?.run {
            setSleepNotification(this)
        }

        // 步行
        list?.find { it.nts_id == NOTIFY_TYPE_WALK }?.run {
            setWalkNotification(this)
        }
        setListener()
    }

    /**
     * 設定心率通知
     */
    fun setHeartNotification(entity: NotificationEntity) {
        switchHeart.isChecked = entity.ns_enable == 1

        HEART_LOW_SCOPE.indexOf(entity.ns_min.toString()).also {
            spinnerHeartLow.setSelection(if (it == -1) 0 else it)
        }

        HEART_HEIGHT_SCOPE.indexOf(entity.ns_max.toString()).also {
            spinnerHeartHeight.setSelection(if (it == -1) 0 else it)
        }
    }

    /**
     * 設定有氧通知
     */
    fun setOxygenNotification(entity: NotificationEntity) {
        switchOxygen.isChecked = entity.ns_enable == 1

        OXYGEN_LOW_SCOPE.indexOf(entity.ns_min.toString()).also {
            spinnerOxygenLow.setSelection(if (it == -1) 0 else it)
        }
    }

    /**
     * 設定睡眠通知
     */
    fun setSleepNotification(entity: NotificationEntity) {
        switchSleep.isChecked = entity.ns_enable == 1
        // 身睡低於
        SLEEP_LOW_SCOPE.indexOf(entity.ns_min.let { (it * 1f / 60 / 60) }.toString().replace(".0", "")).also {
            spinnerSleepLow.setSelection(if (it == -1) 0 else it)
        }

        // 身睡高於
        SLEEP_HEIGHT_SCOPE.indexOf(entity.ns_max.let { (it * 1f / 60 / 60) }.toString().replace(".0", "")).also {
            spinnerSleepHeight.setSelection(if (it == -1) 0 else it)
        }

        // 在每日N時
        sleepNotificationHour.indexOf(entity.ns_hour).also {
            spinnerSleep.setSelection(if (it == -1) 0 else it)
        }
    }

    /**
     * 設定步行通知
     */
    fun setWalkNotification(entity: NotificationEntity) {
        switchWalk.isChecked = entity.ns_enable == 1

        WALK_SCOPE_LOW.indexOf(entity.ns_min.toString()).also {
            spinnerWalkLow.setSelection(if (it == -1) 0 else it)
        }

        WALK_SCOPE_HIGH.indexOf(entity.ns_max.toString()).also {
            spinnerWalkHeight.setSelection(if (it == -1) 0 else it)
        }

        walkNotificationHour.indexOf(entity.ns_hour).also {
            spinnerWalk.setSelection(if (it == -1) 0 else it)
        }
    }

    /**
     * 上傳通知設定
     */
    fun uploadUserNotification() {
        var list: ArrayList<RequestUserNotificationEntity> = ArrayList<RequestUserNotificationEntity>()
        with(list) {
            add(RequestUserNotificationEntity(USER_ID, NOTIFY_TYPE_HEART, switchHeart.isChecked).also {
                it.setNs_min(HEART_LOW_SCOPE.get(spinnerHeartLow.selectedItemPosition).toInt())
                it.setNs_max(HEART_HEIGHT_SCOPE.get(spinnerHeartHeight.selectedItemPosition).toInt())
            })
            add(RequestUserNotificationEntity(USER_ID, NOTIFY_TYPE_OXYGEN, switchOxygen.isChecked).also {
                it.setNs_min(OXYGEN_LOW_SCOPE.get(spinnerOxygenLow.selectedItemPosition).toInt())
            })
            add(RequestUserNotificationEntity(USER_ID, NOTIFY_TYPE_SLEEP, switchSleep.isChecked).apply {
                // 需要轉換成秒數
                setNs_min((SLEEP_LOW_SCOPE.get(spinnerSleepLow.selectedItemPosition).toFloat() * 60 * 60).toInt())
                setNs_max((SLEEP_HEIGHT_SCOPE.get(spinnerSleepHeight.selectedItemPosition).toFloat() * 60 * 60).toInt())
                setNs_hour(sleepNotificationHour.get(spinnerSleep.selectedItemPosition))
            })
            add(RequestUserNotificationEntity(USER_ID, NOTIFY_TYPE_WALK, switchWalk.isChecked).apply {
                setNs_min(WALK_SCOPE_LOW.get(spinnerWalkLow.selectedItemPosition).toInt())
                setNs_max(WALK_SCOPE_HIGH.get(spinnerWalkHeight.selectedItemPosition).toInt())
                setNs_hour(walkNotificationHour.get(spinnerWalk.selectedItemPosition))
            })
        }

        val data = Data.Builder()
            .putString(
                UpdateNotificationWorker.DATA_KEY,
                Gson().toJson(list, ArrayList<RequestUserNotificationEntity>()::class.java)
            )
            .build()
        val constraints = Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
        val oneTimeWorkRequest = OneTimeWorkRequest.Builder(UpdateNotificationWorker::class.java!!)
            .setInputData(data)
            .setConstraints(constraints)
            .build()
        WorkManager.getInstance().enqueue(oneTimeWorkRequest)
    }

    // ==================== Inner Object ====================
    var SelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {

        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            if (parent == null) return;

            if ( parent.equals(spinnerWalkLow) && (spinnerWalkHeight.selectedItemPosition < spinnerWalkLow.selectedItemPosition) )
                spinnerWalkHeight.setSelection(spinnerWalkLow.selectedItemPosition)

            if ( parent.equals(spinnerWalkHeight) && (spinnerWalkLow.selectedItemPosition > spinnerWalkHeight.selectedItemPosition) )
                spinnerWalkLow.setSelection(spinnerWalkHeight.selectedItemPosition)

            uploadUserNotification()
        }
    }


} // class close