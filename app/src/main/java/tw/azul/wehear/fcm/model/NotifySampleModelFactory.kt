package tw.azul.wehear.fcm.model

/**
 * 推播測試物件
 */
class NotifySampleModelFactory {

    companion object {

        //===== Walk =====
        /*
        {"more_or_less": 0, "str_hex": 5c25bb04b06a9, "value": 13, "google.c.a.e": 1, "nts_id": 14, "goal": 0
        , "user_phone": 0912983242, "head": 10:00了，, "body": 今天步行低於1000步今天步行13步, "standard": 1000
        , "gcm.message_id": 0:1545976593791953%8092433480924334
        , "body_self": 今天步行低於1000步今天步行13步, "pass": 0, "content": 今天只走了13步，您是否要聯絡他，關心一下狀況。, "name": shen, "user_id": 221}
         */
        fun walkFailModel() : NotificationEntity {

            return NotificationEntity().apply {
                more_or_less = 0
                value = 13
                nts_id = 14
                goal = 0
                user_phone = "0912983242"
                body = "今天步行低於1000步今天步行13步"
                standard = 1000
                body_self = "今天步行低於1000步今天步行13步"
                pass = 0
                content = "今天只走了13步，您是否要聯絡他，關心一下狀況。"
                name = "shen"
                user_id = 221
            }
        }

        fun walkSuccessModel() : NotificationEntity {
            return NotificationEntity().apply {
                more_or_less = 0
                value = 1000
                nts_id = 14
                goal = 1
                user_phone = "0912983242"
                body = "今天步行低於1000步今天步行13步"
                standard = 1000
                body_self = "今天步行低於1000步今天步行13步"
                pass = 0
                content = "今天只走了13步，您是否要聯絡他，關心一下狀況。"
                name = "shen"
                user_id = 221
            }
        }

        fun walkFamilySuccessModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = false
                more_or_less = 0
                value = 1000
                nts_id = 14
                goal = 1
                user_phone = "0912983242"
                body = "今天步行低於1000步今天步行13步"
                standard = 1000
                body_self = "今天步行低於1000步今天步行13步"
                pass = 0
                content = "今天只走了13步，您是否要聯絡他，關心一下狀況。"
                name = "shen"
                user_id = 222
            }
        }

        fun walkFamilyFailModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = false
                more_or_less = 0
                value = 13
                nts_id = 14
                goal = 0
                user_phone = "0912983242"
                body = "今天步行低於1000步今天步行13步"
                standard = 1000
                body_self = "今天步行低於1000步今天步行13步"
                pass = 0
                content = "今天只走了13步，您是否要聯絡他，關心一下狀況。"
                name = "shen"
                user_id = 222
                head = "10:00了，"
            }
        }

        //===== Walk =====
        /*
        {"more_or_less": 0, "str_hex": 5c25b8640c1c1, "value": 60, "google.c.a.e": 1, "nts_id": 13, "goal": 0, "user_phone": 0912983242, "head": , "body": 昨晚深睡低於1小時昨晚深睡1小時0分鐘, "standard": 1, "gcm.message_id": 0:1545975921091952%8092433480924334, "body_self": 昨晚深睡低於1小時昨晚深睡1小時0分鐘, "pass": 1, "content": 昨晚的睡眠品質並不好，您是否要聯絡他，關心一下狀況。, "name": shen, "user_id": 221}
         */
        fun sleepDeepModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = true
                more_or_less = 0
                standard = 2
                value = 65
                nts_id = 13
                body = "昨晚深睡低於1小時實際深睡1小時0分鐘"
                goal = 0
                pass = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }
        fun sleepLightModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = true
                more_or_less = 1
                standard = 5
                value = 360
                nts_id = 13
                body = "昨晚淺睡高於1小時實際深睡1小時0分鐘"
                goal = 0
                pass = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }
        fun sleepDeepFamilyModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = false
                more_or_less = 0
                standard = 2
                value = 30
                nts_id = 13
                body = "昨晚深睡低於1小時實際深睡1小時0分鐘"
                goal = 0
                pass = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }
        fun sleepLightFamilyModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = false
                more_or_less = 1
                standard = 4
                value = 280
                nts_id = 13
                body = "昨晚淺睡高於1小時實際深睡1小時0分鐘"
                goal = 0
                pass = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }

        // ===== 血氧 =====
        fun  oxygenLowWarnModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = true
                more_or_less = 0
                standard = 95
                value = 91
                nts_id = 12
                body = "血氧低於95％\n目前血氧為91%"
                pass = 1
                goal = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }

        fun  oxygenLowUrgentModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = true
                more_or_less = 0
                standard = 95
                value = 85
                nts_id = 12
                body = "血氧低於95％\n目前血氧為85%"
                pass = 0
                goal = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }

        fun  oxygenLowWarnFamilyModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = false
                more_or_less = 0
                standard = 95
                value = 91
                nts_id = 12
                body = "血氧低於95％\n目前血氧為91%"
                pass = 1
                goal = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }

        fun  oxygenLowUrgentFamilyModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = false
                more_or_less = 0
                standard = 95
                value = 85
                nts_id = 12
                body = "血氧低於95％\n目前血氧為85%"
                pass = 0
                goal = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }


        // ===== 心率 =====
        fun  heartLowUrgentModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = true
                more_or_less = 0
                standard = 40
                value = 30
                nts_id = 11
                body = "心率低於40次/分鐘\n目前心率為30次/分鐘"
                pass = 0
                goal = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }

        fun  heartLowWarnModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = true
                more_or_less = 0
                standard = 70
                value = 60
                nts_id = 11
                body = "心率低於70次/分鐘\n目前心率為60次/分鐘"
                pass = 1
                goal = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }

        fun  heartHighUrgentModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = true
                more_or_less = 1
                standard = 200
                value = 250
                nts_id = 11
                body = "心率高於200次/分鐘\n目前心率為250次/分鐘"
                pass = 0
                goal = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }

        fun  heartHighWarnModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = true
                more_or_less = 1
                standard = 130
                value = 140
                nts_id = 11
                body = "心率高於130次/分鐘\n目前心率為140次/分鐘"
                pass = 1
                goal = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }


        fun  heartLowUrgentFamilyModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = false
                more_or_less = 0
                standard = 40
                value = 30
                nts_id = 11
                body = "心率低於40次/分鐘\n目前心率為30次/分鐘"
                pass = 0
                goal = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }

        fun  heartLowWarnFamilyModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = false
                more_or_less = 0
                standard = 70
                value = 60
                nts_id = 11
                body = "心率低於70次/分鐘\n目前心率為60次/分鐘"
                pass = 1
                goal = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }

        fun  heartHighUrgentFamilyModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = false
                more_or_less = 1
                standard = 200
                value = 250
                nts_id = 11
                body = "心率高於200次/分鐘\n目前心率為250次/分鐘"
                pass = 0
                goal = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }

        fun  heartHighWarnFamilyModel() : NotificationEntity {
            return NotificationEntity().apply {
                isMe = false
                more_or_less = 1
                standard = 130
                value = 140
                nts_id = 11
                body = "心率高於130次/分鐘\n目前心率為140次/分鐘"
                pass = 1
                goal = 0
                user_phone = "0912983242"
                name = "shen"
                user_id = 222
            }
        }

    }
}