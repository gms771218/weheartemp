package tw.azul.wehear.net.api

import androidx.lifecycle.LiveData
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import tw.azul.wehear.net.model.base.ListEntity
import tw.azul.wehear.net.model.base.ResponseIdResultEntity
import tw.azul.wehear.net.model.base.SingleEntity
import tw.azul.wehear.net.model.notification.NotificationEntity
import tw.azul.wehear.net.model.notification.NotificationStatusEntity
import tw.azul.wehear.net.model.notification.NotificationTypeEntity
import tw.azul.wehear.net.retrofit.RetrofitClient

/**
 * 通知api
 */
interface NotificationApi {

    /**
     * 取得通知狀態列表
     * 電池電低、產品警報、SOS...etc
     * */
    @GET(RetrofitClient.BASE_URL + "notification/statuses")
    fun getNotificationStatus(): LiveData<ApiResponse<ListEntity<NotificationStatusEntity>>>

    /**
     * 取得通知類型列表
     * 門鈴、電話、遙控器...etc
     */
    @GET(RetrofitClient.BASE_URL + "notification/types")
    fun getNotificationType(): LiveData<ApiResponse<ListEntity<NotificationTypeEntity>>>

    /**
     * 取得使用者通知設定
     */
    @GET(RetrofitClient.BASE_URL + "notification/user")
    fun getUserNotification(@Query("user_id") user_id: Int): LiveData<ApiResponse<ListEntity<NotificationEntity>>>

    /**
     * 新增修改使用者通知設定
     *
     * @JvmSuppressWildcards Kotlin 使用原因
     * https://www.kotlintc.com/articles/4389
     */
    @Multipart
    @POST(RetrofitClient.BASE_URL + "notification")
    fun postUserNotification(
        @PartMap param: Map<String, @JvmSuppressWildcards RequestBody>
    ): LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>>


    @Multipart
    @POST(RetrofitClient.BASE_URL + "notification")
    fun postUserNotificationFormCall(
        @PartMap param: Map<String, @JvmSuppressWildcards RequestBody>
    ): Call<SingleEntity<ResponseIdResultEntity>>


    /**
     * app發送通知
     *
     *  user_id : 使用者id
     *  nts_id  : 通知類型id
     *  str_hex : hex字串
     *  ns_latitude : 緯度
     *  ns_longitude : 經度
     *
     *  return 空陣列
     */
    @GET(RetrofitClient.BASE_URL + "notification/app")
    fun getAppNotification(
        @Query("user_id") user_id: Int, @Query("nts_id") nts_id: Int, @Query("str_hex") str_hex: String
        , @Query("ns_latitude") ns_latitude: String, @Query("ns_longitude") ns_longitude: String
    ): LiveData<ApiResponse<ListEntity<ResponseIdResultEntity>>>

    /**
     * 主機發送心跳包
     *
     * device_mac : 裝置mac位置
     *
     * return 空陣列
     */
    @GET(RetrofitClient.BASE_URL + "notification/heartbeat")
    fun getHostHeartNotification(@Query("device_mac") device_mac: String): LiveData<ApiResponse<ListEntity<ResponseIdResultEntity>>>

    /**
     * 主機發送通知
     *
     * str_hex : hex字串
     *
     * return 空陣列
     */
    @GET(RetrofitClient.BASE_URL + "notification/host")
    fun getHostNotification(@Query("str_hex") str_hex: String): LiveData<ApiResponse<ListEntity<ResponseIdResultEntity>>>

}