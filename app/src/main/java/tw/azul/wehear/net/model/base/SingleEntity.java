package tw.azul.wehear.net.model.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by gms on 2018/4/19.
 *
 * Entity 主要的資料類型
 *
 * format : {"status":,"msg":[],"data": { EntityType } }
 *
 */

public class SingleEntity<EntityType> extends StatusCodeBase {

    @Expose
    @SerializedName("data")
    public EntityType data;

}
