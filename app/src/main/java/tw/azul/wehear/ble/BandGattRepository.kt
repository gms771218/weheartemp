@file:Suppress("unused", "EXPERIMENTAL_API_USAGE", "FunctionName", "PrivatePropertyName", "LocalVariableName")

package tw.azul.wehear.ble

import android.app.Application
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattCharacteristic.FORMAT_UINT32
import android.bluetooth.BluetoothGattCharacteristic.FORMAT_UINT8
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
import android.bluetooth.BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import tw.azul.wehear.api.NTS
import tw.azul.wehear.ble.GenericAttributeProfile.*
import tw.idv.fy.kotlin.utils.*
import tw.idv.fy.kotlin.utils.LogLabel.Prefix
import java.util.*
import java.util.Calendar.*

class BandGattRepository private constructor(private val application: Application) {

    companion object {
        private const val TAG = "BandGattRepository"
        @Volatile
        private var singleton: BandGattRepository? = null
        fun Instance(application: Application): BandGattRepository = singleton ?: synchronized(Companion) {
            singleton = singleton ?: BandGattRepository(application)
            return singleton!!
        }
    }

    private val mMainHandler = Handler(Looper.getMainLooper())
    private lateinit var mHandler: Handler
    init {
        object : HandlerThread(TAG) {
            override fun onLooperPrepared() {
                super.onLooperPrepared()
                mHandler = Handler(looper) {
                    return@Handler false
                }
            }
        }.start()
    }

    private val characteristicRequestLiveData  = MutableLiveData<BluetoothGattCharacteristic>() // Log.Level: V
    private val characteristicResponseLiveData = MutableLiveData<BluetoothGattCharacteristic>() // Log.Level: W
    private val descriptorRequestLiveData      = MutableLiveData<BluetoothGattDescriptor>()     // Log.Level: D
    private val descriptorResponseLiveData     = MutableLiveData<BluetoothGattDescriptor>()     // Log.Level: D

    val delegate = object: IBluetoothGattCallback {

        override var gatt: BluetoothGatt? = null

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                //gatt.services.forEach {
                //    it.uuid.LogD("[S] ")
                //    it.characteristics.forEach {
                //        it.uuid.LogI("---[C] ")
                //        it.descriptors.forEach {
                //            it.uuid.LogW("   ---[D] ")
                //        }
                //    }
                //}
                immediateAlert()
                timeSync()
            }
        }

        private val onCharacteristicWritePrefix = Prefix("[BandGatt] onCharacteristicWrite:")
        override fun onCharacteristicWrite(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int
        ) {
            characteristic.value.toHexString().LogV(prefix = onCharacteristicWritePrefix)
            characteristicRequestLiveData.postValue(characteristic)
        }

        private val onCharacteristicReadPrefix = Prefix("[BandGatt] onCharacteristicRead:")
        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int
        ) {
            characteristic.value.toHexString().LogW(prefix = onCharacteristicReadPrefix)
            characteristicResponseLiveData.postValue(characteristic)
        }

        private val onDescriptorWritePrefix = Prefix("[BandGatt] onDescriptorWrite:")
        override fun onDescriptorWrite(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {
            descriptor.value.toHexString().LogD(prefix = onDescriptorWritePrefix)
            descriptorRequestLiveData.postValue(descriptor)
        }

        private val onDescriptorReadPrefix = Prefix("[BandGatt] onDescriptorRead:")
        override fun onDescriptorRead(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int){
            descriptor.value.toHexString().LogD(prefix = onDescriptorReadPrefix)
            descriptorResponseLiveData.postValue(descriptor)
        }

        private val onCharacteristicChangedPrefix = Prefix("[BandGatt] onCharacteristicChanged:")
        override fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic){
            characteristic.value.toHexString().LogW(prefix = onCharacteristicChangedPrefix)
            characteristicResponseLiveData.postValue(characteristic)
        }
    }

    fun immediateAlert(gatt: BluetoothGatt? = delegate.gatt) {
        gatt?.getCharacteristic(ImmediateAlert)
            ?.apply {
                ValidIfNeedWaitDelay {
                    setValue(0x01, FORMAT_UINT8, 0)
                    gatt.writeCharacteristic(this)
                }
                ValidIfNeedWaitDelay {
                    setValue(0x00, FORMAT_UINT8, 0)
                    gatt.writeCharacteristic(this)
                }
            }
    }

    fun timeSync(gatt: BluetoothGatt? = delegate.gatt) {
        gatt?.getCharacteristic(TimeSync)
            ?.apply {
                ValidIfNeedWaitDelay {
                    value = Calendar.getInstance().run {
                        byteArrayOf(
                           *get(YEAR).run { intArrayOf(and(0xFF), shr(8)) },
                            get(MONTH) + 1,
                            get(DAY_OF_MONTH),
                            get(HOUR_OF_DAY),
                            get(MINUTE),
                            get(SECOND) + 1 //超前一秒彌補通訊延遲
                        )
                    }
                    gatt.writeCharacteristic(this)
                }
            }
    }

    @MainThread
    fun batteryLevel(gatt: BluetoothGatt? = delegate.gatt): LiveData<Int> {
        var power: Int = -1
        val powerLiveData: LiveData<Int> = Transformations.map(characteristicResponseLiveData) { characteristic ->
            power = when (characteristic.uuid) {
                BatteryLevel.characteristicUUID -> {
                    characteristic?.getIntValue(FORMAT_UINT8, 0)?.apply {
                        LogE("電量:")
                    } ?: power
                }
                else -> power
            }
            power
        }
        var requestOK = false
        descriptorRequestLiveData.observeForever(object : Observer<BluetoothGattDescriptor> {
            override fun onChanged(descriptor: BluetoothGattDescriptor?) {
                if (requestOK && gatt != null && descriptor != null && descriptor.uuid == BatteryLevel.descriptorUUID) {
                    descriptorRequestLiveData.removeObserver(this)
                    descriptor.characteristic.apply {
                        ValidIfNeedWaitDelay {
                            gatt.setCharacteristicNotification(this, true)
                        }
                        ValidIfNeedWaitDelay {
                            gatt.readCharacteristic(this)
                        }
                    }
                }
            }
        })
        var responseOK = false
        descriptorResponseLiveData.observeForever(object : Observer<BluetoothGattDescriptor> {
            override fun onChanged(descriptor: BluetoothGattDescriptor?) {
                if (responseOK && gatt != null && descriptor != null && descriptor.uuid == BatteryLevel.descriptorUUID) {
                    descriptorResponseLiveData.removeObserver(this)
                    ValidIfNeedWaitDelay {
                        descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                        gatt.writeDescriptor(descriptor)
                    }.then {
                        requestOK = true
                    }
                }
            }
        })
        gatt?.getCharacteristic(BatteryLevel)
            ?.getDescriptor(BatteryLevel.descriptorUUID)
            ?.apply {
                ValidIfNeedWaitDelay {
                    gatt.readDescriptor(this)
                }.then {
                    responseOK = true
                }
            }
        return powerLiveData
    }

    @MainThread
    fun getClockAlarm(gatt: BluetoothGatt? = delegate.gatt): LiveData<Array<Pair<Int,Int>>> {
        var responseOK = false
        val clockAlarmLiveData = MediatorLiveData<Array<Pair<Int,Int>>>().apply {
            addSource(characteristicResponseLiveData) { responseCharacteristic ->
                if (responseOK && responseCharacteristic?.uuid == ClockAlarm.characteristicUUID) {
                    val arrayIntPair: Array<Pair<Int,Int>> = responseCharacteristic.run {
                        arrayOf(
                            getIntValue(FORMAT_UINT32,  0) to getIntValue(FORMAT_UINT8,  4),
                            getIntValue(FORMAT_UINT32,  5) to getIntValue(FORMAT_UINT8,  9),
                            getIntValue(FORMAT_UINT32, 10) to getIntValue(FORMAT_UINT8, 14),
                            getIntValue(FORMAT_UINT32, 15) to getIntValue(FORMAT_UINT8, 19)
                        )
                    }
                    postValue(arrayIntPair)
                }
            }
        }
        gatt?.getCharacteristic(ClockAlarm)
            ?.apply {
                ValidIfNeedWaitDelay {
                    gatt.readCharacteristic(this)
                }.then {
                    responseOK = true
                }
            }
        return clockAlarmLiveData
    }

    @MainThread
    fun setClockAlarm(gatt: BluetoothGatt? = delegate.gatt, bleCommIntPairArray: Array<Pair<Int, Int>>) {
        var requestOK = false
        characteristicRequestLiveData.observeForever(object: Observer<BluetoothGattCharacteristic> {
            override fun onChanged(requstCharacteristic: BluetoothGattCharacteristic?) {
                if (requestOK && requstCharacteristic?.uuid == ClockAlarm.characteristicUUID) {
                    characteristicRequestLiveData.removeObserver(this)
                    mHandler.postDelayed({ characteristicResponseLiveData.postValue(requstCharacteristic) }, DELAY)
                    //(requstCharacteristic?.value?.toUByteArray()?.toHexStrList()?.joinToString()?:"null").LogE()
                }
            }
        })
        gatt?.getCharacteristic(ClockAlarm)
            ?.apply {
                ValidIfNeedWaitDelay {
                    bleCommIntPairArray.forEachIndexed { index, pair ->
                        setValue(pair.first, FORMAT_UINT32, index * 5)
                        setValue(pair.second, FORMAT_UINT8, index * 5 + 4)
                    }
                    gatt.writeCharacteristic(this)
                }.then {
                    requestOK = true
                }
            }
    }

    fun hostAlarm(gatt: BluetoothGatt? = delegate.gatt, nts: NTS) {
        gatt?.getCharacteristic(HostAlarm)
            ?.apply {
                ValidIfNeedWaitDelay {
                    setValue(nts.bleComm(), FORMAT_UINT8, 0)
                    gatt.writeCharacteristic(this)
                }
            }
    }

    fun calmAlarm(gatt: BluetoothGatt? = delegate.gatt) {
        gatt?.getCharacteristic(HostAlarm)
            ?.apply {
                ValidIfNeedWaitDelay {
                    setValue(0xFF, FORMAT_UINT8, 0)
                    gatt.writeCharacteristic(this)
                }
            }
    }

    @MainThread
    fun getHistoryRSC(gatt: BluetoothGatt? = delegate.gatt): LiveData<ByteArray> {
        var responseOK = false
        val rscLiveData = MediatorLiveData<ByteArray>().apply {
            addSource(characteristicResponseLiveData) { responseCharacteristic ->
                if (responseOK && responseCharacteristic?.uuid == HistoryRSC.characteristicUUID) {
                    postValue(responseCharacteristic.value)
                }
            }
        }
        gatt?.getCharacteristic(HistoryRSC)
            ?.apply {
                ValidIfNeedWaitDelay {
                    gatt.readCharacteristic(this)
                }.then {
                    responseOK = true
                }
            }
        return rscLiveData
    }

    @MainThread
    fun getRSC(gatt: BluetoothGatt? = delegate.gatt): LiveData<ByteArray> {
        val rscLiveData = MediatorLiveData<ByteArray>()
        setRscNotify(gatt, isEnabled = false).apply {
            observeForever(object : Observer<BluetoothGattDescriptor> {
                override fun onChanged(d: BluetoothGattDescriptor?) {
                    removeObserver(this)
                    rscLiveData.addSource(peekRSC(gatt)) { responseCharacteristic ->
                        val descriptor = responseCharacteristic.getDescriptor(RSC.descriptorUUID)
                        if (responseCharacteristic?.uuid == RSC.characteristicUUID &&
                            descriptor?.value?.contentEquals(DISABLE_NOTIFICATION_VALUE) == true) {
                            rscLiveData.postValue(responseCharacteristic.value)
                        }
                    }
                }
            })
        }
        return rscLiveData
    }

    @MainThread
    fun peekRSC(gatt: BluetoothGatt? = delegate.gatt): LiveData<BluetoothGattCharacteristic> {
        var responseOK = false
        val rscCharacteristicLiveData = MediatorLiveData<BluetoothGattCharacteristic>().apply {
            addSource(characteristicResponseLiveData) { responseCharacteristic ->
                if (responseOK && responseCharacteristic?.uuid == RSC.characteristicUUID) {
                    postValue(responseCharacteristic)
                }
            }
        }
        gatt?.getCharacteristic(RSC)
            ?.apply {
                ValidIfNeedWaitDelay {
                    gatt.readCharacteristic(this)
                }.then {
                    responseOK = true
                }
            }
        return rscCharacteristicLiveData
    }

    @MainThread
    fun enabledNotifyRSC(gatt: BluetoothGatt? = delegate.gatt): LiveData<ByteArray> = notifyRSC(gatt, true)

    @MainThread
    fun disableNotifyRSC(gatt: BluetoothGatt? = delegate.gatt): Unit = run { notifyRSC(gatt, false) }

    @MainThread
    private fun notifyRSC(gatt: BluetoothGatt? = delegate.gatt, isTurnOn: Boolean = true): LiveData<ByteArray> {
        var requestOK = false
        val rscLiveData = MediatorLiveData<ByteArray>().apply {
            addSource(characteristicResponseLiveData) { responseCharacteristic ->
                val descriptor = responseCharacteristic.getDescriptor(RSC.descriptorUUID)
                if (requestOK
                    && responseCharacteristic?.uuid == RSC.characteristicUUID
                    && descriptor?.value?.contentEquals(ENABLE_NOTIFICATION_VALUE) == true) {
                    postValue(responseCharacteristic.value)
                }
            }
        }
        setRscNotify(gatt, isEnabled = isTurnOn).apply {
            observeForever(object : Observer<BluetoothGattDescriptor> {
                override fun onChanged(descriptor: BluetoothGattDescriptor?) {
                    removeObserver(this)
                    if (gatt != null && descriptor != null) {
                        ValidIfNeedWaitDelay {
                            //"切換通知".LogV()
                            gatt.setCharacteristicNotification(descriptor.characteristic, isTurnOn)
                        }.then {
                            requestOK = true
                        }
                    }
                }
            })
        }
        return rscLiveData
    }

    private fun setRscNotify(gatt: BluetoothGatt? = delegate.gatt, isEnabled: Boolean): LiveData<BluetoothGattDescriptor> {
        val result = MutableLiveData<BluetoothGattDescriptor>()
        var requestOK = false
        descriptorRequestLiveData.observeForever(object: Observer<BluetoothGattDescriptor> {
            override fun onChanged(descriptor: BluetoothGattDescriptor?) {
                if (requestOK && gatt != null && descriptor != null && descriptor.uuid == RSC.descriptorUUID) {
                    descriptorRequestLiveData.removeObserver(this)
                    result.postValue(descriptor)
                }
            }
        })
        var responseOK = false
        descriptorResponseLiveData.observeForever(object: Observer<BluetoothGattDescriptor> {
            override fun onChanged(descriptor: BluetoothGattDescriptor?) {
                if (responseOK && gatt != null && descriptor != null && descriptor.uuid == RSC.descriptorUUID) {
                    descriptorResponseLiveData.removeObserver(this)
                    descriptor.value = if (isEnabled) ENABLE_NOTIFICATION_VALUE else DISABLE_NOTIFICATION_VALUE
                    ValidIfNeedWaitDelay {
                        //"寫描述子".LogV()
                        gatt.writeDescriptor(descriptor)
                    }.then {
                        requestOK = true
                    }
                }
            }
        })
        gatt?.getCharacteristic(RSC)
            ?.getDescriptor(RSC.descriptorUUID)
            ?.apply {
                ValidIfNeedWaitDelay {
                    //"讀描述子".LogV()
                    gatt.readDescriptor(this)
                }.then {
                    responseOK = true
                }
            }
        return result
    }

    fun callInNotify(gatt: BluetoothGatt? = delegate.gatt, byteArray: ByteArray) {
        gatt?.getCharacteristic(CallInNotify)
            ?.apply {
                ValidIfNeedWaitDelay {
                    value = byteArray
                    gatt.writeCharacteristic(this)
                }
            }
    }

    fun emitNotify(gatt: BluetoothGatt? = delegate.gatt, bleCommList: List<ByteArray>) {
        gatt?.getCharacteristic(EmitNotify)
            ?.apply {
                bleCommList.forEach {byteArray: ByteArray ->
                    ValidIfNeedWaitDelay {
                        value = byteArray
                        gatt.writeCharacteristic(this)
                    }
                }
            }
    }

    fun settingNotify(gatt: BluetoothGatt? = delegate.gatt, params: ByteArray? = null): LiveData<ByteArray> {
        var responseOK = false
        val paramsLiveData = MediatorLiveData<ByteArray>().apply {
            addSource(characteristicResponseLiveData) { responseCharacteristic ->
                if (responseOK && responseCharacteristic.uuid == SettingNotify.characteristicUUID) {
                    postValue(responseCharacteristic.value)
                }
            }
        }
        if (params == null) {
            gatt?.getCharacteristic(SettingNotify)
                ?.apply {
                    ValidIfNeedWaitDelay {
                        gatt.readCharacteristic(this)
                    }.then{
                        responseOK = true
                    }
                }
            return paramsLiveData
        }
        var requestOK = false
        characteristicRequestLiveData.observeForever(object: Observer<BluetoothGattCharacteristic> {
            override fun onChanged(characteristic: BluetoothGattCharacteristic?) {
                if (requestOK && gatt != null && characteristic != null && characteristic.uuid == SettingNotify.characteristicUUID) {
                    characteristicRequestLiveData.removeObserver(this)
                    ValidIfNeedWaitDelay {
                        gatt.readCharacteristic(characteristic)
                    }.then{
                        responseOK = true
                    }
                }
            }
        })
        gatt?.getCharacteristic(SettingNotify)
            ?.apply {
                ValidIfNeedWaitDelay {
                    value = params
                    gatt.writeCharacteristic(this)
                }.then{
                    requestOK = true
                }
            }
        return paramsLiveData
    }

    @WorkerThread
    private fun ValidIfNeedWaitDelay(predicate: () -> Boolean) = MutableLiveData<Result>().apply {
        mHandler.post {
            val start_time = System.currentTimeMillis()
            while (!predicate()) {
                if (System.currentTimeMillis().minus(start_time) > 5 * 1000L) {
                    postValue(Result.Failure("Faty: timeout"))
                    return@post
                }
                Thread.sleep(DELAY)
            }
            postValue(Result.Success)
        }
    }

    private fun LiveData<Result>.then(block: (result: Result) -> Unit) = mMainHandler.post {
        observeForever(object : Observer<Result> {
            override fun onChanged(result: Result) {
                removeObserver(this)
                runCatching {
                    require(result == Result.Success) { result.msg }
                    block(result)
                }.exceptionOrNull()?.printStackTrace()
            }
        })
    }

    private sealed class Result(open val msg: String) {
        object Success: Result("Success")
        data class Failure(override val msg:String): Result(msg)
    }

    private val DELAY = 100L
}

sealed class GenericAttributeProfile(
    service: String,
    characteristic: String,
    descriptor: String? = null
) {
    /*
        轉譯 UUID
     */
    companion object {
        private fun toUUID(uuid: String?): UUID? = uuid?.run {
            UUID.fromString(if (length > 4) this else "0000$this-0000-1000-8000-00805F9B34FB")
        }
    }
    /*
        轉換
     */
    val serviceUUID = toUUID(service)
    val characteristicUUID = toUUID(characteristic)
    val descriptorUUID: UUID? = toUUID(descriptor)
    /*
        部屬
     */
    object ImmediateAlert : GenericAttributeProfile(
        service = "1802",
        characteristic = "2a06"
    )
    object TimeSync : GenericAttributeProfile(
        service = "1814",
        characteristic = "1524"
    )
    object BatteryLevel : GenericAttributeProfile(
        service = "180F",
        characteristic = "2A19",
        descriptor = "2902"
    )
    object ClockAlarm : GenericAttributeProfile(
        service = "1814",
        characteristic = "1527"
    )
    object HostAlarm : GenericAttributeProfile(
        service = "1814",
        characteristic = "1531"
    )
    object RSC : GenericAttributeProfile(
        service = "1814",
        characteristic = "2A53",
        descriptor = "2902"
    )
    object HistoryRSC : GenericAttributeProfile(
        service = "1814",
        characteristic = "1525"
    )
    object CallInNotify : GenericAttributeProfile(
        service = "1814",
        characteristic = "1526"
    )
    object EmitNotify : GenericAttributeProfile(
        service = "1814",
        characteristic = "1528"
    )
    object SettingNotify : GenericAttributeProfile(
        service = "1814",
        characteristic = "1530"
    )
}

/*
    kotlin extension used by self
 */
private fun BluetoothGatt.getCharacteristic(gattProfile: GenericAttributeProfile): BluetoothGattCharacteristic? =
    getService(gattProfile.serviceUUID)?.getCharacteristic(gattProfile.characteristicUUID)

private fun ByteArray?.toHexString() = this?.run {
    toUByteArray().joinToString(transform = UByte::toStringByRadix16)
} ?: "null"