package tw.azul.wehear.portal.form

import android.app.Application
import androidx.lifecycle.*
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.request.RequestUpdateUserEntity
import tw.azul.wehear.net.model.user.UserEntity
import tw.azul.wehear.net.repository.UserRepository

class AddressViewModel(val app: Application) : AndroidViewModel(app) {

    private var userRepository: UserRepository = UserRepository.getInstance()

    val address: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }

    fun getUser(user_id: Int) : LiveData<Result> {
        var liveData = MediatorLiveData<Result>()
        liveData.addSource(userRepository.getUser(user_id),  {
            if (it.status != Status.LOADING) {
                if (it.status == Status.SUCCESS) {
                    address.postValue(it.data?.user_address)
                    liveData.postValue(Result(true , ""))
                } else if (it.status == Status.ERROR) {
                    liveData.postValue(Result(false ,""))
                }
            }
        })
        return liveData
    }

    fun saveUser(user_id: Int): LiveData<Result> {
        var liveData = MediatorLiveData<Result>()
        liveData.addSource(userRepository.postUser(
            user_id
            , RequestUpdateUserEntity.create(user_id
                , UserEntity()
                , UserEntity().apply {
                    this.user_address = address.value
                })
        )
            , Observer {})
        return liveData
    }


    class Result {
        var isSuccess: Boolean? = null
        var msg: String = ""

        constructor(isSuccess: Boolean?, msg: String) {
            this.isSuccess = isSuccess
            this.msg = msg
        }
    }

}
