package tw.azul.wehear.net.api

import tw.azul.wehear.net.model.family.FamilyEntity
import tw.azul.wehear.net.model.base.ListEntity
import androidx.lifecycle.LiveData
import okhttp3.RequestBody
import retrofit2.http.*
import tw.azul.wehear.net.retrofit.RetrofitClient
import tw.azul.wehear.net.model.base.ResponseIdResultEntity
import tw.azul.wehear.net.model.base.SingleEntity


/**
 * 家人api
 */
interface FamilyApi {

    /**
     * 取得家人資料
     * @param user_id
     * @param user_phone
     * @return
     */
    @GET(RetrofitClient.BASE_URL + "family")
    fun getFamily(@Query("user_id") user_id: Int, @Query("user_phone") user_phone: String): LiveData<ApiResponse<SingleEntity<FamilyEntity>>>


    /**
     * 取得家人列表
     *
     * @return
     */
    @GET(RetrofitClient.BASE_URL + "family/list")
    fun getFamilys(@Query("user_id") user_id: Int): LiveData<ApiResponse<ListEntity<FamilyEntity>>>


    /**
     * 修改家人資料
     *
     */
    @Multipart
    @POST(RetrofitClient.BASE_URL + "family")
    fun postFamily(
        @Part("user_id") user_id: RequestBody
        , @Part("user_id_family") user_id_family: RequestBody
        , @Part("fts_id") fts_id: RequestBody
        , @Part("fs_nickname") fs_nickname: RequestBody
        , @Part("fs_accept") fs_accept: RequestBody
    ): LiveData<ApiResponse<SingleEntity<ResponseIdResultEntity>>>


}