package tw.azul.wehear.net.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * 新增步數物件
 */
public class RequestStepEntity {

    @Expose
    @SerializedName("steps")
    private List<StepEntity> steps;
    @Expose
    @SerializedName("user_id")
    private int user_id;

    public RequestStepEntity(int user_id) {
        this.user_id = user_id;
        steps = new ArrayList<StepEntity>();
    }

    public void addStep(StepEntity entity){
        steps.add(entity);
    }


    public static class StepEntity {

        /**
         * 步數類型id
         */
        @Expose
        @SerializedName("st_id")
        public int st_id;

        /**
         * 步數紀錄分段
         */
        @Expose
        @SerializedName("step_split")
        public int step_split;

        /**
         * 步數紀錄日期時間(秒)
         */
        @Expose
        @SerializedName("step_datetime")
        public int step_datetime;

        /**
         * 移動時間
         */
        @Expose
        @SerializedName("step_moving_time")
        public int step_moving_time;

        /**
         * 步數
         */
        @Expose
        @SerializedName("step_count")
        public int step_count;

        /**
         * 距離(公尺)
         */
        @Expose
        @SerializedName("step_distance")
        public double step_distance;

        /**
         * 步伐長度(公分)
         */
        @Expose
        @SerializedName("step_stride_length")
        public double step_stride_length;

        /**
         * 卡路里
         */
        @Expose
        @SerializedName("step_calorie")
        public double step_calorie;
    }
}
