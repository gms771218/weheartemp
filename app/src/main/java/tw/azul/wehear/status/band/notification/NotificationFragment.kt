package tw.azul.wehear.status.band.notification

import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.provider.Settings
import android.provider.Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.NotificationManagerCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController

import tw.azul.wehear.R
import tw.azul.wehear.databinding.FragmentBandNotificationBinding

class NotificationFragment : Fragment() {

    companion object {
        fun newInstance() = NotificationFragment()
    }

    private lateinit var dataBinding: FragmentBandNotificationBinding
    private lateinit var viewModel: NotificationViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_band_notification, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(NotificationViewModel::class.java)
        dataBinding.viewModel = viewModel
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBleStatusLiveData().observe(viewLifecycleOwner::getLifecycle) { bleStatus ->
            if (bleStatus.band.isConnected != true) {
                findNavController().popBackStack(R.id.bandSettingFragment, false)
            }
        }
        runCatching {
            val activity = requireNotNull(activity)
            if (!NotificationManagerCompat.getEnabledListenerPackages(activity).contains(activity.packageName)) {
                AlertDialog.Builder(activity, R.style.Theme_MaterialComponents_Light_Dialog_Alert).apply {
                    setPositiveButton("前往設定") { _, _ ->
                        Intent().apply {
                            action = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                                ACTION_NOTIFICATION_LISTENER_SETTINGS
                            } else {
                                "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"
                            }
                            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(this)
                        }
                    }
                    setNegativeButton("知道了") { _, _ -> findNavController().popBackStack() }
                    setTitle("授權提示")
                    setMessage("「App通知提醒」需要「通知存取」權限")
                    setCancelable(false)
                    show()
                }
            }
        }.onFailure {
            findNavController().popBackStack()
        }.exceptionOrNull()?.printStackTrace()
    }
}
