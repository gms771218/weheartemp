package tw.azul.wehear.account

import android.accounts.Account
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.google.gson.Gson

class AccountRoom {

    data class Token(val user_id: Int? = -1, val token: String? = null) {
        @Suppress("unused")
        val userId get() = user_id
    }

    sealed class Status {
        object Init : Status()
        object Connecting : Status()
        data class Error(val account: Account? = null, val errCode:Int?=null, val errMsg: String? = null) : Status()
        data class Connected(val account: Account? = null, val tokenStr: String? = null) : Status()
    }

    companion object {
        val StatusLiveData = MediatorLiveData<Status>()
        val TokenLiveData: LiveData<Token> = MediatorLiveData<Token>().apply {
            addSource(StatusLiveData) { it ->
                if (it is Status.Connected) Gson().fromJson<Token>(it.tokenStr, Token::class.java)?.let { setValue(it) }
            }
        }
        fun reset() {
            (TokenLiveData as MutableLiveData).postValue(null)
            StatusLiveData.postValue(Status.Init)
        }
    }
}

//@Suppress("unused")
//private class SelfMediatorLiveData<T>: MediatorLiveData<T>() {
//
//    @Deprecated(message = "Unsupported", level = DeprecationLevel.HIDDEN)
//    override fun setValue(value: T) = throw UnsupportedOperationException()
//
//    fun _setValue(value: T) = super.setValue(value)
//
//    @Deprecated(message = "Unsupported", level = DeprecationLevel.HIDDEN)
//    override fun postValue(value: T) = throw UnsupportedOperationException()
//
//    fun _postValue(value: T) = super.postValue(value)
//
//    @Deprecated(message = "Unsupported", level = DeprecationLevel.HIDDEN)
//    override fun <S : Any?> addSource(source: LiveData<S>, onChanged: Observer<in S>) =
//        throw UnsupportedOperationException()
//
//    fun <S : Any?> _addSource(source: LiveData<S>, onChanged: (s: S) -> Unit) = super.addSource(source, onChanged)
//
//    @Deprecated(message = "Unsupported", level = DeprecationLevel.HIDDEN)
//    override fun <S : Any?> removeSource(toRemote: LiveData<S>) = throw UnsupportedOperationException()
//
//    fun <S : Any?> _removeSource(toRemote: LiveData<S>) = super.removeSource(toRemote)
//}
