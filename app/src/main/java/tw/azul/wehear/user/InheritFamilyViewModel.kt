package tw.azul.wehear.user

import android.view.View
import androidx.lifecycle.MediatorLiveData
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.user.family.FamilyViewModel

class InheritFamilyViewModel : FamilyViewModel() {

    val visibilityLiveData = MediatorLiveData<Visibility>().apply { value = GONE }

    fun peekFSData() {
        // visibilityLiveData 是老司機
        visibilityLiveData.apply {
            // 先帶領 TokenLiveData 引出 token
            val tokenLiveData = AccountRoom.TokenLiveData
            addSource(tokenLiveData) { token ->
                // 一到手就丟棄
                removeSource(tokenLiveData)
                runCatching {
                    // 再去伸手觸擊 家庭資料
                    val fsLiveData = getFamilys(requireNotNull(token.userId))
                    addSource(fsLiveData) { response ->
                        when (response.status) {
                            Status.SUCCESS, Status.ERROR -> {
                                // 不管有沒有到手都要丟棄
                                removeSource(fsLiveData)
                                // 得到想要並去散播
                                value = if (response?.data?.count { it.fs_accept == 0 } ?: 0 > 0) {
                                    VISIBLE
                                } else {
                                    GONE
                                }
                            }
                            Status.LOADING -> {/* nothing to do */}
                        }
                    }
                }.exceptionOrNull()?.printStackTrace() // 遇到意外, 路人皆知
            }
        }
    }
}

private typealias Visibility = Int
private const val GONE   : Visibility = View.GONE
private const val VISIBLE: Visibility = View.VISIBLE