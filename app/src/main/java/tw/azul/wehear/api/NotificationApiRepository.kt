@file:Suppress("EnumEntryName", "NonAsciiCharacters", "PrivatePropertyName")

package tw.azul.wehear.api

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.collection.LruCache
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import tw.azul.wehear.App
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.ble.BandGattRepository
import tw.idv.fy.kotlin.utils.LogD
import tw.azul.wehear.fcm.WehearFirebaseMessagingService

class NotificationApiRepository private constructor(private val application: Application) {

    companion object {
        private const val MAX_TIME_DIFF = 3 * 24 * 60 * 60 // 三天分的量 (儲存單位: 秒)
        @Volatile
        private var singleton: NotificationApiRepository? = null
        fun Instance(application: Application): NotificationApiRepository = singleton ?: synchronized(Companion) {
            singleton = singleton ?: NotificationApiRepository(application)
            return singleton!!
        }
    }

    private val ACTION_CALM_ALARM = application.packageName + ".action_calm_alarm"
    init {
        LocalBroadcastManager.getInstance(application).registerReceiver(object: BroadcastReceiver(){
            override fun onReceive(context: Context?, intent: Intent?) {
                if (ACTION_CALM_ALARM == intent?.action) {
                    BandGattRepository.Instance(application).calmAlarm()
                }
            }
        }, IntentFilter(ACTION_CALM_ALARM))
    }

    private val notificationHexLruCache = object: LruCache<String, Int>(MAX_TIME_DIFF) {
        override fun create(key: String): Int {
            return App.GetPreferences(application).getInt(key, 0)
        }
        override fun entryRemoved(evicted: Boolean, key: String, oldValue: Int, newValue: Int?) {
            if (evicted || newValue == null) {
                App.GetPreferences(application).edit().remove(key).apply()
            } else {
                App.GetPreferences(application).edit().putInt(key, newValue).apply()
            }
        }
    }

    data class MessageData(
        val str_hex: String,
        val nts_id: Int,
        val user_id: Int,
        val body: String,
        val body_self: String = body
    )

    /**
     * 新增修改通知
     */
    fun notification(nts:NTS, ns_enable:Boolean) {
        nts.setEnabled(application, ns_enable)
        notification(nts.nts_id, ns_enable)
    }

    /**
     * 新增修改通知
     */
    fun notification(nts_id:Int, ns_enable:Boolean) {
        WebThread {
            val jsonMap = WebAPI.Instance.preparePOST(
                "nts_id"    to nts_id,
                "ns_enable" to if (ns_enable) 1 else 0
            )
            WebAPI.Instance.Notification(jsonMap)
        }
    }

    /**
     * App 發送通知
     */
    fun emit(nts:NTS, str_hex: String) {
        // 儲存 str_hex
        notificationHexLruCache.put(str_hex.toLowerCase(), (System.currentTimeMillis() / 1000).toInt())
        WebThread {
            val userData = AccountRoom.TokenLiveData.value
            val user_id  = userData?.user_id?: 0
            if (nts.isEnabled(application)) {
                // 發送至手錶端
                BandGattRepository.Instance(application).hostAlarm(nts = nts)
                // 發送至本地端
                WehearFirebaseMessagingService.SendBroadcast(
                    application, Gson().toJson(
                        MessageData(
                            str_hex = str_hex,
                            nts_id = nts.nts_id,
                            user_id = user_id,
                            body = nts.body()
                        )
                    )
                )
                // 發送至後台
                WebAPI.Instance.NotificationEmit(nts.nts_id, str_hex)
            }
        }
    }

    /**
     * 檢查 str_hex 是否重複 (三天內不重複)
     * 重複 = false, 不重複 = true 並儲存之
     */
    fun checkValidHex(str_hex: String): Boolean = notificationHexLruCache.run {
        (get(str_hex.toLowerCase()) ?: 0).let { time ->
            val now = (System.currentTimeMillis() / 1000).toInt()
            if (now.minus(time) > MAX_TIME_DIFF) { // 三天內不重複
                // 儲存之
                put(str_hex.toLowerCase(), now)
                // 返回
                true
            } else {
                "$str_hex 重複了".LogD()
                false
            }
        }
    }
}

/**
 * 通知類型列表
 */
enum class NTS(val nts_id:Int) {
      門鈴(1) {
          override fun bleComm() = 0x00
          override fun body() = "有人按門鈴，快開門"
      }
    , 電話(2) {
          override fun bleComm() = 0x01
          override fun body() = "電話響了，快接聽"
      }
    , 遙控器(3) { // 警示器
          override fun bleComm() = 0x02
          override fun body() = "有人呼叫您！"
      }
    , 火災(4) {
          override fun bleComm() = 0x03
          override fun body() = "發生火災！"
      }
    , SOS(5) {
          override fun bleComm() = 0x02
      }
    , 瓦斯(6)
    , 門窗(7)
    , 心律(11)
    , 血氧(12)
    , 昨晚睡眠(13)
    , 今日步行(14)
    , 步行目標(15);

    open fun bleComm():Int = 0x00
    open fun body():String = toString()

    companion object {
        fun get(nts_id:Int):NTS? = values().firstOrNull { it.nts_id == nts_id }
    }
}

fun NTS.setEnabled(app:Application, isEnabled:Boolean) {
    App.GetPreferences(app).edit().putBoolean(name, isEnabled).apply()
}

fun NTS.isEnabled(app:Application):Boolean = App.GetPreferences(app).getBoolean(name, true)