package tw.azul.wehear.status.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.layout_tile_bell.view.*
import tw.azul.wehear.R

class BellTile : FrameLayout, IConnectStatusTile, IMuteStatusTile {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun onFinishInflate() {
        super.onFinishInflate()
        View.inflate(context, R.layout.layout_tile_bell, this)
    }

    override fun setStatus(status: IConnectStatusTile.Status) {
        super.setStatus(status)
        when(status) {
            IConnectStatusTile.Status.UnConnect -> {
                status_background.isChecked = false
                status_connected.visibility = GONE
            }
            IConnectStatusTile.Status.Connected -> {
                status_background.isChecked = true
                status_connected.visibility = VISIBLE
            }
        }
    }

    fun setStatus(pair: Pair<Boolean, IConnectStatusTile.Status>) {
        val (mute, status) = pair
        setStatus(status)
        setMute(mute, status == IConnectStatusTile.Status.Connected)
    }

}