package tw.azul.wehear.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import tw.azul.wehear.MainActivity
import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.ble.AccessoryRepository
import tw.azul.wehear.fcm.WehearFirebaseMessagingService
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.user.family.FamilyViewModel
import tw.azul.wehear.user.user.UserViewModel

/**
 * 04_設定_家人
 */
open class SettingMenuFragment : Fragment() {

    // 是否為聽障者
    var IS_DEAF = true
    var USER_ID = AccountRoom.TokenLiveData.value?.userId!!

    lateinit var btnUser: View
    lateinit var btnWalk: View
    lateinit var btnHeart: View
    lateinit var btnFamily: View
    lateinit var btnNotification: View

    lateinit var imgUserPoint: View
    lateinit var imgWalkPoint: View
    lateinit var imgHeartPoint: View
    lateinit var imgFamilyPoint: View
    lateinit var imgNotificationPoint: View


    lateinit var userViewModel: UserViewModel
    lateinit var familyViewModel: FamilyViewModel

    lateinit var mNavigation: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.layout_setting_menu, container, false)
        mNavigation = NavHostFragment.findNavController(this)
        initView(view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? MainActivity)?.showToolbarAndShowNavigation()
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        familyViewModel = ViewModelProviders.of(this).get(FamilyViewModel::class.java)
        
        familyViewModel.getFamilys(USER_ID).observe(this, Observer {
            if (it.status != Status.LOADING) {
                if (it.data != null) {
                    with(imgFamilyPoint) {
                        visibility = if (
                            it.data.count { it.fs_accept == 0 } > 0
                        ) View.VISIBLE else View.INVISIBLE
                    }
                }
            }
        })

        AccessoryRepository.Instance(activity?.application!!).getBleStatusLiveData().observe(this ,
            Observer {
                IS_DEAF = it.band.isPaired!!
                btnWalk.visibility = if (!IS_DEAF) View.GONE else View.VISIBLE
                btnHeart.visibility = if (!IS_DEAF) View.GONE else View.VISIBLE
            })
    }

    open fun initView(view: View) {
        btnUser = view.findViewById(R.id.layout_user)
        btnWalk = view.findViewById(R.id.layout_walk)
        btnHeart = view.findViewById(R.id.layout_heart)
        btnFamily = view.findViewById(R.id.layout_family)
        btnNotification = view.findViewById(R.id.layout_notification)

        imgUserPoint = view.findViewById(R.id.img_user_point)
        imgWalkPoint = view.findViewById(R.id.img_walk_point)
        imgHeartPoint = view.findViewById(R.id.img_heart_point)
        imgFamilyPoint = view.findViewById(R.id.img_family_point)
        imgNotificationPoint = view.findViewById(R.id.img_notification_point)

        btnUser.setOnClickListener { mNavigation.navigate(R.id.action_mainUserFragment_to_userFragment) }
        btnWalk.setOnClickListener { mNavigation.navigate(R.id.action_mainUserFragment_to_walkFragment) }
        btnFamily.setOnClickListener { mNavigation.navigate(R.id.action_mainUserFragment_to_myFamilyFg) }
        btnNotification.setOnClickListener {
            if (IS_DEAF)
                mNavigation.navigate(R.id.action_mainUserFragment_to_settingNotificationDeafFragment)
            else
                mNavigation.navigate(R.id.action_mainUserFragment_to_settingNotificationFamilyFragment)
        }

    }


}