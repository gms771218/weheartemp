package tw.azul.wehear.portal.form

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import tw.azul.wehear.GOTOMAINACTIVITYDEFAULT
import tw.azul.wehear.IGotoMainActivity
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.request.RequestUpdateUserEntity
import tw.azul.wehear.net.model.user.UserEntity
import tw.azul.wehear.net.repository.UserRepository

class GoalViewModel : ViewModel() {

    val SUGGEST_STEP_GOAL = 10000

    var delegate: IGotoMainActivity = GOTOMAINACTIVITYDEFAULT

    private var userRepository: UserRepository = UserRepository.getInstance()

    val stepGoal: MutableLiveData<Int> = MutableLiveData<Int>().apply { value = SUGGEST_STEP_GOAL }

    fun getUser(user_id: Int): LiveData<UserEntity> {
        var liveData = MediatorLiveData<UserEntity>()
        liveData.addSource(userRepository.getUser(user_id), {
            if (it.status != Status.LOADING) {
                if (it.status == Status.SUCCESS) {
                    var _value = it.data?.user_step_goal
                    stepGoal.postValue(if (_value == 0) SUGGEST_STEP_GOAL else _value)
                    liveData.postValue(it.data)
                } else if (it.status == Status.ERROR) {
                    liveData.postValue(null)
                }
            }
        })
        return liveData
    }

    fun saveUser(user_id: Int): LiveData<Result> {
        var liveData = MediatorLiveData<Result>()
        liveData.addSource(userRepository.postUser(
            user_id
            , RequestUpdateUserEntity.create(user_id
                , UserEntity()
                , UserEntity().apply {
                    this.user_step_goal = stepGoal.value
                })
        )
            , Observer {
                if(it.status != Status.LOADING){
                    if (it.status == Status.SUCCESS) {
                        liveData.postValue(Result(true , ""))
                    }else if (it.status == Status.ERROR){
                        liveData.postValue(Result(false , "使用者資料儲存錯誤"))
                    }
                }
            })
        return liveData
    }

    class Result {
        var isSuccess: Boolean? = null
        var msg: String = ""

        constructor(isSuccess: Boolean?, msg: String) {
            this.isSuccess = isSuccess
            this.msg = msg
        }
    }

}