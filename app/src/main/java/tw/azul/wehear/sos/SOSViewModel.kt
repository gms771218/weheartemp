package tw.azul.wehear.sos

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel;
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.repository.NotificationRepository

class SOSViewModel : ViewModel() {

    // SOS推播類型ID
    val SOS_NOTIFICATION_TYPE_ID = 5

    var notificationRepository: NotificationRepository = NotificationRepository.getInstance()

    fun sendSOS(user_id: Int, str_hex: String, latitude: Float, longitude: Float): LiveData<Result> {
        return Transformations.map(
            notificationRepository.getAppNotification(user_id, SOS_NOTIFICATION_TYPE_ID, str_hex, latitude, longitude)
        ) {
            when (it.status) {
                Status.LOADING -> null
                Status.SUCCESS -> if (it.data?.size ?: 0 > 0) {
                    Result(false, it.data?.get(0)?.msg ?: it?.message ?: "")
                } else {
                    Result(true, "")
                }
                Status.ERROR -> Result(false, it.data?.get(0)?.msg ?: it?.message ?: "")
            }
        }
    }

    data class Result(var isSuccess: Boolean? = null, var msg: String = "")

}
