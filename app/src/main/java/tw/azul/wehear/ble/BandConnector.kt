@file:Suppress("FunctionName", "LocalVariableName")

package tw.azul.wehear.ble

import android.app.Application
import android.bluetooth.*
import android.bluetooth.BluetoothGatt.GATT_SUCCESS
import android.bluetooth.BluetoothProfile.STATE_CONNECTED
import android.bluetooth.BluetoothProfile.STATE_DISCONNECTED
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import tw.azul.wehear.App
import tw.azul.wehear.api.DeviceApiRepository

class BandConnector private constructor(private val application: Application) {

    companion object {
        @Volatile
        private var singleton: BandConnector? = null
        fun Instance(application: Application): BandConnector = singleton ?: synchronized(Companion) {
            singleton = singleton ?: BandConnector(application)
            return singleton!!
        }
    }

    private val mMainHandler = Handler(Looper.getMainLooper())
    private val connectionStateLiveData = MutableLiveData<ConnectionState>().apply { value = ConnectionState.Init }

    private val defaultInterceptorResultLiveData: LiveData<InterceptorResult> =
        MutableLiveData<InterceptorResult>().apply { value = InterceptorResult.Accept() }

    sealed class InterceptorResult {
        data class Accept(val device_id: String? = null) : InterceptorResult()
        object Reject : InterceptorResult()
    }

    fun connectGatt(
        bluetoothDevice: BluetoothDevice? = null,
        doConnect: Boolean? = bluetoothDevice != null,
        interceptor: (() -> LiveData<InterceptorResult>) = { defaultInterceptorResultLiveData }
    ): LiveData<ConnectionState> {
        if (bluetoothDevice == null || doConnect == false) {
            return connectionStateLiveData
        }
        when {
            !BluetoothAdapter.getDefaultAdapter().isEnabled -> {
                connectionStateLiveData.postValue(ConnectionState.Init)
                return connectionStateLiveData
            }
            connectionStateLiveData.value is ConnectionState.CONNECTED -> {
                return connectionStateLiveData
            }
            connectionStateLiveData.value is ConnectionState.DISCONNECTED -> {
                (connectionStateLiveData.value as ConnectionState.DISCONNECTED).apply{
                    gatt.disconnect()
                    gatt.close()
                }
            }
        }
        connectionStateLiveData.postValue(ConnectionState.Init)
        object : BluetoothGattCallback(delegate = BandGattRepository.Instance(application).delegate)
        {
            init {
                gatt = bluetoothDevice.connectGatt(application, false, this) // 這被 delegate 轉介走
            }
            override fun onConnectionStateChange(bluetoothGatt: BluetoothGatt, status: Int, newState: Int) {
                android.util.Log.v("Faty", "[Band] $bluetoothDevice = $newState (STATE_CONNECTED = $STATE_CONNECTED)")
                when (newState) {
                    STATE_CONNECTED -> bluetoothGatt.discoverServices()
                    STATE_DISCONNECTED -> closeGATT(bluetoothGatt)
                }
            }
            override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
                android.util.Log.v("Faty", "[Band] $bluetoothDevice = $status (GATT_SUCCESS = $GATT_SUCCESS)")
                if (status == GATT_SUCCESS) {
                    // 開始 AOP
                    mMainHandler.post {
                        interceptor().observeForever { result ->
                            when (result) {
                                is InterceptorResult.Accept -> {
                                    if (result.device_id != null) {
                                        gatt.device.setDeviceId(result.device_id)
                                    }
                                    connectionStateLiveData.postValue(ConnectionState.CONNECTED(gatt))
                                    delegate.onServicesDiscovered(gatt, status) // AOP 最後一哩
                                }
                                else -> {
                                    if (result != InterceptorResult.Reject) {
                                        android.util.Log.wtf("Faty", "InterceptorResult is o.w.")
                                    }
                                    closeGATT(gatt)
                                }
                            }
                        }
                    }
                } else closeGATT(gatt)
            }

            private fun closeGATT(gatt: BluetoothGatt) {
                this.gatt?.disconnect()
                this.gatt?.close()
                this.gatt = null
                connectionStateLiveData.postValue(ConnectionState.DISCONNECTED(gatt))
            }

        }
        return connectionStateLiveData
    }

    // 檢查清單中是否有本家 deviceId
    fun check(device: BluetoothDevice?, deviceIdList: List<String>) = deviceIdList.any { it == device?.getDeviceId() }

    fun reset() {
        connectionStateLiveData.value.run {
            when(this) {
                is ConnectionState.CONNECTED -> gatt
                is ConnectionState.DISCONNECTED -> gatt
                else -> null
            }
        }?.apply {
            val device_id: String? = device.getDeviceId()
            if (device_id != null) {
                DeviceApiRepository.Instance.remove(device_id).observeForever {
                    android.util.Log.w("Faty", "$device_id.remove = $it")
                }
            }
            device.setDeviceId(null)
        }?.also {gatt->
            // 斷線震動
            //BandGattRepository.Instance(application).immediateAlert(gatt) // 不想給他震動了
            // 斷線
            gatt.disconnect()
            gatt.close()
        }
        connectionStateLiveData.postValue(ConnectionState.Init)
    }

    sealed class ConnectionState {
        object Init: ConnectionState()
        data class CONNECTED(val gatt: BluetoothGatt) : ConnectionState()
        data class DISCONNECTED(val gatt: BluetoothGatt) : ConnectionState()
    }

    private fun BluetoothDevice?.setDeviceId(device_id: String?) = apply {
        App.GetPreferences(application).edit().putString(this?.toString(), device_id).apply()
    }

    private fun BluetoothDevice?.getDeviceId(): String? = run {
        App.GetPreferences(application).getString(this?.toString(), null)
    }

}