package tw.azul.wehear.portal.device

import android.app.Application
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.*
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import tw.azul.feature.ble.repository.BleRepository
import tw.azul.wehear.GOTOMAINACTIVITYDEFAULT
import tw.azul.wehear.IGotoMainActivity
import tw.azul.wehear.R
import tw.azul.wehear.api.DT
import tw.azul.wehear.api.DeviceApiRepository
import tw.azul.wehear.ble.DeviceRepository
import tw.azul.wehear.ble.HostConnector

open class ChooseHostViewModel(val app: Application) : AndroidViewModel(app) {

    lateinit var lifecycleOwner: LifecycleOwner // 最後 connect() 用的
    var delegate: IGotoMainActivity = GOTOMAINACTIVITYDEFAULT

    private val bleRepository = BleRepository.Instance(app)
    fun bleStatusAddObserver(lifecycleOwner: LifecycleOwner, observer: Observer<Boolean>) {
        bleRepository.bleStatusAddObserver(lifecycleOwner, observer)
    }
    val bleStatusDelObserver  = bleRepository::bleStatusDelObserver
    val enabledButtonLiveData:LiveData<Boolean>
    private val chosenBluetoothDevice = MutableLiveData<BluetoothDevice>().apply {
        enabledButtonLiveData = Transformations.map(this) { it != null }
    }

    val hostListLiveData: LiveData<List<BluetoothDevice>> = MutableLiveData<List<BluetoothDevice>>().apply {
        value = DeviceRepository.Instance(app).hostListLiveData.value
    }

    val showLoading:LiveData<Int>
    private val mShowLoadingLiveData = MutableLiveData<Boolean>().apply {
        value = false
        showLoading = Transformations.map(this) { if (it == true) View.VISIBLE else View.GONE }
    }

    fun setBluetoothDevice(bluetoothDevice: BluetoothDevice) {
        chosenBluetoothDevice.postValue(bluetoothDevice)
    }

    fun checkDevice(view: View) {
        val bluetoothDevice = chosenBluetoothDevice.value
        when {
            bluetoothDevice == null -> {
                //android.widget.Toast.makeText(app, "不知名錯誤", android.widget.Toast.LENGTH_SHORT).show()
            }
            bluetoothDevice.name == "HX_SHOCK" -> {
                connect(view, DT.HX_SHOCK(device = bluetoothDevice))
            }
            bluetoothDevice.name == "WeHear" -> {
                connect(view, DT.WeHearHost(device = bluetoothDevice))
            }
            else -> {
                //android.widget.Toast.makeText(app, "不知名錯誤", android.widget.Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun check(view: View, dt: DT, is_cover: String = "0"): LiveData<HostConnector.InterceptorResult> {
        val resultLiveData = MediatorLiveData<HostConnector.InterceptorResult>()
        DeviceApiRepository.Instance.add(dt, is_cover).observe(lifecycleOwner::getLifecycle) {
            when {
                (it is DeviceApiRepository.Device.Failure && it.errCode == 2112) -> {
                    mShowLoadingLiveData.postValue(false)
                    AlertDialogBuilder(view.context) {
                        setTitle("提醒")
                        setMessage(it.errMsg)
                        setCancelable(false)
                        setPositiveButton("繼續配對") { d, _ ->
                            mShowLoadingLiveData.postValue(true)
                            resultLiveData.apply {
                                addSource(check(view, dt, "1"), ::postValue)
                            }
                            d.dismiss()
                        }
                        setNegativeButton("取消") { d, _ ->
                            resultLiveData.postValue(HostConnector.InterceptorResult.Reject)
                            d.dismiss()
                        }
                        show()
                    }
                }
                it is DeviceApiRepository.Device.Failure -> {
                    //android.widget.Toast.makeText(app, it.errMsg, android.widget.Toast.LENGTH_SHORT).show()
                    mShowLoadingLiveData.postValue(false)
                    resultLiveData.postValue(HostConnector.InterceptorResult.Reject)
                }
                it is DeviceApiRepository.Device.Success -> {
                    resultLiveData.postValue(HostConnector.InterceptorResult.Accept(it.device_id))
                }
            }
        }
        return resultLiveData
    }

    private fun connect(view: View, dt: DT) {
        mShowLoadingLiveData.postValue(true)
        val connectLiveData = MediatorLiveData<HostConnector.ConnectionState>().apply {
            var isFirst = true
            val source = HostConnector.Instance(app).connectGatt(dt.device, interceptor = { device_unique ->
                dt.apply {
                    when (this) {
                        is DT.HX_SHOCK -> hex = device_unique
                        is DT.WeHearHost -> hex = device_unique
                    }
                }.run {
                    check(view, this)
                }
            })
            addSource(source) {
                if (isFirst) { // 濾過第一個值
                    isFirst = false
                    return@addSource
                }
                postValue(it)
            }
        }
        connectLiveData.observe(lifecycleOwner, object : Observer<HostConnector.ConnectionState> {
            override fun onChanged(status: HostConnector.ConnectionState?) {
                when (status) {
                    null -> {
                        //android.widget.Toast.makeText(app, "不知名錯誤", android.widget.Toast.LENGTH_SHORT).show()
                        mShowLoadingLiveData.postValue(false)
                        connectLiveData.removeObserver(this)
                    }
                    is HostConnector.ConnectionState.Init -> {
                        //android.widget.Toast.makeText(app, "主機連線中", android.widget.Toast.LENGTH_SHORT).show()
                    }
                    is HostConnector.ConnectionState.DISCONNECTED -> {
                        //android.widget.Toast.makeText(app, "連線失敗", android.widget.Toast.LENGTH_SHORT).show()
                        mShowLoadingLiveData.postValue(false)
                        connectLiveData.removeObserver(this)
                        Snackbar.make(view, "主機連線失敗", Snackbar.LENGTH_INDEFINITE).apply {
                            setActionTextColor(ResourcesCompat.getColor(view.resources, R.color.colorAccent, null))
                            setAction("知道了") { dismiss() }
                            show()
                        }
                    }
                    is HostConnector.ConnectionState.CONNECTED -> {
                        //android.widget.Toast.makeText(app, "主機連線成功", android.widget.Toast.LENGTH_SHORT).show()
                        mShowLoadingLiveData.postValue(false)
                        connectLiveData.removeObserver(this)
                        DeviceRepository.Instance(app).hostLiveData.postValue(dt.device) // 要手動注入hostLiveData
                        view.runCatching(::gotoNext).exceptionOrNull()?.printStackTrace()
                    }
                }
            }
        })
    }

    open fun gotoNext(view: View) {
        view.findNavController().navigate(R.id.action_chooseHostFragment_to_infoFragment)
    }

    @Suppress("FunctionName")
    private fun AlertDialogBuilder(context: Context, block: AlertDialog.Builder.() -> AlertDialog) =
        AlertDialog.Builder(context, R.style.Theme_MaterialComponents_Light_Dialog_Alert).run(block)
}