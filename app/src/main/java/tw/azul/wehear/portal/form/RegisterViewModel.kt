package tw.azul.wehear.portal.form

import android.app.Application
import android.app.DatePickerDialog
import android.view.View
import android.widget.DatePicker
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRepository
import tw.azul.wehear.account.AccountRoom.Status.*
import java.util.*

class RegisterViewModel(val app: Application) : AndroidViewModel(app) {

    data class Birthday(var year: Int = 1950, var month: Int = 1, var day: Int = 1) {
        override fun toString(): String = "${year}年${month}月${day}日"
    }

    class BirthdayLiveData(date: Birthday = Birthday()) : MutableLiveData<String>() {
        private lateinit var mDate: Birthday
        init {
            setDate(date)
        }
        fun setDate(d: Birthday) {
            mDate = d
            value = d.toString()
        }
        fun setDate(block: Birthday.() -> Unit) = Birthday().apply(block).also(this::setDate)
        fun getDate():Birthday = mDate
    }

    val name: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }

    val gender: MutableLiveData<Int> = MutableLiveData()

    val birthday: BirthdayLiveData = BirthdayLiveData()

    val acc: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }

    val pwd: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }

    val chk: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }

    val aid: MutableLiveData<Int> = MutableLiveData<Int>().apply { value = R.id.aid_0 }

    val loading: LiveData<Boolean> = MediatorLiveData()

    val errStatusLiveData = MutableLiveData<String>()

    fun datePick(view: View) {
        DatePickerDialog(
            view.context,
            DatePickerDialog.OnDateSetListener { _: DatePicker, y: Int, m: Int, d: Int ->
                birthday.setDate {
                    year  = y
                    month = m + 1
                    day   = d
                }
            },
            1950,
            0,
            1
        ).apply {
            datePicker.maxDate = Calendar.getInstance().timeInMillis
        }.show()
    }

    fun register(view: View) {
        var checkValid = true
        val name: String? = name.CheckEmptyOrNull()
        if (name == null) {
            errStatusLiveData.postValue("姓名不能為空")
            checkValid = false
        }
        val acc:String? = acc.CheckEmptyOrNull()
        if (acc?.length != 10) {
            errStatusLiveData.postValue("使用者手機格式錯誤")
            checkValid = false
        }
        val pwd: String? = pwd.CheckEmptyOrNull()
        if (pwd == null) {
            errStatusLiveData.postValue("密碼不能為空")
            checkValid = false
        }
        val chk: String? = chk.CheckEmptyOrNull()
        if (chk == null || chk != pwd) {
            errStatusLiveData.postValue("密碼確認有誤")
            checkValid = false
        }
        val gender = 1 - gender.CheckEmptyOrZero()
        val (year, month, day) = birthday.getDate()
        val aid =  when(aid.CheckEmptyOrZero()) {
            R.id.aid_1 -> 1
            R.id.aid_0 -> 0
            else -> 0
        }
        if (!checkValid) return
        AccountRepository.Instance(app).register(acc!!, pwd!!, name!!, gender, year, month, day, aid)
            .apply {
                (loading as MediatorLiveData).addSource(this) { status ->
                    when (status) {
                        is Init, is Connecting -> {
                            loading.value = true
                        }
                        is Error, is Connected -> {
                            loading.value = false
                            loading.removeSource(this)
                            if (status is Error){
                                errStatusLiveData.postValue(status.errMsg)
                            }
                            if (status is Connected) {
                                view.findNavController().navigate(R.id.action_registerFragment_to_submitFragment)
                            }
                        }
                    }
                }
            }
    }

    // Kotlin Extension used by Self
    private fun LiveData<String>.CheckEmptyOrNull():String? = value?.takeIf { it.isNotEmpty() }
    private fun LiveData<Int>.CheckEmptyOrZero(): Int = value ?: 0
}
