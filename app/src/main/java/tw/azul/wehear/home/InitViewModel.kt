package tw.azul.wehear.home

import android.app.Application
import androidx.lifecycle.*
import tw.azul.wehear.BuildConfig
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.ble.DeviceRepository
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.version.VersionEntity
import tw.azul.wehear.net.repository.VersionRepository

class InitViewModel(val app: Application) : AndroidViewModel(app) {

    val accountStatusLiveData = AccountRoom.StatusLiveData
    val accountTokenLiveData  = AccountRoom.TokenLiveData

    val versionRepository = VersionRepository.getInstance()

    private val deviceRepository = DeviceRepository.Instance(app)
    val deviceLiveData = MutableLiveData<Boolean>().apply { value = true }

    fun checkVersionLiveData(): LiveData<VersionEntity> {
        return Transformations.map(versionRepository.version) {
            if (it.status!=Status.LOADING)
                if (it.data?.vs_name != BuildConfig.VERSION_NAME) it.data!! else VersionEntity()
            else
                null
        }
    }

}
