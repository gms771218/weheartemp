package tw.azul.wehear.status.host

import android.app.Application
import android.view.View
import androidx.navigation.findNavController
import tw.azul.wehear.R
import tw.azul.wehear.portal.device.ChooseHostViewModel

class ChooseViewModel(app: Application) : ChooseHostViewModel(app) {
    override fun gotoNext(view: View) {
        view.findNavController().navigate(R.id.action_hostChooseFragment_to_hostAddressFragment)
    }
}
