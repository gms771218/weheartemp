@file:Suppress("EXPERIMENTAL_API_USAGE", "FunctionName")

package tw.azul.wehear.ble.model

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import tw.azul.wehear.App
import tw.azul.wehear.ble.BandGattRepository
import java.util.*
import java.util.Calendar.*
import kotlin.math.max

class BandAlarmRepository private constructor(private val application: Application) {

    companion object {
        private fun DefaultBandAlarmBleCommIntPair(): Array<Pair<Int, Int>> = arrayOf(
            0xC0FFFFFF.toInt() to 0x7F,
            0xC8FFFFFF.toInt() to 0x7F,
            0xD0FFFFFF.toInt() to 0x7F,
            0xD8FFFFFF.toInt() to 0x7F
        )
        @Volatile
        private var singleton: BandAlarmRepository? = null
        fun Instance(application: Application): BandAlarmRepository = singleton ?: synchronized(Companion) {
            singleton = singleton ?: BandAlarmRepository(application)
            return singleton!!
        }
    }

    fun getClockAlarmsList(): LiveData<List<BandAlarmEntity>> {
        val responseLiveData = BandGattRepository.Instance(application).getClockAlarm()
        return MediatorLiveData<List<BandAlarmEntity>>().apply {
            addSource(responseLiveData) {
                val defaultBleCommIntPairArray = DefaultBandAlarmBleCommIntPair()
                it.mapNotNull { commPair ->
                    when (defaultBleCommIntPairArray.contains(commPair)) {
                        true -> null
                        else -> BleCommIntPairToBandAlarmEntity(application, commPair)
                    }
                }.apply {
                    postValue(this)
                }
            }
        }
    }

    fun setClockAlarmsList(bandAlarmList: List<BandAlarmEntity>) {
        val bleCommIntPairArray: Array<Pair<Int, Int>> = DefaultBandAlarmBleCommIntPair().apply {
            bandAlarmList.forEachIndexed { index, bandAlarmEntity ->
                this[index] = bandAlarmEntity.toBleCommIntPair(application)
            }
            //forEach { it.LogI() }
        }
        BandGattRepository.Instance(application).setClockAlarm(bleCommIntPairArray = bleCommIntPairArray)
    }

}

/*
    Kotlin Extension used by self
 */
private fun BandAlarmEntity.toBleCommIntPair(application: Application): Pair<Int, Int> {
    // 主題處理
    val index = key
    App.GetPreferences(application)
       .edit()
       .putString(application.packageName + ".bandalarm.themeName." + index, themeName)
       .apply()
    // 時間處理
    val (nowYear, nowMonth, nowDay, nowHour, nowMinute) = Calendar.getInstance().run {
        arrayOf(max(get(YEAR), 2000), get(MONTH) + 1, get(DAY_OF_MONTH), get(HOUR_OF_DAY), get(MINUTE))
    }
    val (tomYear, tomMonth, tomDay) = Calendar.getInstance().run {
        add(DAY_OF_MONTH, 1)
        Triple(max(get(YEAR), 2000), get(MONTH) + 1, get(DAY_OF_MONTH))
    }
    val hour   = hour
    val minute = minute
    val weeks  = weeks
                .let { arrayOf(it[1], it[2], it[3], it[4], it[5], it[6], it[0]) }  // 低位到高位: 周一到周日
                .foldIndexed(0) { offset, result, w ->
                    when (w) {
                        false -> result
                        true  -> 1.shl(offset).or(result)
                    }
                }
    val isOnce  = weeks == 0
    val isToday = when {
        (nowHour   > hour)   -> false
        (nowHour   < hour)   -> true
        (nowMinute < minute) -> true
        else -> false
    }
    val (year, month, day) = when {
        // 非清空(僅關閉) → 指向 2063年
        !enable -> Triple(2063, 1, 1)
        // 非單次(週期性) → 指向 2000年
        !isOnce -> Triple(2000, 1, 1)
        // 今天 → 指向今天 (一次性)
        isToday -> Triple(nowYear, nowMonth, nowDay)
        // 不是今天 → 指向明天 (一次性)
        else -> Triple(tomYear, tomMonth, tomDay)
    }
    /*
        生成 Hex (BleComm)
     */
    val hexL = weeks // 低部(應為 8位
    var hexH = 0     // 高部(應為32位)
    // 年 << 26(佔6位)
    hexH = hexH.or(0b111111.and(year - 2000) shl 26)
    // 月 << 22(佔4位)
    hexH = hexH.or(0b1111.and(month) shl 22)
    // 日 << 17(佔5位)
    hexH = hexH.or(0b11111.and(day) shl 17)
    // 時 << 12(佔5位)
    hexH = hexH.or(0b11111.and(hour) shl 12)
    // 分 << 6(佔6位)
    hexH = hexH.or(0b111111.and(minute) shl 6)
    // 編號 << 3(佔3位)
    hexH = hexH.or(0b111.and(index) shl 3)
    // 保留位皆0 (佔3位, 另1位在hexL)
    /* nothing to do */
    /*
        轉出 (反轉hexH, hexL)
     */
    return hexH.reverseWith8bit() to hexL
}

fun BleCommIntPairToBandAlarmEntity(application: Application, commPair: Pair<Int, Int>): BandAlarmEntity {
    val hexH = commPair.first.reverseWith8bit()
    val hexL = commPair.second
    val isOnce = hexL == 0
    val weeks  = BooleanArray(7) { offset -> hexL.shr(offset).and(1) == 1 }
                            .let { booleanArrayOf(it[6], it[0], it[1], it[2], it[3], it[4], it[5]) }
    val key    = hexH.shr(3).and(0b111)
    val minute = hexH.shr(6).and(0b111111)
    val hour   = hexH.shr(12).and(0b11111)
    val day    = hexH.shr(17).and(0b11111)
    val month  = hexH.shr(22).and(0b1111)
    val year   = hexH.shr(26).and(0b111111) + 2000
    val enable = when {
        year == 2063 && month == 1 && day == 1 -> false
        year == 2000 && month == 1 && day == 1 && !isOnce -> true
        else -> {
            val emitTime = Calendar.getInstance().apply {
                set(year, month - 1, day, hour, minute)
            }
            Calendar.getInstance().before(emitTime)
        }
    }
    return BandAlarmEntity(
        key = key,
        hour = hour,
        minute = minute,
        weeks = weeks,
        enable = enable,
        themeName = App.GetPreferences(application)
                       .getString(application.packageName + ".bandalarm.themeName." + key, null) ?: ""
    )
}

private fun Int.reverseWith8bit() = reverse()
private fun Int.reverse(w: Int = 8) = run{
    val mask = (-1 shl w).inv()
    (0..32 step w).reduce { rev: Int, offset: Int -> shr(offset - w).and(mask).shl(32 - offset).or(rev) }
}