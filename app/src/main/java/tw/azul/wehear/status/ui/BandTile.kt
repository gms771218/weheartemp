package tw.azul.wehear.status.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.layout_tile_band.view.*
import tw.azul.wehear.R

class BandTile : FrameLayout, IPairStatusTile, IConnectStatusTile {

    private var pairStatus: IPairStatusTile.Status = IPairStatusTile.Status.UnPair

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun onFinishInflate() {
        super.onFinishInflate()
        View.inflate(context, R.layout.layout_tile_band, this)
    }

    override fun setStatus(status: IPairStatusTile.Status) {
        when(status) {
            IPairStatusTile.Status.UnPair -> {
                pair_background     .isChecked  = false
                unconnect_background.isChecked  = false
                status_unpair       .visibility = VISIBLE
                status_unconnect    .visibility = GONE
                status_connected    .visibility = GONE
                status_power        .visibility = GONE
            }
            IPairStatusTile.Status.Paired -> {
                pair_background     .isChecked  = true
                unconnect_background.isChecked  = true
                status_unpair       .visibility = GONE
                status_unconnect    .visibility = VISIBLE
                status_connected    .visibility = GONE
                status_power        .visibility = GONE
            }
        }
        pairStatus = status
    }

    override fun setStatus(status: IConnectStatusTile.Status) {
        when(status) {
            IConnectStatusTile.Status.UnConnect -> setStatus(pairStatus)
            IConnectStatusTile.Status.Connected -> {
                pair_background     .isChecked  = true
                unconnect_background.isChecked  = false
                status_unpair       .visibility = GONE
                status_unconnect    .visibility = GONE
                status_connected    .visibility = VISIBLE
                status_power        .visibility = VISIBLE
            }
        }
    }

    fun setBandPower(power: Int) {
        if (power in 0..100)
            status_power.text = String.format("%d%%", power)
        else
            status_power.text = "n/a"
    }
}