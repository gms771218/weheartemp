package tw.azul.wehear.user.family


import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.Group
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.google.gson.Gson
import tw.azul.wehear.MainActivity

import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.family.FamilyEntity
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import tw.azul.wehear.BuildConfig


/**
 * 搜尋家人
 */
class SearchFamilyFragment : Fragment() {

    companion object {
        val SMS_TITLE = "WeHear App"
        val SMS_BODY = "WeHear App下載連結：https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}。趕快下載並註冊！"
    }

    val USER_ID = AccountRoom.TokenLiveData.value?.userId!!

    lateinit var etIptPhone: EditText
    lateinit var btnInvite: Button
    lateinit var layoutGroup: Group
    lateinit var tvName: TextView
    lateinit var tvPhone: TextView
    lateinit var tvInviteMsg: TextView

    lateinit var familyViewModel: FamilyViewModel

    var familyEntity: FamilyEntity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_search_family, container, false)
        initView(view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? MainActivity)?.showToolbarAndHideNavigation()
        familyViewModel = ViewModelProviders.of(this).get(FamilyViewModel::class.java)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        etIptPhone.removeTextChangedListener(TextWatcher)
    }

    fun initView(view: View) {

        layoutGroup = view.findViewById(R.id.group_result)
        etIptPhone = view.findViewById(R.id.et_ipt_phone)
        etIptPhone.addTextChangedListener(TextWatcher)
        tvName = view.findViewById(R.id.tv_name)
        tvPhone = view.findViewById(R.id.tv_phone)
        tvInviteMsg = view.findViewById(R.id.tv_invite_msg)
        btnInvite = view.findViewById(R.id.btn_invite)

        btnInvite.setOnClickListener({
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm?.hideSoftInputFromWindow(etIptPhone.windowToken, 0)
            goEditFamilyPage(familyEntity)
        })
    }


    fun searchPhone(phone: String) {
        familyViewModel.getFamily(USER_ID, phone).observe(this, Observer {
            if (it.status != Status.LOADING) {
                if (it.status == Status.ERROR) {
                    // 查無資料. reason:有資料回傳物件，沒資料回傳陣列。用解析錯誤來判斷是否有資料
                    showNotResult()
                } else if (it.status == Status.SUCCESS) {
                    setResult(it.data)
                }
            }
        })
    }

    fun setResult(entity: FamilyEntity?) {
        familyEntity = entity
        entity!!.run {
            layoutGroup.visibility = View.VISIBLE
            tvName.text = entity.user_name
            tvPhone.text = entity.user_phone
            when (entity.relation) {
                0 -> {
                    // 未邀請
                    btnInvite.text = "傳送家人邀請"
                    btnInvite.isEnabled = true
                    btnInvite.visibility = View.VISIBLE
                    tvInviteMsg.visibility = View.INVISIBLE
                }
                1 -> {
                    // 已邀請
                    btnInvite.text = "已發送邀請"
                    btnInvite.isEnabled = false
                    btnInvite.visibility = View.VISIBLE
                    tvInviteMsg.visibility = View.INVISIBLE
                }

                2 -> {
                    // 已為家人
                    btnInvite.visibility = View.INVISIBLE
                    tvInviteMsg.visibility = View.VISIBLE
                }
            }
        }
    }


    fun goEditFamilyPage(entity: FamilyEntity?) {
        NavHostFragment.findNavController(this).navigate(R.id.action_searchFamilyFragment_to_editFamilyFragment
            , Bundle()
                .apply {
                    putString("edit_family", Gson().toJson(entity))
                })
    }

    fun showNotResult() {
        layoutGroup.visibility = View.GONE
        val builder: AlertDialog.Builder = AlertDialog.Builder(context, R.style.Theme_MaterialComponents_Light_Dialog_Alert).apply {
            setTitle("未搜尋到此號碼")
            setMessage("此號碼使用者還不是WeHear的會員，是否傳送下載APP連結給此家人，請家人下載APP與註冊會員？")
            setPositiveButton("發送下載連結") { _, _ ->
                //android.widget.Toast.makeText(context, "發送下載連結", android.widget.Toast.LENGTH_SHORT).show()
                val uri = Uri.parse(String.format("smsto:%s", etIptPhone.text.toString()))
                val it = Intent(Intent.ACTION_SENDTO, uri)
                it.putExtra("subject", SMS_TITLE)
                it.putExtra("sms_body", SMS_BODY)
                it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(it)
            }
            setNegativeButton("取消") { dialog, _ -> dialog.dismiss() }
        }
        builder.create().apply {
            show()
            getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.TRANSPARENT)
            getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(Color.TRANSPARENT)
            getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.color2))
            getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.color2))
        }
    }

    fun showSearchStatus() {
        layoutGroup.visibility = View.GONE
    }

    var TextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (s?.length == 10) {
                searchPhone(etIptPhone.text.toString())
            } else {
                showSearchStatus()
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }


} // class close
