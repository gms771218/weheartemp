@file:Suppress("PrivatePropertyName", "WrongConstant", "FunctionName")

package tw.azul.wehear.ble.model

import android.app.Notification
import android.app.Notification.DEFAULT_ALL
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import androidx.annotation.MainThread
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_MAX
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import tw.azul.wehear.App
import tw.azul.wehear.PortalActivity
import tw.azul.wehear.R
import tw.azul.wehear.ble.AccessoryRepository
import tw.azul.wehear.ble.BandGattRepository
import java.util.*

class BandBatteryRepository private constructor() {

    companion object {
        val Instance = BandBatteryRepository()
    }

    private var powerLiveData = MediatorLiveData<Int>()

    @MainThread
    fun batteryLevel(): LiveData<Int> = Transformations.map(powerLiveData) { it }

    class LifecycleService: androidx.lifecycle.LifecycleService() {

        companion object {
            private val NOTIFICATION_ID = UUID.randomUUID().hashCode()
        }

        private lateinit var CHANNEL_ID: String

        override fun onCreate() {
            super.onCreate()
            var sourceLiveData: LiveData<Int>? = null
            AccessoryRepository.Instance(application).getBleStatusLiveData().observe(::getLifecycle) { bleStatus ->
                val mediatorLiveData = Instance.powerLiveData
                val isBandConnected = bleStatus?.band?.isConnected ?: false
                when {
                    isBandConnected && sourceLiveData == null -> {
                        sourceLiveData = BandGattRepository.Instance(application).batteryLevel()
                        mediatorLiveData.addSource(sourceLiveData as LiveData, mediatorLiveData::postValue)
                    }
                    !isBandConnected && sourceLiveData != null -> {
                        mediatorLiveData.removeSource(sourceLiveData as LiveData)
                        sourceLiveData = null
                    }
                }
            }
            // 電量觀察
            Instance.batteryLevel().observe(::getLifecycle) { power ->
                if (power in 0 until 30) {
                    var key = "power_notify_${power/10}"
                    if (!App.GetPreferences(application).getBoolean(key , false)){
                        emitNotification(power)
                        App.GetPreferences(application).edit().putBoolean("power_notify_${power/10}" , true).commit()
                    }
                }else{
                    if (App.GetPreferences(application).contains("power_notify_3")
                    && App.GetPreferences(application).getBoolean("power_notify_3",true)){
                        App.GetPreferences(application).edit()
                            .putBoolean("power_notify_3" , false)
                            .putBoolean("power_notify_2" , false)
                            .putBoolean("power_notify_1" , false)
                            .commit()
                    }
                }
            }
        }

        override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
            super.onStartCommand(intent, flags, startId)
            CHANNEL_ID = "$packageName.power"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel(CHANNEL_ID, "電量", NotificationManager.IMPORTANCE_MAX).apply {
                    enableVibration(true)
                    enableLights(true)
                    setShowBadge(true)
                    setBypassDnd(true)
                    lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                    (getSystemService(NOTIFICATION_SERVICE) as NotificationManager)
                        .createNotificationChannel(this)
                }
            }
            with(application) {
                startForeground(1,
                    NotificationCompat.Builder(this, AccessoryRepository.TAG)
                        .setContentTitle(getText(R.string.app_name))
                        .setContentText(getText(R.string.notification_bt_hint))
                        .setSmallIcon(android.R.drawable.stat_sys_data_bluetooth)
                        .setColor(ResourcesCompat.getColor(resources, R.color.bluetooth_sd_color, null))
                        .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.app_launcher_round))
                        .build()
                )
            }
            return START_NOT_STICKY
        }

        private fun emitNotification(power: Int) {
            NotificationCompat.Builder(application, CHANNEL_ID).apply {
                setContentIntent(PendingIntent.getActivity(
                    application,
                    0,
                    Intent.makeRestartActivityTask(ComponentName(application, PortalActivity::class.java)),
                    PendingIntent.FLAG_ONE_SHOT
                ))
                setAutoCancel(true)
                setSmallIcon(android.R.drawable.ic_dialog_alert)
                setLargeIcon(BitmapFactory.decodeResource(application.resources, R.mipmap.app_launcher_round))
                setContentTitle("通知")
                setContentText("電量 $power% 請充電")
                setNumber(1)
                setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
                setDefaults(DEFAULT_ALL)
                priority = PRIORITY_MAX
                color = Color.RED
                NotificationManagerCompat.from(application).notify(NOTIFICATION_ID, build())
            }
        }
    }
}