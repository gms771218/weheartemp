package tw.azul.wehear.status

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_main_status.*
import tw.azul.wehear.MainActivity
import tw.azul.wehear.api.DeviceApiRepository
import tw.azul.wehear.databinding.FragmentMainStatusBinding

class StatusFragment : Fragment() {

    companion object {
        fun newInstance() = StatusFragment()
    }

    private lateinit var databinding:FragmentMainStatusBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        databinding = FragmentMainStatusBinding.inflate(inflater)
        return databinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        databinding.setLifecycleOwner(viewLifecycleOwner)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ViewModelProviders.of(this).get(StatusViewModel::class.java).apply {
            val lifecycleOwner = viewLifecycleOwner::getLifecycle
            // 手錶
            bandPairStatus.observe(lifecycleOwner, tile_band::setStatus)
            bandConnectStatus.observe(lifecycleOwner, tile_band::setStatus)
            bandPowerLiveData.observe(lifecycleOwner, tile_band::setBandPower)
            // 主機
            hostPairStatus.observe(lifecycleOwner, tile_host::setStatus)
            hostConnectStatus.observe(lifecycleOwner, tile_host::setStatus)
            // 電話
            phoneConnectStatus.observe(lifecycleOwner, tile_phone::setStatus)
            // 門鈴
            bellConnectStatus.observe(lifecycleOwner, tile_bell::setStatus)
            // 火災警報器
            alarmConnectStatus.observe(lifecycleOwner, tile_alarm::setStatus)
            // 無線警示器
            warnConnectStatus.observe(lifecycleOwner, tile_warn::setStatus)
            // BLE Connection 觀察
            bleConnectionStatusObserve(viewLifecycleOwner)
            // 注入 裝置狀態
            DeviceApiRepository.Instance.status().observe(lifecycleOwner, device_status::postValue)
            // 注入 viewmodel
            databinding.viewmodel = this
        }
        (activity as? MainActivity)?.hideToolbarAndShowNavigation()
    }
}
