package tw.azul.wehear.net.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 使用者資料
 */
public class UserEntity {

    /**
     * 使用者姓名
     */
    @Expose
    @SerializedName("user_name")
    public String user_name = "unknown";

    /**
     * 性別，使用者性別是否為男性
     */
    @Expose
    @SerializedName("user_is_male")
    public Boolean user_is_male = null;

    /**
     * 使用者身高(公分)
     */
    @Expose
    @SerializedName("user_height")
    public Integer user_height = -1;

    /**
     * 使用者體重(公斤)
     */
    @Expose
    @SerializedName("user_weight")
    public Integer user_weight = -1;

    /**
     * 使用者生日年
     */
    @Expose
    @SerializedName("user_birthday_year")
    public Integer user_birthday_year = -1;

    /**
     * 使用者生日月
     */
    @Expose
    @SerializedName("user_birthday_month")
    public Integer user_birthday_month = -1;

    /**
     * 使用者生日日
     */
    @Expose
    @SerializedName("user_birthday_day")
    public Integer user_birthday_day = -1;

    /**
     * 使用者電話
     */
    @Expose
    @SerializedName("user_phone")
    public String user_phone = "unknown";

    /**
     * 使用者步伐長度(公分)
     */
    @Expose
    @SerializedName("user_stride_length")
    public Integer user_stride_length = -1;

    /**
     * 使用者每天走路步數目標
     */
    @Expose
    @SerializedName("user_step_goal")
    public Integer user_step_goal = -1;

    /**
     * 使用者每週強度目標(分鐘)
     */
    @Expose
    @SerializedName("user_weekly_intensity")
    public Integer user_weekly_intensity = -1;

    /**
     * 使用者上床時間時
     */
    @Expose
    @SerializedName("user_sleep_hour")
    public Integer user_sleep_hour = -1;

    /**
     * 使用者上床時間分
     */
    @Expose
    @SerializedName("user_sleep_minute")
    public Integer user_sleep_minute = -1;

    /**
     * 使用者醒來時間時
     */
    @Expose
    @SerializedName("user_wake_hour")
    public Integer user_wake_hour = -1;

    /**
     * 使用者醒來時間分
     */
    @Expose
    @SerializedName("user_wake_minute")
    public Integer user_wake_minute = -1;

    /**
     * user_vo2_max
     */
    @Expose
    @SerializedName("user_vo2_max")
    public Integer user_vo2_max = 0;

    /**
     * user_cycling_vo2_max
     */
    @Expose
    @SerializedName("user_cycling_vo2_max")
    public Integer user_cycling_vo2_max = 0;

    /**
     * user_lactate_threshold
     */
    @Expose
    @SerializedName("user_lactate_threshold")
    public Integer user_lactate_threshold = 0;

    /**
     * user_ftp
     */
    @Expose
    @SerializedName("user_ftp")
    public Integer user_ftp = 0;

    /**
     * 使用者應用程式token
     */
    @Expose
    @SerializedName("user_app_token")
    public String user_app_token = "unknown";

    /**
     * 使用者地址
     */
    @Expose
    @SerializedName("user_address")
    public String user_address = "unknown";

    /**
     * 使用者是否有帶助聽器(1或0)
     * 該值參考用，實際狀況還是要看裝置資料
     */
    @Expose
    @SerializedName("user_aid")
    public Integer user_aid = -1;

    @Expose
    @SerializedName("user_android")
    public Integer user_android = 1;



    public String getUser_name() {
        return user_name;
    }

    public Boolean isUser_is_male() {
        return user_is_male;
    }

    public Integer getUser_height() {
        return user_height;
    }

    public Integer getUser_weight() {
        return user_weight;
    }

    public Integer getUser_birthday_year() {
        return user_birthday_year;
    }

    public Integer getUser_birthday_month() {
        return user_birthday_month;
    }

    public Integer getUser_birthday_day() {
        return user_birthday_day;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public Integer getUser_stride_length() {
        return user_stride_length;
    }

    public Integer getUser_step_goal() {
        return user_step_goal;
    }

    public Integer getUser_weekly_intensity() {
        return user_weekly_intensity;
    }

    public Integer getUser_sleep_hour() {
        return user_sleep_hour;
    }

    public Integer getUser_sleep_minute() {
        return user_sleep_minute;
    }

    public Integer getUser_wake_hour() {
        return user_wake_hour;
    }

    public Integer getUser_wake_minute() {
        return user_wake_minute;
    }

    public Integer getUser_vo2_max() {
        return user_vo2_max;
    }

    public Integer getUser_cycling_vo2_max() {
        return user_cycling_vo2_max;
    }

    public Integer getUser_lactate_threshold() {
        return user_lactate_threshold;
    }

    public Integer getUser_ftp() {
        return user_ftp;
    }

    public String getUser_app_token() {
        return user_app_token;
    }

    public String getUser_address() {
        return user_address;
    }

    public Integer getUser_aid() {
        return user_aid;
    }
}
