package tw.idv.fy.kotlin.utils

import tw.idv.fy.kotlin.utils.LogLabel.Prefix
import tw.idv.fy.kotlin.utils.LogLabel.TAG
import kotlin.math.max

/**
 * 客製化 Log
 */
fun Any.LogV(label: String = "", tag: TAG = TAG(), prefix: Prefix = Prefix(label)) = LogCat.v  ("$tag", "$prefix $this")
fun Any.LogD(label: String = "", tag: TAG = TAG(), prefix: Prefix = Prefix(label)) = LogCat.d  ("$tag", "$prefix $this")
fun Any.LogI(label: String = "", tag: TAG = TAG(), prefix: Prefix = Prefix(label)) = LogCat.i  ("$tag", "$prefix $this")
fun Any.LogW(label: String = "", tag: TAG = TAG(), prefix: Prefix = Prefix(label)) = LogCat.w  ("$tag", "$prefix $this")
fun Any.LogE(label: String = "", tag: TAG = TAG(), prefix: Prefix = Prefix(label)) = LogCat.e  ("$tag", "$prefix $this")
fun Any.LogA(label: String = "", tag: TAG = TAG(), prefix: Prefix = Prefix(label)) = LogCat.wtf("$tag", "$prefix $this")

sealed class LogLabel(open val label: String) {
    class TAG(override val label: String = "Faty") : LogLabel(label.maxLength())
    class Prefix(override val label: String = "" ) : LogLabel(label)
    override fun toString() = label
}

typealias LogCat = android.util.Log
//class LogCat { // 若不想 proguard 則可以用這個 dummy 來禁止 log 輸出
//    companion object {
//        fun v  (vararg args:Any){}
//        fun d  (vararg args:Any){}
//        fun i  (vararg args:Any){}
//        fun w  (vararg args:Any){}
//        fun e  (vararg args:Any){}
//        fun wtf(vararg args:Any){}
//    }
//}

fun String.maxLength(limit: Int = 23) = dropLast(max(length - limit, 0))