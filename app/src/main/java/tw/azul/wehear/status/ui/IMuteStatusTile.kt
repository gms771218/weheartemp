package tw.azul.wehear.status.ui

import android.view.View
import tw.azul.wehear.R

interface IMuteStatusTile {
    fun setMute(isMute: Boolean, isConnected: Boolean? = true) {
        (this as? View)?.findViewById<View>(R.id.status_mute)?.visibility = when (isMute && isConnected == true) {
            true -> View.VISIBLE
            false -> View.GONE
        }
    }
}