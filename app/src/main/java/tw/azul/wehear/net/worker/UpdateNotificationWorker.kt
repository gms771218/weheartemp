package tw.azul.wehear.net.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.gson.Gson
import retrofit2.Call
import tw.azul.wehear.net.model.base.ResponseIdResultEntity
import tw.azul.wehear.net.model.base.SingleEntity
import tw.azul.wehear.net.model.request.RequestUserNotificationEntity
import tw.azul.wehear.net.repository.NotificationRepository
import java.util.*

/**
 * 上傳通知設定
 */
class UpdateNotificationWorker(context: Context, parameters: WorkerParameters) : Worker(context, parameters) {

    companion object {
        var DATA_KEY = "Key.UpdateNotification"
    }

    override fun doWork(): Result {
        val result = downloads()
        return if (result) Result.success() else Result.failure()
    }


    private fun downloads(): Boolean {

        val jsString = inputData.getString(DATA_KEY) ?: ""
        val list: List<RequestUserNotificationEntity> =
            Gson().fromJson(jsString, Array<RequestUserNotificationEntity>::class.java).toList()

        val _callList = getCalls(list)
        var _call: Call<SingleEntity<ResponseIdResultEntity>>? = null
        while (_callList.size > 0) {
            _call = _callList.removeAt(0)
            if (!run(_call))
                return false
        }
        return true
    }

    fun run(call: Call<SingleEntity<ResponseIdResultEntity>>): Boolean {
        var responseBody: SingleEntity<ResponseIdResultEntity>? = null
        responseBody = call.execute().body()
        return responseBody?.status == 1000
    }

    protected fun getCalls(list: List<RequestUserNotificationEntity>): MutableList<Call<SingleEntity<ResponseIdResultEntity>>> {
        val _list = ArrayList<Call<SingleEntity<ResponseIdResultEntity>>>()
        for (entity in list)
            _list.add(NotificationRepository.getInstance().postUserNotificationFormCall(entity))
        return _list
    }


}