package tw.azul.wehear.user.heart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import androidx.fragment.app.Fragment
import com.gms.widget.layout.R

/**
 * 步行目標設定
 */
class SettingHeartFragment : Fragment() {

    private companion object {
        private val DISPLAY_VALUE: Array<String> = Array(11) { i -> (i * 5 + 5).toString() }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.layout_edit_heart_time, container, false)
        initView(view)
        return view
    }

    open fun initView(view: View) {

        view.findViewById<NumberPicker>(R.id.picker).apply {
            wrapSelectorWheel = false
            displayedValues = SettingHeartFragment.DISPLAY_VALUE
            maxValue = 10
            minValue = 0
            value = 3
        }
    }
}