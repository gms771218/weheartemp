package tw.azul.wehear.status

import android.app.Application
import android.view.View
import androidx.lifecycle.*
import androidx.navigation.findNavController
import tw.azul.wehear.R
import tw.azul.wehear.api.DeviceApiRepository
import tw.azul.wehear.api.NTS
import tw.azul.wehear.api.setEnabled
import tw.azul.wehear.ble.AccessoryRepository
import tw.azul.wehear.ble.DeviceRepository
import tw.azul.wehear.ble.model.BandBatteryRepository
import tw.azul.wehear.status.ui.IConnectStatusTile
import tw.azul.wehear.status.ui.IPairStatusTile

class StatusViewModel(val app:Application) : AndroidViewModel(app) {

    /*
        手錶配對狀態
     */
    val bandPairStatus = MediatorLiveData<IPairStatusTile.Status>().apply {
        DeviceRepository.Instance(app).bandLiveData.also {
            postValue(if (it.value == null) {
                IPairStatusTile.Status.UnPair
            } else {
                IPairStatusTile.Status.Paired
            })
            addSource(it) { device ->
                postValue(if (device == null) {
                    IPairStatusTile.Status.UnPair
                } else {
                    IPairStatusTile.Status.Paired
                })
            }
        }
    } as LiveData<IPairStatusTile.Status>
    /*
        手錶連動狀態
     */
    val bandConnectStatus = MediatorLiveData<IConnectStatusTile.Status>().apply {
        value = IConnectStatusTile.Status.UnConnect
    }// as LiveData<IConnectStatusTile.Status>
    /*
        手錶電源狀態
     */
    val bandPowerLiveData: LiveData<Int> = BandBatteryRepository.Instance.batteryLevel()

    /*
        主機配對狀態
     */
    val hostPairStatus = MediatorLiveData<IPairStatusTile.Status>().apply {
        DeviceRepository.Instance(app).hostLiveData.also {
            postValue(if (it.value == null) {
                IPairStatusTile.Status.UnPair
            } else {
                IPairStatusTile.Status.Paired
            })
            addSource(it) { device ->
                postValue(if (device == null) {
                    IPairStatusTile.Status.UnPair
                } else {
                    IPairStatusTile.Status.Paired
                })
            }
        }
    } as LiveData<IPairStatusTile.Status>
    /*
        主機連線狀態
     */
    val hostConnectStatus = MediatorLiveData<IConnectStatusTile.Status>().apply {
        value = IConnectStatusTile.Status.UnConnect
    }// as LiveData<IConnectStatusTile.Status>

    /*
        裝置狀態
     */
    val device_status: MutableLiveData<List<DeviceApiRepository.Status?>> = MutableLiveData()
    /*
        電話狀態 (nts_id = 2)
     */
    val phoneConnectStatus = MediatorLiveData<Pair<Boolean, IConnectStatusTile.Status>>().apply {
        value = true to IConnectStatusTile.Status.UnConnect
        val nts_id = 2
        addSource(device_status) { list ->
            val status = list?.firstOrNull {
                it?.nts_id == nts_id
            }?.also { status ->
                NTS.電話.setEnabled(app, status.ns_enable > 0)
            }
            postValue(
                (status?.ns_enable ?: 0 <= 0)
                to
                if (status?.is_connect ?: 0 > 0) IConnectStatusTile.Status.Connected else IConnectStatusTile.Status.UnConnect
            )
        }
    } as LiveData<Pair<Boolean, IConnectStatusTile.Status>>
    /*
        門鈴狀態 (nts_id = 1)
     */
    val bellConnectStatus = MediatorLiveData<Pair<Boolean, IConnectStatusTile.Status>>().apply {
        value = true to IConnectStatusTile.Status.UnConnect
        val nts_id = 1
        addSource(device_status) { list ->
            val status = list?.firstOrNull {
                it?.nts_id == nts_id
            }?.also { status ->
                NTS.門鈴.setEnabled(app, status.ns_enable > 0)
            }
            postValue(
                (status?.ns_enable ?: 0 <= 0)
                to
                if (status?.is_connect ?: 0 > 0) IConnectStatusTile.Status.Connected else IConnectStatusTile.Status.UnConnect
            )
        }
    } as LiveData<Pair<Boolean, IConnectStatusTile.Status>>
    /*
        火警狀態 (nts_id = 4)
     */
    val alarmConnectStatus = MediatorLiveData<Pair<Boolean, IConnectStatusTile.Status>>().apply {
        value = true to IConnectStatusTile.Status.UnConnect
        val nts_id = 4
        addSource(device_status) { list ->
            val status = list?.firstOrNull {
                it?.nts_id == nts_id
            }?.also { status ->
                NTS.火災.setEnabled(app, status.ns_enable > 0)
            }
            postValue(
                (status?.ns_enable ?: 0 <= 0)
                to
                if (status?.is_connect ?: 0 > 0) IConnectStatusTile.Status.Connected else IConnectStatusTile.Status.UnConnect
            )
        }
    } as LiveData<Pair<Boolean, IConnectStatusTile.Status>>
    /*
        警示狀態 (nts_id = 3)
     */
    val warnConnectStatus = MediatorLiveData<Pair<Boolean, IConnectStatusTile.Status>>().apply {
        value = true to IConnectStatusTile.Status.UnConnect
        val nts_id = 3
        addSource(device_status) { list ->
            val status = list?.firstOrNull {
                it?.nts_id == nts_id
            }?.also { status ->
                NTS.遙控器.setEnabled(app, status.ns_enable > 0)
            }
            postValue(
                (status?.ns_enable ?: 0 <= 0)
                to
                if (status?.is_connect ?: 0 > 0) IConnectStatusTile.Status.Connected else IConnectStatusTile.Status.UnConnect
            )
        }
    } as LiveData<Pair<Boolean, IConnectStatusTile.Status>>

    /*
        藍牙裝置連線關注
     */
    fun bleConnectionStatusObserve(lifecycleOwner: LifecycleOwner) {
        AccessoryRepository.Instance(app).getBleStatusLiveData().observe(lifecycleOwner, Observer { bleStatus ->
            when {
                bleStatus.band.isConnected == true -> bandConnectStatus.postValue(IConnectStatusTile.Status.Connected)
                bleStatus.band.isPaired == true -> bandConnectStatus.postValue(IConnectStatusTile.Status.UnConnect)
                else -> bandConnectStatus.postValue(IConnectStatusTile.Status.UnConnect)
            }
            when {
                bleStatus.host.isConnected == true -> hostConnectStatus.postValue(IConnectStatusTile.Status.Connected)
                bleStatus.host.isPaired == true -> hostConnectStatus.postValue(IConnectStatusTile.Status.UnConnect)
                else -> hostConnectStatus.postValue(IConnectStatusTile.Status.UnConnect)
            }
        })
    }

    /*
        點擊手錶
     */
    fun bandOnClick(view: View) {
        when (bandPairStatus.value) {
            IPairStatusTile.Status.Paired -> {
                view.findNavController().navigate(R.id.action_mainStatusFragment_to_bandSettingFragment)
            }
            IPairStatusTile.Status.UnPair -> {
                view.findNavController().navigate(R.id.action_mainStatusFragment_to_bandSearchFragment)
            }
        }
    }

    /*
        主機手錶
     */
    fun hostOnClick(view: View) {
        when (hostPairStatus.value) {
            IPairStatusTile.Status.Paired -> {
                view.findNavController().navigate(R.id.action_mainStatusFragment_to_hostSettingFragment)
            }
            IPairStatusTile.Status.UnPair -> {
                view.findNavController().navigate(R.id.action_mainStatusFragment_to_hostSearchFragment)
            }
        }
    }

}
