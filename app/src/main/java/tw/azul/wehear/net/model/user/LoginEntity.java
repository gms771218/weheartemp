package tw.azul.wehear.net.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginEntity {

    /**
     * 使用者id
     */
    @Expose
    @SerializedName("user_id")
    public int user_id;

    /**
     * 使用者Token
     */
    @Expose
    @SerializedName("token")
    public String token;

}
