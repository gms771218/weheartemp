package tw.azul.wehear.status.host

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_host_setting.*
import tw.azul.wehear.MainActivity

import tw.azul.wehear.R

class SettingFragment : Fragment() {

    companion object {
        fun newInstance() = SettingFragment()
    }

    private lateinit var viewModel: SettingViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_host_setting, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SettingViewModel::class.java)
        viewModel.deviceNameLiveData.observe(viewLifecycleOwner::getLifecycle, device_name::setText)
        viewModel.deviceAddrLiveData.observe(viewLifecycleOwner::getLifecycle, device_addr::setText)
        btn_unbind.setOnClickListener(viewModel::hostUnbind)
        // 決定 Toolbar 行為
        (activity as? MainActivity)?.showToolbarAndHideNavigation()
    }

}
