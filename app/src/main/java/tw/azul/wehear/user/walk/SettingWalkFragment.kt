package tw.azul.wehear.user.walk

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.NumberPicker
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gms.widget.layout.R
import com.google.android.material.snackbar.Snackbar
import tw.azul.wehear.MainActivity
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.net.model.base.Status
import tw.azul.wehear.net.model.request.RequestUpdateUserEntity
import tw.azul.wehear.net.model.user.UserEntity
import tw.azul.wehear.user.user.UserViewModel

/**
 * 步行目標設定
 */
class SettingWalkFragment : Fragment() {

    private companion object {
        private val DISPLAY_VALUE: Array<String> = Array(30) { i -> (i * 1000 + 1000).toString() }

        // 每日建議步數
        val DEFAULT_SELECT_VALUE = 11
    }

    val USER_ID = AccountRoom.TokenLiveData.value?.userId!!

    lateinit var picker: NumberPicker

    private lateinit var progressDig: AlertDialog

    lateinit var userViewModel: UserViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_edit_walk_count, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as? MainActivity)?.showToolbarAndHideNavigation()

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        userViewModel.getUser(USER_ID).observe(viewLifecycleOwner, Observer {
            if (it.status != Status.LOADING) {
                progressDig.dismiss()
                it.data?.apply {
                    picker.displayedValues.indexOf(it.data.user_step_goal.toString()).also { index ->
                        picker.value = if (index == -1) DEFAULT_SELECT_VALUE else index
                    }
                }
            } else {
                if (!::progressDig.isInitialized) {
                    progressDig = AlertDialog.Builder(context).setMessage("請稍候").create()
                }
                progressDig.show()
            }
        })

    }


    private fun initView(view: View) {

        picker = view.findViewById<NumberPicker>(R.id.picker).apply {
            wrapSelectorWheel = false
            displayedValues = SettingWalkFragment.DISPLAY_VALUE
            maxValue = 29
            minValue = 0
            value = DEFAULT_SELECT_VALUE
        }

        view.findViewById<Button>(R.id.btn_check).setOnClickListener {

            RequestUpdateUserEntity.create(USER_ID, UserEntity(), UserEntity().also {
                it.user_step_goal = picker.value * 1000 + 1000
            }).apply {
                userViewModel.updateUser(USER_ID, this).observe(viewLifecycleOwner, Observer {

                    if (it.status != Status.LOADING) {
                        progressDig.dismiss()
                        if (it.data?.status != 1000)
                            //android.widget.Toast.makeText(context, it.data?.msg, android.widget.Toast.LENGTH_SHORT).show()
                            Snackbar.make(view, "錯誤 ${it.data?.msg?:""}", Snackbar.LENGTH_INDEFINITE).apply {
                                setActionTextColor(ResourcesCompat.getColor(resources, tw.azul.wehear.R.color.colorAccent, null))
                                setAction("知道了") { dismiss() }
                                show()
                            }
                        else
                            activity?.onBackPressed()
                    } else {
                        if (!::progressDig.isInitialized) {
                            progressDig = AlertDialog.Builder(context).setMessage("請稍候").create()
                        }
                        progressDig.show()
                    }

                })
            }

        }

    }
}