package tw.azul.wehear.status.host.form

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.fragment_host_setting_address.*

import tw.azul.wehear.R
import tw.azul.wehear.account.AccountRoom
import tw.azul.wehear.databinding.FragmentHostSettingAddressBinding

class AddressFragment : Fragment() {

    companion object {
        fun newInstance() = AddressFragment()
    }

    private lateinit var dataBinding: FragmentHostSettingAddressBinding
    private lateinit var viewModel: AddressViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_host_setting_address, container, false)
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AddressViewModel::class.java)
        dataBinding.viewModel = viewModel
        dataBinding.setLifecycleOwner(viewLifecycleOwner)

        viewModel.getUser(AccountRoom.TokenLiveData.value?.userId!!).observe(viewLifecycleOwner::getLifecycle ){}
        btn_next.setOnClickListener {
            if (value_tall.valid()){
                viewModel.saveUser(AccountRoom.TokenLiveData.value?.userId!!).observe(viewLifecycleOwner::getLifecycle){}
                viewModel.gotoMainPage(it)
            }
        }
    }

}
