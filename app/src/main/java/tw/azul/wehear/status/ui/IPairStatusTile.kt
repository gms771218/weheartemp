package tw.azul.wehear.status.ui

interface IPairStatusTile {
    enum class Status {
        UnPair,
        Paired,
    }
    fun setStatus(status: Status)
}